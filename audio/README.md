## ROS Node for audio detection with Respeaker v2 6 Mic Array

## DEPENDENCIES:
```
$roscd respeaker_ros
$sudo pip install -r requirements.txt
```

##SETUP:

Register respeaker udev rules to access usb speaker w/out permission from user space
```
 $ git config --global user.email
 $ sudo cp -f $(rospack find respeaker_ros)/config/60-respeaker.rules /etc/udev/rules.d/60-respeaker.rules
 $ sudo systemctl restart udev
```

(optional)
Make sure firmware on respeaker is up to date
```
 $ cd usb_4_mic_array
 $ sudo python dfu.py --download 6_channels_firmware.bin  # The 6 channels version 
```

##USAGE:
```
 $ roslaunch audio_detection respeaker.launch
```

##TOPICS
* /audio: Raw audio, audio_common_msgs/AudioData Message
* /speech_audio: Audio data while speeching, audio_common_msgs/AudioData
* /is_speeching: Result of VAD (voice activity detected), Bool
* /sound_direction: Result of DoA (direction of arrival), Int32
* /sound_localization: Result of DoA as Pose, geometry_msgs/PoseStamped
 
