#ifndef OBJECT_DETECTION_TRACKING_DETECTION_TRACKER_H
#define OBJECT_DETECTION_TRACKING_DETECTION_TRACKER_H

#include <base/BaseNode.h>
#include <common/timer.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <objdet_msgs/DetectionArray.h>
#include <sensor_msgs/Image.h>
#include <deque>
#include <mutex>
#include "tracking/tracked_detection.h"

namespace object_detection {
class DetectionTracker {
  struct CvImageMsg {
    sensor_msgs::ImageConstPtr msg;
    cv_bridge::CvImageConstPtr cv_ptr;
  };

 public:
  DetectionTracker(ros::NodeHandle* nh, std::string image_topic);
  bool initialize();
  ~DetectionTracker() = default;

 private:
  const size_t MAX_IMAGES = 60;

  void image_cb(const sensor_msgs::ImageConstPtr& image);
  void detection_cb(const objdet_msgs::DetectionArrayConstPtr& dets);

  ros::NodeHandle* nh;
  std::string image_topic;

  image_transport::ImageTransport it;
  image_transport::Subscriber image_sub;
  ros::Subscriber detection_sub;
  ros::Publisher detection_pub;

  std::mutex tracks_lock;
  TrackedDetectionSet tracks;
  ros::Time tracks_stamp;

  std::mutex images_lock;
  std::deque<CvImageMsg> images;

  ThreadsafeTimer monitor;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_TRACKING_DETECTION_TRACKER_H
