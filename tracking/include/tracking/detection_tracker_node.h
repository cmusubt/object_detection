#ifndef OBJECT_DETECTION_TRACKING_DETECTION_TRACKER_NODE_H
#define OBJECT_DETECTION_TRACKING_DETECTION_TRACKER_NODE_H

#include <base/BaseNode.h>
#include <memory>
#include "tracking/detection_tracker.h"

namespace object_detection {
class DetectionTrackerNode : public BaseNode {
 public:
  DetectionTrackerNode(std::string);
  virtual bool initialize() override;
  virtual bool execute() override;
  ~DetectionTrackerNode() = default;

 private:
  std::vector<std::unique_ptr<DetectionTracker>> detection_trackers;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_TRACKING_DETECTION_TRACKER_NODE_H
