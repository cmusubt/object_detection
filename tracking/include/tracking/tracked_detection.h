#ifndef OBJECT_DETECTION_TRACKING_TRACKED_DETECTION_H
#define OBJECT_DETECTION_TRACKING_TRACKED_DETECTION_H

#include <common/math.h>
#include <objdet_msgs/Detection.h>
#include <ros/ros.h>
#include <algorithm>
#include <opencv2/tracking.hpp>

namespace object_detection {
template <typename Tracker = cv::TrackerMedianFlow>
class TrackedDetection {
 public:
  TrackedDetection(const cv::Mat& image, const ros::Time& image_stamp,
                   const objdet_msgs::Detection& det,
                   const float max_track_time = 1.0f)
      : last_stamp(image_stamp),
        original_stamp(image_stamp),
        original_det(det),
        max_track_time(max_track_time) {
    tracker = Tracker::create();

    // Store the initial width and height, and check it to ensure nothing
    // changes throughout the image lifetime.
    image_width = image.cols;
    image_height = image.rows;
    roi = cv::Rect2d(det.x1 * image_width, det.y1 * image_height,
                     (det.x2 - det.x1) * image_width,
                     (det.y2 - det.y1) * image_height);

    status = tracker->init(image, roi);

    if (!status) {
      ROS_WARN_STREAM("Unable to initialize tracker. This shouldn't happen."
                      << " Tried to initialize with image at " << image_stamp
                      << " and detection" << det);
    }
  }

  void update(const cv::Mat& image, const ros::Time& image_stamp) {
    assert(image.cols == image_width);
    assert(image.rows == image_height);

    // Don't update the tracker when it's invalid.
    if (!status) {
      return;
    }

    if (image_stamp <= last_stamp) {
      ROS_WARN_STREAM("Received an out of order image. Last stamp: "
                      << last_stamp << ", current stamp: " << image_stamp);
      return;
    }

    if ((image_stamp - original_stamp) > ros::Duration(max_track_time)) {
      status = false;
    }

    status &= tracker->update(image, roi);
    last_stamp = image_stamp;
  }

  // Matching is a function of both the class and iou
  float match(const TrackedDetection& other) const {
    auto class_match = original_det.id == other.original_det.id;
    return class_match * iou(other.roi);
  }

  bool get_status() const { return status; }

  objdet_msgs::Detection get_detection() const {
    objdet_msgs::Detection new_det;
    new_det.x1 = clamp(static_cast<float>(roi.x / image_width), 0.0f, 1.0f);
    new_det.y1 = clamp(static_cast<float>(roi.y / image_height), 0.0f, 1.0f);
    new_det.x2 = clamp(static_cast<float>((roi.x + roi.width) / image_width),
                       0.0f, 1.0f);
    new_det.y2 = clamp(static_cast<float>((roi.y + roi.height) / image_height),
                       0.0f, 1.0f);
    new_det.label = original_det.label;
    new_det.id = original_det.id;
    new_det.confidence = original_det.confidence;
    return new_det;
  }

 private:
  float iou(const cv::Rect2d& other) const {
    auto intersection = (roi & other).area();
    return intersection == 0 ? 0 : (intersection / (roi | other).area());
  }

  ros::Time last_stamp;
  ros::Time original_stamp;
  objdet_msgs::Detection original_det;
  float max_track_time;

  int image_width;
  int image_height;

  cv::Ptr<cv::Tracker> tracker;
  cv::Rect2d roi;
  bool status;
};

class TrackedDetectionSet {
 public:
  const float MATCH_SCORE_THRESHOLD = 0.5f;

  TrackedDetectionSet() = default;

  template <typename... Args>
  void emplace(Args&&... args) {
    tracked_dets.emplace_back(std::forward<Args>(args)...);

    // Don't store invalid tracks in the vector. This _shouldn't_ happen, but
    // just in case it does ...
    if (!tracked_dets.back().get_status()) {
      tracked_dets.pop_back();
    }
  }

  size_t size() const { return tracked_dets.size(); }

  size_t valid_size() const {
    size_t valids = 0;
    for (const auto& tracked_det : tracked_dets) {
      valids += tracked_det.get_status();
    }
    return valids;
  }

  void update(const cv::Mat& image, const ros::Time& image_stamp) {
    for (size_t i = 0; i < tracked_dets.size();) {
      auto& tracked_det = tracked_dets[i];
      tracked_det.update(image, image_stamp);

      // Remove tracked_det if it's no longer valid
      if (tracked_det.get_status() == false) {
        std::swap(tracked_det, tracked_dets.back());
        tracked_dets.pop_back();
      } else {
        ++i;
      }
    }
  }

  // Bring tracks from other into this set, matching intelligently.
  void merge(TrackedDetectionSet& other) {
    auto& other_dets = other.tracked_dets;

    // Match each existing detection to a new one if possible.
    for (size_t i = 0; i < tracked_dets.size(); ++i) {
      size_t best_match_index = other_dets.size();
      float best_match_score = MATCH_SCORE_THRESHOLD;
      for (size_t j = 0; j < other_dets.size(); ++j) {
        const auto match_score = tracked_dets[i].match(other_dets[j]);
        if (match_score > best_match_score) {
          best_match_score = match_score;
          best_match_index = j;
        }
      }

      if (best_match_index < other_dets.size()) {  // found a match!
        // Swap element to back and then move it out for efficiency
        std::swap(other_dets[best_match_index], other_dets.back());
        tracked_dets[i] = std::move(other_dets.back());
        other_dets.pop_back();
      }
    }

    // Whatever's left in other_dets will just be added directly since it must
    // be a new detection, or a new class.
    for (auto& tracked_det : other_dets) {
      tracked_dets.push_back(std::move(tracked_det));
    }

    // Need to clear the other detections since we've been moving from it.
    other_dets.clear();
  }

  objdet_msgs::DetectionArray get_detections() {
    objdet_msgs::DetectionArray dets;

    for (const auto& tracked_det : tracked_dets) {
      if (!tracked_det.get_status()) {
        continue;
      }

      dets.detections.emplace_back(tracked_det.get_detection());
    }

    return dets;
  }

 private:
  std::vector<TrackedDetection<cv::TrackerMedianFlow>> tracked_dets;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_TRACKING_TRACKED_DETECTION_H
