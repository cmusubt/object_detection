#include "tracking/detection_tracker.h"
#include <algorithm>

namespace object_detection {
DetectionTracker::DetectionTracker(ros::NodeHandle* nh, std::string image_topic)
    : nh(nh), image_topic(image_topic), it(*nh), monitor(image_topic) {}

bool DetectionTracker::initialize() {
  image_sub = it.subscribe(image_topic, 10, &DetectionTracker::image_cb, this);
  detection_sub = nh->subscribe(image_topic + "/detections", 10,
                                &DetectionTracker::detection_cb, this);
  detection_pub = nh->advertise<objdet_msgs::DetectionArray>(
      image_topic + "/detections_tracked", 10);

  return true;
}

void DetectionTracker::image_cb(const sensor_msgs::ImageConstPtr& image) {
  monitor.tic("image_cb");
  // This keeps the image data around while the cv_bridge wrapper to it exists.
  CvImageMsg cv_img_msg{image, cv_bridge::toCvShare(image, "")};

  objdet_msgs::DetectionArray dets;

  {
    std::lock_guard<std::mutex> tracks_guard(tracks_lock);
    monitor.tic("image_cb_crit_1");

    monitor.tic("image_cb_update");
    tracks.update(cv_img_msg.cv_ptr->image, image->header.stamp);
    monitor.toc("image_cb_update");
    dets = tracks.get_detections();

    // Push back an image to the queue before releasing the tracks_lock. This
    // ensures that whenever both are held, the track set and the latest image
    // correspond to one another, allowing us to iterate to the end().
    {
      std::lock_guard<std::mutex> images_guard(images_lock);
      if (images.size() == MAX_IMAGES) {
        images.pop_front();
      }
      images.push_back(cv_img_msg);
    }
    monitor.toc("image_cb_crit_1");
  }

  dets.header = image->header;
  detection_pub.publish(dets);
  monitor.toc("image_cb");
  return;
}

void DetectionTracker::detection_cb(
    const objdet_msgs::DetectionArrayConstPtr& dets) {
  monitor.tic("det_cb");

  // Keep a copy of the image (which is cheap since they're pointers), rather
  // than keeping the full set of images.
  std::deque<CvImageMsg> local_images;
  {
    std::lock_guard<std::mutex> images_guard(images_lock);
    local_images = images;
  }

  // Now we can iterate through the images at our own pace since we have a copy
  // of the pointers. Find the image which matches the received detection.
  auto det_stamp = dets->header.stamp;
  auto iter = std::find_if(
      local_images.begin(), local_images.end(),
      [&det_stamp](const decltype(local_images)::value_type& local_image) {
        return local_image.msg->header.stamp == det_stamp;
      });
  if (iter == local_images.end()) {
    ROS_WARN_STREAM("Unable to find matching image for detection at time "
                    << det_stamp
                    << ". You probably need to increase the buffer size, or "
                       "get a faster detector");
    return;
  }

  // Track all of the detections we just received.
  TrackedDetectionSet new_tracks;
  for (const auto& det : dets->detections) {
    new_tracks.emplace(iter->cv_ptr->image, iter->msg->header.stamp, det);
  }

  monitor.tic("det_cb_update");
  // Feed forward the tracked detections through all images.
  ++iter;
  for (; iter != local_images.end(); ++iter) {
    new_tracks.update(iter->cv_ptr->image, iter->msg->header.stamp);
  }
  monitor.toc("det_cb_update");

  monitor.tic("det_cb_update_2");
  // Marks how far we've gotten
  auto new_tracks_stamp = local_images.back().msg->header.stamp;
  {
    // Now, we have to catch up with whatever may have happened in the
    // image_cb while we were doing our business here. Note that the order
    // matters here in order to prevent deadlock.
    std::lock_guard<std::mutex> tracks_guard(tracks_lock);
    std::lock_guard<std::mutex> images_guard(images_lock);

    monitor.tic("det_cb_crit");

    // First, find the image with the current stamp.
    auto image_iter = std::find_if(
        std::begin(images), std::end(images),
        [&new_tracks_stamp](const decltype(images)::value_type& image) {
          return image.msg->header.stamp == new_tracks_stamp;
        });
    if (image_iter == std::end(images)) {
      ROS_WARN("Image no longer in the list, probably need to increase buffer");
      return;
    }

    // Until we match the current tracks, keep updating ours.
    ++image_iter;
    while (image_iter != images.end()) {
      new_tracks.update(image_iter->cv_ptr->image,
                        image_iter->msg->header.stamp);
      ++image_iter;
    }

    tracks.merge(new_tracks);
    monitor.toc("det_cb_crit");
  }
  monitor.toc("det_cb_update_2");

  monitor.toc("det_cb");
  return;
}
}  // namespace object_detection
