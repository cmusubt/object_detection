#include "tracking/detection_tracker_node.h"

BaseNode* BaseNode::get() {
  auto* tracker_node =
      new object_detection::DetectionTrackerNode("detection_tracker_node");
  return tracker_node;
}

namespace object_detection {

DetectionTrackerNode::DetectionTrackerNode(std::string node_name)
    : BaseNode(std::move(node_name)) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool DetectionTrackerNode::initialize() {
  std::vector<std::string> topics;
  get_private_node_handle()->getParam("topics", topics);
  if (topics.empty()) {  // Checking for false seems to not work properly.
    ROS_WARN("No topics set for detection tracker!");
    return false;
  }

  for (const auto& topic : topics) {
    auto* cb_queue = new ros::CallbackQueue();
    auto* spinner = new ros::AsyncSpinner(2 /* threads */, cb_queue);
    auto* nh = new ros::NodeHandle();
    nh->setCallbackQueue(cb_queue);
    auto* pnh = new ros::NodeHandle("~");
    pnh->setCallbackQueue(cb_queue);

    callback_queues.push_back(cb_queue);
    async_spinners.push_back(spinner);
    node_handles.push_back(nh);
    private_node_handles.push_back(pnh);

    detection_trackers.emplace_back(new DetectionTracker(nh, topic));
    if (!detection_trackers.back()->initialize()) {
      return false;
    }
  }

  return true;
}

bool DetectionTrackerNode::execute() { return true; }

}  // namespace object_detection
