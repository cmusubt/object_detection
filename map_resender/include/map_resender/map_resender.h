#ifndef OBJECT_DETECTION_MAP_RESENDER_MAP_WRITER_H
#define OBJECT_DETECTION_MAP_RESENDER_MAP_WRITER_H

#include <base/BaseNode.h>
#include <basestation_msgs/Radio.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <std_msgs/Bool.h>
#include <tf2_ros/transform_listener.h>
#include <memory>
#include <unordered_map>

namespace object_detection {
namespace map_resender {
using PointType = pcl::PointXYZ;
using PointCloud = pcl::PointCloud<PointType>;

class MapResender : public BaseNode {
 public:
  MapResender(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~MapResender() = default;

 private:
  const std::string MAP_FRAME = "map";

  enum class SliceAxis { X, Y, Z };

  void key_pose_cb(const nav_msgs::OdometryConstPtr& key_pose);
  void key_pose_cloud_cb(const PointCloud::ConstPtr& cloud);
  void key_pose_path_cb(const nav_msgs::PathConstPtr& key_pose_path);
  void retransmit_cb(const basestation_msgs::RadioConstPtr& radio_msg);

  PointCloud::Ptr build_global_map(
      const std::vector<PointCloud::ConstPtr>& key_pose_clouds);
  PointCloud::Ptr downsample_cloud(const PointCloud::ConstPtr& cloud);
  PointCloud::Ptr make_2d_slice(const PointCloud::ConstPtr& cloud,
                                const SliceAxis axis);

  std::unordered_map<uint32_t, geometry_msgs::PoseStamped> key_poses;
  std::vector<PointCloud::ConstPtr> key_pose_clouds_2D;
  std::vector<PointCloud::ConstPtr> key_pose_clouds_3D;

  ros::Subscriber key_pose_sub;
  ros::Subscriber key_pose_cloud_sub;
  ros::Subscriber key_pose_path_sub;
  ros::Subscriber retransmit_sub;
  ros::Publisher full_cloud_pub;

  tf2_ros::Buffer tf_buffer;
  std::unique_ptr<tf2_ros::TransformListener> tf_listener_ptr;

  float slice_2d_min = -0.1f;
  float slice_2d_max = 0.1f;
  float leaf_size = 0.1f;
};
}  // namespace map_resender
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MAP_RESENDER_MAP_WRITER_H
