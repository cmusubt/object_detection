#include "map_resender/map_resender.h"
#include <common/serial.h>
#include <common/tf2.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <boost/filesystem.hpp>

BaseNode* BaseNode::get() {
  auto* resender =
      new object_detection::map_resender::MapResender("map_resender");
  return resender;
}

namespace object_detection {
namespace map_resender {
MapResender::MapResender(std::string node_name)
    : BaseNode(std::move(node_name)) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool MapResender::initialize() {
  ros::NodeHandle* nh = get_node_handle();
  tf_listener_ptr = std::unique_ptr<tf2_ros::TransformListener>(
      new tf2_ros::TransformListener(tf_buffer));

  key_pose_sub =
      nh->subscribe("key_pose_to_map", 10, &MapResender::key_pose_cb, this);
  key_pose_cloud_sub = nh->subscribe("velodyne_cloud_key_pose", 10,
                                     &MapResender::key_pose_cloud_cb, this);
  key_pose_path_sub =
      nh->subscribe("key_pose_path", 10, &MapResender::key_pose_path_cb, this);
  retransmit_sub =
      nh->subscribe("radio_command", 10, &MapResender::retransmit_cb, this);

  full_cloud_pub = nh->advertise<PointCloud>("complete_cloud", 1);

  nh->param<float>("slice_2d_min", slice_2d_min, slice_2d_min);
  nh->param<float>("slice_2d_max", slice_2d_max, slice_2d_max);
  nh->param<float>("leaf_size", leaf_size, leaf_size);

  ROS_INFO("Map resender will slice between %f and %f", slice_2d_min,
           slice_2d_max);
  ROS_INFO("Map resender will downsample cloud with leaf size %f", leaf_size);

  return true;
}

bool MapResender::execute() { return true; }

void MapResender::key_pose_cb(const nav_msgs::OdometryConstPtr& key_pose) {
  const auto key_pose_index =
      static_cast<uint32_t>(key_pose->pose.covariance[0]);

  // The key poses aren't necessarily in the MAP_FRAME (usually /map) as they're
  // usually in /map_rot. Since a geometry_msgs/Pose message doesn't store any
  // frame information, we must transform the Pose here.
  geometry_msgs::Pose map_frame_key_pose;
  try {
    const auto key_pose_to_map_tf = tf_buffer.lookupTransform(
        MAP_FRAME, sanitize_frame(key_pose->header.frame_id),
        key_pose->header.stamp);
    tf2::doTransform(key_pose->pose.pose, map_frame_key_pose,
                     key_pose_to_map_tf);
  } catch (tf2::TransformException& e) {
    ROS_WARN("Error looking up Key Pose TF: %s", e.what());
    return;
  }

  key_poses[key_pose_index].pose = map_frame_key_pose;
  key_poses[key_pose_index].header.stamp = key_pose->header.stamp;
  key_poses[key_pose_index].header.frame_id = MAP_FRAME;
}

void MapResender::key_pose_cloud_cb(const PointCloud::ConstPtr& cloud) {
  const auto& ds_cloud =
      key_pose_clouds_3D.emplace_back(downsample_cloud(cloud));
  key_pose_clouds_2D.push_back(make_2d_slice(ds_cloud, SliceAxis::Y));
}

void MapResender::key_pose_path_cb(
    const nav_msgs::PathConstPtr& key_pose_path) {
  geometry_msgs::TransformStamped key_pose_to_map_tf;
  try {
    key_pose_to_map_tf = tf_buffer.lookupTransform(
        MAP_FRAME, sanitize_frame(key_pose_path->header.frame_id),
        key_pose_path->header.stamp);
  } catch (tf2::TransformException& e) {
    ROS_WARN("Error looking up Key Pose Path TF: %s", e.what());
    return;
  }

  // Update the current key poses, making sure to transform into the MAP_FRAME
  // so that the final output looks right.
  for (size_t i = 0; i < key_pose_path->poses.size(); ++i) {
    // Only keep key poses we actually have, since we need the stamps, which
    // aren't sent over with the key_pose_path.
    if (key_poses.count(i) > 0) {
      tf2::doTransform(key_pose_path->poses[i].pose, key_poses[i].pose,
                       key_pose_to_map_tf);
    }
  }
}

void MapResender::retransmit_cb(
    const basestation_msgs::RadioConstPtr& radio_msg) {
  using basestation_msgs::Radio;

  auto msg_t = radio_msg->message_type;
  auto send_2D = msg_t == Radio::MESSAGE_TYPE_SEND_2D_MAP;
  auto send_3D = msg_t == Radio::MESSAGE_TYPE_SEND_3D_MAP;

  if (send_2D || send_3D) {
    auto what = send_2D ? "2D" : "3D";
    auto& which = send_2D ? key_pose_clouds_2D : key_pose_clouds_3D;
    auto global_map = build_global_map(which);
    auto downsampled_global_map = downsample_cloud(global_map);
    ROS_INFO(
        "Sending downsampled %s global map with %lu points (from %lu points)",
        what, downsampled_global_map->size(), global_map->size());
    full_cloud_pub.publish(downsampled_global_map);
  }
}

PointCloud::Ptr MapResender::build_global_map(
    const std::vector<PointCloud::ConstPtr>& key_pose_clouds) {  // NOLINT

  PointCloud::Ptr global_map(new PointCloud);
  const auto reserve = std::accumulate(
      key_pose_clouds.begin(), key_pose_clouds.end(), 0,
      [](size_t i, const auto& cloud) { return i + cloud->size(); });
  global_map->reserve(reserve);

  for (const auto& cloud : key_pose_clouds) {
    // There's a bit of timestamp error since it seems the conversion to / from
    // PCL messages isn't perfect. ROS stores its timestamps as 2 ints, while
    // timestamps returned from PCL are doubles. The conversion is lossy, so
    // exact match with == is impossible.
    const auto stamp = pcl_conversions::fromPCL(cloud->header.stamp);
    const auto it =
        std::find_if(key_poses.begin(), key_poses.end(),
                     [&stamp](const decltype(key_poses)::value_type& kv) {
                       const auto& key_stamp = kv.second.header.stamp;
                       return std::abs((key_stamp - stamp).toSec()) < 0.1;
                     });

    if (it == key_poses.end()) {
      // ROS_WARN_STREAM("Didn't find key pose for cloud at time " << stamp);
      continue;
    }

    const auto index = it->first;
    const auto& key_pose = key_poses[index];

    const auto key_pose_to_map_eigen = eigen_tf_from_pose(key_pose.pose);
    PointCloud::Ptr global_key_pose(new PointCloud);
    pcl::transformPointCloud(*cloud, *global_key_pose, key_pose_to_map_eigen);
    *global_map += *global_key_pose;
  }

  global_map->header.frame_id = MAP_FRAME;
  pcl_conversions::toPCL(ros::Time::now(), global_map->header.stamp);
  global_map->height = 1;
  global_map->width = global_map->size();
  global_map->is_dense = false;
  return global_map;
}

PointCloud::Ptr MapResender::downsample_cloud(
    const PointCloud::ConstPtr& cloud) {
  PointCloud::Ptr downsampled_cloud(new PointCloud);
  pcl::VoxelGrid<PointType> filter;
  filter.setInputCloud(cloud);
  filter.setLeafSize(leaf_size, leaf_size, leaf_size);
  filter.filter(*downsampled_cloud);

  downsampled_cloud->header = cloud->header;
  downsampled_cloud->height = 1;
  downsampled_cloud->width = downsampled_cloud->size();
  downsampled_cloud->is_dense = false;
  return downsampled_cloud;
}

PointCloud::Ptr MapResender::make_2d_slice(const PointCloud::ConstPtr& cloud,
                                           const SliceAxis axis) {
  PointCloud::Ptr sliced_cloud(new PointCloud);
  sliced_cloud->points.reserve(cloud->size());
  for (const auto& point : cloud->points) {
    float coord;
    switch (axis) {
      case SliceAxis::X:
        coord = point.x;
        break;
      case SliceAxis::Y:
        coord = point.y;
        break;
      case SliceAxis::Z:
        coord = point.z;
        break;
    }

    if (slice_2d_min <= coord && coord <= slice_2d_max) {
      sliced_cloud->push_back(point);
    }
  }

  sliced_cloud->points.shrink_to_fit();
  sliced_cloud->header = cloud->header;
  sliced_cloud->height = 1;
  sliced_cloud->width = sliced_cloud->size();
  sliced_cloud->is_dense = false;
  return sliced_cloud;
}
}  // namespace map_resender
}  // namespace object_detection
