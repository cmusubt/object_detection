#!/bin/sh
OBJDET_DIR=`pwd`/$(dirname $0)
WORKSPACE_DIR=$(realpath $OBJDET_DIR/../..)
cd $WORKSPACE_DIR

FILENAME=$(basename $0)

USAGE="usage: ./$FILENAME [build type] [package] \n\n
\rAvailable build types: \n
\t clean: Clean all build artifacts \n
\t gcc-debug: Debug build with GCC \n
\t gcc-release: Release build with GCC (preferred) \n
\t gcc-tests: Build tests with GCC \n
\t gcc-run-tests: Build and run tests with GCC \n
\t clang-debug: Debug build with Clang \n
\t clang-release: Release build with Clang \n
\t clang-tests: Build tests with Clang \n
\t clang-run-tests: Build and run tests with Clang \n
\t format: Autoformat code in all modules and submodules \n
\t clang-tidy [package]: Lint a specific package \n
\n
\rExamples: \n
\t ./$FILENAME gcc-release \n
\t ./$FILENAME clang-debug modules \n
\t ./$FILENAME clang-tidy flir_ros \n
\t ./$FILENAME format
"

case $1 in
    (-h | --help) :
        echo $USAGE
        exit 0
        ;;
    (clean) :
        catkin clean
        ;;
    (debug | gcc-debug) :
        catkin build $2 -DCMAKE_BUILD_TYPE=Debug
        ;;
    (release | gcc-release) :
        catkin build $2 -DCMAKE_BUILD_TYPE=Release
        ;;
    (tests | gcc-tests) :
        catkin build $2 -DCMAKE_BUILD_TYPE=Release \
            --catkin-make-args tests
        ;;
    (run-tests | gcc-run-tests) :
        catkin build $2 -DCMAKE_BUILD_TYPe=Release \
            -v \
            --catkin-make-args run_tests
        ;;
    clang-debug) :
        catkin build $2 \
            -DCMAKE_C_COMPILER=/usr/bin/clang \
            -DCMAKE_CXX_COMPILER=/usr/bin/clang++ \
            -DCMAKE_BUILD_TYPE=Debug
        ;;
    clang-release) :
        catkin build $2 \
            -DCMAKE_C_COMPILER=/usr/bin/clang \
            -DCMAKE_CXX_COMPILER=/usr/bin/clang++ \
            -DCMAKE_BUILD_TYPE=Release
        ;;
    clang-tests) :
        catkin build $2 \
            -DCMAKE_C_COMPILER=/usr/bin/clang \
            -DCMAKE_CXX_COMPILER=/usr/bin/clang++ \
            -DCMAKE_BUILD_TYPE=Release \
            --catkin-make-args tests
        ;;
    clang-run-tests) :
        catkin build $2 \
            -DCMAKE_C_COMPILER=/usr/bin/clang \
            -DCMAKE_CXX_COMPILER=/usr/bin/clang++ \
            -DCMAKE_BUILD_TYPE=Release \
            -v \
            --catkin-make-args run_tests
        ;;
    format) :
        # Run clang-format on every file, for C++, CUDA, and Arduino
        find $OBJDET_DIR \
            \( -name "*.h" -or \
                -name "*.cpp" -or \
                -name "*.ino" -or \
                -name "*.cu" \) \
            -exec clang-format -i -style=Google {} \;

        # Run Black on every python file. TODO(vasua): Run on training repo
        black -l 80 -t py35 $OBJDET_DIR \
            --exclude "/(training|ci_phase|ci_jenkins|sensor_calibration|respeaker_ros|usb_4_mic_array)/"
        exit 0
        ;;
    clang-tidy) :
        cd build/$2
        run-clang-tidy -checks="*,
            -llvm-include-order,
            -fuchsia-default-arguments,
            -modernize-make-unique,
            -hicpp-vararg,
            -cppcoreguidelines-pro-type-vararg,
            -cppcoreguidelines-pro-bounds-array-to-pointer-decay,
            -hicpp-no-array-decay,
            -cppcoreguidelines-pro-type-union-access"
        exit 0
        ;;
    *) :
        echo "Unrecognized build command!\n"
        echo $USAGE
        exit 1
        ;;
esac

# Friendly reminder for whoever's using this script
echo ""
case $SHELL in
    */zsh) :
        echo "Make sure to run this command next:"
        echo ""
        echo "    source $WORKSPACE_DIR/devel/setup.zsh"
        ;;
    *) :
        echo "Make sure to run this command next:"
        echo ""
        echo "    source $WORKSPACE_DIR/devel/setup.bash"
        ;;
esac
