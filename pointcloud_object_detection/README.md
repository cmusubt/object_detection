# Overview
3D object detection from pointclouds

# How to install
This should be in the objdet workspace like the other object 
detection packages (e.g. in workspaces/objdet/src)

Currently we are using [Pointnet++]("https://github.com/charlesq34/pointnet2") 
to do detections. This requires
compilation of custom operations. To compile the operations run:

```
cd pointcloud_object_detection/tf_ops
sh compile_ops.sh
```

**Note:** the build scripts here are different than those in the original 
ointnet++ repo, due to small compilation issues on the Xavier. See 
more on the issues [here]("https://github.com/charlesq34/pointnet2/issues/48").
 Basically, the 
`-D_GLIBCXX_USE_CXX11_ABI=0` flag has been removed.


Then in `/object_detection`, build the workspace and source it properly
```
./build.sh release
source objdet/devel/setup.bash
```

**Note:** you may get build errors on the `tracking` package. That's fine.

# How to run
```
roslaunch pointcloud_object_detection pointcloud_object_detection.launch
```

Arguments in the launch file include number of points per pointcloud (must be 
constant throughout a given run but can vary run to run, the rosnode does the 
downsampling), the gpu name to use, the model file path to use, etc.


# Known issues / TODO
Ensure  network labels (1=backpack, etc.) match what the rest of 
object_detection is expecting

[Convert published pointcloud snippet to XYZRGB (not just XYZ)?]
(https://answers.ros.org/question/289576/understanding-the-bytes-in-a-pcl2-message/)


Run an actual detection network, not just pointnet++

Right now network only takes a fixed number of points (can be defined
 before every run). Generally just an issue with using pointnet++ which really
depends on this. Make this not a requirement. 

To speed up clustering, we assume per pc only one object per category exists. 
This of course may not be true.

Add in confidences into predictions? Right now its just 1 (in range 0-1)




# Contact
Bob DeBortoli: debortor@oregonstate.edu
