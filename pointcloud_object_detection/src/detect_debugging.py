#!/usr/bin/env python

"""
Main file for doing 3d object detection from pointclouds
Contact: Bob DeBortoli (debortor@oregonstate.edu)

Copyright Carnegie Mellon University / Oregon State University <2019>
This code is proprietary to the CMU SubT challenge. Do not share or distribute without express permission of a project lead (Sebastion or Matt).
"""

from tensorflow.contrib import tensorrt as trt
import tensorflow as tf
from tensorflow.python.platform import gfile
import sys
import os
from utils import load_real_data
import pdb
import numpy as np
import time

# imports for custom pointnet++ operations
sys.path.append(os.path.join("../", "tf_ops/sampling"))
sys.path.append(os.path.join("../", "tf_ops/grouping"))
sys.path.append(os.path.join("../", "tf_ops/3d_interpolation"))
from tf_sampling import farthest_point_sample, gather_point
from tf_grouping import query_ball_point, group_point, knn_point
from tf_interpolate import three_nn, three_interpolate

sys.path.append(os.path.join("../", "models"))
import pointnet2_sem_seg


def debug_model(sess, ops, offline_data_filename):
    """
    Loads data and runs it through the model
    in an offline manner
    """

    data_split = [0.95, 0.02, 0.3]
    data, val_data, test_data = load_real_data(
        data_split, offline_data_filename
    )

    for pc, label, sample_weight in data:

        t1 = time.time()

        input_pc = np.expand_dims(pc, 0)
        input_label = np.expand_dims(label, 0)
        input_smpw = np.expand_dims(sample_weight, 0)

        feed_dict = {
            ops["pointclouds_pl"]: input_pc,
            ops["labels_pl"]: input_label,
            ops["smpws_pl"]: input_smpw,
            ops["is_training_pl"]: False,
        }

        loss_val, pred_val = sess.run(
            [ops["loss"], ops["pred"]], feed_dict=feed_dict
        )

        print(time.time() - t1)

        print(loss_val)
        print(pred_val)


if __name__ == "__main__":

    model_fname = "../models/weights/initial_model.ckpt"
    offline_data_filename = (
        "/media/nvidia/XavierSSD500/offline_pc_data/test_scannet_style.pickle"
    )
    gpu_name = "/GPU:0"

    model = pointnet2_sem_seg

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.allow_soft_placement = True
    config.log_device_placement = True

    with tf.Session(config=config) as sess:
        with tf.device(gpu_name):

            pointclouds_pl, labels_pl, smpws_pl = model.placeholder_inputs(
                1, (int(15e3))
            )
            is_training_pl = tf.placeholder(tf.bool, shape=())

            # simple model
            bn_decay = tf.minimum(0.99, 0.99)
            pred, end_points = model.get_model(
                pointclouds_pl, is_training_pl, 5, bn_decay=bn_decay
            )

            model.get_loss(pred, labels_pl, smpws_pl)
            losses = tf.get_collection("losses")
            total_loss = tf.add_n(losses, name="total_loss")

            # Add ops to save and restore all the variables.
            saver = tf.train.Saver()

        # Restore variables from disk.
        saver.restore(sess, model_fname)
        print("Model restored.")

        ops = {
            "pointclouds_pl": pointclouds_pl,
            "labels_pl": labels_pl,
            "smpws_pl": smpws_pl,
            "is_training_pl": is_training_pl,
            "pred": pred,
            "loss": total_loss,
        }

        # eval_one_epoch(sess, ops)

        debug_model(sess, ops, offline_data_filename)
