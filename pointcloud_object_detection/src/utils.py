#!/usr/bin/env python3

"""
Helper functions for debugging, etc.
Contact: Bob DeBortoli (debortor@oregonstate.edu)

Copyright Carnegie Mellon University / Oregon State University <2019>
This code is proprietary to the CMU SubT challenge. Do not share or distribute without express permission of a project lead (Sebastion or Matt).
"""
import pdb
import pickle


def load_real_data(data_split, pickle_fname):
    """
    Return real data in  pickle format split into train/val/test
    Used for debugging
    """

    # read data in
    data = []
    with open(pickle_fname, "rb") as input_file:
        while True:
            try:
                data.append(pickle.load(input_file, encoding="latin1"))
            except EOFError:
                break

    # split data
    return (
        data[: int(data_split[0] * len(data))],
        data[
            int(len(data) * (data_split[0])) : int(
                len(data) * (data_split[0] + data_split[1])
            )
        ],
        data[int(len(data) * (data_split[0] + data_split[1])) :],
    )
