#!/usr/bin/env python3

"""
Main file for doing 3d object detection from pointclouds
Contact: Bob DeBortoli (debortor@oregonstate.edu)

Copyright Carnegie Mellon University / Oregon State University <2019>
This code is proprietary to the CMU SubT challenge. Do not share or distribute without express permission of a project lead (Sebastion or Matt).
"""

from tensorflow.contrib import tensorrt as trt
import tensorflow as tf
from tensorflow.python.platform import gfile
import sys
import os
from utils import load_real_data
import pdb
import numpy as np
import time
import math
import rospkg


from objdet_msgs.msg import Detection3D, Detection3DArray
import rospy
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs.point_cloud2 as pc2


# imports for custom pointnet++ operations
pkg_path = rospkg.RosPack().get_path("pointcloud_object_detection")
sys.path.append(os.path.join(pkg_path, "tf_ops/sampling"))
sys.path.append(os.path.join(pkg_path, "tf_ops/grouping"))
sys.path.append(os.path.join(pkg_path, "tf_ops/3d_interpolation"))
from tf_sampling import farthest_point_sample, gather_point
from tf_grouping import query_ball_point, group_point, knn_point
from tf_interpolate import three_nn, three_interpolate

sys.path.append(os.path.join(pkg_path, "models"))
import pointnet2_sem_seg


# mappings between PointField types and numpy types
type_mappings = [
    (PointField.INT8, np.dtype("int8")),
    (PointField.UINT8, np.dtype("uint8")),
    (PointField.INT16, np.dtype("int16")),
    (PointField.UINT16, np.dtype("uint16")),
    (PointField.INT32, np.dtype("int32")),
    (PointField.UINT32, np.dtype("uint32")),
    (PointField.FLOAT32, np.dtype("float32")),
    (PointField.FLOAT64, np.dtype("float64")),
]

pftype_to_nptype = dict(type_mappings)

# sizes (in bytes) of PointField types
pftype_sizes = {
    PointField.INT8: 1,
    PointField.UINT8: 1,
    PointField.INT16: 2,
    PointField.UINT16: 2,
    PointField.INT32: 4,
    PointField.UINT32: 4,
    PointField.FLOAT32: 4,
    PointField.FLOAT64: 8,
}

# prefix to the names of dummy fields we add to get byte alignment correct. this needs to not
# clash with any actual field names
DUMMY_FIELD_PREFIX = "__"


def fields_to_dtype(fields, point_step):
    """Convert a list of PointFields to a numpy record datatype.
     Inspired from: http://docs.ros.org/jade/api/ros_numpy/html/point__cloud2_8py_source.html
    """
    offset = 0
    np_dtype_list = []
    for f in fields:
        while offset < f.offset:
            # might be extra padding between fields
            np_dtype_list.append(
                ("%s%d" % (DUMMY_FIELD_PREFIX, offset), np.uint8)
            )
            offset += 1

        dtype = pftype_to_nptype[f.datatype]
        if f.count != 1:
            dtype = np.dtype((dtype, f.count))

        np_dtype_list.append((f.name, dtype))
        offset += pftype_sizes[f.datatype] * f.count

    # might be extra padding between points
    while offset < point_step:
        np_dtype_list.append(("%s%d" % (DUMMY_FIELD_PREFIX, offset), np.uint8))
        offset += 1

    return np_dtype_list


def pointcloud2_to_array(cloud_msg, squeeze=True):
    """ Converts a rospy PointCloud2 message to a numpy recordarray 

    Inspired from: http://docs.ros.org/jade/api/ros_numpy/html/point__cloud2_8py_source.html

    Reshapes the returned array to have shape (height, width), even if the height is 1.

    The reason for using np.fromstring rather than struct.unpack is speed... especially
    for large point clouds, this will be <much> faster.
    """
    # construct a numpy record type equivalent to the point type of this cloud
    dtype_list = fields_to_dtype(cloud_msg.fields, cloud_msg.point_step)

    # parse the cloud into an array
    cloud_arr = np.fromstring(cloud_msg.data, dtype_list)

    # remove the dummy fields that were added
    cloud_arr = cloud_arr[
        [
            fname
            for fname, _type in dtype_list
            if not (fname[: len(DUMMY_FIELD_PREFIX)] == DUMMY_FIELD_PREFIX)
        ]
    ]

    pc_len = len(cloud_arr)
    cloud_arr = np.hstack(
        (
            cloud_arr["x"].reshape(pc_len, 1),
            cloud_arr["y"].reshape(pc_len, 1),
            cloud_arr["z"].reshape(pc_len, 1),
        )
    )

    return cloud_arr


def init_model(sess, gpu_name, npoints, model_fname):
    """
    Build the model and fill in the weights
    """

    model = pointnet2_sem_seg

    with tf.device(gpu_name):
        pointclouds_pl, labels_pl, smpws_pl = model.placeholder_inputs(
            1, npoints
        )
        is_training_pl = tf.placeholder(tf.bool, shape=())

        # simple model
        bn_decay = tf.minimum(0.99, 0.99)  # default value from pointnet++
        pred, end_points = model.get_model(
            pointclouds_pl, is_training_pl, 5, bn_decay=bn_decay
        )

        # Add ops to save and restore all the variables.
        saver = tf.train.Saver()

        # Restore variables from disk.
        saver.restore(sess, model_fname)
        rospy.loginfo("Model restored.")

    ops = {
        "pointclouds_pl": pointclouds_pl,
        "labels_pl": labels_pl,
        "smpws_pl": smpws_pl,
        "is_training_pl": is_training_pl,
        "pred": pred,
    }

    return ops


def data_through_network(data, sess, ops):
    """
    :param data: np array of the pc
    :param sess: tensorflow session
    :param ops: tensorflow references
    :return: list of detections
    """

    input_pc = np.expand_dims(data, 0)
    input_label = np.expand_dims(np.ones(len(data)), 0)
    input_smpw = np.expand_dims(np.ones(len(data)), 0)

    feed_dict = {
        ops["pointclouds_pl"]: input_pc,
        ops["labels_pl"]: input_label,
        ops["smpws_pl"]: input_smpw,
        ops["is_training_pl"]: False,
    }
    t1 = time.time()
    pred_val = sess.run(ops["pred"], feed_dict=feed_dict)
    print("Remove", time.time() - t1)

    return pred_val


def process_data(msg, npoints):
    """
    Process and downsample input pointclouds to be fed into network
    :param msg: ros pointcloud
    :return: numpy array downsampled to the proper amt
    """
    data = pointcloud2_to_array(msg)

    if len(data) < npoints:
        # the network will not be able to process a pc with so few points
        rospy.logwarn("Received pc with # points less than discretization!")
        return None

    return data[np.linspace(0, len(data) - 1, npoints).astype("int")]


def cluster_and_pub_detections(
    pred_labels, pc, det_pub, class_dict, obj_minpoints, timestamp, frame_id
):
    """
    Process the labels, cluster them, and publish a detection if an
    object is predicted to be in this cloud
    :param pred_labels: the predicted labels for every point in the cloud
    :param pc: pointcloud
    det_pub is the Publisher for the detection message
    class_dict: mapping from labels (ints) to human readable classes
    obj_minpoints minimum number of points to consider a cluster an object detection
    timestamp: ros time of key_pose cloud
    """

    detections = []

    human_label_mappings = {
        "b": "backpack",
        "d": "drill",
        "s": "survivor",
        "f": "fire extinguisher",
    }

    unique_labels = np.unique(pred_labels)

    if unique_labels.max() == 0:
        # no detections
        return detections

    # we have actual detections
    # TODO: remove assumption of having only one object of a certain type per pc

    detection_arr = Detection3DArray()
    detection_arr.header.stamp = timestamp
    detection_arr.header.frame_id = frame_id

    for category in unique_labels:

        if category != 0:  # we have an actual object

            object_points = pc[np.where(pred_labels == category)[0]]

            obj_pts_filt, centroid = filter_points(object_points)

            if len(obj_pts_filt) == 0:
                rospy.logwarn(
                    "Object prediction contained points very spread apart. No centroid could be computed. Pc not processed"
                )
                continue

            if len(obj_pts_filt) < obj_minpoints:
                rospy.logwarn(
                    "Object detected but not enough points in detection to be published"
                )
                continue

            # compute size of box
            size_x = abs(obj_pts_filt[:, 0].min() - obj_pts_filt[:, 0].max())
            size_y = abs(obj_pts_filt[:, 1].min() - obj_pts_filt[:, 1].max())
            size_z = abs(obj_pts_filt[:, 2].min() - obj_pts_filt[:, 2].max())

            det_msg = Detection3D()
            det_msg.x = size_x
            det_msg.y = size_y
            det_msg.z = size_z
            det_msg.pose.position.x = centroid[0]
            det_msg.pose.position.y = centroid[1]
            det_msg.pose.position.z = centroid[2]
            det_msg.label = human_label_mappings[class_dict[category]]
            det_msg.id = category
            det_msg.confidence = 1
            det_msg.pc = pc2.create_cloud_xyz32(
                detection_arr.header, obj_pts_filt
            )

            detection_arr.detections.append(det_msg)

    det_pub.publish(detection_arr)


def filter_points(object_points):
    """
    Takes a set of 3d points and returns a set without outliers
    :param object_points: set of 3d points pertaining to a single predicted object
    :return:
    """

    thresh = rospy.get_param(
        "obj_size_thresh"
    )  # how close must be to centroid to be considered part of the object

    # compute the centroid
    centroid = np.array(
        [
            np.mean(object_points[:, 0]),
            np.mean(object_points[:, 1]),
            np.mean(object_points[:, 2]),
        ]
    )

    # removes points not near the centroid
    obj_pts_filt = object_points[
        np.array(
            np.where(
                (object_points[:, 0] - centroid[0]) ** 2
                + (object_points[:, 1] - centroid[1]) ** 2
                + (object_points[:, 2] - centroid[2]) ** 2
                < thresh ** 2
            )
        )[0]
    ]

    # re-compute centroid given new set of points
    if len(obj_pts_filt) == 0:
        return obj_pts_filt, []

    new_centroid = np.array(
        [
            np.mean(obj_pts_filt[:, 0]),
            np.mean(obj_pts_filt[:, 1]),
            np.mean(obj_pts_filt[:, 2]),
        ]
    )

    return obj_pts_filt, new_centroid


def pc_callback(msg, args):
    """
    Processing pointcloud messages
    """

    sess, ops, npoints, det_pub, class_dict, obj_minpoints = args
    pc = process_data(msg, npoints)

    if pc is None:
        rospy.logwarn(
            "Something went wrong processing the pointclouds from ros into the pointcloud_object_detection framework"
        )
        return
    t1 = time.time()
    pred_labels = data_through_network(pc, sess, ops)
    # print("Remove",time.time() - t1)
    pred_labels = np.argmax(
        pred_labels, axis=2
    ).T  # argmax to actual actual 1-4 labels

    # cluster and publish detections
    cluster_and_pub_detections(
        pred_labels,
        pc,
        det_pub,
        class_dict,
        obj_minpoints,
        msg.header.stamp,
        msg.header.frame_id,
    )


def get_class_mappings(class_mapping_fname):
    """
    build a dictionary of the  class mappings
    """

    class_dict = {}

    with open(class_mapping_fname, "r") as f:
        txt = f.readlines()

        for i, line in enumerate(txt):
            if line.find("id") != -1:

                id = int(line[line.find(":") + 1 :])
                category = txt[i + 1][
                    txt[i + 1].find("'") + 1 : -txt[i + 1][::-1].find("'") - 1
                ]

                class_dict[id] = category

    return class_dict


if __name__ == "__main__":

    model_fname = rospy.get_param(
        "model_fname"
    )  # filename of model to be loaded
    gpu_name = rospy.get_param("gpu_name")  # name of device to load model onto
    npoints = rospy.get_param(
        "npoints"
    )  # number of points to downsample incoming pointclouds to in order to feed into pointnet++
    pc_topic = rospy.get_param(
        "pc_topic"
    )  # topic to subscribe to for pointclouds
    obj_minpoints = rospy.get_param(
        "obj_minpoints"
    )  # minimum number of classified points to consider it an object detection

    # get the class mappings e.g. 1 = backpack
    class_mapping_fname = rospy.get_param("class_mapping_fname")
    class_dict = get_class_mappings(class_mapping_fname)

    # start tensorflow session
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.per_process_gpu_memory_fraction = 0.05
    config.allow_soft_placement = True
    config.log_device_placement = True

    with tf.Session(config=config) as sess:

        ops = init_model(sess, gpu_name, npoints, model_fname)

        det_pub = rospy.Publisher(
            "pointcloud_detection", Detection3DArray, queue_size=10
        )

        pc_sub = rospy.Subscriber(
            pc_topic,
            PointCloud2,
            pc_callback,
            (sess, ops, npoints, det_pub, class_dict, obj_minpoints),
        )

        rospy.init_node("pointcloud_obj_detection", anonymous=True)
        rate = rospy.Rate(1)

        while not rospy.is_shutdown():
            rate.sleep()
