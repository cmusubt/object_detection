#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

const int DOWNSAMPLE = 200;

int main(int argc, char** argv) {
  if (argc < 2) {
    std::cerr << "Not enough arguments" << std::endl;
    return -1;
  }

  std::string input_filename(argv[1]);
  std::ifstream input_file(input_filename);
  std::string line;
  std::getline(input_file, line);
  long lines_read = 1;
  std::istringstream iss(line);
  long num_points;
  iss >> num_points;

  num_points /= DOWNSAMPLE;

  std::string output_filename = input_filename;
  output_filename.replace(output_filename.find("."),
                          std::string(".pts").length(), ".pcd");
  std::cout << "output filename: " << output_filename << std::endl;
  std::ofstream output_file(output_filename);
  output_file << "VERSION .7\n";
  output_file << "FIELDS x y z rgb\n";
  output_file << "SIZE 4 4 4 4\n";
  output_file << "TYPE F F F F\n";
  output_file << "COUNT 1 1 1 1\n";
  output_file << "WIDTH " << num_points << "\n";
  output_file << "HEIGHT 1\n";
  output_file << "POINTS " << num_points << "\n";
  output_file << "DATA ascii\n";

  while (std::getline(input_file, line)) {
    ++lines_read;

    if (lines_read % DOWNSAMPLE != 0) {
      continue;
    }

    iss = std::istringstream(line);

    float x;
    float y;
    float z;
    int intensity;
    int r;
    int g;
    int b;
    iss >> x >> y >> z >> intensity >> r >> g >> b;

    // http://docs.pointclouds.org/trunk/structpcl_1_1_point_x_y_z_r_g_b.html
    uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
    float rgb_float = *reinterpret_cast<float*>(&rgb);

    output_file << x << " " << y << " " << z << " " << rgb_float << "\n";
  }

  std::cout << std::flush;
  output_file << std::flush;
  output_file.close();
  input_file.close();
  return 0;
}
