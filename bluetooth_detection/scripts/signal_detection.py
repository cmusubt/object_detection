#!/usr/bin/env python3
from bt_proximity import BluetoothRSSI
import bluetooth
import subprocess
import time
import threading
import sys
import std_msgs.msg
import rospy
from objdet_msgs.msg import WirelessSignalReading, WirelessSignalReadingArray

SLEEP = 1


# scan for bt signals continuously
class BluetoothScanner(object):
    def __init__(self, device_name="scanner"):
        self.bt_addr_set = set()
        self.device_name = device_name
        self.addr_lock = threading.Lock()
        self.last_pub_time = rospy.Time()
        self.pub_bt = rospy.Publisher(
            "bluetooth_info", WirelessSignalReadingArray, queue_size=10
        )
        self.readings = WirelessSignalReadingArray()
        self.threads = []
        self.no_device_found = 0

        bt_thread_scan = threading.Thread(target=self.bt_search, args=())
        bt_thread_scan.start()

    # search for bluetooth signals
    def bt_search(self):
        while not rospy.is_shutdown():
            try:
                nearby_devices = bluetooth.discover_devices(
                    duration=3, flush_cache=True, lookup_names=True
                )
                rospy.loginfo(nearby_devices)
                with self.addr_lock:
                    for addr, name in nearby_devices:
                        if addr not in self.bt_addr_set:
                            th = self.bt_new_thread(addr, name)
                if len(nearby_devices) > 0:
                    rospy.loginfo("Found %d devices!" % len(nearby_devices))
                else:
                    rospy.loginfo("No devices found!")
                self.no_device_found = 0
            except Exception as e:
                if "No such device" not in str(e):
                    rospy.logwarn("Exception: %s", e)
                else:
                    self.no_device_found += 1
                    if self.no_device_found % 50000 == 1:
                        rospy.logwarn("Exception: %s", e)

    # if new address found, make thread to keep searching for its rssi vals
    def bt_new_thread(self, addr, name):
        self.bt_addr_set.add(addr)
        th = BtThread(addr, name, self)
        self.threads.append(th)
        th.start()

    # if addr not found for 5 scans
    # stop searching for its rssi vals until addr is found again
    def bt_thread_finished(self, thread, addr):
        with self.addr_lock:
            self.bt_addr_set.remove(addr)

    # add readings to radio readings array
    def on_thread_callback(self, time, addr, name, rssi):
        if time != self.last_pub_time:
            # update newest time and publish last reading array
            self.last_pub_time = time
            self.pub_bt.publish(self.readings)

            self.readings = WirelessSignalReadingArray()
            self.readings.header.stamp = time
            self.readings.header.frame_id = (
                "/" + self.device_name + "/bluetooth"
            )
        # add individual radio reading from an address
        msg = WirelessSignalReading()
        msg.mac_address = addr
        msg.name = name
        msg.rssi = int(rssi[-1])
        self.readings.readings.append(msg)
        rospy.loginfo("bluetooth publishing!!! %s" % (addr))

    def bt_join_threads(self):
        for th in self.threads:
            th.join()


# scan for wifi signals continuously
class WifiScanner(object):
    def __init__(
            self, interface="wlp0s20f3", use_iwlist=True, device_name="scanner", password="passme24"
    ):
        self.wf_addr_set = set()
        self.use_iwlist = use_iwlist
        self.interface = interface
        self.device_name = device_name
        self.password = password
        self.prev_output = ""
        self.pub_wf = rospy.Publisher(
            "wifi_info", WirelessSignalReadingArray, queue_size=10
        )
        self.wf_thread_scan = threading.Thread(target=self.wf_search, args=())
        self.wf_thread_scan.start()

    # search for wifi signals
    def wf_search(self):
        r = rospy.Rate(10)

        while not rospy.is_shutdown():
            r.sleep()
            if self.use_iwlist:
                scan_output = self.get_iwlist_ouput()
                readings = self.parse_iwlist_output(self.interface, scan_output)
            else:
                scan_output = self.get_wpa_output()
                if scan_output == self.prev_output:
                    continue
                readings = self.parse_wpa_output(scan_output)
                self.prev_output = scan_output

            self.publish_readings(readings, self.device_name)

    def get_wpa_output(self):
        try:
            scan_command = "echo " + self.password + " | sudo -S wpa_cli scan -i " + self.interface
            subprocess.check_output(["sh", "-c", scan_command])
            scan_results_command = "echo " + self.password + " | sudo -S wpa_cli scan_results -i " + self.interface
            bytes_output = subprocess.check_output(["sh", "-c", scan_results_command])
            scan_output = str(bytes_output, "utf-8")
            return scan_output

        except Exception as e:
            rospy.logwarn("Exception: %s", e)
            return ""

    @staticmethod
    def parse_wpa_output(scan_output):
        readings = []
        try:
            data = scan_output.splitlines()[1:]
            for device in data:
                d = device.split("\t")
                # find addr
                addr = d[0]

                # find signal level
                sig = d[2]

                # find name
                name = d[4]
                reading = (addr, name, int(sig))
                readings.append(reading)

        except Exception as e:
            rospy.logwarn("Exception: %s", e)

        return readings

    def get_iwlist_ouput(self):
        rospy.loginfo_once(
            "other scanner recommended; set iwlist=False in launch file"
        )
        try:
            scan_command = "echo " + self.password + " | sudo iwlist " + self.interface + " scan"
            bytes_output = subprocess.check_output(["sh", "-c", scan_command])
            scan_output = str(bytes_output, "utf-8")
            return scan_output

        except Exception as e:
            rospy.logwarn("Exception: %s", e)
            return ""

    @staticmethod
    def parse_iwlist_output(interface, scan_output):
        readings = []
        try:
            # parse ouput for wifi info
            data = scan_output.split("- Address: ")

            for d in data[1:]:
                # find MAC address
                addr = d[0:17]

                # find name
                start = d.find('ESSID:"')
                end = d[start:].find('"\n')
                name = d[start + len('ESSID:"') : start + end]

                # find signal level
                start = d.find("Signal level=")
                if interface == "wlp02s0f3":  # if device is nuc
                    end = d[start:].find("dBm  \n")
                else:  # if device is xavier
                    end = d[start:].find(" dBm  \n")
                sig = d[start + len("Signal level=") : start + end]

                reading = (addr, name, int(sig))
                readings.append(reading)

        except Exception as e:
            rospy.logwarn("Exception: %s", e)

        return readings

    def publish_readings(self, readings, device_name):
        timestamp = rospy.Time.now()

        msgs = WirelessSignalReadingArray()
        msgs.header.stamp = timestamp
        msgs.header.frame_id = "/" + self.device_name + "/wifi"

        for reading in readings:
            msg = WirelessSignalReading()
            msg.mac_address = reading[0]
            msg.name = reading[1]
            msg.rssi = reading[2]
            msgs.readings.append(msg)
            #  rospy.loginfo("wifi publishing!!! %s", reading[0])

        self.pub_wf.publish(msgs)


# search specific addr for its rssi val quickly
class BtThread(threading.Thread):
    def __init__(self, addr, name, parent, sleep=SLEEP):
        super(BtThread, self).__init__()
        self.parent = parent
        self.addr = addr
        self.sleep = sleep
        self.name = name
        self.daemon = True
        self.wait = 5

    # called continously & runs function to find objects bt rssi
    def run(self):
        self.bluetooth_listen(self.parent, self.addr, self.name, self.sleep)
        self.parent and self.parent.bt_thread_finished(self, self.addr)

    # continuously check rssi values for given devide
    def bluetooth_listen(self, manager, addr, name, sleep=1):
        last_seen = 0
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            b = BluetoothRSSI(addr=addr)
            rssi = b.get_rssi()
            timestamp = rospy.Time.now()
            rospy.loginfo("---")

            # Sleep and then skip to next iteration if device not found
            if rssi is None:
                last_seen += 1
                rospy.loginfo(
                    "addr: {} cannot be found: {}".format(
                        addr, self.wait - last_seen
                    )
                )
                if last_seen >= self.wait:
                    return
                r.sleep()
                continue
            else:
                rospy.loginfo("addr: {}, rssi: {}".format(addr, rssi[-1]))

            manager.on_thread_callback(timestamp, addr, name, rssi)
            # Delay between iterations
            r.sleep()


# start wifi & bt signal searches
def main():
    rospy.init_node("phone_scanner")
    interface = rospy.get_param("~interface")
    use_iwlist = rospy.get_param("~use_iwlist")
    device_name = rospy.get_param("~device_name", "")
    password = rospy.get_param("~password", "passme24")
    #bt_scan = BluetoothScanner(device_name)
    wf_scan = WifiScanner(interface, use_iwlist, device_name, password)

    try:
        rospy.spin()
    except Exception as e:
        pass

    #bt_scan.bt_join_threads()
    wf_scan.wf_thread_scan.join()


if __name__ == "__main__":
    main()
