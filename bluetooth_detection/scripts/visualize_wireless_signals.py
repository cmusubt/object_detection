#!/usr/bin/env python3

import math
import collections
import rospy
import threading
from visualization_msgs.msg import Marker, MarkerArray
from objdet_msgs.msg import WirelessSignalReadingArray
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point


class WirelessSignalVisualizer(object):
    TARGET_ODOM_FREQ = 5  # Drop any odometry that comes in faster than this
    TARGET_ODOM_ELAPSED = rospy.Duration.from_sec(1.0 / TARGET_ODOM_FREQ)

    CIRCLE_SEGMENTS = 36
    CIRCLE_STEP = 2 * math.pi / CIRCLE_SEGMENTS

    # https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/
    colors = [
        (230, 25, 75),
        (60, 180, 75),
        (255, 225, 25),
        (0, 130, 200),
        (245, 130, 48),
        (145, 30, 180),
        (70, 240, 240),
        (240, 50, 230),
        (210, 245, 60),
        (250, 190, 190),
        (0, 128, 128),
        (230, 190, 255),
        (170, 110, 40),
        (255, 250, 200),
        (128, 0, 0),
        (170, 255, 195),
        (128, 128, 0),
        (255, 215, 180),
        (0, 0, 128),
        (128, 128, 128),
        (255, 255, 255),
        (0, 0, 0),
    ]

    def __init__(self, odom_topic, rssi_topic):
        # Subscribing to odometry instead of tf because we won't get transforms
        # at the base station, and this node should be useful if rssi values are
        # sent over the network.
        self._odom_sub = rospy.Subscriber(
            odom_topic, Odometry, self._odom_cb, queue_size=10
        )
        self._odom_history = collections.deque(
            maxlen=self.TARGET_ODOM_FREQ * 10
        )
        self._odom_frame = ""
        self._odom_lock = threading.Lock()
        self._last_odom_time = rospy.Time()  # 0 seconds

        self._rssi_topic = rssi_topic
        self._rssi_sub = rospy.Subscriber(
            rssi_topic, WirelessSignalReadingArray, self._rssi_cb
        )
        self._mac_indices = dict()
        self._mac_counters = dict()

        self._marker_pub = rospy.Publisher(
            rssi_topic + "/markers", MarkerArray, queue_size=10
        )

    def _odom_cb(self, odom):
        if odom.header.stamp - self._last_odom_time < self.TARGET_ODOM_ELAPSED:
            return

        self._last_odom_time = odom.header.stamp
        with self._odom_lock:
            self._odom_history.appendleft(
                (odom.header.stamp, odom.pose.pose.position)
            )
            self._odom_frame = odom.header.frame_id

    def _rssi_cb(self, readings):
        matched_stamp = None
        matched_pos = None
        rssi_stamp = readings.header.stamp
        with self._odom_lock:
            odom_frame = self._odom_frame
            for stamp, position in self._odom_history:
                if stamp < rssi_stamp:
                    matched_stamp = stamp
                    matched_pos = position
                    break

        if (rssi_stamp - matched_stamp) >= (
            self.TARGET_ODOM_FREQ * self.TARGET_ODOM_ELAPSED
        ):
            rospy.logwarn("matched stamp was too early!")
            return

        markers = MarkerArray()
        for reading in readings.readings:
            distance = self._distance_model(reading.rssi)
            if reading.mac_address not in self._mac_indices:
                self._mac_indices[reading.mac_address] = len(self._mac_indices)
                self._mac_counters[reading.mac_address] = 0

            index = self._mac_indices[reading.mac_address]
            counter = self._mac_counters[reading.mac_address]
            self._mac_counters[reading.mac_address] += 1

            marker = Marker()
            marker.header.frame_id = odom_frame
            marker.header.stamp = readings.header.stamp
            marker.ns = self._rssi_topic + "/" + reading.mac_address
            marker.id = counter
            marker.type = Marker.LINE_STRIP
            marker.action = Marker.ADD
            marker.pose.position = matched_pos
            marker.scale.x = 0.02

            color = self.colors[index % len(self.colors)]
            marker.color.r = color[0] / 255.0
            marker.color.g = color[1] / 255.0
            marker.color.b = color[2] / 255.0
            marker.color.a = 1.0
            #  marker.duration = rospy.Duration(120)

            for i in range(self.CIRCLE_SEGMENTS + 1):
                angle = i * self.CIRCLE_STEP
                p = Point()
                p.x = math.cos(angle) * distance
                p.y = math.sin(angle) * distance
                p.z = 0
                marker.points.append(p)

            markers.markers.append(marker)

        if len(markers.markers) > 0:
            self._marker_pub.publish(markers)


class BluetoothVisualizer(WirelessSignalVisualizer):
    def _distance_model(self, rssi):
        # This is the same model as is currently used in the simulator. This
        # will need to get updated once we have antennas properly mounted on the
        # payload.
        # TODO(vasua): Maybe pull this model from the same place as simulator
        return 10 ** ((rssi - 38) / (10 * -4.18))


def main():
    rospy.init_node("wireless_signal_visualizer")

    rv = BluetoothVisualizer("integrated_to_map", "bluetooth_info")
    rospy.spin()


if __name__ == "__main__":
    main()
