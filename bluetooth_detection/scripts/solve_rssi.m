positions = [
 191.145 -108.866  2.74115
  191.69 -109.446  2.80317
 192.375  -110.09  2.79472
 193.016 -110.684  2.67112
 193.553 -111.288  2.74073
 194.126 -111.798  2.71648
 194.899 -112.348  2.76575
 195.573 -112.866  2.77836
 196.177 -113.399  2.67518
 196.986 -113.859  2.74299
 197.679 -114.304  2.79231
];

rssis = [
16
14
11
 9
 6
 4
 2
 0
 0
-2
-3
];

distances = 10.^((rssis - 38) / (10 * -4.18));
distances_sq = distances .* distances;

A = [];
b = [];
for i = 2:size(positions, 1)
    A = [A; 2 * (positions(i, :) - positions(1, :))];
    b = [b; dot(positions(i, :), positions(i, :)) - dot(positions(1, :), positions(1, :)) + distances_sq(i) - distances_sq(1)];
end