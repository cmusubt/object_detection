#!/usr/bin/env python3
import select
import bluetooth
import rospy
import time 
from objdet_msgs.msg import WirelessSignalReading, WirelessSignalReadingArray

class SubtDiscoverer(bluetooth.DeviceDiscoverer):

    def __init__(self):
        self.MAC_to_device_name_dict = {}
        self.device_name_to_MAC_dict = {}
        self.MAC_to_rssi_history = {}
        self.with_names = False
        self.device_name = "nuc"
        self.pub_bt = rospy.Publisher("bluetooth_info", WirelessSignalReadingArray, queue_size=100)
        self.device_found = False
        super().__init__()
        

        # add readings to radio readings array
    def on_thread_callback(self, addr, name):
        reading_array = WirelessSignalReadingArray()
        timestamp = rospy.Time.now()
        reading_array.header.stamp = timestamp
        reading_array.header.frame_id = ("/" + self.device_name + "/bluetooth")
        next_reading = self.MAC_to_rssi_history[addr][-1]
        # add individual radio reading from an address
        msg = WirelessSignalReading()
        msg.mac_address = addr
        msg.name = name
        msg.rssi = next_reading
        reading_array.readings.append(msg)
        self.pub_bt.publish(reading_array)
        self.device_found = True
        #rospy.loginfo("Bluetooth  %s %s" % (addr,str(msg.rssi)))
        
    def pre_inquiry(self):
        self.done = False
        self.with_names = True #len(self.device_name_to_MAC_dict) < len(self.MAC_to_rssi_history) 

    def device_discovered(self, address, device_class, rssi, name):
        
        if not address in self.MAC_to_rssi_history:
            self.MAC_to_rssi_history[address] = []

        self.MAC_to_rssi_history[address].append(rssi)
        rospy.loginfo("Adding new RSSI " + str(rssi) + " to address " + str(address) + " name is " + str(name))
            
        if not address in self.MAC_to_device_name_dict:
            # New DEVICE!
            if not name:
                # No NAME! --- rescan with names!
                self.with_names = True
            else:
                self.device_name_to_MAC_dict[name] = address
                self.MAC_to_device_name_dict[address] = name.decode("utf-8") 
        else:
            self.on_thread_callback(address, self.MAC_to_device_name_dict[address])

    def inquiry_complete(self):
        if not self.device_found:
            reading_array = WirelessSignalReadingArray()
            timestamp = rospy.Time.now()
            reading_array.header.stamp = timestamp
            reading_array.header.frame_id = ("/" + self.device_name + "/bluetooth")
            self.pub_bt.publish(reading_array)
            
        self.device_found = False
        self.done = True


def main():
    rospy.init_node("bluetooth_scanner")

    d = SubtDiscoverer()
    scan_duration = 3
       
    d.find_devices(lookup_names=True, duration=scan_duration)

    readfiles = [d, ]

    scan_count = 1
 
    while not rospy.is_shutdown():
        rfds = select.select(readfiles, [], [])[0]

        if d in rfds:
            d.process_event()

        if d.done:           
            d.find_devices(lookup_names=d.with_names, duration=scan_duration)
            readfiles = [d, ]
            scan_count+=1

        if scan_count % 20 == 0:
            for device in d.MAC_to_rssi_history:
                device_name = "UNKNOWN"
                
                if device in d.MAC_to_device_name_dict:
                    device_name = d.MAC_to_device_name_dict[device]

                    # We reverse it to show the most recent readings on the left side
                    rssi_history_string = (','.join(str(x) for x in d.MAC_to_rssi_history[device][::-1]))                    
                    rospy.loginfo(device_name + " : " + rssi_history_string)

if __name__ == "__main__":
    main()

        



