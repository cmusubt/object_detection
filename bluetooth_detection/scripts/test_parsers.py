#!usr/bin/env python3
import subprocess
from signal_detection import WifiScanner
import unittest

INTERFACE = "wlp0s20f3"

# check if wifi results parsers are working correctly
class TestWifiParsers(unittest.TestCase):
    def test_wpa_cli(self):
        # get correctly parsed wifi readings
        path_correct_readings = "../data/tests/wpa_parsed_readings.txt"
        correct_readings_file = open(path_correct_readings, "r")
        correct_readings = correct_readings_file.read()[:-1]

        # get scan output
        path = "../data/tests/wpa_scan_output.txt"
        output_file = open(path, "r")
        output = output_file.read()

        # parse with WifiScanner
        readings = WifiScanner.parse_wpa_output(output)

        self.assertEqual(str(readings), correct_readings)

        correct_readings_file.close()
        output_file.close()

    def test_iwlist(self):
        # get correctly parsed wifi readings
        path_correctly_parsed = "../data/tests/iwlist_parsed_readings.txt"
        correct_parsing_file = open(path_correctly_parsed, "r")
        correct_parsing = correct_parsing_file.read()[:-1]

        # get scan output
        path = "../data/tests/iwlist_scan_output.txt"
        output_file = open(path, "r")
        output = output_file.read()

        # parse with WifiScanner
        parsing = WifiScanner.parse_iwlist_output(INTERFACE, output)

        self.assertEqual(str(parsing), correct_parsing)

        correct_parsing_file.close()
        output_file.close()


if __name__ == "__main__":
    unittest.main()
