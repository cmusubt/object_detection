ROS node for bluetooth detection with RSSI.

DEPENDENCIES:

	sudo apt-get install bluetooth libbluetooth-dev
	sudo python -m pip install pybluez
	
	
FUNCTIONALITY:
	Multi-threading implementation.
	BluetoothScanner thread does a scan for duration=4 seconds and creates a new bt_thread
	for every MAC address it has yet to see. bt_threads keep track of a specific
	address and measures the rssi signal strength at 10Hz. Forwards the rssi back 
	to the manager if the signal is within THRESHHOLD. After not detecting a signal 
	for 5 iterations, bt_threads automatically end. WifiScanner thread does scan for 
	duration=1 second. It parses result for each device found and publishes rssi vals.


USAGE:
	Open sudo visudo, below  $ %sudo   ALL=(ALL:ALL) ALL, add:
		$<usr> ALL=(ALL) NOPASSWD: /sbin/wpa_cli
		$<usr> ALL=(ALL) NOPASSWD: /sbin/iwlist

	Edit phone_scan.launch parameters
		set device to wifi-interface name
		   to find device name, run $sudo wpa_cli scan
		set iwlist to True/False. RECOMMEND using --False--
		   if you want to use iwlist to scan for devices
		   instead of wpa_cli, set to True
		set scanner to name of device scanning
		   ex) scanner, nuc, xavier,...
	
	To run scanner:
		$roslaunch bluetooth_detection example_phone_scan.launch


	Messages published are of form:
	  WirelessSignalReadingArray.msg: 	
	     Header header
	     WirelessSignalReading[] reading	  	

	  where WirelessSignalReading.msg is:
	    string mac_address
	    string name #essig or device name
	    int32 rssi

    To test that parsers are working as inteded,
    run one of the following based on what scan you're using (wpa_cli[recommended], iwlist):
	$python3 wpa_cli_parser_check.py
	$python3 iwlist_parser_check.py
 
