"""
Purpose of file:
Record x/y/z position for:
	  What data did the robot try to transmit
	  What got received at base station
	  What got acknowledged back at the robot?
Record how many total artifacts got reported per robot per run

Lots of repeated code...tried to make modular, but base station and robot
analysis is just different enough that it didnt make sense.

Contact: Robert DeBortoli (debortor@oregonstate.edu)
"""
import pdb
import os
import rosbag
import csv
import matplotlib.pyplot as plt
import numpy as np
import argparse


def analyze_objdet(
    objdet_bagfile_folder,
    robot_output_fname,
    topics=["/artifact_localizations"],
):
    """
	Write-out the locations of artifacts reported by the robot

	Note: this is technically into the compressor, which we 
	consider for these purposes into DDS.
	"""
    with open(robot_output_fname, "a+") as csv_file:
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(
            [
                "Timestamp",
                "x",
                "y",
                "z",
                "report_id",
                "class_id",
                "valid",
                "array stamp",
            ]
        )

        for fname in sorted(os.listdir(objdet_bagfile_folder)):
            bag = rosbag.Bag(os.path.join(objdet_bagfile_folder, fname))
            for topic, msg, t in bag.read_messages(topics=topics):
                for artifact in msg.localizations:
                    writer.writerow(
                        [
                            artifact.stamp.to_sec(),
                            artifact.x,
                            artifact.y,
                            artifact.z,
                            artifact.report_id,
                            artifact.class_id,
                            artifact.valid,
                            msg.header.stamp.to_sec(),
                        ]
                    )
            print("Done", os.path.join(objdet_bagfile_folder, fname))
            bag.close()


def analyze_auto(auto_bagfile_folder, uav_output_fname, topics):
    """
	For analyzing the perception data coming out of a small drone
	"""
    with open(uav_output_fname, "a+") as csv_file:
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(
            [
                "Timestamp",
                "x",
                "y",
                "z",
                "report_id",
                "class_id",
                "valid",
                "array stamp",
            ]
        )

        for fname in sorted(os.listdir(auto_bagfile_folder)):
            bag = rosbag.Bag(os.path.join(auto_bagfile_folder, fname))
            for topic, msg, t in bag.read_messages(topics=topics):
                for artifact in msg.localizations:
                    for artifact in msg.localizations:
                        writer.writerow(
                            [
                                msg.header.stamp.to_sec(),
                                artifact.x,
                                artifact.y,
                                artifact.z,
                                artifact.report_id,
                                artifact.class_id,
                                "",
                                msg.header.stamp.to_sec(),
                            ]
                        )

        bag.close()


def analyze_basestation(
    basestation_bagfile_folder,
    base_station_received_fname,
    base_station_acked_fname,
    base_station_received_compressed_fname,
    vehicle,
):
    """
	Write out artifacts and locations received by the base station.

	Also write out artifacts acked by base station
	"""

    received_file = open(base_station_received_fname, "a+")
    received_writer = csv.writer(received_file, delimiter=",")
    received_writer.writerow(
        ["Timestamp", "x", "y", "z", "report_id", "class_id", "valid"]
    )

    acked_file = open(base_station_acked_fname, "a+")
    acked_writer = csv.writer(acked_file, delimiter=",")
    acked_writer.writerow(["Timestamp"])

    received_comp_file = open(base_station_received_compressed_fname, "a+")
    received_comp_writer = csv.writer(received_comp_file, delimiter=",")
    received_comp_writer.writerow(
        ["Timestamp", "x", "y", "z", "class_id", "report_id"]
    )

    topics = [
        "/" + vehicle + "/frame/wifi_detection",
        "/" + vehicle + "/artifact_localizations_compressed_ack",
        "/" + vehicle + "/artifact_localizations_compressed",
    ]

    for fname in sorted(os.listdir(basestation_bagfile_folder)):
        bag = rosbag.Bag(os.path.join(basestation_bagfile_folder, fname))
        for topic, msg, t in bag.read_messages(topics=topics):

            if topic == "/" + vehicle + "/frame/wifi_detection":
                received_writer.writerow(
                    [
                        msg.artifact_stamp.to_sec(),
                        msg.artifact_x,
                        msg.artifact_y,
                        msg.artifact_z,
                        msg.artifact_report_id,
                        msg.artifact_type,
                    ]
                )

            elif (
                topic
                == "/" + vehicle + "/artifact_localizations_compressed_ack"
            ):
                acked_writer.writerow([msg.stamp.to_sec()])

            elif topic == "/" + vehicle + "/artifact_localizations_compressed":
                for artifact in msg.localizations:
                    received_comp_writer.writerow(
                        [
                            msg.header.stamp.to_sec(),
                            artifact.x,
                            artifact.y,
                            artifact.z,
                            artifact.class_id,
                            artifact.report_id,
                        ]
                    )

            else:
                print("Dont recognize this basestation message type")

        bag.close()

    received_file.close()
    acked_file.close()
    received_comp_file.close()


def read_comms_csv(csv_fname, skip_valid_status=False):
    """
	Read messages received/relevant to comms
	analysis into array
	"""
    return_arr = []

    with open(csv_fname) as csv_file:
        reader = csv.reader(csv_file, delimiter=",")
        for i, line in enumerate(reader):
            # skip headers
            if i == 0:
                continue
            return_arr.append(
                [
                    float(line[k])
                    for k in range(len(line))
                    if (not skip_valid_status) or k != 6
                ]
            )

    return return_arr


def compare_objdet_basestation(
    objdet_transmitted_fname, base_station_received_fname
):
    """
	Find positions of artifacts received at base station
	and those not received.
	"""

    objdet_artifacts = []

    objdet_artifacts = read_comms_csv(
        objdet_transmitted_fname, skip_valid_status=True
    )

    base_station_artifacts = read_comms_csv(base_station_received_fname)

    artifacts_received, artifacts_missed = [], []

    for obj_art in objdet_artifacts:
        received = False
        (
            obj_stamp,
            obj_x,
            obj_y,
            obj_z,
            obj_report_id,
            obj_class_id,
            obj_arr_stamp,
        ) = obj_art

        for b_art in base_station_artifacts:
            b_stamp, b_x, b_y, b_z, b_report_id, b_class_id = b_art

            if (
                (not received)
                and (obj_stamp == b_stamp)
                and (b_report_id == obj_report_id)
            ):
                # put this in for uav! should work for ugv too:
                # putting this into map frame
                artifacts_received.append(obj_art)
                received = True

        if not received:
            print("Missed artifact not in darpa frame! in robot local frame")
            artifacts_missed.append(obj_art)

    return objdet_artifacts, artifacts_received, artifacts_missed


def compare_base_station_robot(
    base_station_received_compressed_fname, base_station_acked_fname
):
    """
	Return which artifacts have been acked by base station
	"""

    received_artifacts, acks, acked_artifacts, unacked_artifacts = (
        [],
        [],
        [],
        [],
    )

    received_artifacts = read_comms_csv(base_station_received_compressed_fname)
    acks = read_comms_csv(base_station_acked_fname)

    for artifact in received_artifacts:
        acked = False
        art_stamp, art_x, art_y, art_z, art_class_id, art_report_id = artifact

        for acked_art in acks:
            acked_stamp = acked_art[0]

            if art_stamp == acked_stamp:
                acked = True
                break

        if not acked:
            unacked_artifacts.append(artifact)
        else:
            acked_artifacts.append(artifact)

    return acked_artifacts, unacked_artifacts


def plot_comms_performance(
    artifacts_pushed,
    artifacts_received,
    artifacts_missed,
    acked_artifacts,
    unacked_artifacts,
    base_station_received_fname,
):
    """
	Inputs:
		artifacts_pushed: off of robot
		artifacts_received: by base station. type is artifact transmitted off of robot!
		artifacts_missed: never got to robot
		acked_artifacts: acked by base station
		unacked_artifacts: not acked by base station

	Spatial plot (in /map frame) with each position being a different color for:
		- Artifact pushed by robot
		- Artifact received by base station
		- Artifact acked by base station

		- Add warning that we don't know what artifact acks were received by robot

		- Add legend with colors and 
			- Total # transmissions/received
			- Total # of unique detections transmitted/received/acked
	"""

    if len(artifacts_pushed) != (
        len(artifacts_received) + len(artifacts_missed)
    ):
        print("Some pushed artifacts not accounted for!")
        pdb.set_trace()

    # read in the artifacts in the global frame
    report_id_to_coord_dict = {}
    with open(base_station_received_fname) as csv_file:
        reader = csv.reader(csv_file, delimiter=",")
        for i, line in enumerate(reader):
            # skip headers
            if i == 0:
                continue
            report_id_to_coord_dict[float(line[4])] = [
                float(line[1]),
                float(line[2]),
                float(line[3]),
            ]

    # must do this by report_id because otherwise an artifact could have been transmitted multipel
    # times and not get an ack many times and then finally got one

    # acked implies pushed/received
    # received implies pushed

    acked_ids, received_ids, pushed_ids = [], [], []
    acked_poses, received_poses, pushed_poses = [], [], []

    # go through it in reverse to get the last artifact
    for acked_art in acked_artifacts[::-1]:
        if acked_art[5] not in acked_ids:
            acked_ids.append(acked_art[5])
            if acked_art[5] not in report_id_to_coord_dict:
                acked_poses.append([acked_art[1], acked_art[2]])
            else:  # put into global frame. important for uav
                acked_poses.append(
                    [
                        report_id_to_coord_dict[acked_art[5]][0],
                        report_id_to_coord_dict[acked_art[5]][1],
                    ]
                )

    for received_art in artifacts_received[::-1]:
        if (received_art[4] not in acked_ids) and (
            received_art[4] not in received_ids
        ):
            received_ids.append(received_art[4])
            received_poses.append([received_art[1], received_art[2]])

    for pushed_art in artifacts_pushed[::-1]:
        if (
            (pushed_art[4] not in received_ids)
            and (pushed_art[4] not in acked_ids)
            and (pushed_art[4] not in pushed_ids)
        ):

            pushed_ids.append(pushed_art[4])
            pushed_poses.append([pushed_art[1], pushed_art[2]])

    total_artifacts_transmitted = (
        len(pushed_ids) + len(received_ids) + len(acked_ids)
    )
    num_artifacts_not_received = len(pushed_ids)
    num_artifacts_not_acked = len(received_ids)

    print("Total artifacts transmitted:", total_artifacts_transmitted)
    print(
        "Num artifacts not received by basestation:", num_artifacts_not_received
    )
    print(
        "Num artifacts received but not acked by base station:",
        num_artifacts_not_acked,
    )
    print(
        "Due to recording issues we dont know what artifacts were acked by the robot"
    )

    pdb.set_trace()

    pushed_poses = np.array(pushed_poses)
    received_poses = np.array(received_poses)
    acked_poses = np.array(acked_poses)

    # plot stuff
    if len(pushed_poses) > 0:
        plt.plot(pushed_poses[:, 0], pushed_poses[:, 1], "r.")
    if len(received_poses) > 0:
        plt.plot(received_poses[:, 0], received_poses[:, 1], "y.")
    if len(acked_poses) > 0:
        plt.plot(acked_poses[:, 0], acked_poses[:, 1], "g.")

    plt.show()


def get_total_arts_pushed(robot_transmitted_fname):
    """
	Just get the number of unique artifacts that were tried to
	push through, to get an idea of how many false positives we had
	"""

    objdet_artifacts = []
    objdet_artifacts = read_comms_csv(
        robot_transmitted_fname, skip_valid_status=True
    )

    obj_ids, unique_objs = [], []
    for obj_art in objdet_artifacts:
        (
            obj_stamp,
            obj_x,
            obj_y,
            obj_z,
            obj_report_id,
            obj_class_id,
            obj_arr_stamp,
        ) = obj_art

        if obj_report_id not in obj_ids:
            obj_ids.append(obj_report_id)
            unique_objs.append(obj_art)

    print("Number of unique artfacts pushed:", len(obj_ids))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--robot_bagfile_folder",
        default="/BackupSSD/urban_circuit_bags/2020-02-25/R3/2020-02-25",
        help="Folder containing objdet bagfiles for a specific run",
    )
    parser.add_argument("--vehicle", default="ugv3", help="e.g. uav1 or ugv3")
    parser.add_argument(
        "--base_station_bagfile_folder",
        default="/BackupSSD/urban_circuit_bags/2020-02-25/basestation/bags",
        help="Folder containing base station bag files for a specific run",
    )

    CMDLINE_ARGS = parser.parse_args()

    print(
        "\nAssuming no other csv files from analyze comms are in the target directory!!"
    )

    # top-level folder containing objdet or auto files
    # for ugv and uav respectively
    robot_bagfile_folder = ""
    robot_transmitted_fname = (
        "./" + CMDLINE_ARGS.vehicle + "_robot_comms_transmitted.csv"
    )

    # top-level folder containing base_station bag files
    base_station_received_fname = (
        "./" + CMDLINE_ARGS.vehicle + "_basestation_comms_received.csv"
    )
    base_station_acked_fname = (
        "./" + CMDLINE_ARGS.vehicle + "_basestation_comms_acked.csv"
    )
    base_station_received_compressed_fname = (
        "./"
        + CMDLINE_ARGS.vehicle
        + "_basestation_comms_received_compressed.csv"
    )
    auto_topics = [
        "/" + CMDLINE_ARGS.vehicle + "/artifact_localizations_compressed"
    ]

    if CMDLINE_ARGS.vehicle.find("gv") != -1:
        analyze_objdet(
            CMDLINE_ARGS.robot_bagfile_folder, robot_transmitted_fname
        )
    elif CMDLINE_ARGS.vehicle.find("av") != -1:
        analyze_auto(
            CMDLINE_ARGS.robot_bagfile_folder,
            robot_transmitted_fname,
            auto_topics,
        )
    else:
        print("Dont know this type of vehicle:", CMDLINE_ARGS.vehicle)
    analyze_basestation(
        CMDLINE_ARGS.base_station_bagfile_folder,
        base_station_received_fname,
        base_station_acked_fname,
        base_station_received_compressed_fname,
        CMDLINE_ARGS.vehicle,
    )

    (
        artifacts_pushed,
        artifacts_received,
        artifacts_missed,
    ) = compare_objdet_basestation(
        robot_transmitted_fname, base_station_received_fname
    )
    acked_artifacts, unacked_artifacts = compare_base_station_robot(
        base_station_received_compressed_fname, base_station_acked_fname
    )

    plot_comms_performance(
        artifacts_pushed,
        artifacts_received,
        artifacts_missed,
        acked_artifacts,
        unacked_artifacts,
        base_station_received_fname,
    )

    get_total_arts_pushed(robot_transmitted_fname)
