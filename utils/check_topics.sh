# Analyze a large amount of bag files and quickly determine what topics were present/missing durign the run
# CLI argument 1 is the folder of bag files for a specific run
# Contact: Bob DeBortoli (debortor@oregonstate.edu)
for file in $1*.bag
do
  echo "$file"
  rosbag info "$file" | grep -E '(/rs_front/color/image|/rs_back/color/image|/rs_right/color/image|/rs_left/color/image|/rs_up/color/image|/rs_down/color/image|/artifact_localizations|gas_info|wifi_info|/thermal)'
done
