function lut_adj_img = lut_process_img_fixed_curve(img)
    imgg = im2gray(img);
    histg = imhist(imgg);
    
    %% load histogram
    data_struct = load('fixed_lut_curve.mat');
    cumsumhistnorm = data_struct.cumsumhistnorm
    
    %% convert with LUT
    imgd = im2double(img);
    [m,n,o] = size(img);        % get size of input image
    img_lut = zeros(m,n,o);     % asign output image

    a = linspace(0,1,size(histg,1));% create 1D scale size of color LUT

    img_lut(:,:,1) = interp1(a,cumsumhistnorm,imgd(:,:,1));
    img_lut(:,:,2) = interp1(a,cumsumhistnorm,imgd(:,:,2));
    img_lut(:,:,3) = interp1(a,cumsumhistnorm,imgd(:,:,3));


    lut_adj_img = uint8(256 .* img_lut);       % convert type to uint8

end