#!/usr/bin/env python3

from __future__ import print_function
import rosbag
import argparse
import pprint
import sys

parser = argparse.ArgumentParser(description="Get message sizes for all topics")
parser.add_argument("bagfile", help="Bagfile(s) to use", nargs="+")
args = parser.parse_args()


class TopicSize(object):
    def __init__(self):
        self.count = 0
        self.size = 0


def parse_sizes(bag, topic_sizes):
    messages = bag.read_messages(raw=True)
    for (topic, raw, timestamp) in messages:
        if topic not in topic_sizes:
            topic_sizes[topic] = TopicSize()

        topic_sizes[topic].count += 1
        topic_sizes[topic].size += len(raw[1])


def get_average_sizes(topic_sizes):
    sizes = dict()
    for key in topic_sizes:
        sizes[key] = topic_sizes[key].size

    return sizes


def print_sizes(sizes):
    sorted_sizes = sorted(sizes.items(), key=lambda x: x[1], reverse=True)
    for topic, size in sorted_sizes:
        print("%60s %15d bytes" % (topic, size))


def main():
    topic_sizes = dict()
    for bag_name in sorted(args.bagfile):
        print("Loading bag ... ", end="")
        sys.stdout.flush()
        bag = rosbag.Bag(bag_name)
        print("Loaded!")

        parse_sizes(bag, topic_sizes)

    sizes = get_average_sizes(topic_sizes)
    print_sizes(sizes)


if __name__ == "__main__":
    main()
