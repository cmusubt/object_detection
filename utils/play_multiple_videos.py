#!/usr/bin/env python3

import math
import pathlib
import argparse
import cv2
import numpy as np


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("video_paths", nargs="+")
    parser.add_argument(
        "-s",
        "--window_scale",
        type=float,
        default=1.0,
        help="Scale factor to apply to final window",
    )
    parser.add_argument(
        "--tile_width",
        type=int,
        default=0,
        help="Number of windows to tile horizontally",
    )
    parser.add_argument(
        "--tile_height",
        type=int,
        default=0,
        help="Number of windows to tile vertically",
    )
    args = parser.parse_args()

    for path in args.video_paths:
        if not pathlib.Path(path).exists():
            print("Could not find file %s" % path)
            return

    tile_width = args.tile_width
    if tile_width == 0:
        tile_width = math.ceil(math.sqrt(len(args.video_paths)))
    tile_height = args.tile_height
    if tile_height == 0:
        tile_height = math.ceil(len(args.video_paths) / tile_width)

    caps = [cv2.VideoCapture(path) for path in args.video_paths]
    valid_count = len(caps)
    image_width = int(caps[0].get(cv2.CAP_PROP_FRAME_WIDTH))
    image_height = int(caps[0].get(cv2.CAP_PROP_FRAME_HEIGHT))
    blank_image = np.zeros((image_height, image_width, 3), dtype=np.uint8)
    for cap in caps:
        assert int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)) == image_width
        assert int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)) == image_height

    caps.extend([None] * (tile_width * tile_height - len(caps)))

    while valid_count > 0:
        frames = []
        for i, cap in enumerate(caps):
            if cap is None:
                frames.append(blank_image)
            else:
                ret, frame = cap.read()
                if not ret:
                    cap.release()
                    caps[i] = None
                    valid_count -= 1
                    frames.append(blank_image)
                else:
                    text = "/".join(args.video_paths[i].split("/")[-5:-3])
                    shadow_text(
                        frame, text, (10, 20), font_scale=0.5, font_weight=1
                    )
                    frames.append(frame)

        tiled = tile_frames(frames, tile_width, tile_height)
        cv2.imshow("Tiled", tiled)
        key = cv2.waitKey(0) & 0xFF

        if key == ord("q"):
            break

    for cap in caps:
        if cap is not None:
            cap.release()


def tile_frames(frames, tile_width, tile_height):
    assert len(frames) == tile_width * tile_height
    rows = []
    for i in range(tile_height):
        rows.append(np.hstack(frames[i * tile_width : (i + 1) * tile_width]))
    return np.vstack(rows)


def shadow_text(frame, text, loc, font_scale=0.5, font_weight=2):
    shadow_color = (0, 0, 0)
    shadow_loc = tuple(np.array(loc) + 2)
    font_color = (255, 255, 255)
    font_type = cv2.FONT_HERSHEY_SIMPLEX

    cv2.putText(
        frame,
        text,
        shadow_loc,
        font_type,
        font_scale,
        shadow_color,
        font_weight,
    )
    cv2.putText(
        frame, text, loc, font_type, font_scale, font_color, font_weight
    )


if __name__ == "__main__":
    main()
