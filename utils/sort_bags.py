#!/usr/bin/env python3
from __future__ import print_function
import argparse
import os
import pprint

parser = argparse.ArgumentParser(description="Sort split bags into folders")
parser.add_argument("folder", help="Flat folder containing all bag files")
args = parser.parse_args()


def main():
    assert os.path.isdir(args.folder)
    files = os.listdir(args.folder)
    ordered = sorted(files)

    grouped = []
    expected = None
    for name in ordered:
        try:
            index = int(name.split("_")[-1].split(".")[0])
        except:
            print("File %s doesn't belong in the folder!" % name)
            continue

        if index == expected:
            expected += 1
        else:
            expected = index + 1
            grouped.append([])

        grouped[-1].append(name)

    #  pprint.pprint(grouped)

    for i, group in enumerate(grouped):
        # A fixed number of leading zeros is bad practice, but we can't even fit
        # 100 runs on the disk ...
        run_folder = os.path.join(args.folder, "run_%02d" % i)
        os.mkdir(run_folder)

        for name in group:
            old = os.path.join(args.folder, name)
            new = os.path.join(run_folder, name)
            os.rename(old, new)


if __name__ == "__main__":
    main()
