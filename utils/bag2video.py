#!/usr/bin/env python2
# Inspired by this: https://github.com/OSUrobotics/bag2video

from __future__ import print_function
import cv_bridge
import rosbag
import rospy
import argparse
import os
import cv2
import numpy as np

parser = argparse.ArgumentParser(description="Pull video out of bag file")
parser.add_argument("topic", help="Topic to extract video from")
parser.add_argument("bagfile", help="Bagfile(s) to use", nargs="+")
parser.add_argument(
    "--combine",
    action="store_true",
    default=False,
    help="Combine bag files together (in order)",
)
parser.add_argument(
    "--sort", type=bool, default=True, help="Sort input filenames"
)
parser.add_argument(
    "--rotate",
    default=False,
    action="store_true",
    help="Rotate output video 180 degrees",
)
args = parser.parse_args()


def get_framerate(bag):
    messages = bag.read_messages(topics=[args.topic])
    times = []
    for i, (topic, msg, timestamp) in enumerate(messages):
        if i > 100:
            break
        times.append(timestamp)

    differences = [t2 - t1 for (t1, t2) in zip(times, times[1:])]
    diffs_nsec = [d.to_nsec() for d in differences]
    average_nsec = sum(diffs_nsec) / len(diffs_nsec)
    frame_rate = 1e9 / average_nsec
    return frame_rate
    #  return int(frame_rate)


def get_video_name(bag_name, topic, combined):
    root, ext = os.path.splitext(bag_name)
    return (
        root
        + topic.replace("/", "_")
        + ("_combined" if combined else "")
        + ".mkv"
    )


def get_video_size(bag):
    messages = bag.read_messages(topics=[args.topic])
    topic, msg, timestamp = next(messages)
    return (msg.width, msg.height)


def write_video(writer, bag):
    bridge = cv_bridge.CvBridge()
    messages = bag.read_messages(topics=[args.topic])
    for i, (topic, msg, timestamp) in enumerate(messages):
        print("\r[%s] Writing frame %s" % (bag.filename, i), end="")
        img = np.asarray(bridge.imgmsg_to_cv2(msg, "bgr8"))

        if args.rotate:
            img = img[::-1, ::-1, :]

        writer.write(img)
    print()


def main():

    bags = args.bagfile
    if args.sort:
        bags = sorted(bags)

    if args.combine:
        initialized = False
        for bag_name in bags:
            try:
                bag = rosbag.Bag(bag_name)
                if not initialized:
                    framerate = get_framerate(bag)
                    video_name = get_video_name(
                        bag_name, args.topic, args.combine
                    )
                    width, height = get_video_size(bag)
                    writer = cv2.VideoWriter(
                        video_name,
                        cv2.VideoWriter_fourcc(*"MJPG"),
                        framerate,
                        (width, height),
                    )
                    initialized = True
                write_video(writer, bag)
            except Exception as e:
                print("[%s] %s" % (bag_name, e))
                print("Are you sure the topic exists in the bag file?")
                continue

        if initialized:
            writer.release()
        return

    for bag_name in bags:
        try:
            bag = rosbag.Bag(bag_name)
            framerate = get_framerate(bag)
            video_name = get_video_name(bag_name, args.topic, args.combine)
            width, height = get_video_size(bag)
            writer = cv2.VideoWriter(
                video_name,
                cv2.VideoWriter_fourcc(*"MJPG"),
                framerate,
                (width, height),
            )
            write_video(writer, bag)
            writer.release()
        except Exception as e:
            print("[%s] %s", (bag_name, e))
            continue


if __name__ == "__main__":
    main()
