#!/usr/bin/env python3

from __future__ import print_function
import rosbag
import argparse
import pprint
import sys

parser = argparse.ArgumentParser(description="Get framerates for all topics")
parser.add_argument("bagfile", help="Bagfile(s) to use", nargs="+")
args = parser.parse_args()


def parse_timestamps(bag, timestamps):
    messages = bag.read_messages(raw=True)
    for (topic, _, timestamp) in messages:
        if topic in timestamps:
            timestamps[topic].append(timestamp)
        else:
            timestamps[topic] = []

    return timestamps


def diff_timestamps(timestamps):
    diffs = dict()
    for (topic, times) in timestamps.items():
        diffs[topic] = [(t2 - t1).to_nsec() for t1, t2 in zip(times, times[1:])]

    return diffs


def get_rate_from_diffs(diffs):
    rates = dict()
    for key in diffs:
        diff = diffs[key]
        if len(diff) != 0:
            average_diff_nsec = sum(diff) / len(diff)
            average_rate = 1e9 / average_diff_nsec
            rates[key] = average_rate
        else:
            rates[key] = None

    return rates


def main():
    timestamps = dict()
    for bag_name in sorted(args.bagfile):
        print("Loading bag ... ", end="")
        sys.stdout.flush()
        bag = rosbag.Bag(bag_name)
        print("Loaded!")

        parse_timestamps(bag, timestamps)

    diffs = diff_timestamps(timestamps)
    rates = get_rate_from_diffs(diffs)
    pprint.pprint(rates)


if __name__ == "__main__":
    main()
