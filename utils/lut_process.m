function [] = lut_process(folder_path)

%%%%%%%%%%%%%%%%%%%%%%%%
% WARNING: THIS FUNCTION IS DESTRUTIVE(I.E. FILES WILL BE CHANGED IN-PLACE)
%%%%%%%%%%%%%%%%%%%%%%%%

imagefiles = dir(fullfile(folder_path, '**/*.png'));

for k=1:numel(imagefiles)
    imgfname = strcat(imagefiles(k).folder, '/', imagefiles(k).name);
    img = imread(imgfname);
    lut_adj_img = lut_process_img_fixed_curve(img);
    newimgfname = imgfname;%strcat(imgfname(1:strfind(imgfname, '.')-1), '_newluv.png');
    imwrite(lut_adj_img, newimgfname);
end