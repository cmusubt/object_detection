# Takes a specified topic and bagfile(s) and plots the data

# sample usage:
# python plot_data.py --bagfile /media/bob/BackupHDD/bob/Spring2020/subt_logs/tegrastats_issue/*.bag \
#                     --topic /tegrastats \
#                     --field_names thermal_temp gpu_percentage gpu_temp 

import rosbag
import rospy
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import argparse
import pdb

parser = argparse.ArgumentParser(description="Pull video out of bag file")
parser.add_argument("--topic", help="Topic to extract video from")
parser.add_argument("--field_names", help="Field names to track", nargs="+")
parser.add_argument("--bagfile", help="Bagfile(s) to use", nargs="+")
parser.add_argument(
    "--sort", type=bool, default=True, help="Sort input filenames"
)
args = parser.parse_args()

def plot_topic(bag_fnames, topic, field_names):
    '''
    '''
    times = []
    val_dict = {}
    for field in field_names:
        val_dict[field] = []

    for bag_fname in bag_fnames:
        bag = rosbag.Bag(bag_fname)
        for topic, msg, t in bag.read_messages(topics=[topic]):
            times.append(t.to_sec())
            for field in field_names:
                val_dict[field].append(getattr(msg, field))


    for field in field_names:
        plt.clf()
        plt.plot(times, val_dict[field])
        plt.title(field)
        plt.xlabel('time (secs)')
        plt.ylabel('value')

        if len(bag_fnames) > 0:
            plot_fname = bag_fname[:bag_fname.rindex('/')] + topic + '_' + field + '.png'
        else:
            plot_fname = bag_fname[:-4] + topic + '_' + field + '.png'

        plt.savefig(plot_fname)


if __name__ == "__main__":
    bags = args.bagfile
    if args.sort:
        bags = sorted(bags)

    plot_topic(bags, args.topic, args.field_names)

    