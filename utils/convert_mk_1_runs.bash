#!/bin/bash

dir=$1

declare -a topics=(
    "/rs_front/color/image"
    "/rs_back/color/image"
    "/rs_left/color/image"
    "/rs_right/color/image"
    "/thermal_left/image_rect"
    "/thermal_right/image_rect"
)

for f in $(find $dir -iname "run_*" -type d); do
    for (( i=0; i<${#topics[@]}; i++)); do
        ./bag2video.py --combine ${topics[i]} \
            $(find $f -iname "*.bag" | sort | xargs echo) &
    done

    wait
done
