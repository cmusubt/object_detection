"""
Converts raw robot data to videos we can use for analysis.
Provide azure storage explorer links and this script
will automatically detect specific runs, etc. and output
videos accordingly

TO BE RUN FROM WHEREVER ./AZCOPY IS INSTALLED, 
E.G. /home/debortor/Downloads/azcopy_linux_amd64_10.4.3

Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""
import json
import pdb
import datetime
import time
import numpy as np
import os

def azuze_output_to_fnames(robot_data, min_run_length, local_data_folder, skip_actual_copy=False):
	'''
	min_run_length is in mins
	'''

	if skip_actual_copy == True:
		print("SKIPPING ACTUAL COPYING")
	
	# folder name
	json_data = json.loads(robot_data)['CloudHub.Azure.Storage.Blobs']
	folder_name = 'https://subtdatasets.blob.core.windows.net/' + json_data['containerName'] + '/' + json_data['sourceFolder']
	
	# get all fnames
	raw_fname_list = json_data['items']
	clean_fname_list = []
	clean_timestamp_list = []
	for fname in raw_fname_list:
		bag_fname = fname['relativePath']
		clean_fname_list.append(bag_fname)
		timestamp = bagfname2timestamp(bag_fname)
		clean_timestamp_list.append(timestamp)

	# condense by min/max times (todo@debortor)
	# break into runs
	run_list = []
	for i, timestamp in enumerate(clean_timestamp_list):
		if len(run_list) == 0:
			run_list.append([[i, timestamp]])
			continue
		# check to see if this timestamp belongs to a run already recognized
		run_list = update_run_list(run_list, timestamp, i)

	# remove fnames that are part of a < x min run
	np_run_list = []
	for run in run_list:
		np_run_list.append(np.array(run))
	run_list = np_run_list
	run_list = cull_runs(run_list, min_run_length)
	
	# break into run fnames
	run_fnames = []
	for run in run_list:
		run_fnames.append([])
		for ind in run[:,0]:
			run_fnames[-1].append(folder_name + clean_fname_list[int(ind)])

	# make the local folder to be copied to
	run_dirs = make_local_dirs(local_data_folder, json_data, run_list)

	# actually copy to local machine
	if not skip_actual_copy:
		copy_runs_to_local(run_fnames, run_dirs)
	
	return run_dirs

def update_run_list(run_list, timestamp, i):
	'''
	Puts timestamp into specific run or makes a new one
	'''
	clustering_thresh = 3 * 60 # 3 mins
	run_id = -1
	for k in range(len(run_list)):
		beg_time = run_list[k][0][1]
		end_time = run_list[k][-1][1]
		if abs(timestamp - beg_time) < clustering_thresh or \
		   abs(timestamp - end_time) < clustering_thresh or \
		   ((timestamp > beg_time) and (timestamp < end_time)):
		   # belongs to this run
		   run_list[k].append([i, timestamp])
		   return run_list
	# matching run not found, make new run
	run_list.append([[i, timestamp]])
	return run_list

def bagfname2timestamp(bag_fname):
	just_time = bag_fname[bag_fname.find('2021'):bag_fname.rindex('_')]
	element = datetime.datetime.strptime(just_time,"%Y-%m-%d-%H-%M-%S")  
	tuple = element.timetuple()
	timestamp = time.mktime(tuple)	
	return timestamp

def cull_runs(run_list, min_run_length):
	'''
	Remove runs that dont meet certain criteria, like a minimum time
	min_run_length is in mins
	'''
	keep_inds = []
	for i, run in enumerate(run_list):
		max_time = run[:,1].max()
		min_time = run[:,1].min()
		if (max_time - min_time) > (min_run_length * 60):
			keep_inds.append(i)		
	culled_list = []
	for ind in keep_inds:
		culled_list.append(run_list[ind])
	return culled_list

def make_local_dirs(local_data_folder, json_data, run_list):
	'''
	'''
	# date dir
	date_dir = local_data_folder + '/' + json_data['containerName']
	if not os.path.isdir(date_dir):
		os.mkdir(date_dir)
	# robot dir
	robot_dir = date_dir + '/' + json_data['sourceFolder'][:json_data['sourceFolder'].find('/')]
	if not os.path.isdir(robot_dir):
		os.mkdir(robot_dir)
	# run dirs
	run_dirs = []
	for i in range(len(run_list)):
		run_dir = robot_dir + '/run_0' + str(i)
		if not os.path.isdir(run_dir):
			os.mkdir(run_dir)
		run_dirs.append(run_dir)
	return run_dirs

def copy_runs_to_local(run_fnames, run_dirs):
	'''
	'''
	if (len(run_fnames) != len(run_dirs)):
		print('Number of runs for fnames vs. directories made do not match up!')
		pdb.set_trace()
	for i in range(len(run_fnames)):
		for k in range(len(run_fnames[i])):
			os.system('./azcopy copy ' + str(run_fnames[i][k]) + ' ' + str(run_dirs[i]))
	
def generate_raw_vids(run_dirs, local_objdet_folder):
	'''
	'''
	img_topic_names =  ['/camera/image_raw',
					    '/rs_down/color/image',
						'/rs_up/color/image',
						'/rs_front/color/image',
						'/rs_left/color/image',
						'/rs_right/color/image',
						'/rs_back/color/image',						
						'/device_0/sensor_1/Color_0/image/data']
	for run_dir in run_dirs:
		for img_topic in img_topic_names:
			os.system('python3 '+ local_objdet_folder +'/utils/bag2video.py '+ img_topic + ' '+ run_dir + '/*.bag --combine')

def generate_cnn_output(run_dirs, local_objdet_folder):
	'''
	'''
	for run_dir in run_dirs:
		os.system('python3 '+ local_objdet_folder +'/inference/scripts/offline_image_det_vis.py '+ ' '+ run_dir + '/*.bag --combine')


if __name__ == "__main__":
	#azure_outputs = ['{"CloudHub.Azure.Storage.Blobs":{"accountUri":"https://subtdatasets.blob.core.windows.net/","sasToken":"sv=2020-04-08&st=2021-06-01T17%3A47%3A36Z&se=2021-06-08T17%3A47%3A36Z&sr=c&sp=rl&sig=YZPIBldhNNVr49vcTSvNLlkv070yuoBbArIyAi2HWPk%3D","containerName":"2021-05-03","sourceFolder":"DS2/bags/","items":[{"relativePath":"auto_flight_2021-05-03-10-57-43_1.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-10-58-40_2.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-10-59-37_3.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-00-33_4.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-01-27_5.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-02-22_6.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-03-18_7.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-04-13_8.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-05-08_9.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-06-03_10.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-06-57_11.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-37-32_1.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-38-29_2.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-39-25_3.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-40-20_4.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-41-15_5.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-42-11_6.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-43-06_7.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-44-00_8.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-44-54_9.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-45-48_10.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-46-43_11.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-11-47-38_12.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-12-11-51_1.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-12-12-49_2.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-12-13-44_3.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-12-14-39_4.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-12-15-34_5.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-03-12-16-31_6.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-10-57-43_2.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-10-58-19_3.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-10-58-55_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-10-59-32_5.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-00-08_6.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-00-44_7.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-01-20_8.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-01-57_9.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-02-33_10.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-03-09_11.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-03-45_12.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-04-22_13.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-04-58_14.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-05-34_15.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-06-10_16.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-06-47_17.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-07-23_18.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-37-32_2.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-38-09_3.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-38-45_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-39-21_5.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-39-57_6.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-40-33_7.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-41-10_8.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-41-46_9.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-42-22_10.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-42-58_11.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-43-35_12.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-44-11_13.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-44-47_14.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-45-23_15.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-46-00_16.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-46-36_17.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-47-12_18.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-11-47-48_19.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-12-11-51_2.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-12-12-27_3.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-12-13-04_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-12-13-40_5.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-12-14-16_6.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-12-14-52_7.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-12-15-29_8.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-12-16-05_9.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-03-12-16-41_10.bag","snapshot":""}],"service":"gen2"}}']
	#azure_outputs = ['{"CloudHub.Azure.Storage.Blobs":{"accountUri":"https://subtdatasets.blob.core.windows.net/","sasToken":"sv=2020-04-08&st=2021-06-02T17%3A43%3A25Z&se=2021-06-09T17%3A43%3A25Z&sr=c&sp=rl&sig=MHQKspU%2BBsakucnqQvcYZ239HFFyVQ%2BGGreFpd%2BA8sY%3D","containerName":"2021-05-27","sourceFolder":"UGV1/xavier/","items":[{"relativePath":"objdet_2021-05-27-11-26-29_0.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-11-50-44_0.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-11-52-10_1.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-11-53-31_2.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-11-56-12_0.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-12-03-38_0.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-05-04_1.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-06-26_2.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-07-47_3.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-09-07_4.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-10-27_5.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-12-13-42_0.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-15-03_1.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-16-22_2.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-17-42_3.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-19-02_4.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-20-22_5.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-21-42_6.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-23-01_7.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-24-22_8.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-25-41_9.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-27-01_10.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-28-22_11.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-29-41_12.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-31-02_13.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-32-22_14.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-33-41_15.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-35-02_16.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-36-23_17.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-12-40-21_0.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-41-42_1.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-43-00_2.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-44-19_3.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-45-39_4.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-47-00_5.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-48-20_6.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-49-41_7.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-51-02_8.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-52-23_9.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-12-53-44_10.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-13-02-44_0.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-04-04_1.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-05-22_2.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-06-40_3.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-07-58_4.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-09-16_5.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-10-34_6.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-11-52_7.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-13-11_8.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-14-32_9.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-15-53_10.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-17-12_11.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-18-31_12.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-19-51_13.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-21-11_14.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-22-31_15.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-23-52_16.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-25-14_17.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-26-36_18.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-27-59_19.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-29-22_20.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-30-46_21.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-32-11_22.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-33-37_23.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-35-05_24.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-36-35_25.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-13-38-07_26.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-14-03-36_0.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-04-57_1.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-06-16_2.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-07-36_3.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-08-55_4.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-10-15_5.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-11-35_6.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-12-55_7.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-14-17_8.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-15-40_9.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-17-04_10.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-18-26_11.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-19-48_12.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-14-31-35_0.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-14-49-33_0.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-50-53_1.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-52-11_2.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-53-31_3.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-14-54-51_0.bag","snapshot":""},{"relativePath":"objdet_2021-05-27-14-56-11_1.bag.active","snapshot":""},{"relativePath":"objdet_2021-05-27-14-57-20_0.bag","snapshot":""}],"service":"gen2"}}']
	#azure_outputs = ['{"CloudHub.Azure.Storage.Blobs":{"accountUri":"https://subtdatasets.blob.core.windows.net/","sasToken":"sv=2020-04-08&st=2021-06-02T21%3A58%3A46Z&se=2021-06-09T21%3A58%3A46Z&sr=c&sp=rl&sig=3glkV6jC%2BUc5MNv66k239Zi9qMyqNtq%2FX3WcSOL0IXs%3D","containerName":"2021-05-25","sourceFolder":"DS2/bags/","items":[{"relativePath":"camera_flight_2021-05-25-12-45-53_3.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-12-46-29_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-12-47-06_5.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-12-47-42_6.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-42-51_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-43-27_5.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-44-04_6.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-44-40_7.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-45-16_8.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-45-52_9.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-46-29_10.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-47-05_11.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-47-41_12.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-48-17_13.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-48-54_14.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-49-30_15.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-50-06_16.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-13-50-48_17.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-15-39-58_3.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-15-40-34_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-15-41-10_5.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-25-15-41-46_6.bag","snapshot":""}],"service":"gen2"}}']
	azure_outputs = ['{"CloudHub.Azure.Storage.Blobs":{"accountUri":"https://subtdatasets.blob.core.windows.net/","sasToken":"sv=2020-04-08&st=2021-06-07T20%3A26%3A55Z&se=2021-06-14T20%3A26%3A55Z&sr=c&sp=rl&sig=xISx4HFdNhDFC33STzNkdhWWLIGBwdkgbf2Hnm6FY3M%3D","containerName":"2021-05-27","sourceFolder":"DS3/bags/","items":[{"relativePath":"auto_flight_2021-05-27-12-46-21_1.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-12-47-59_2.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-12-49-31_3.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-12-50-58_4.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-12-52-23_5.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-14-15-57_1.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-14-17-40_2.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-17-47_1.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-19-14_2.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-20-29_3.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-21-33_4.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-22-35_5.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-23-35_6.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-24-35_7.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-51-22_1.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-52-55_2.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-54-14_3.bag","snapshot":""},{"relativePath":"auto_flight_2021-05-27-15-55-30_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-12-46-21_1.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-12-47-17_2.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-12-48-12_3.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-12-49-08_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-12-50-04_5.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-12-51-00_6.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-12-51-56_7.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-12-52-52_8.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-14-15-57_1.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-14-16-53_2.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-14-17-49_3.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-17-47_1.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-18-43_2.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-19-38_3.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-20-34_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-21-30_5.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-22-26_6.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-23-22_7.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-24-17_8.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-25-13_9.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-51-22_1.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-52-18_2.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-53-14_3.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-54-09_4.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-55-05_5.bag","snapshot":""},{"relativePath":"camera_flight_2021-05-27-15-56-01_6.bag","snapshot":""}],"service":"gen2"}}']
	local_data_folder = '/home/debortor/data'
	local_objdet_folder = '/home/debortor/workspaces/objdet/src/object_detection'
	min_run_length = 5
	
	for robot_data in azure_outputs:
		run_dirs = azuze_output_to_fnames(robot_data, min_run_length, local_data_folder)

		# generate raw videos (can be used for labelling)
		generate_raw_vids(run_dirs, local_objdet_folder)

		# generate cnn output vids
		os.system('source ' + local_objdet_folder + '../../devel/setup.bash')
		generate_cnn_output(run_dirs, local_objdet_folder)