#!/bin/bash

# Script to get tensorflow checkpoint files from remote server and put them in the right place locally

# Passed arguments are:
	# Name/ip of remote server + where its stored
	# The local folder where tf ckpts are stored 
	# The checkpoint folders for the remote server

#e.g. ./scp_ckpts.sh debortor@23.96.121.112:/home/debortor/Documents/checkpoints/ /media/bob/BackupHDD/bob/Spring2020/subt_logs/checkpoints rgb_positive_inception_640_360

for exp_folder in "$@"; do

		# skip the remote name and local folder
		if [ "$exp_folder" != "$1" ] && [ "$exp_folder" != "$2" ]; then
		    if [ ! -d $2/$exp_folder/train ]; then
  				mkdir $2/$exp_folder/train
            fi

            scp  "$1$exp_folder/*/*/model.ckpt-*" $2/$exp_folder/train
            scp  "$1$exp_folder/*/*/graph.pbtxt" $2/$exp_folder/train
            scp  "$1$exp_folder/*/*/checkpoint" $2/$exp_folder/train
		fi
done
