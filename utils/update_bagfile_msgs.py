#!/usr/bin/env python3

"""
Very simple script.
Goes through bag files updates message types so if you want to
play a bagfile with an old message md5 it works

objdet_msgs/ArtifactLocalization.msg definition changed on Dec 12th, 2019.
basetstaion_msgs/WifiDetection.msg definition changed on Dec 13th, 2019.
PR 91: bob-cell-phone-visualization
Approximate commit: a225a3be929dcc7f58e80677a370176200abc0b1 


Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""
import rosbag
import glob
from objdet_msgs.msg import ArtifactLocalization, ArtifactLocalizationArray
from geometry_msgs.msg import Point


def convert_localization_msg(old_msg):
    """
    Convert artifact localization array message
    """
    new_msg = ArtifactLocalizationArray()
    new_msg.header = old_msg.header

    for old_localization in old_msg.localizations:

        new_localization = ArtifactLocalization()

        new_localization.stamp = old_localization.stamp
        new_localization.valid = old_localization.valid

        new_localization.x = old_localization.x
        new_localization.y = old_localization.y
        new_localization.z = old_localization.z
        new_localization.confidence = old_localization.confidence
        new_localization.class_id = old_localization.class_id
        new_localization.report_id = old_localization.report_id

        new_localization.images = old_localization.images
        new_localization.cloud = old_localization.cloud

        new_msg.localizations.append(new_localization)

    return new_msg


if __name__ == "__main__":
    top_level_folder = "/media/dstest/XavierSSD500/2019-08-18-old"
    topic_name = "/artifact_localizations"

    print("\n\nEnsure new messages have been built!\n\n")

    for bagfile in sorted(glob.glob(top_level_folder + "/*.bag")):

        bag = rosbag.Bag(bagfile)
        new_bag = rosbag.Bag(bagfile[:-4] + "_new.bag", "w")

        for topic, msg, t in bag.read_messages():

            if topic == topic_name:
                # this line will call a custom function every time
                new_msg = convert_localization_msg(msg)
                new_bag.write(topic, new_msg)
            else:
                new_bag.write(topic, msg)

        bag.close()
        new_bag.close()
