#!/usr/bin/env python3

'''
Without a video, just drawing the bounding boxes on images and 
writing those combined images out to a folder for manual verification
'''
from datetime import datetime
import cv2
import drawing_utils
import bbox_writer
import pdb
import os

def get_fnames(dataset_path):
	'''
	'''
	fnames = []

	for root, dirs, files in os.walk(dataset_path):
		for file in files:
			if file.endswith("txt"):
				file_path = os.path.join(root, file)
				fnames.append(file_path)

	return fnames

def write_bboxes(fnames, output_folder):
	'''
	Put the bounding boxes on images and write to folder
	'''
	for fname in fnames:
		# load img
		img = cv2.imread(fname[:-4] + '.png')

		# load labels
		bboxes, classes = bbox_writer.read_bboxes(fname)

		# write label on img
		drawing_utils.draw_bboxes(img, bboxes, classes)

		# write-out img
		cv2.imwrite(output_folder + '/' +datetime.now().strftime('%Y-%m-%d-%H-%M-%S--%f')[:-3] + '.png', img)

if __name__ == '__main__':
	
	dataset_path = '/home/bob/Downloads/thing3'
	output_folder = '/home/bob/Downloads/validate_labels_' + datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
	os.mkdir(output_folder)

	fnames = get_fnames(dataset_path)
	write_bboxes(fnames, output_folder)


