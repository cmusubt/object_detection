#!/usr/bin/env python3

'''
Given a dataset of image/label pairs, converting that 
to fisheye images/labels
'''
from datetime import datetime
import cv2
import drawing_utils
import bbox_writer
import pdb
import os
from validate_labels import get_fnames
from wand.image import Image
import numpy as np
import time
import pdb


BARREL_IMG_SIZE = (640,512)

def adjust_bboxes(orig_bboxes, orig_img_size, fisheye_coeffs):
	'''
	'''
	new_bboxes = []
	for bbox in orig_bboxes:
		if (bbox[0] > orig_img_size[0]) or (bbox[1] > orig_img_size[1]):
			return [], False
		# resize coords
		x_min =  int(bbox[0] * BARREL_IMG_SIZE[0] / orig_img_size[0])
		y_min =  int(bbox[1] * BARREL_IMG_SIZE[1] / orig_img_size[1])

		x_max = int((bbox[0] + bbox[2]) * BARREL_IMG_SIZE[0] / orig_img_size[0])
		y_max = int((bbox[1] + bbox[3]) * BARREL_IMG_SIZE[1] / orig_img_size[1])

		# clip for bboxes that have boundaries out of image
		x_max = min(BARREL_IMG_SIZE[0]-1, x_max)
		y_max = min(BARREL_IMG_SIZE[1]-1, y_max)

		# create fake image
		t1 = time.time()
		fake_img = np.zeros((BARREL_IMG_SIZE[0], BARREL_IMG_SIZE[1], 3))
		fake_img[x_min, y_min] = 1
		fake_img[x_max, y_max] = 1

		t2 = time.time()
		
		fake_img_distort = Image.from_array(fake_img)
		fake_img_distort.virtual_pixel = 'black'
		fake_img_distort.distort('barrel', fisheye_coeffs)

		t3 = time.time()

		new_img = np.array(fake_img_distort)
		fake_img_distort.close()		

		t4 = time.time()

		gt_zero = np.where(new_img>0)
		x_min_new, y_min_new = gt_zero[0][0],gt_zero[1][0]
		x_max_new, y_max_new = gt_zero[0][-1],gt_zero[1][-1] 

		t5 = time.time()

		new_bboxes.append(np.array([x_min_new, y_min_new, (x_max_new-x_min_new), (y_max_new- y_min_new)]))


	return new_bboxes, True



def write_fisheyed_bboxes(fnames, output_folder):
	'''
	Convert the image and its label to fisheye and write-out
	'''

	fisheye_coeffs = (0.5, 0, 0, 0.75)

	# build mask for making circular crops
	# print(fnames)
	with Image(filename=fnames[0][:-4] + '.png') as img:
		orig_img_size = img.size
		img.resize(BARREL_IMG_SIZE[0], BARREL_IMG_SIZE[1])
		img.virtual_pixel = 'black'
		img.distort('barrel', fisheye_coeffs)
		img = np.array(img)
		x_size = int(BARREL_IMG_SIZE[1]/2) - np.nonzero(img[:,:,0])[0].min()
		y_size = int(BARREL_IMG_SIZE[0]/2) - np.nonzero(img[:,:,0])[1].min()
		crop_radius = max(x_size, y_size)

	mask = np.zeros((BARREL_IMG_SIZE[1], BARREL_IMG_SIZE[0], 3), dtype=np.uint8) 
	cv2.circle(mask,(int(BARREL_IMG_SIZE[0]/2), int(BARREL_IMG_SIZE[1]/2)), crop_radius, (1,1,1),-1)
	
	num_img = 0
	for fname in fnames:
		t1 = time.time()
		if  not os.path.isfile(fname[:-4] + '.png'):
			continue
		with Image(filename=fname[:-4] + '.png') as img:
			orig_img_size = img.size
			img.resize(BARREL_IMG_SIZE[0], BARREL_IMG_SIZE[1])
			img.virtual_pixel = 'black'
			img.distort('barrel', fisheye_coeffs)
			# convert to opencv/numpy array format
			img = np.array(img)
			img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
			img = img * mask # make a circular crop

		# load labels
		bboxes, classes = bbox_writer.read_bboxes(fname)

		# correct for fisheye
		t2 = time.time()
		print(fname[:-4] + '.png')
		bboxes, returned_ok = adjust_bboxes(bboxes, orig_img_size, fisheye_coeffs)
		t3 = time.time()
		if not returned_ok:
			continue

		new_fname = output_folder + '/' + str(num_img).zfill(7)
		bbox_writer.write_bboxes(bboxes, classes, new_fname+'.txt')
		cv2.imwrite( new_fname + '.png', img)
		num_img += 1

	print('New fisheye dataset created. todo: make faster at some point...')

if __name__ == '__main__':
	
	dataset_path = '/home/debortor/Downloads/undistorted_imgs/train'
	output_folder = '/home/debortor/Downloads/fisheye_' + datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
	os.mkdir(output_folder)

	fnames = get_fnames(dataset_path)
	write_fisheyed_bboxes(fnames, output_folder)


