'''
Once data has been compiled, this will:
	1) Sanitize the labels ('s' -> 'survivor')
	2) Remap classes ('helmet-light' -> 'helmet') if desired
	3) Remove unwanted classes
	4) Print the final labels all to a text file so we can visually inspect after everything is done

Contact: Robert DeBortoli (debortor@oregonstate.edu)
'''

import os
import pdb


FINAL_FNAMES = []
KEEP_CLASSES = ["backpack", "survivor", "helmet", "rope", "fire extinguisher", "drill", "vent"]
CLS_MAP_DICT = {"helmet-light": "helmet", "rope-non-bunched": "rope", "fire": "fire extinguisher",
				"fireextinguisher": "fire extinguisher"}
FIRST_LETTERS = [s[0] for s in KEEP_CLASSES]

use_min_bboxes = False
# defined as total pixel area because shape can change depending on object
min_bbox_sizes = {"backpack": 900,
				  "survivor": 900,
				  "helmet": 150,
				  "rope": 900,
				  "fire extinguisher": 900,
				  "drill": 900,
				  "vent": 900}
				 

for letter in FIRST_LETTERS:
	if FIRST_LETTERS.count(letter) > 1:
		print('Not every class has a unique first letter so sanitize_labels \
			   will not work as intended')
		pdb.set_trace()

def get_fnames(dataset_path):
	'''
	'''
	fnames = []

	for root, dirs, files in os.walk(dataset_path):
		for file in files:
			if file.endswith("txt"):
				file_path = os.path.join(root, file)
				fnames.append(file_path)

	return fnames

def check_labels(fnames):
	'''
	Put the bounding boxes on images and write to folder
	'''
	for fname in fnames:
		with open(fname) as in_:
			content = in_.read()
		lines = content.splitlines()

		needs_rewrite = False
		for line in lines:
			if line.count(',') > 4:
				needs_rewrite = True

		if needs_rewrite:
			rewrite_content = ""
			for line in lines:
				if line.count(',') > 4:
					rewrite_content += (line[:line.find(' ')] + "\n" + line[line.find(' ') + 1:] + "\n")
				else:
					rewrite_content += line + "\n"
		
			with open(fname, "w+") as out:
				out.write(rewrite_content)

def sanitize_line(line):
	'''
	Turn stuff like 's' into 'survivor'

	Returns the line and if we changed it or not
	'''	
	line_changed = False

	elements = line.split(',')
	if elements[-1] in FIRST_LETTERS:
		line_changed = True
		for obj_cls in KEEP_CLASSES:
			if elements[-1] == obj_cls[0]:
				line = line[:-1] + obj_cls
				return line, line_changed

	return line, line_changed

def remap_line(line):
	'''
	Consolidate classes like 'helmet-light' - > 'helmet'

	Returns the line and if we changed it or not
	'''
	line_changed = False
	elements = line.split(',')
	if elements[-1] in CLS_MAP_DICT.keys():
		line_changed = True
		line = line[:-1*line[::-1].find(',')] + CLS_MAP_DICT[elements[-1]]
	return line, line_changed

def remove_unwanted_classes(line):
	'''
	Removes labels with classes like drill from cave data

	Returns the line and if we changed it or not
	'''
	elements = line.split(',')

	if elements[-1] not in KEEP_CLASSES:
		return "", True
	return line, False


def split_line_by_object(line):
	'''
	Takes a line and splits it into multiple lines if it contains multiple objects
	'''
	if line.count(',') == 4:
		return [line]

	split_lines = []
	last_split = 0
	for i in range(len(line)):
		if line[i] == ' ':
			split_lines.append(line[last_split:i])
			last_split = i+1

	split_lines.append(line[last_split:i+1])
	return split_lines

def check_corner_ordering(line):
	'''
	Make sure that if we have x1,y1,x2,y2 that
	x1<x2 and y1<y2, if not switch them
	'''
	if len(line) == 0:
		return line, False
	line_changed = False
	elements = line.split(',')
	if (float(elements[0]) > float(elements[2])):
		tmp = elements[2]
		elements[2] = elements[0]
		elements[0] = tmp
		line_changed = True

	if (float(elements[1]) > float(elements[3])):
		tmp = elements[3]
		elements[3] = elements[1]
		elements[1] = tmp
		line_changed = True

	if (line_changed):
		line = str(elements).replace('[','').replace(']','').replace('\'','').replace(' ','')

	return line, line_changed

def check_bbox_size(line):
	'''
	Make sure the bounding box size has some minimum area
	To avoid forcing the CNN to learn to detect some small speck
	'''
	if len(line) == 0:
		return line, False
	line_changed = False
	elements = line.split(',')
	obj_width = float(elements[2]) - float(elements[0])
	obj_height = float(elements[3]) - float(elements[1])
	if (obj_width * obj_height) < min_bbox_sizes[elements[-1]]:
		line = ""
		line_changed = True
	return line, line_changed



def clean_labels(fnames, output_fname):
	'''
	1) Sanitize the labels ('s' -> 'survivor')
	2) Remap classes ('helmet-light' -> 'helmet') if desired
	3) Remove unwanted classes
	4) Print the final labels all to a text file so we can visually inspect after everything is done
	'''

	with open(output_fname, "a+") as out:
		out.write("---\n")

	for fname in fnames:
		with open(fname) as in_:
			content = in_.read()
		lines = content.splitlines()

		rewrite_file = False
		rewrite_content = ""

		# first rough check to make sure we only have one label per line
		lines_rough = []
		for line in lines:
			if len(line) > 1:
				try:
					assert line[-1].isalpha(), "Failed for fname {%s, %s}".format(fname, line)
					assert ",," not in line, "Failed for fname {%s, %s}".format(fname, line)
				except:
					pdb.set_trace()
				split_lines = split_line_by_object(line)
				
				if len(split_lines) > 1:
					rewrite_file = True

				for rough_line in split_lines:
					lines_rough.append(rough_line)


		# now more detailed cleanup
		for line in lines_rough:		

			line, line_changed = sanitize_line(line)
			if line_changed:
				rewrite_file = True

			line, line_changed = remap_line(line)
			if line_changed:
				rewrite_file = True

			line, line_changed = remove_unwanted_classes(line)
			if line_changed:
				rewrite_file = True	

			# make sure label ordering is correct
			line, line_changed = check_corner_ordering(line)
			if line_changed:
				rewrite_file = True	


			# make sure bounding boxes are large enough		
			if use_min_bboxes:
				line, line_changed = check_bbox_size(line)
				if line_changed:
					rewrite_file = True

			# line is all cleaned up by now
			if line != "":
				# dont add a newline unless the line actually contains something 
				rewrite_content += line + "\n"

		if rewrite_file:
			with open(fname, "w+") as out:
				out.write(rewrite_content)

		with open(output_fname, "a+") as out:
				out.write(rewrite_content)





if __name__ == '__main__':
	
	dataset_path = "/home/debortor/data/2021-06-03-ueye/dataset_full/train" # "/home/bob/Downloads/testing_stuff"# 
	output_fname = "/home/debortor/Downloads/labels.txt" #  "/home/bob/Downloads/testing_stuff_labels.txt"

	fnames = get_fnames(dataset_path)
	clean_labels(fnames, output_fname)
