# Applying various data augmentations which cannot be done
# during training using the  tensorflow 1.xx object detection api 
# training configuration

# Contact: Robert DeBortoli (debortor@oregonstate.edu)

import cv2
import numpy as np
import pdb
import os
import random
from datetime import datetime
from scipy import ndimage
import math

def get_files_of_obj_class(dataset_path, obj_class):
	'''
	obj_class is the text (e.g. 'backpack')

	returns a list of files names with this object in it
	'''
	fnames_with_class = []

	for root, dirs, files in os.walk(dataset_path):
		for file in files:
			if file.endswith("txt"):
				file_path = os.path.join(root, file)
				with open(file_path) as in_:
					content = in_.read()
				lines = content.splitlines()

				rewrite_content = ""
				for line in lines:
					if line.find(obj_class) != -1:
						fnames_with_class.append(file_path)

	return fnames_with_class

def get_files_without_objs(dataset_path):
	'''
	obj_class is the text (e.g. 'backpack')

	returns a list of files names without objects in them
	'''
	fnames_without_objs = []

	for root, dirs, files in os.walk(dataset_path):
		for file in files:
			if file.endswith("txt"):
				file_path = os.path.join(root, file)
				with open(file_path) as in_:
					content = in_.read()
				lines = content.splitlines()

				if len(lines) == 0:
					fnames_without_objs.append(file_path)

	return fnames_without_objs

def apply_gaussian_blur(orig_img):
	'''
	Apply gaussian blur and return augmented image
	'''
	amt = random.choice([3])
	aug_img = cv2.blur(orig_img, (amt, amt))
	return aug_img

def apply_motion_blur(orig_img):
	'''
	Apply motion blur and return augmented image
	'''
	# Specify the kernel size. 
	# The greater the size, the more the motion. 
	kernel_size = np.random.random_integers(10,15, 1)[0]
	
	# Create the vertical kernel. 
	kernel_v = np.zeros((kernel_size, kernel_size)) 
	
	# Create a copy of the same for creating the horizontal kernel. 
	kernel_h = np.copy(kernel_v) 
	
	# Fill the middle row with ones. 
	kernel_v[:, int((kernel_size - 1)/2)] = np.ones(kernel_size) 
	kernel_h[int((kernel_size - 1)/2), :] = np.ones(kernel_size) 
	
	# Normalize. 
	kernel_v /= kernel_size 
	kernel_h /= kernel_size 
	
	# Apply the vertical kernel. 
	vertical_mb = cv2.filter2D(orig_img, -1, kernel_v)
	
	# Apply the horizontal kernel. 
	horizonal_mb = cv2.filter2D(orig_img, -1, kernel_h) 

	if random.random() > 0.5:
		return cv2.blur(horizonal_mb,(3,3))
	else:
		return cv2.blur(vertical_mb,(3,3))

def apply_different_lighting(orig_img):
	'''
	'''
	# the problem with adjusting v value in hsv image is saturation 
	# of helmet light so lets just multiply by a constant
	return orig_img * random.uniform(0.7, 1.2)

def apply_rotation(orig_img):
	'''
	'''	
	angle = (random.random() - 0.5) * 180 # +/- 180 degrees
	image_center = tuple(np.array(orig_img.shape[1::-1]) / 2)
	rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
	result = cv2.warpAffine(orig_img, rot_mat, orig_img.shape[1::-1], flags=cv2.INTER_LINEAR)
	return result


def apply_scaling(orig_img):
	'''
	'''
	scale_amt = random.uniform(0.5, 2) # percent of original size
	new_width = int(orig_img.shape[1] * scale_amt)
	new_height = int(orig_img.shape[0] * scale_amt)
	return cv2.resize(orig_img, (new_width, new_height), interpolation = cv2.INTER_AREA)

def apply_stretch_y(orig_img):
	'''
	Applying scaling, but only in height direction
	'''
	scale_amt = random.uniform(0.5, 2) # percent of original size
	new_width = int(orig_img.shape[1] * scale_amt)
	return cv2.resize(orig_img, (new_width, orig_img.shape[0]), interpolation=cv2.INTER_AREA)

def apply_stretch_x(orig_img):
	'''
	Applying scaling, but only in width direction
	'''
	scale_amt = random.uniform(0.5, 2) # percent of original size
	new_height = int(orig_img.shape[0] * scale_amt)
	return cv2.resize(orig_img, (orig_img.shape[1], new_height), interpolation=cv2.INTER_AREA)

def apply_occlusion(orig_img):
	'''
	Simulating an object being in front of the object of interest
	'''
	max_x_size, max_y_size = int(0.4* orig_img.shape[0]), int(0.4* orig_img.shape[1])

	rand_color = (random.random() * 255, random.random() * 255, random.random() * 255, 255)

	random_spot = [np.clip(int(random.random() * orig_img.shape[0] - 1), 0, 100), 
				   np.clip(int(random.random() * orig_img.shape[0] - 1), 0, 100),
				   np.clip(int(random.random() * orig_img.shape[0] - 1), 0, 100)]

	orig_img[random_spot[0]: random_spot[0] + int(random.random() * max_x_size),\
			random_spot[1]: random_spot[1] + int(random.random() * max_y_size)] = \
			rand_color

	return orig_img

def entire_img_apply_vertical_flip(orig_img, bbox_labels):
	'''
	For flipping an entire image and consequently adjusting the labels
	'''
	adjusted_labels = []
	for bbox_label in bbox_labels:
		bbox_label = bbox_label.split(',')
		new_bbox = [int(float(bbox_label[0])),
				    orig_img.shape[0] - int(float(bbox_label[1])),
				    int(float(bbox_label[2])), 
				    orig_img.shape[0] - int(float(bbox_label[3])), 
				    bbox_label[4]]
		adjusted_labels.append(str(new_bbox).replace('[','').replace(']','').replace('\'','').replace(' ',''))

	return cv2.flip(orig_img, 0), adjusted_labels

def entire_img_apply_horizontal_flip(orig_img, bbox_labels):
	'''
	For flipping an entire image and consequently adjusting the labels
	'''
	adjusted_labels = []
	for bbox_label in bbox_labels:
		bbox_label = bbox_label.split(',')
		new_bbox = [orig_img.shape[1] - int(float(bbox_label[2])),
				    int(float(bbox_label[1])),
				    orig_img.shape[1] - int(float(bbox_label[0])), 
				    int(float(bbox_label[3])), bbox_label[4]]
		adjusted_labels.append(str(new_bbox).replace('[','').replace(']','').replace('\'','').replace(' ',''))

	return cv2.flip(orig_img, 1), adjusted_labels

def apply_horiztonal_flip(orig_img):
	'''
	ONLY for obj_img! If you want to flip and entire image,
	use entire_img_apply_horiztonal_flip which also adjusts the labels
	'''
	return cv2.flip(orig_img, 1)

def apply_vertical_flip(orig_img):
	return cv2.flip(orig_img, 0)
	

def add_seg_obj(full_img, seg_augment_options, class_seg_img_path, obj_class):
	'''
	Add an object from a set of segmented out objects from other images

	Return the new image and the new bounding box
	'''

	# choose a segmentation object
	fname_list = os.listdir(class_seg_img_path)
	obj_fname = random.choice(fname_list)
	obj_img = cv2.imread(class_seg_img_path + '/' + obj_fname, cv2.IMREAD_UNCHANGED) # extra flag to read in transparent background	

	# augment it based on the options provided
	# don't gaussian blur because we do that later anyways on the full img and 
	# we dont want to do it twice for smaller objs

	if 'motion_blur' in seg_augment_options and random.random() > 0.2:
		obj_img = apply_motion_blur(obj_img)

	if 'change_lighting' in seg_augment_options and random.random() > 0.3:
		obj_img = apply_different_lighting(obj_img)

	if 'rotation' in seg_augment_options and random.random() < 0.3:
		obj_img = apply_rotation(obj_img)

	if 'scale' in seg_augment_options and random.random() < 0.5:
		obj_img = apply_scaling(obj_img)

	if 'occlusion' in seg_augment_options and random.random() < 0.3:
		obj_img = apply_occlusion(obj_img)

	if 'flip_horizontal' in seg_augment_options and random.random() < 0.5:
		obj_img = apply_horiztonal_flip(obj_img)

	if 'flip_vertical' in seg_augment_options and random.random() < 0.5:
		obj_img = apply_vertical_flip(obj_img)

	if 'non-uniform-stretch' in seg_augment_options and random.random() < 0.5:
		if random.random() < 0.5:
			obj_img = apply_stretch_y(obj_img)
		else:
			obj_img = apply_stretch_x(obj_img)

	# randomly insert it

	# crop if bigger in a dimension that the image we're inserting it into
	if obj_img.shape[0] > full_img.shape[0]:
		obj_img = obj_img[:full_img.shape[0]]
	if obj_img.shape[1] > full_img.shape[1]:
		obj_img = obj_img[:,:full_img.shape[1]] 

	range_0, range_1 = full_img.shape[0] - obj_img.shape[0] - 1, full_img.shape[1] - obj_img.shape[1] - 1
	random_spot = [int(random.random() * range_0), int(random.random() * range_1)]
	object_square =  full_img[random_spot[0]: random_spot[0] + obj_img.shape[0],\
							  random_spot[1]: random_spot[1] + obj_img.shape[1]]

	# determine brightness to see if we need to brighten or darken inserted object
	luminance_orig = np.mean(0.2126*object_square[:,:,0] + 0.7152*object_square[:,:,1] + 0.0722*object_square[:,:,2])
	luminance_new = np.mean(0.2126*obj_img[:,:,0][np.where(obj_img[:,:,3] > 0)] +\
				        0.7152*obj_img[:,:,1][np.where(obj_img[:,:,3] > 0)] + \
				        0.0722*obj_img[:,:,2][np.where(obj_img[:,:,3] > 0)])


	luminance_ratio = luminance_orig / luminance_new
	object_square[np.where(obj_img[:,:,3] > 0)] = obj_img[np.where(obj_img[:,:,3] > 0)][:,:3]
	obj_img[:,:,:3] = obj_img[:,:,:3] * np.clip(luminance_ratio, 0.7, 1.3)

	# add only the pixels which are non-transparent
	object_square[np.where(obj_img[:,:,3] > 0)] = obj_img[np.where(obj_img[:,:,3] > 0)][:,:3]

	# return the image and the bbox!!
	return full_img, [random_spot[1], random_spot[0], \
	                  random_spot[1] + obj_img.shape[1], \
	                  random_spot[0] + obj_img.shape[0], \
	                  obj_class]


def augment_data(dataset_path, segmented_obj_path, augment_options, augment_amounts, seg_augment_options):
	'''
	segmented_obj_path: directory of images of artifacts and only artifacts, so they have transparent backgrounds
						and are cropped to only be the object
	'''

	# make different augmented dir
	augmented_folder = dataset_path +'_'+  datetime.now().strftime('%Y-%m-%d-%H-%M-%S') + '_augmented'
	os.mkdir(augmented_folder)
	amt_added = {}

	for obj_class in augment_amounts.keys():

		obj_fnames = get_files_of_obj_class(dataset_path, obj_class)
		no_objs_fnames = get_files_without_objs(dataset_path)
		amt_added[obj_class] = 0
		
		possible_augmentations =  [k for k in augment_options.keys() if augment_options[k] == True]

		while amt_added[obj_class] < augment_amounts[obj_class]:

			added_seg_obj_to_new_img = False
			
			# choose whether to augment existing image or insert object from another set
			if augment_options['add_seg_objs'] and random.random() < 0.3:
				if len(no_objs_fnames) == 0:
					continue
				
				orig_label_fname = random.choice(no_objs_fnames)
				img = cv2.imread(orig_label_fname[:-4] + '.png')

				bbox_labels = []

				# read in the labels from the image
				if not os.path.exists(segmented_obj_path):
                    print('segmented_obj_path does not exist')
                    pdb.set_trace()
				class_seg_img_path = segmented_obj_path + '/' + obj_class

				if os.path.exists(class_seg_img_path) and \
					obj_class in seg_augment_options:
					
					augment_ops = seg_augment_options[obj_class]
					img, bbox = add_seg_obj(img, augment_ops, class_seg_img_path, obj_class)
					bbox_labels.append(str(bbox).replace('[','').replace(']','').replace('\'','').replace(' ',''))

					added_seg_obj_to_new_img = True

			# if we haven't already grabbed an empty image for putting an object into
			if not added_seg_obj_to_new_img:
				
				if len(obj_fnames) == 0:
					continue

				# read in bbox labels
				orig_label_fname = random.choice(obj_fnames)
				img = cv2.imread(orig_label_fname[:-4] + '.png')
				
				with open(orig_label_fname) as in_:
					content = in_.read()
				bbox_labels = content.splitlines()						

			if augment_options['gaussian_blur'] and random.random() > 0.5:
				img = apply_gaussian_blur(img)

			if augment_options['motion_blur'] and random.random() > 0.5:
				img = apply_motion_blur(img)

			if augment_options['change_lighting'] and random.random() > 0.5:
				img = apply_different_lighting(img)	

			if augment_options['horizontal_flips'] and random.random() > 0.5:
				img, bbox_labels = entire_img_apply_horizontal_flip(img, bbox_labels)

			if augment_options['vertical_flips'] and random.random() > 0.5:
				img, bbox_labels = entire_img_apply_vertical_flip(img, bbox_labels)		
			

			new_fname_root = datetime.now().strftime('%Y-%m-%d-%H-%M-%S--%f')[:-3]			
				
			cv2.imwrite(augmented_folder + '/' + new_fname_root + '.png', img)

			# write out the new bbox_labels, which may have changed over the course of augmentation
			new_label_fname = augmented_folder + '/' + new_fname_root + '.txt'

			with open(new_label_fname, 'w') as f:
				for item in bbox_labels:
					f.write("%s\n" % item)

			amt_added[obj_class] += 1

	print('Augmented dataset generated')


if __name__ == '__main__':
	
	dataset_path = '/media/bob/BackupHDD/bob/Winter2021/subt/subt_data_labelling/old/confirmed-2021-06-02'
	segmented_obj_path = '/media/bob/BackupHDD/bob/Winter2021/subt/subt_data_labelling/seg_objs'

	# keys are function names for different types of augmentation
	augment_options = {'gaussian_blur': True,
					   'motion_blur': True,
					   'change_lighting': True,
					   'add_seg_objs': True,
					   'horizontal_flips': True,
					   'vertical_flips': True}
	
	augment_amounts = {'backpack': 25,
					   'survivor': 25,
					   'helmet': 25,
					   'rope': 25,
					   'fire extinguisher' : 25,
					   'vent': 25,
					   'drill': 25}


	seg_augment_options = { 'backpack': ['change_lighting', 'rotation', 'scale', 'occlusion', 'flip_horizontal', 'flip_vertical', 'motion_blur'],
							'helmet': ['change_lighting', 'rotation', 'scale','occlusion', 'flip_horizontal', 'flip_vertical', 'motion_blur'],
							'fire extinguisher': ['change_lighting', 'rotation', 'scale','occlusion', 'flip_horizontal', 'flip_vertical', 'motion_blur'],
							'vent': ['change_lighting', 'rotation', 'scale','occlusion', 'flip_horizontal', 'flip_vertical', 'motion_blur'],
							'drill': ['change_lighting', 'rotation', 'scale','occlusion', 'flip_horizontal', 'flip_vertical', 'motion_blur'],
							'survivor': ['change_lighting', 'scale','occlusion', 'flip_horizontal', 'motion_blur'],
							'rope': ['change_lighting', 'rotation', 'scale','occlusion', 'flip_horizontal', 'flip_vertical', 'motion_blur', 'non-uniform-stretch']}


	augment_data(dataset_path, segmented_obj_path, augment_options, augment_amounts, seg_augment_options)
