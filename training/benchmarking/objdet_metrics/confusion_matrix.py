import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt

from object_detection.core import standard_fields
from object_detection.metrics import tf_example_parser
from object_detection.utils import label_map_util

import pdb
import os
import csv

flags = tf.app.flags

flags.DEFINE_string('label_map', None, 'Path to the label map')
flags.DEFINE_string('detections_record', None, 'Path to the detections record file')
flags.DEFINE_string('output_path', None, 'Path to the output the results in a csv.')
flags.DEFINE_string('config_path', None, 'Path to pipeline.config.')
flags.DEFINE_string('detections_record_prc', None, 'Path to detections_record_prc')
flags.DEFINE_string('det_thresh', None, 'Threshold fo generating confusion matrix')


FLAGS = flags.FLAGS

IOU_THRESHOLD = 0.5

def compute_iou(groundtruth_box, detection_box):
    g_ymin, g_xmin, g_ymax, g_xmax = tuple(groundtruth_box.tolist())
    d_ymin, d_xmin, d_ymax, d_xmax = tuple(detection_box.tolist())
    
    xa = max(g_xmin, d_xmin)
    ya = max(g_ymin, d_ymin)
    xb = min(g_xmax, d_xmax)
    yb = min(g_ymax, d_ymax)

    intersection = max(0, xb - xa + 1) * max(0, yb - ya + 1)

    boxAArea = (g_xmax - g_xmin + 1) * (g_ymax - g_ymin + 1)
    boxBArea = (d_xmax - d_xmin + 1) * (d_ymax - d_ymin + 1)

    return intersection / float(boxAArea + boxBArea - intersection)

def process_detections(detections_record, categories, det_thresh):
    record_iterator = tf.python_io.tf_record_iterator(path=detections_record)
    data_parser = tf_example_parser.TfExampleDetectionAndGTParser()

    confusion_matrix = np.zeros(shape=(len(categories) + 1, len(categories) + 1))

    image_index = 0
    for string_record in record_iterator:
        example = tf.train.Example()
        example.ParseFromString(string_record)
        decoded_dict = data_parser.parse(example)
        
        image_index += 1
        
        if decoded_dict:
            groundtruth_boxes = decoded_dict[standard_fields.InputDataFields.groundtruth_boxes]
            groundtruth_classes = decoded_dict[standard_fields.InputDataFields.groundtruth_classes]
            
            detection_scores = decoded_dict[standard_fields.DetectionResultFields.detection_scores]
            detection_classes = decoded_dict[standard_fields.DetectionResultFields.detection_classes][detection_scores >= det_thresh]
            detection_boxes = decoded_dict[standard_fields.DetectionResultFields.detection_boxes][detection_scores >= det_thresh]
            
            matches = []
            
            if image_index % 1000 == 0:
                print("Processed %d images" %(image_index))
            
            for i in range(len(groundtruth_boxes)):
                for j in range(len(detection_boxes)):
                    iou = compute_iou(groundtruth_boxes[i], detection_boxes[j])
                    
                    if iou > IOU_THRESHOLD:
                        matches.append([i, j, iou])
                    
            matches = np.array(matches)
            if matches.shape[0] > 0:
                # Sort list of matches by descending IOU so we can remove duplicate detections
                # while keeping the highest IOU entry.
                matches = matches[matches[:, 2].argsort()[::-1][:len(matches)]]
                
                # Remove duplicate detections from the list.
                matches = matches[np.unique(matches[:,1], return_index=True)[1]]
                
                # Sort the list again by descending IOU. Removing duplicates doesn't preserve
                # our previous sort.
                matches = matches[matches[:, 2].argsort()[::-1][:len(matches)]]
                
                # Remove duplicate ground truths from the list.
                matches = matches[np.unique(matches[:,0], return_index=True)[1]]
                
            for i in range(len(groundtruth_boxes)):
                if matches.shape[0] > 0 and matches[matches[:,0] == i].shape[0] == 1:
                    confusion_matrix[groundtruth_classes[i] - 1][detection_classes[int(matches[matches[:,0] == i, 1][0])] - 1] += 1 
                else:
                    confusion_matrix[groundtruth_classes[i] - 1][confusion_matrix.shape[1] - 1] += 1
                    
            for i in range(len(detection_boxes)):
                if matches.shape[0] > 0 and matches[matches[:,1] == i].shape[0] == 0:
                    confusion_matrix[confusion_matrix.shape[0] - 1][detection_classes[i] - 1] += 1
        else:
            print("Skipped image %d" % (image_index))

    print("Processed %d images" % (image_index))

    return confusion_matrix

def display(confusion_matrix, categories, output_path):
    print("\nConfusion Matrix:")
    print(confusion_matrix, "\n")
    with open(output_path, 'w', newline='') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        
        heading = ['--']
        for cat in categories:
            heading.append(cat['name'])
        heading.append('Missed')
        wr.writerow(heading)

        for i in range(len(confusion_matrix)):
            if i < len(categories):
                row = [categories[i]['name']]
            else: # last row of false positives
                row = ['False positives,']
            row.extend(confusion_matrix[i])
            wr.writerow(row)

    results = []

    for i in range(len(categories)):
        id = categories[i]["id"] - 1
        name = categories[i]["name"]
        
        total_target = np.sum(confusion_matrix[id,:])
        total_predicted = np.sum(confusion_matrix[:,id])
        
        precision = float(confusion_matrix[id, id] / total_predicted)
        recall = float(confusion_matrix[id, id] / total_target)
        
        results.append({'category' : name, 'precision_@{}IOU'.format(IOU_THRESHOLD) : precision, 'recall_@{}IOU'.format(IOU_THRESHOLD) : recall})
    
    df = pd.DataFrame(results)
    print(df)
    df.to_csv(output_path, mode='a')

def get_img_dims(config_path):
    height, width = -1, -1
    with open(config_path) as f:
        Lines = f.readlines()
        for line in Lines:
            # space before "score threshold" is important
            if line.find('height') != -1:
                height = int(float(line[line.find(':')+1:].replace(' ','')))
            if line.find('width') != -1:
                width = int(float(line[line.find(':')+1:].replace(' ','')))
                return width, height

def prep_prc_curve(detections_record_prc, categories, config_path):
    gnd_trth_folder = detections_record_prc[:detections_record_prc.rfind('/')] + '/groundtruths/'
    os.mkdir(gnd_trth_folder)
    detections_folder = detections_record_prc[:detections_record_prc.rfind('/')] + '/detections/'
    os.mkdir(detections_folder)
    img_width, img_height = get_img_dims(config_path)
    low_thresh = 0.00001

    record_iterator = tf.python_io.tf_record_iterator(path=detections_record_prc)
    data_parser = tf_example_parser.TfExampleDetectionAndGTParser()

    confusion_matrix = np.zeros(shape=(len(categories) + 1, len(categories) + 1))

    image_index = 0
    for string_record in record_iterator:
        example = tf.train.Example()
        example.ParseFromString(string_record)
        decoded_dict = data_parser.parse(example)
        
        image_index += 1
        
        if decoded_dict:
            groundtruth_boxes = decoded_dict[standard_fields.InputDataFields.groundtruth_boxes]
            groundtruth_classes = decoded_dict[standard_fields.InputDataFields.groundtruth_classes]
            
            detection_scores = decoded_dict[standard_fields.DetectionResultFields.detection_scores]
            detection_classes = decoded_dict[standard_fields.DetectionResultFields.detection_classes][detection_scores >= low_thresh]
            detection_boxes = decoded_dict[standard_fields.DetectionResultFields.detection_boxes][detection_scores >= low_thresh]
            

            gnd_trth_fname = gnd_trth_folder + str(image_index) + '.txt'
            with open(gnd_trth_fname, 'w') as fp: 
                pass
            for k in range(len(groundtruth_boxes)):
                # <class_name> <left> <top> <right> <bottom>
                class_id = groundtruth_classes[k] - 1
                class_name = categories[class_id]["name"]
                row = class_name.split(' ')[0] + ' ' + str(int(groundtruth_boxes[k][0]*img_width)) + ' '+\
                                                       str(int(groundtruth_boxes[k][1]*img_height)) + ' '+\
                                                       str(int(groundtruth_boxes[k][2]*img_width)) + ' '+\
                                                       str(int(groundtruth_boxes[k][3]*img_height))
                with open(gnd_trth_fname, 'a') as the_file:
                    the_file.write(row+'\n')  

                    
            # <class_name> <confidence> <left> <top> <right> <bottom> 
            det_fname = detections_folder + str(image_index) + '.txt'
            with open(det_fname, 'w') as fp: 
                pass
            for k in range(len(detection_boxes)):
                # <class_name> <left> <top> <right> <bottom>
                class_id = detection_classes[k] - 1
                class_name = categories[class_id]["name"]
                row = class_name.split(' ')[0] + ' ' +\
                      str(detection_scores[k]) + ' ' + \
                      str(int(detection_boxes[k][0]*img_width)) + ' '+\
                      str(int(detection_boxes[k][1]*img_height)) + ' '+\
                      str(int(detection_boxes[k][2]*img_width)) + ' '+\
                      str(int(detection_boxes[k][3]*img_height))
                with open(det_fname, 'a') as the_file:
                    the_file.write(row+'\n')             
                
                
    
def main(argv):
    del argv
    required_flags = ['detections_record', 'label_map', 'output_path', 'config_path', 'detections_record_prc', 'det_thresh']
    for flag_name in required_flags:
        if not getattr(FLAGS, flag_name):
            raise ValueError('Flag --{} is required'.format(flag_name))

    label_map = label_map_util.load_labelmap(FLAGS.label_map)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=100, use_display_name=True)

    confusion_matrix = process_detections(FLAGS.detections_record, categories, float(FLAGS.det_thresh))

    display(confusion_matrix, categories, FLAGS.output_path)

    prep_prc_curve(FLAGS.detections_record_prc, categories, FLAGS.config_path)
    
if __name__ == '__main__':
    tf.app.run(main)
