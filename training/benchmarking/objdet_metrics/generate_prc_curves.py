"""
Converts raw robot data to videos we can use for analysis.
Provide azure storage explorer links and this script
will automatically detect specific runs, etc. and output
videos accordingly

TO BE RUN FROM WHEREVER ./AZCOPY IS INSTALLED, 
E.G. /home/debortor/Downloads/azcopy_linux_amd64_10.4.3

Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""
import pdb
import os
from datetime import datetime
import json
from confusion_matrix import get_img_dims 

def get_objdet_path():
	'''
	Get tensorflow object detection api path
	'''
	if os.path.exists(os.path.expanduser('~/tensorflow')):
		objdet_path = os.path.expanduser('~/tensorflow') + '/models/research'
	elif os.path.exists(os.path.expanduser('~/Documents/tensorflow')):
		objdet_path = os.path.expanduser('~/Documents/tensorflow') + '/models/research'
	else:
		print('cannot find tensorflow object detection api folder')
		pdb.set_trace()
	return objdet_path


def source_objdet():
	objdet_path = get_objdet_path()
	os.system('export PYTHONPATH=$PYTHONPATH:' + objdet_path + ':' + objdet_path + '/slim')
	# os.system('source  ../../../../../devel/setup.bash')

def generate_thresh_config(thresh_dir, thresh, base_config_path):
	'''
	Replace the detection threshold in the config with the appropriate one for the experiment
	'''
	new_config = ""
	with open(base_config_path) as f:
		Lines = f.readlines()
		for line in Lines:
			# space before "score threshold" is important
			if line.find(' score_threshold') == -1:
				new_config += line
			else:
				new_config += 'score_threshold: ' + str(thresh) + '\n'
	
	thresh_config_fname = thresh_dir + '/pipeline_'+str(thresh).replace('.','_')+'.config'
	with open(thresh_config_fname, "w") as text_file:
		text_file.write(new_config)

	return thresh_config_fname


def freeze_models(model_dir_names, ckpts, detection_threshs, eval_experiment_dir, \
			      precision_modes, batch_sizes, num_img_channels, compile_openvino_models):
	frozen_model_fnames = []
	if len(model_dir_names) != len(ckpts):
		print('Model dir and ckpt lists must be of same length')
		pdb.set_trace()

	objdet_path = get_objdet_path()

	for i in range(len(model_dir_names)):		
		eval_dir = eval_experiment_dir + model_dir_names[i][model_dir_names[i].rfind('/'):]
		os.mkdir(eval_dir)

		base_config_path = model_dir_names[i] + '/pipeline.config'
		if not os.path.exists(base_config_path):
			print('could not find pipeline config for: ', model_dir_names[i])
			pdb.set_trace()

		for ckpt in ckpts[i]:
			ckpt_path = model_dir_names[i] + '/train/model.ckpt-' + str(ckpt)
			ckpt_dir = eval_dir + '/' + str(ckpt) 
			os.mkdir(ckpt_dir)

			for thresh in detection_threshs:				
				thresh_dir = ckpt_dir + '/' + str(thresh).replace('.','_')
				os.mkdir(thresh_dir)

				thresh_config_fname = generate_thresh_config(thresh_dir, thresh, base_config_path)
				
				for prec_mode in precision_modes:			
					for batch_size in batch_sizes:
						os.system('python3 ../../tensorrt/export_tf_trt_graph.py ' + \
								  ' --config_path '+ thresh_config_fname + \
								  ' --checkpoint_path '+ ckpt_path +  \
								  ' --score_threshold '+ str(thresh) + \
								  ' --precision_mode ' + prec_mode +\
								  ' --batch_size ' + str(batch_size))

						os.system('mv ' + model_dir_names[i] + '/train/frozen_trt_graph_'+str(batch_size)+'bs_'+prec_mode+'.pb '\
									    + thresh_dir + '/' )
						frozen_model_fnames.append(thresh_dir + '/frozen_trt_graph_'+str(batch_size)+'bs_'+prec_mode+'.pb')						

						# compile into openvino format
						# train_ckpt_prefix = thresh_dir + '/frozen_trt_graph_' + \
						# 				    str(batch_size)+'bs_'+prec_mode+'.pb'
						if (not compile_openvino_models):
							continue
						openvino_batch_dir = thresh_dir + '/bs' + str(batch_size) + '/'
						os.mkdir(openvino_batch_dir)

						os.system('python3  ' + objdet_path + '/object_detection/export_inference_graph.py '+\
								  ' --input_type image_tensor '+\
								  ' --pipeline_config_path '+ thresh_config_fname +\
								  ' --trained_checkpoint_prefix ' + ckpt_path + \
								  ' --output_directory ' + openvino_batch_dir +\
								  ' --batch_size ' + str(batch_size))

						owd = os.getcwd()
						os.chdir('/opt/intel/openvino_2019.3.376/deployment_tools/model_optimizer')
						img_width, img_height = get_img_dims(thresh_config_fname)
						os.system('python3 mo_tf.py '+\
								  ' --input_model ' + openvino_batch_dir + '/frozen_inference_graph.pb' +\
								  ' --tensorflow_use_custom_operations_config /opt/intel/openvino_2019.3.376/'+\
								  											  'deployment_tools/model_optimizer/'+\
								  											  'extensions/front/tf/'+\
								  											  'ssd_support_api_v1.14.json '+\
								  ' --tensorflow_object_detection_api_pipeline_config ' + thresh_config_fname +\
								  ' --data_type=' + prec_mode + \
								  ' --output_dir '+ openvino_batch_dir +
								  ' --input_shape ['+str(batch_size)+','+ str(img_height)+','+ \
								  					 str(img_width)+','+str(num_img_channels)+ ']')

						os.chdir(owd)

					# Also freeze with extremely low threshold for computing pr curve
					low_thresh = 0.00001
					thresh_config_fname_low = generate_thresh_config(thresh_dir, low_thresh, base_config_path)

					os.system('python3 ../../tensorrt/export_tf_trt_graph.py ' + \
								  ' --config_path '+ thresh_config_fname_low + \
								  ' --checkpoint_path '+ ckpt_path +  \
								  ' --score_threshold '+ str(low_thresh) + \
								  ' --precision_mode ' + prec_mode +\
								  ' --batch_size ' + str(batch_size))

					os.system('mv ' + model_dir_names[i] + '/train/frozen_trt_graph_'+str(batch_size)+'bs_'+prec_mode+'.pb '\
								    + thresh_dir + '/frozen_trt_graph_'+str(batch_size)+'bs_'+prec_mode+'_prc.pb')
					frozen_model_fnames.append(thresh_dir + '/frozen_trt_graph_'+str(batch_size)+'bs_'+prec_mode+'_prc.pb')
	return frozen_model_fnames

def infer_detections(frozen_model_fnames, eval_record_fname):
	'''
	For every .pb file, generate .record file of detections 
	'''
	for frozen_model_fname in frozen_model_fnames:
  		os.system('python3 infer_detections.py ' + \
				  ' --input_tfrecord_paths=' + eval_record_fname +  \
				  ' --output_tfrecord_path=' + frozen_model_fname[:-3] + '_detections.tfrecord' + \
				  '	--inference_graph=' + frozen_model_fname)	

def fname2thresh(frozen_model_fname):
	'''
	'''
	dir_name = os.path.dirname(frozen_model_fname)
	thresh = float(dir_name[dir_name.rfind('/')+1:].replace('_','.'))
	return thresh

def generate_confusion_matrices(frozen_model_fnames, model_dir):
	'''
	'''
	for frozen_model_fname in frozen_model_fnames:			
			# prc model has super low threshold
			if frozen_model_fname.find('prc') != -1:
				continue
			# find any config file to use
			files = os.listdir(frozen_model_fname[:frozen_model_fname.rfind('/')])
			for f in files:
				if (f.find('.config') != -1):
					config_path = os.path.join(frozen_model_fname[:frozen_model_fname.rfind('/')],f)

			# generate confusion matrix and prep pr curve
			thresh = fname2thresh(frozen_model_fname)
			os.system('python3 confusion_matrix.py '+ \
				      ' --detections_record ' + frozen_model_fname[:-3] + '_detections.tfrecord' +\
				      ' --detections_record_prc ' + frozen_model_fname[:-3] + '_prc_detections.tfrecord' +\
				      ' --label_map ../../../inference/data/label_maps/finalcomp_label.pbtxt' +\
				      ' --output_path ' + frozen_model_fname[:-3]+ '_confusion_mat.csv' +\
				      ' --config_path ' + config_path +\
				      ' --det_thresh '+ str(thresh)) 

			# generate pr curve
			detections_record = frozen_model_fname[:-3] + '_detections.tfrecord' # its fine its not "prc"
			detections_record[:detections_record.rfind('/')] + '/groundtruths/'
			pr_folder = detections_record[:detections_record.rfind('/')] + '/pr_results/'
			os.mkdir(pr_folder)
			os.system('python3 ./Object-Detection-Metrics/pascalvoc.py -t 0.5' +\
					  ' -det ' + detections_record[:detections_record.rfind('/')] + '/detections/' +\
					  ' -gt ' + detections_record[:detections_record.rfind('/')] + '/groundtruths/' +\
					  ' -sp ' + pr_folder + \
					  ' -np')

def write_cnn_vids(test_vid_dir, frozen_model_fnames, batch_sizes):
	'''
	'''
	for frozen_model_fname in frozen_model_fnames:
		# prc version has super low threshold
		if (frozen_model_fname.find('prc') != -1):
			continue
		out_dir = frozen_model_fname[:frozen_model_fname.rfind('/')] + '/videos'
		os.mkdir(out_dir)
		for batch_size in batch_sizes:
			for video_fname in os.listdir(test_vid_dir):
				os.system("python3 ../../../inference/scripts/offline_video_inference.py " +\
			              " --path_to_model " + frozen_model_fname +\
			              " --batch_size " + str(batch_size) +\
			              " --video_path " + os.path.join(test_vid_dir, video_fname) +\
			              " --label_map_path ../../../inference/data/label_maps/finalcomp_label.pbtxt" +\
			              " --output_dir " + out_dir)




if __name__ == "__main__":
	model_dir = '/media/bob/BackupHDD/bob/Winter2021/subt/subt_training/2021-06-03-model-evaluation'
	eval_record_fname = '/media/bob/BackupHDD/bob/Winter2021/subt/subt_training/eval-00000.record'
	test_vid_dir = '/media/bob/BackupHDD/bob/Winter2021/subt/subt_training/2021-06-03-model-evaluation/test_vids'
	experiments_to_analyze = ['2021-06-03-512-240-actually-seg-obj', '2021-06-03-mnv2-512-360-lower-lr-seg-rs']
	num_img_channels = 3 # for rgb=3, for thermal=1
	ckpts = [[192978], [45660]] 
	detection_threshs = [0.1, 0.3] # to evaluate on-board robot perf.
	precision_modes = ['FP32'] 
	batch_sizes = [1]

	compile_openvino_models = True
	compute_quantative_results = True
	compute_qualitative_results = True

	print('\n\n*******\nDid you source:\n-the tensorflow objdet api\n-the subt object_detection repo\n-the openvino setupvars?\n'+\
		  'Press c then Enter to continue\n*******\n\n')
	pdb.set_trace()

	# make dirs
	eval_experiment_dir = model_dir + '/quantitative_eval_' + datetime.now().strftime('%Y-%m-%d-%H-%M-%S--%f')[:-3]
	os.mkdir(eval_experiment_dir)

	for i in range(len(experiments_to_analyze)):
		experiments_to_analyze[i] = model_dir  + '/' + experiments_to_analyze[i] 
	model_dir_names = experiments_to_analyze

	# freeze model with super low thresh and with normal thresh
	# and compile into every format we would ever want
	frozen_model_fnames = freeze_models(model_dir_names, ckpts, detection_threshs, eval_experiment_dir, \
				  						precision_modes, batch_sizes, num_img_channels, compile_openvino_models)

	if compute_quantative_results:
		# run infer detections
		infer_detections(frozen_model_fnames, eval_record_fname)
		# run confusion matrix and pr curves
		generate_confusion_matrices(frozen_model_fnames, model_dir)
	if compute_qualitative_results:
		# run through videos
		write_cnn_vids(test_vid_dir, frozen_model_fnames, batch_sizes)