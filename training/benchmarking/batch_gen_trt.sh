#!/bin/sh

[ -z "$1" ] && echo "No model directory path specified" && exit 1

TENSORRT=../tensorrt/export_tf_trt_graph.py
for d in $(ls -dr $1/*/) ; do
    echo $d
    $TENSORRT \
        --inference_folder $d \
        --batch_size 1
    $TENSORRT \
        --inference_folder $d \
        --batch_size 4
done
