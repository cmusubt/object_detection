#!/usr/bin/env python3

import multiprocessing
import tarfile
from bs4 import BeautifulSoup
import requests
import pathlib
import os
import urllib.request
import shutil

out_location = "downloads"
os.makedirs(out_location, exist_ok=True)
url = "https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md"
r = requests.get(url)
data = r.text
soup = BeautifulSoup(data, "lxml")

# https://stackoverflow.com/questions/45540860/download-all-the-files-in-the-website
# https://stackoverflow.com/questions/7243750/download-file-from-web-in-python-3
tar_files = []
for link in soup.find_all('a'):
    ref = link.get("href")
    if ref.endswith(".tar.gz"):
        tar_files.append(ref)

def download_and_extract(ref):
    names = ref.split("/")
    filename = names[-1]
    out = pathlib.Path(out_location) / filename

    print("Downloading", out)
    # Download the file from `url` and save it locally under `file_name`:
    with urllib.request.urlopen(ref) as response, open(out, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)

    with tarfile.open(out) as tar:
        tar.extractall(out_location)


pool = multiprocessing.Pool()
pool.map(download_and_extract, tar_files)
