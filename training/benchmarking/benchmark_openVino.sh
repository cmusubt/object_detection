#!/bin/sh

[ -z "$1" ] && echo "No model directory path specified" && exit 1
export OMP_NUM_THREADS=1
DATE=`date +%Y%m%d%H%M%S`
for f in $(find $1 -name "*.xml"); do
    ./benchmark_od_model_openVino.py --path_to_model $f >> openVino_graphs_${DATE}.csv
    echo "-----------"
done
