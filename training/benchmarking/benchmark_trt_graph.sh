#!/bin/sh

[ -z "$1" ] && echo "No model directory path specified" && exit 1

./benchmark_od_model.py --header >> trt_graphs.csv

for f in $(find $1 -name "*trt*1bs*.pb"); do
    ./benchmark_od_model.py --path_to_model $f \
        --batch_size 1 >> trt_graphs.csv
    echo "-----------"
done

for f in $(find $1 -name "*trt*4bs*.pb"); do
    ./benchmark_od_model.py --path_to_model $f \
        --batch_size 4 >> trt_graphs.csv
    echo "-----------"
done
