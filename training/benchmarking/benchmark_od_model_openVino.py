#!/usr/bin/env python3

import sys
import pathlib
import numpy as np
import time
import os
import cv2
import argparse
from inference.scripts.multi_object_detector_openvino import MultiObjectDetector

try:
    from progressbar import progressbar
except:
    progressbar = lambda x: x


parser = argparse.ArgumentParser("Benchmark inference time of a OD API model")
parser.add_argument("--header", action="store_true", default=True,
        help="Print the header for the CSV file indicating categories.")
parser.add_argument("--path_to_model", type=str, required=True,
        help="Path to model (.pb file) to benchmark.")
parser.add_argument("--benchmark_frames", type=int, default=100,
        help="Number of frames to perform inference on.")
parser.add_argument("--startup_frames", type=int, default=10,
        help="Number of frames to perform inference on before timing.")
parser.add_argument("--benchmark_delay", type=float, default=0.0,
        help="Sleep for this long (seconds) after startup before benchmarking.")

args = parser.parse_args()

# https://stackoverflow.com/questions/5574702/how-to-print-to-stderr-in-python
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def main():
    log_order = [
            "Path to model",
            "Model loading time",
            "Startup inference time (total)",
            "Benchmark inference time (total)",
            "Average step time",
            "Minimum batch time",
            "Maximum batch time",
            "Average batch time",
    ]

    if args.header:
        print(",".join(log_order))
        

    csv_output = []

    eprint("Benchmarking", args.path_to_model)
    csv_output.append(args.path_to_model)

    start = time.time()
    mod = MultiObjectDetector(args.path_to_model)
    end = time.time()
    csv_output.append("%0.3f" % (end - start))
    vid = pathlib.Path(__file__).absolute().parent / "140_frames.mp4"
    cap = cv2.VideoCapture(str(vid))

    start = time.time()
    for i in progressbar(range(args.startup_frames)): # Number of frames to perform as warmup
        ret, frame = cap.read()
        if not ret:
            eprint("Read error in benchmark video. Maybe need more frames.")
            return

        mod.detect_single_image(frame)
    end = time.time()
    #  eprint("Startup time taken: %0.3fs" % (end - start))
    csv_output.append("%0.3f" % (end - start))

    time.sleep(args.benchmark_delay)

    frame_times = []
    start = time.time()
    for i in progressbar(range(args.benchmark_frames)):
        ret, frame = cap.read()
        if not ret:
            eprint("Read error in benchmark video. Maybe need more frames.")
            return

        single_start = time.time()
        mod.detect_single_image(frame)
        single_end = time.time()
        frame_times.append(single_end - single_start)

    end = time.time()

    cap.release()

    #  eprint("Total time taken: %0.3fs" % (end - start))
    csv_output.append("%0.3f" % (end - start))
    avg_step_time = (end - start) / args.benchmark_frames
    eprint("Average time per step: %0.3fs (%0.2f Hz)" % (avg_step_time, 1.0 /
        avg_step_time))
    csv_output.append("%0.3f" % avg_step_time)
    min_frame_time = min(frame_times) 
    eprint("Min time per inference: %0.3fs (%0.2f Hz)" % (min_frame_time, 1.0 /
        min_frame_time))
    csv_output.append("%0.3f" % min_frame_time)
    max_frame_time = max(frame_times)
    eprint("Max time per inference: %0.3fs (%0.2f Hz)" % (max_frame_time, 1.0 /
        max_frame_time))
    csv_output.append("%0.3f" % max_frame_time)
    avg_frame_time = sum(frame_times) / len(frame_times)
    eprint("Avg time per inference: %0.3fs (%0.2f Hz)" % (avg_frame_time, 1.0 /
        avg_frame_time))
    csv_output.append("%0.3f" % avg_frame_time)
    print(",".join(csv_output))

    # Add all of the raw readings for future processing
    csv_output.extend(["%0.3f" % t for t in frame_times])

    print(",".join(csv_output))


if __name__ == "__main__":
    main()
