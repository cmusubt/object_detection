#!/bin/sh

[ -z "$1" ] && echo "No model directory path specified" && exit 1

./benchmark_od_model.py --header >> reference_graphs.csv

for f in $(find $1 -name "frozen_inference_graph.pb"); do
    ./benchmark_od_model.py --path_to_model $f \
        --batch_size 1 >> reference_graphs.csv
    echo "-----------"
done

for f in $(find $1 -name "frozen_inference_graph.pb"); do
    ./benchmark_od_model.py --path_to_model $f \
        --batch_size 4 >> reference_graphs.csv
    echo "-----------"
done
