#!/usr/bin/env python3

import pathlib
import argparse
import subprocess
import os

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config_path", 
            type=lambda p: pathlib.Path(p).resolve(), required=True,
            help="Path to pipeline.config file")
    parser.add_argument("--checkpoint_prefix", type=str, required=True,
            help="Checkpoint prefix to convert")
    parser.add_argument("--batch_size", type=int, default=1,
            help="Maximum allowed batch size for inference")
    args = parser.parse_args()
 
    model_research_dir = pathlib.Path(os.environ['MODEL_RESEARCH_DIR'])

    ckpt = pathlib.Path(args.checkpoint_prefix)
    ckpt_num = int(ckpt.name.split("-")[-1])
    output_directory = ckpt.parent / ("output_inference_graph_%d" % ckpt_num)
    output_directory.mkdir(exist_ok=True)

    subprocess.run(
            [
                "python3",
                str(model_research_dir / "object_detection" / "export_inference_graph.py"),
                "--input_type", "image_tensor",
                "--pipeline_config_path", args.config_path,
                "--trained_checkpoint_prefix", args.checkpoint_prefix,
                "--output_directory", str(output_directory)])

    subprocess.run(
            [
                "python3",
                "export_tf_trt_graph.py",
                "--inference_folder", output_directory,
                "--batch_size", str(args.batch_size)])


if __name__ == "__main__":
    main()
