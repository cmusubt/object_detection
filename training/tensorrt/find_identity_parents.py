#!/usr/bin/env python3

import pathlib
import uff
import argparse
import tensorflow as tf

parser = argparse.ArgumentParser()
parser.add_argument("--path_to_model", type=pathlib.Path, required=True, 
        help="model path")
args = parser.parse_args()

def load_graph(path_to_model):
    od_graph = tf.Graph()
    od_graph_def = tf.GraphDef()

    with tf.gfile.GFile(path_to_model, 'rb') as f:
        od_graph_def.ParseFromString(f.read())

    with od_graph.as_default():
        tf.import_graph_def(od_graph_def, name='')

    return od_graph


with load_graph(str(args.path_to_model)).as_default():
    graph = tf.get_default_graph()
    output_names = ["num_detections", "detection_scores", "detection_classes",
        "detection_boxes"]

    parents = []
    for output_name in output_names:
        output = graph.get_operation_by_name(output_name)
        assert(output.type == "Identity")
        parents.extend([i.op.name for i in output.inputs])

    uff_path = args.path_to_model.parent / (args.path_to_model.stem + ".uff")
    flagfile_path = args.path_to_model.parent / (args.path_to_model.stem +
            ".flagfile")
    plan_path = args.path_to_model.parent / (args.path_to_model.stem + ".plan")

    flags = {
            "--fp16" : True,
            "--height" : 640,
            "--width" : 360,
            "--input" : "image_tensor",
            "--max_batch_size" : 4,
            "--max_workspace_size" : 1 << 20,
            "--outputs" : ",".join(parents),
            "--plan_filename" : plan_path,
            "--uff_filename" : uff_path,
    }

    try:
        uff.from_tensorflow_frozen_model(
                frozen_file=str(args.path_to_model),
                output_nodes = parents,
                output_filename=str(uff_path))

        with open(flagfile_path, "w") as f:
            f.writelines(("%s=%s\n" % kv for kv in flags.items()))
        
        print("Successfully converted model and wrote flags!")


    except Exception as e:
        print("Exception:", e)
        print("Failed to convert model")
    
    print(",".join(parents))
        
