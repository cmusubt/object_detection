#!/usr/bin/env python3

import pathlib
import argparse
from tf_trt_models.detection import build_detection_graph
import tensorflow.contrib.tensorrt as trt

parser = argparse.ArgumentParser("Create TF TRT optimized graphs",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--inference_folder",
        type=lambda p: pathlib.Path(p).resolve(),
        help="Path to folder containing config and frozen graph")
parser.add_argument("--config_path", type=lambda p: pathlib.Path(p).resolve(),
        help="Path to pipeline.config file")
parser.add_argument("--checkpoint_path", 
        type=lambda p: pathlib.Path(p).resolve(),
        help="Path to .ckpt file")
parser.add_argument("--batch_size", type=int, default=1,
        help="Maximum allowed batch size for inference")
parser.add_argument("--score_threshold", type=float, default=None,
        help="NMS / post processing score threshold")
# Allow 2GB of RAM usage by default
parser.add_argument("--max_workspace_size_bytes", type=int, default=1<<31,
        help="Maximum amount of memory to allow for inference")
parser.add_argument("--precision_mode", 
        choices=["FP32", "FP16", "INT8"], default="FP16",
        help="Precision mode for use for inference with TRT")
parser.add_argument("--minimum_segment_size", type=int, default=50,
        help="Minimum number of nodes to require before converting to TRT")
parser.add_argument("--force", action="store_true", default=False,
        help="Force recreation of network if file exists already")

args = parser.parse_args()


def main():
   
    if args.inference_folder is not None:
        assert(args.inference_folder.exists())
        config_path = args.inference_folder / "pipeline.config"
        checkpoint_path = args.inference_folder / "model.ckpt"
    elif args.config_path is not None and args.checkpoint_path is not None:
        assert(args.config_path.exists())
        #assert(args.checkpoint_path.exists())
        config_path = args.config_path
        checkpoint_path = args.checkpoint_path
    else:
        print("One of {inference_folder, {config_path, checkpoint_path}} is "
                "required!")
        return

    print("Building detection graph!")
    print("Config path:", config_path)
    print("Checkpoint path:", checkpoint_path)
    
    # Make sure the output goes in the same directory as the input frozen graph
    trt_name = "frozen_trt_graph_%dbs_%s.pb" % (args.batch_size,
            args.precision_mode)
    trt_graph_path = checkpoint_path.parent / trt_name
    if trt_graph_path.is_file() and not args.force:
        print("Output file %s exists already, not forcing!" % trt_graph_path)
        return

    frozen_graph, input_names, output_names = build_detection_graph(
            config=str(config_path),
            checkpoint=str(checkpoint_path),
            batch_size=args.batch_size,
            score_threshold=args.score_threshold,
    )

    try:
        trt_graph = trt.create_inference_graph(
                input_graph_def=frozen_graph,
                outputs=output_names,
                max_batch_size=args.batch_size,
                max_workspace_size_bytes=args.max_workspace_size_bytes,
                precision_mode=args.precision_mode,
                minimum_segment_size=args.minimum_segment_size,
        )
    except ValueError as e:
        print("Provided precision mode is invalid! Failed:", e)
        return
    except RuntimeError as e:
        print("Malforme status message! Failed:", e)
        return

    with trt_graph_path.open("wb") as f:
        f.write(trt_graph.SerializeToString())


if __name__ == "__main__":
    main()
