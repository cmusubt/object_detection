#!/bin/bash

for f in $(find $1 -iname "frozen_inference_graph.pb"); do
    echo $f
    python3 find_identity_parents.py --path_to_model $f 2>&1 > $f.log
done
