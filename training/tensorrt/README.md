After training the network, export an inference graph as follows, which should
put everything you need for the next step in the `output_inference_graph`
folder.

```
python3 $MODEL_RESEARCH_DIR/object_detection/export_inference_graph.py \
    --input_type image_tensor \
    --pipeline_config_path models/sample_mobilenet_v1_0.5_ssd_quantized/pipeline.config \
    --trained_checkpoint_prefix models/sample_mobilenet_v1_0.5_ssd_quantized/model.ckpt-200007 \
    --output_directory models/sample_mobilenet_v1_0.5_ssd_quantized/output_inference_graph
```
