#include <iostream>
#include <string>

#include <NvInfer.h>
#include <NvUffParser.h>
#include <gflags/gflags.h>

DEFINE_string(uff_filename, "", "UFF file filename to convert");
DEFINE_string(plan_filename, "", "Filename for output TRT plan");
DEFINE_string(input, "image_tensor", "Names of input ops for the network");
DEFINE_string(outputs, "", "Names of output ops for the network");
DEFINE_uint64(height, 360, "Input image height");
DEFINE_uint64(width, 640, "Input image width");
DEFINE_uint64(max_batch_size, 4, "Maximum batch size");
DEFINE_uint64(max_workspace_size, 1 << 20, "Maximum workspace size");
DEFINE_bool(fp16, true, "Use fp16 inference instead of fp32");

class Logger : public nvinfer1::ILogger {
  void log(Severity severity, const char* msg) override {
    std::cout << msg << std::endl;
  }
} gLogger;

int main(int argc, char** argv) {
  gflags::ParseCommandLineFlags(&argc, &argv, true);

  // Load stuff
  nvinfer1::IBuilder* builder = nvinfer1::createInferBuilder(gLogger);
  nvinfer1::INetworkDefinition* network = builder->createNetwork();
  nvuffparser::IUffParser* parser = nvuffparser::createUffParser();

  // Load UFF graph into memory
  parser->registerInput(FLAGS_input.c_str(),
                        nvinfer1::DimsCHW(3, FLAGS_height, FLAGS_width),
                        nvuffparser::UffInputOrder::kNCHW);
  size_t pos;
  while ((pos = FLAGS_outputs.find(",")) != std::string::npos) {
    std::string output = FLAGS_outputs.substr(0, pos);
    std::cout << "Registering output " << output << "\n";
    parser->registerOutput(output.c_str());
    FLAGS_outputs.erase(0, pos + 1);
  }
  std::cout << "Registering output " << FLAGS_outputs << "\n";
  parser->registerOutput(FLAGS_outputs.c_str());

  // Do the parsing
  auto data_type =
      FLAGS_fp16 ? nvinfer1::DataType::kHALF : nvinfer1::DataType::kFLOAT;
  std::cout << "uff filename " << FLAGS_uff_filename << "\n";
  if (!parser->parse(FLAGS_uff_filename.c_str(), *network, data_type)) {
    std::cerr << "Failed to parse!" << std::endl;
    parser->destroy();
    network->destroy();
    builder->destroy();
    return 1;
  }

  std::cout << "Successfully parsed!\n";

  // Configure builder parameters
  std::cout << "Max batch size " << builder->getMaxBatchSize() << "\n"
            << "Max workspace size " << builder->getMaxWorkspaceSize() << "\n"
            << "Half2 mode " << builder->getHalf2Mode() << "\n"
            << "MinFindIterations " << builder->getMinFindIterations() << "\n"
            << "AverageFindIterations " << builder->getAverageFindIterations()
            << "\n"
            << "hasfp16 " << builder->platformHasFastFp16() << "\n"
            << "hasfastint8 " << builder->platformHasFastInt8() << "\n"
            << "int8 mode " << builder->getInt8Mode() << "\n"
            << "max dla batch size " << builder->getMaxDLABatchSize() << "\n"
            << "nb dla cores " << builder->getNbDLACores() << "\n"
            << "dla core " << builder->getDLACore() << "\n"
            << "fp16 mode " << builder->getFp16Mode() << "\n"
            << std::flush;

  // Clean up after ourselves
  parser->destroy();
  network->destroy();
  builder->destroy();
}
