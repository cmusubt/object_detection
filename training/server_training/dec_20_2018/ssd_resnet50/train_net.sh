#!/bin/sh

cd /tensorflow/models/research/
PIPELINE_CONFIG_PATH=/data/datasets/vasua/ssd_resnet50/pipeline_docker.config
MODEL_DIR=/data/datasets/vasua/dec_20_18/session_0
CUDA_VISIBLE_DEVICES=0 python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --alsologtostderr
