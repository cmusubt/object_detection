#!/bin/bash

# SLURM Resource parameters
#SBATCH -w clamps
#SBATCH --ntasks=1
#SBATCH -N 1
#SBATCH --cpus-per-task=8
#SBATCH --ntasks-per-core=1
#SBATCH -t 1-12:00
#SBATCH --gres=gpu:4
#SBATCH --mem=24000
#SBATCH --job-name=train_dec_20_18
#SBATCH -o job_%j.out
#SBATCH -e job_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vasua@andrew.cmu.edu

set -x

nvidia-docker run -v /data:/data vasua/objdet:latest /data/datasets/vasua/ssd_resnet50/train_net.sh &
nvidia-docker run -v /data:/data vasua/objdet:latest /data/datasets/vasua/ssd_mobilenet_fpn/train_net.sh &
nvidia-docker run -v /data:/data vasua/objdet:latest /data/datasets/vasua/ssd_mobilenet_v2/train_net.sh &
nvidia-docker run -v /data:/data vasua/objdet:latest /data/datasets/vasua/faster_rcnn/train_net.sh &

wait
