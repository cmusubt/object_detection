#!/bin/sh

cd /tensorflow/models/research/
PIPELINE_CONFIG_PATH=/data/datasets/vasua/ssd_mobilenet_v2/pipeline_docker_full.config
MODEL_DIR=/data/datasets/vasua/dec_20_18_full/session_2
CUDA_VISIBLE_DEVICES=2 python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --alsologtostderr
