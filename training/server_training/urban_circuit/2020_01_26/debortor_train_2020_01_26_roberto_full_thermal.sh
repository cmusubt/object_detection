#!/bin/bash

# SLURM Resource parameters
#SBATCH -w roberto
#SBATCH --ntasks=1
#SBATCH -N 1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks-per-core=1
#SBATCH -t 48-00:00
#SBATCH --gres=gpu:1
#SBATCH --mem=110000
#SBATCH --job-name=train_2020_01_26_roberto_debortor
#SBATCH -o job_%j.out
#SBATCH -e job_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=debortor@oregonstate.edu

#uname -a


. /home/debortor/.bash_profile

nvidia-docker run \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /project/subt/data/urban_circuit_dataset/thermal_dataset/data/urban_final_thermal:/dataset \
    -v /home/debortor/workspaces/objdet/src/object_detection/training/models/pipeline_configs:/configs \
    -v /home/debortor/Documents/checkpoints/urban_final_thermal_640x360:/checkpoints \
    -v /home/debortor/workspaces/objdet/src/object_detection/training/server_training:/training \
    -e CUDA_VISIBLE_DEVICES=0 \
    vasua/objdet:latest \
    /training/tunnel_circuit/ssd_mobilenet_v1_coco_2018_01_28/train_640x360_full.sh
