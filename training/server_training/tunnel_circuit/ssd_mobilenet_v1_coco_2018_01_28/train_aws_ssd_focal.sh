#!/bin/sh

cd /tensorflow/models/research
PIPELINE_CONFIG_PATH=/configs/tunnel_circuit/ssd_mobilenet_v1_coco_2018_01_28/aws_ssd_focal.config
MODEL_DIR=/checkpoints/ssd_mobilenet_v1_coco_2018_01_28/aws_ssd_focal
python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --alsologtostderr
