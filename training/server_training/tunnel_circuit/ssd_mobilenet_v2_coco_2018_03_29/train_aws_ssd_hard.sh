#!/bin/sh

cd /tensorflow/models/research
PIPELINE_CONFIG_PATH=/configs/tunnel_circuit/ssd_mobilenet_v2_coco_2018_03_29/aws_ssd_hard.config
MODEL_DIR=/checkpoints/ssd_mobilenet_v2_coco_2018_03_29/aws_ssd_hard
python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --alsologtostderr
