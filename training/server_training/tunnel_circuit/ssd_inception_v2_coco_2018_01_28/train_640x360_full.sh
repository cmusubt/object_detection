#!/bin/sh

cd /tensorflow/models/research
PIPELINE_CONFIG_PATH=/configs/tunnel_circuit/ssd_inception_v2_coco_2018_01_28/640x360.config
MODEL_DIR=/checkpoints/ssd_inception_v2_coco_2018_01_28/640x360
python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --alsologtostderr
