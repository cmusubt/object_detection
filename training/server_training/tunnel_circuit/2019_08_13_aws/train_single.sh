#!/bin/bash

set -x

base_dir=/home/ubuntu/training
object_detection=$base_dir/object_detection

# Positives only dataset, fixed.
nvidia-docker run \
    --detach \
    -v $base_dir/base_checkpoints:/models \
    -v $base_dir/data/dataset_08_13:/dataset \
    -v $object_detection/training/models/pipeline_configs:/configs \
    -v $base_dir/checkpoints:/checkpoints \
    -v $object_detection/training/server_training/tunnel_circuit:/training \
    -e CUDA_VISIBLE_DEVICES=0 \
    vasua/objdet:latest \
    /training/ssd_mobilenet_v1_coco_2018_01_28/train_aws_ssd_hard.sh
