#!/bin/sh

cd /tensorflow/models/research
PIPELINE_CONFIG_PATH=/configs/tunnel_circuit/thermal_ssd_mobilenet_v1_coco_2019_07_10/train_640_512.config 
MODEL_DIR=/checkpoints
echo ${PIPELINE_CONFIG_PATH}
echo ${MODEL_DIR}
python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --alsologtostderr
