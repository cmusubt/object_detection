#!/bin/bash

set -x

# SLURM Resource parameters
#SBATCH --ntasks=1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks-per-core=1
#SBATCH -t 0-00:05
#SBATCH -p gpu
#SBATCH --gres=gpu:1
#SBATCH --mem=100000
#SBATCH --job-name=train_thermal_test
#SBATCH -o job_%j.out
#SBATCH -e job_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hengruiz@andrew.cmu.edu

uname -a

nvidia-docker run \
    --detach \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /home/hengruiz/Documents/checkpoints/randy_positive_640_512:/checkpoints \
    -v /project/subt/data/tunnel_circuit_dataset/thermal_dataset/randy_positive_640_512:/dataset \
    -v /home/hengruiz/Documents/object_detection/training/models/pipeline_configs:/configs \
    -v /home/hengruiz/Documents/object_detection/training/server_training/tunnel_circuit/thermal_ssd_mobilenet_v1_coco_2019_07_10:/script \
    -e CUDA_VISIBLE_DEVICES=1 \
    vasua/objdet:latest \
    /script/train_thermal_2019_07_10_randy.sh \
