#!/bin/bash

set -x

base_dir=/home/ubuntu/training
object_detection=$base_dir/object_detection

# https://stackoverflow.com/questions/8880603/loop-through-an-array-of-strings-in-bash
declare -a scripts=(
    "/training/ssd_mobilenet_v1_coco_2018_01_28/train_aws_ssd_hard.sh"
    "/training/ssd_mobilenet_v1_coco_2018_01_28/train_aws_ssd_lite_hard.sh"
    "/training/ssd_mobilenet_v1_coco_2018_01_28/train_aws_ssd_focal.sh"
    "/training/ssd_mobilenet_v1_coco_2018_01_28/train_aws_ssd_lite_focal.sh"
    "/training/ssd_mobilenet_v2_coco_2018_03_29/train_aws_ssd_hard.sh"
    "/training/ssd_mobilenet_v2_coco_2018_03_29/train_aws_ssd_lite_hard.sh"
    "/training/ssd_mobilenet_v2_coco_2018_03_29/train_aws_ssd_focal.sh"
    "/training/ssd_mobilenet_v2_coco_2018_03_29/train_aws_ssd_lite_focal.sh"
)

arraylength=${#scripts[@]}

for (( i=0; i<${arraylength}; i++ )); do
    echo "Script $i: ${scripts[i]}"

    nvidia-docker run \
        --detach \
        -v $base_dir/base_checkpoints:/models \
        -v $base_dir/data/dataset_08_08:/dataset \
        -v $object_detection/training/models/pipeline_configs:/configs \
        -v $base_dir/checkpoints:/checkpoints \
        -v $object_detection/training/server_training/tunnel_circuit:/training \
        -e CUDA_VISIBLE_DEVICES=$i \
        vasua/objdet:latest \
        ${scripts[i]}
done
