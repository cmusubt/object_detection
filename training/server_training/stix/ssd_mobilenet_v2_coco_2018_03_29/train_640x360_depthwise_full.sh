#!/bin/sh

cd /tensorflow/models/research
PIPELINE_CONFIG_PATH=/configs/stix/ssd_mobilenet_v2_coco_2018_03_29/640x360_depthwise.config
MODEL_DIR=/checkpoints/ssd_mobilenet_v2_coco_2018_03_29/640x360_depthwise
python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --alsologtostderr
