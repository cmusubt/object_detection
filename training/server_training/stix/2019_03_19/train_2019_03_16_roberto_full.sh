#!/bin/bash

# SLURM Resource parameters
#SBATCH -w roberto
#SBATCH --ntasks=1
#SBATCH -N 1
#SBATCH --cpus-per-task=8
#SBATCH --ntasks-per-core=1
#SBATCH -t 1-12:00
#SBATCH --gres=gpu:4
#SBATCH --mem=110000
#SBATCH --job-name=train_2019_03_19_roberto
#SBATCH -o job_%j.out
#SBATCH -e job_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vasua@andrew.cmu.edu

set -x

# First 3 are for full dataset
nvidia-docker run \
    --detach \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /data/datasets/vasua/stix/full_fixed:/dataset \
    -v /home/vasua/Documents/object_detection/training/models/pipeline_configs:/configs \
    -v /home/vasua/Documents/checkpoints/2019_03_19/full:/checkpoints \
    -v /home/vasua/Documents/object_detection/training/server_training:/training \
    -e CUDA_VISIBLE_DEVICES=0 \
    vasua/objdet:latest \
    /training/stix/ssd_inception_v2_coco_2018_01_28/train_640x360_full.sh

nvidia-docker run \
    --detach \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /data/datasets/vasua/stix/full_fixed:/dataset \
    -v /home/vasua/Documents/object_detection/training/models/pipeline_configs:/configs \
    -v /home/vasua/Documents/checkpoints/2019_03_19/full:/checkpoints \
    -v /home/vasua/Documents/object_detection/training/server_training:/training \
    -e CUDA_VISIBLE_DEVICES=1 \
    vasua/objdet:latest \
    /training/stix/ssd_mobilenet_v1_coco_2018_01_28/train_640x360_full.sh

nvidia-docker run \
    --detach \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /data/datasets/vasua/stix/full_fixed:/dataset \
    -v /home/vasua/Documents/object_detection/training/models/pipeline_configs:/configs \
    -v /home/vasua/Documents/checkpoints/2019_03_19/full:/checkpoints \
    -v /home/vasua/Documents/object_detection/training/server_training:/training \
    -e CUDA_VISIBLE_DEVICES=2 \
    vasua/objdet:latest \
    /training/stix/ssd_mobilenet_v2_coco_2018_03_29/train_640x360_depthwise_full.sh

# Next 3 are positives only. Other 2 on clamps.
nvidia-docker run \
    --detach \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /data/datasets/vasua/stix/positives_only_fixed:/dataset \
    -v /home/vasua/Documents/object_detection/training/models/pipeline_configs:/configs \
    -v /home/vasua/Documents/checkpoints/2019_03_19/positives:/checkpoints \
    -v /home/vasua/Documents/object_detection/training/server_training:/training \
    -e CUDA_VISIBLE_DEVICES=3 \
    vasua/objdet:latest \
    /training/stix/ssd_inception_v2_coco_2018_01_28/train_640x360_full.sh
