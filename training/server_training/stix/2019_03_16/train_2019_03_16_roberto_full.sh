#!/bin/bash

# SLURM Resource parameters
#SBATCH -w roberto
#SBATCH --ntasks=1
#SBATCH -N 1
#SBATCH --cpus-per-task=8
#SBATCH --ntasks-per-core=1
#SBATCH -t 1-12:00
#SBATCH --gres=gpu:4
#SBATCH --mem=110000
#SBATCH --job-name=train_2019_03_16_roberto
#SBATCH -o job_%j.out
#SBATCH -e job_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vasua@andrew.cmu.edu

set -x

nvidia-docker run \
    --detach \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /data/datasets/vasua/stix/full:/dataset \
    -v /home/vasua/Documents/object_detection/training/models/pipeline_configs:/configs \
    -v /home/vasua/Documents/checkpoints/2019_03_16:/checkpoints \
    -v /home/vasua/Documents/object_detection/training/server_training:/training \
    -e CUDA_VISIBLE_DEVICES=0 \
    vasua/objdet:latest \
    /training/stix/faster_rcnn_inception_v2_coco_2018_01_28/train_640x360_full.sh

nvidia-docker run \
    --detach \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /data/datasets/vasua/stix/full:/dataset \
    -v /home/vasua/Documents/object_detection/training/models/pipeline_configs:/configs \
    -v /home/vasua/Documents/checkpoints/2019_03_16:/checkpoints \
    -v /home/vasua/Documents/object_detection/training/server_training:/training \
    -e CUDA_VISIBLE_DEVICES=1 \
    vasua/objdet:latest \
    /training/stix/faster_rcnn_resnet50_coco_2018_01_28/train_640x360_full.sh

nvidia-docker run \
    --detach \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /data/datasets/vasua/stix/full:/dataset \
    -v /home/vasua/Documents/object_detection/training/models/pipeline_configs:/configs \
    -v /home/vasua/Documents/checkpoints/2019_03_16:/checkpoints \
    -v /home/vasua/Documents/object_detection/training/server_training:/training \
    -e CUDA_VISIBLE_DEVICES=2 \
    vasua/objdet:latest \
    /training/stix/ssd_inception_v2_coco_2018_01_28/train_300x300_full.sh

nvidia-docker run \
    --detach \
    -v /data/datasets/vasua/base_checkpoints:/models \
    -v /data/datasets/vasua/stix/full:/dataset \
    -v /home/vasua/Documents/object_detection/training/models/pipeline_configs:/configs \
    -v /home/vasua/Documents/checkpoints/2019_03_16:/checkpoints \
    -v /home/vasua/Documents/object_detection/training/server_training:/training \
    -e CUDA_VISIBLE_DEVICES=3 \
    vasua/objdet:latest \
    /training/stix/ssd_mobilenet_v1_coco_2018_01_28/train_300x300_full.sh
