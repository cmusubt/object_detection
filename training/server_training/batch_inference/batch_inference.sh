#!/bin/bash

# SLURM Resource parameters
#SBATCH --ntasks=4
#SBATCH -N 1
#SBATCH --cpus-per-task=1
#SBATCH --ntasks-per-core=1
#SBATCH -t 0-12:00
#SBATCH -p gpu
#SBATCH --gres=gpu:4
#SBATCH --mem=10000
#SBATCH --job-name=batch_inference
#SBATCH -o job_%j.out
#SBATCH -e job_%e.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vasua@andrew.cmu.edu

set -x

NAME=batch_inference
MOUNT_DIR=/batch_inference

nvidia-docker run -d --name $NAME -v \
    /home/hades/Documents/projects/SubT/training/batch_inference:$MOUNT_DIR \
    vasua/objdet:py3

MODEL_DIR=models
RUN_DIR=runs
LABELMAP=label.pbtxt

for model in $(ls $MODEL_DIR); do
    for run in $(ls $RUN_DIR); do
        if [[ $run == *.mkv ]];
        then
            echo "Found a .mkv file, skipping"
            continue
        fi

        nvidia-docker exec $NAME \
            $MOUNT_DIR/ftc-object-detection/training/camera_cv.py --help

        nvidia-docker exec $NAME \
            $MOUNT_DIR/ftc-object-detection/training/camera_cv.py \
            --path_to_model $MOUNT_DIR/$MODEL_DIR/$model \
            --path_to_labels $MOUNT_DIR/$LABELMAP \
            --movie $MOUNT_DIR/$RUN_DIR/$run \
            --headless \
            --write_movie
        break
    done
    break
done

docker stop $NAME 
docker rm $NAME 
