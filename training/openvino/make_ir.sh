#!/bin/bash

mo_tf=$HOME/intel/openvino_2019.1.144/deployment_tools/model_optimizer/mo_tf.py

for d in $(find $1 -type d -iname "ssd*") ; do
	echo $d
    $mo_tf \
        --input_model $d/frozen_inference_graph.pb \
        --tensorflow_use_custom_operations_config $HOME/intel/openvino_2019.1.144/deployment_tools/model_optimizer/extensions/front/tf/ssd_v2_support.json\
        --tensorflow_object_detection_api_pipeline_config $d/pipeline.config\
        --data_type=FP32 \
        --output_dir $d
done

for d in $(find $1 -type d -iname "faster_rcnn*") ; do
	echo $d
    $mo_tf \
        --input_model $d/frozen_inference_graph.pb \
        --tensorflow_use_custom_operations_config $HOME/intel/openvino_2019.1.144/deployment_tools/model_optimizer/extensions/front/tf/faster_rcnn_support.json\
        --tensorflow_object_detection_api_pipeline_config $d/pipeline.config\
        --data_type=FP32 \
        --output_dir $d
done

for d in $(find $1 -type d -iname "mask_rcnn*") ; do
	echo $d
    $mo_tf \
        --input_model $d/frozen_inference_graph.pb \
        --tensorflow_use_custom_operations_config $HOME/intel/openvino_2019.1.144/deployment_tools/model_optimizer/extensions/front/tf/mask_rcnn_support.json\
        --tensorflow_object_detection_api_pipeline_config $d/pipeline.config\
        --data_type=FP32 \
        --output_dir $d
done
