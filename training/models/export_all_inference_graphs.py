#!/usr/bin/env python3

import os
import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "---pipeline_config_path",
        help="Path to pipeline config file")
parser.add_argument("-d", "--checkpoint_folder",
        help="Directory containing checkpoint(s) to convert")

args = parser.parse_args()



def main():
    to_convert = []
    start_string = "model.ckpt-"
    for root, dirs, files in os.walk(args.checkpoint_folder):
        for f in files:
            if f.startswith(start_string) and f.endswith(".meta"):
                model_name = f[:-5]
                model_number = model_name[len(start_string):]
                full_path = os.path.join(root, model_name)
                output_dir = os.path.join(root, 
                        "output_inference_graph_%s" % model_number)

                if os.path.isdir(output_dir):
                    print("Output directory %s exists already! Skipping." %
                            output_dir)
                    continue

                to_convert.append((full_path, output_dir))
   
    key = "MODEL_RESEARCH_DIR"
    if key not in os.environ:
        print("%s not set!" % key)

    research_dir = os.environ[key]
    export_script_path = os.path.join(research_dir,
        "object_detection/export_inference_graph.py")
    for full_path, output_dir in to_convert:
        subprocess.run([
            "python3",
            export_script_path,
            "--input_type", "image_tensor",
            "--pipeline_config_path", args.pipeline_config_path,
            "--trained_checkpoint_prefix", full_path,
            "--output_directory", output_dir], check=True)


if __name__ == "__main__":
    main()
