Docker:
  * Mount all models to `/models`, such that `/models/[name]/model.ckpt/` is valid
  * Mount appropriate dataset to `/dataset` such the following are valid:
    * `/dataset/label.pbtxt`
    * `/dataset/train-*.pbtxt`
    * `/dataset/eval-*.pbtxt`
  * Mount all configs to `/configs`, such that `/configs/stix/[name]/*.config/` works
  * Mount a folder to `/checkpoints`, such that `/checkpoints/[name]` is the desired output logging folder
