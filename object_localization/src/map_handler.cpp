/**************************************************************************
map_handler.cpp
called from object_localization_node.cpp

Graeme Best (bestg@oregonstate.edu)
May 2021

Copyright Carnegie Mellon University (CMU) <2019>

This code is proprietary to the CMU SubT challenge. Do not share or distribute
without express permission of a project lead (Sebastian or Matt).
**************************************************************************/

#include "object_localization/map_handler.h"

namespace object_localization_ns {

void MapHandler::initSharedMem() {
  bool shared_mem_found = false;
  while (!shared_mem_found) {
    try { 
      shared_map_ = boost::interprocess::managed_shared_memory(
      boost::interprocess::open_only, "shared_map");
      shared_mem_found = true;
    } catch (boost::interprocess::interprocess_exception& ex){
      ROS_WARN("Could not open shared memory. Is vdbmap running?");
      ROS_WARN("Exception output: %s", ex.what());
      shared_mem_found = false;
      std::this_thread::sleep_for(std::chrono::milliseconds(1000)); // HARDCODING sleep time
    }
  }
}

bool MapHandler::selectiveDeserializeMap() {
  // deserialize if beyond a time delay

  ros::Time current_time = ros::Time::now();

  ros::Duration duration(kMinTimeBetweenMapDeserialization);

  if( (global_map_ == NULL || global_map_->empty()) || (time_last_deserialization_ + duration < current_time) ) {
    // If not initialized or sufficient time has elapsed since last deserialization
    return deserializeMap();
  } else {
    // Don't bother doing it again
    // ROS_INFO("skipping map deserialization");
  }

  return false;
}

bool MapHandler::deserializeMap() {
  auto buffer = shared_map_.find<char>("serialized_grid");
  boost::interprocess::ibufferstream shared_ibufstream(buffer.first,
                                                      buffer.second);
  ROS_INFO("Map deserialization start.");
  shared_mem_semaphore_.wait();
  semaphore_count_ -= 1;
  try {
    // Deserialize the map
    ROS_INFO("deser 1a");
    openvdb::io::Stream strm(shared_ibufstream);
    ROS_INFO("deser 1b");
    grid_ptr_vec_ = strm.getGrids();   
    ROS_INFO("deser 2");

    // Remember the time this happened (for selectiveDeserializeMap) 
    time_last_deserialization_ =  ros::Time::now();
    ROS_INFO("deser 3");

  } catch (openvdb::IoError error) {  

    ROS_WARN("Unable to deserialize grid. Shared memory might be empty. Or the strings don't match.");
    ROS_WARN("Exception output: %s", error.what());
    shared_mem_semaphore_.post();
    semaphore_count_ += 1;
    return false;
  } catch (openvdb::RuntimeError error) {
    ROS_ERROR("Caugh RuntimeError! Unable to deserialize grid. Shared memory might be empty. Or the strings don't match.");
    ROS_ERROR("Exception output: %s", error.what());
    shared_mem_semaphore_.post();
    semaphore_count_ += 1;
    return false;
  }
  ROS_INFO("deser 4");

  global_map_ = openvdb::gridPtrCast<openvdb::BoolGrid>(grid_ptr_vec_->at(0));
  ROS_INFO("deser 5");

  grid_ptr_initialized_ = true;
  shared_mem_semaphore_.post();
  semaphore_count_ += 1;
  ROS_INFO("Map deserialization finished.");

  return true;
}

bool MapHandler::raycast(const Point& sensor_location, const Point& direction_vector, Point& hit_location) {
  // ray cast from sensor_location in direction of direction_vector
  // return the first hit
  // if found, return true
  // if none found within max distance, return false

  // Setup map accessor
  openvdb::BoolGrid::Accessor acc = global_map_->getAccessor();

  // Setup sensor location
  openvdb::Vec3d sensor_location_vdb(sensor_location.x, sensor_location.y, sensor_location.z);
  openvdb::Vec3d sensor_location_ijk = global_map_->worldToIndex(sensor_location_vdb);

  // Setup direction vector
  Point direction_vector_far_away = multiply(normalizeVector(direction_vector), kRaycastMaxRange); // Needs to be far away to be not affected by the following discretization

  Point point_in_direction = addPoint(sensor_location, direction_vector_far_away);


  openvdb::Vec3d point_in_direction_ijk = global_map_->worldToIndex(openvdb::Vec3d(point_in_direction.x, point_in_direction.y, point_in_direction.z));

  openvdb::Vec3d direction_ijk(point_in_direction_ijk-sensor_location_ijk);
  double range = direction_ijk.length();
  direction_ijk.normalize();
  openvdb::math::Ray<double> ray(sensor_location_ijk, direction_ijk);
  
  openvdb::math::DDA<openvdb::math::Ray<double>, 0> dda(ray, 0., range);

  // Do the ray casting, stop when you hit something or reached end
  // want to stop before dda.time == dda.maxTime
  Point unknown_location;
  bool unknown_found = false;
  bool previously_known = false;

  while (dda.time() < dda.maxTime()) {

    // Extract the current voxel
    openvdb::Coord current_ijk(dda.voxel());

    // Check its value
    bool vox_val;
    bool vox_state = acc.probeValue(current_ijk, vox_val);

    if( vox_state==true ) {

      if( vox_val==true ) {

        // Hit found!
        openvdb::Vec3d current_xyz = global_map_->indexToWorld(current_ijk);

        hit_location.x = current_xyz.x();
        hit_location.y = current_xyz.y();
        hit_location.z = current_xyz.z();

        // Ship it!
        return true;
      }
      previously_known = true;

    } else if(vox_state==false && previously_known==true) {

      // Remember unknown boundary with free
      // This will be used if not hit found
      
      openvdb::Vec3d current_xyz = global_map_->indexToWorld(current_ijk);

      unknown_location.x = current_xyz.x();
      unknown_location.y = current_xyz.y();
      unknown_location.z = current_xyz.z();
      unknown_found = true;

      previously_known = false;

    }

    // Continue stepping along ray
    dda.step(); 
  }

  if(unknown_found) {
    // Use furtherest unknown/free boundary location instead
    hit_location = unknown_location;
  } else {
    // Juse put it at a point kNoHitDistance distance away
    hit_location = addPoint(sensor_location, multiply(normalizeVector(direction_vector),kNoHitDistance));
  }
  
  return false;

}

bool MapHandler::raycastMultipleMedian(const Point& sensor_location, const std::vector<Point>& direction_vectors, Point& hit_location) {
  // ray cast from sensor_location in direction of direction_vectors
  // find the first hit for each direction vector
  // Out of all the direction vectors, it finds the median distance
  // this distance is projected into the center of the direction vectors (middle element)
  // Returns true if hit found
  // False if no hit found

  if(global_map_ == NULL || global_map_->empty()) {
    ROS_WARN("raycast: no vdbmap received yet");
    hit_location = sensor_location;
    return false;
  }

  // Setup map accessor
  openvdb::BoolGrid::Accessor acc = global_map_->getAccessor();

  // Setup sensor location
  openvdb::Vec3d sensor_location_vdb(sensor_location.x, sensor_location.y, sensor_location.z);
  openvdb::Vec3d sensor_location_ijk = global_map_->worldToIndex(sensor_location_vdb);

  // Keep track of the closest hit out of all the pixels
  std::vector<float> distance_hits_sq;
  std::vector<float> distance_unknowns_sq;

  // Look at each direction (selected pixel)
  for(const auto& direction_vector : direction_vectors) {

    // Setup direction vector
    Point direction_vector_far_away = multiply(normalizeVector(direction_vector), kRaycastMaxRange); // Needs to be far away to be not affected by the following discretization

    Point point_in_direction = addPoint(sensor_location, direction_vector_far_away);


    openvdb::Vec3d point_in_direction_ijk = global_map_->worldToIndex(openvdb::Vec3d(point_in_direction.x, point_in_direction.y, point_in_direction.z));

    openvdb::Vec3d direction_ijk(point_in_direction_ijk-sensor_location_ijk);
    double range = direction_ijk.length();
    direction_ijk.normalize();
    openvdb::math::Ray<double> ray(sensor_location_ijk, direction_ijk);
    
    openvdb::math::DDA<openvdb::math::Ray<double>, 0> dda(ray, 0., range);

    // Do the ray casting, stop when you hit something or reached end
    // want to stop before dda.time == dda.maxTime
    Point tmp_hit_location;
    Point unknown_location;
    bool unknown_found = false;
    bool previously_known = false;
    bool hit_found = false;

    while ((dda.time() < dda.maxTime()) && (hit_found==false)) {

      // Extract the current voxel
      openvdb::Coord current_ijk(dda.voxel());

      // Check its value
      bool vox_val;
      bool vox_state = acc.probeValue(current_ijk, vox_val);

      if( vox_state==true ) {

        if( vox_val==true ) {

          // Hit found!
          openvdb::Vec3d current_xyz = global_map_->indexToWorld(current_ijk);

          tmp_hit_location.x = current_xyz.x();
          tmp_hit_location.y = current_xyz.y();
          tmp_hit_location.z = current_xyz.z();

          // Ship it!
          hit_found = true;
        }
        previously_known = true;

      } else if(vox_state==false && previously_known==true) {

        // Remember unknown boundary with free
        // This will be used if no hit found
        
        openvdb::Vec3d current_xyz = global_map_->indexToWorld(current_ijk);

        unknown_location.x = current_xyz.x();
        unknown_location.y = current_xyz.y();
        unknown_location.z = current_xyz.z();
        unknown_found = true;

        previously_known = false;

      }

      // Continue stepping along ray
      dda.step(); 
    }

    // If no hit found, use the closest free/unknown boundary, otherwise project to a fixed distance
    if(hit_found==false) {
      if(unknown_found==true) {
        // Use furtherest unknown/free boundary location instead
        tmp_hit_location = unknown_location;
      } else {
        // Juse put it at a point kNoHitDistance distance away
        tmp_hit_location = addPoint(sensor_location, multiply(normalizeVector(direction_vector),kNoHitDistance));
      }
    }

    // Extract the distance
    float distance_sq = normSq(subtract(tmp_hit_location, sensor_location));

    // Remember it if it is better than previous distance
    if(hit_found) { 
      distance_hits_sq.push_back(distance_sq);
    } else if(unknown_found) {
      distance_unknowns_sq.push_back(distance_sq);
    }
  }

  // Extract the median distance of hits. if no hits, extract median distance of unknows
  float median_distance;
  if(distance_hits_sq.size() > 0) {

    // Median of all hits
    sort(distance_hits_sq.begin(), distance_hits_sq.end());
    median_distance = sqrt(distance_hits_sq[floor(distance_hits_sq.size() / 2)]);

  } else if(distance_unknowns_sq.size() > 0) {

    // Median of all unknowns
    sort(distance_unknowns_sq.begin(), distance_unknowns_sq.end());
    median_distance = sqrt(distance_unknowns_sq[floor(distance_unknowns_sq.size() / 2)]);

  } else {

    // Put it a fixed distance away
    median_distance = kNoHitDistance;
  }

  // Project the point into the center of the pixels at the median distance found

  // Get the center direction (assumed to be the middle element, which should be correct)
  int center_index = (int)floor((float)direction_vectors.size()/2.0);
  Point center_direction_vector = direction_vectors[center_index];

  hit_location = addPoint(sensor_location, multiply(normalizeVector(center_direction_vector), median_distance));
  
  // Return true if an actual hit was found
  if(distance_hits_sq.size() > 0) {
    return true;
  } else {
    return false;
  }

}

bool MapHandler::raycastMultipleMinimum(const Point& sensor_location, const std::vector<Point>& direction_vectors, Point& hit_location) {
  // ray cast from sensor_location in direction of direction_vectors
  // find the first hit for each direction vector
  // Out of all the direction vectors, it finds the one with the closest distance
  // this distance is projected into the center of the direction vectors (middle element)
  // Returns true if hit found
  // False if not hit found

  // Setup map accessor
  openvdb::BoolGrid::Accessor acc = global_map_->getAccessor();

  // Setup sensor location
  openvdb::Vec3d sensor_location_vdb(sensor_location.x, sensor_location.y, sensor_location.z);
  openvdb::Vec3d sensor_location_ijk = global_map_->worldToIndex(sensor_location_vdb);

  // Keep track of the closest hit out of all the pixels
  float closest_hit_distance_sq = 0.0;
  bool closest_hit_found = false;
  bool closest_unknown_found = false;

  // Look at each direction (selected pixel)
  for(const auto& direction_vector : direction_vectors) {

    // Setup direction vector
    Point direction_vector_far_away = multiply(normalizeVector(direction_vector), kRaycastMaxRange); // Needs to be far away to be not affected by the following discretization

    Point point_in_direction = addPoint(sensor_location, direction_vector_far_away);


    openvdb::Vec3d point_in_direction_ijk = global_map_->worldToIndex(openvdb::Vec3d(point_in_direction.x, point_in_direction.y, point_in_direction.z));

    openvdb::Vec3d direction_ijk(point_in_direction_ijk-sensor_location_ijk);
    double range = direction_ijk.length();
    direction_ijk.normalize();
    openvdb::math::Ray<double> ray(sensor_location_ijk, direction_ijk);
    
    openvdb::math::DDA<openvdb::math::Ray<double>, 0> dda(ray, 0., range);

    // Do the ray casting, stop when you hit something or reached end
    // want to stop before dda.time == dda.maxTime
    Point tmp_hit_location;
    Point unknown_location;
    bool unknown_found = false;
    bool previously_known = false;
    bool hit_found = false;

    while ((dda.time() < dda.maxTime()) && (hit_found==false)) {

      // Extract the current voxel
      openvdb::Coord current_ijk(dda.voxel());

      // Check its value
      bool vox_val;
      bool vox_state = acc.probeValue(current_ijk, vox_val);

      if( vox_state==true ) {

        if( vox_val==true ) {

          // Hit found!
          openvdb::Vec3d current_xyz = global_map_->indexToWorld(current_ijk);

          tmp_hit_location.x = current_xyz.x();
          tmp_hit_location.y = current_xyz.y();
          tmp_hit_location.z = current_xyz.z();

          // Ship it!
          hit_found = true;
        }
        previously_known = true;

      } else if(vox_state==false && previously_known==true) {

        // Remember unknown boundary with free
        // This will be used if no hit found
        
        openvdb::Vec3d current_xyz = global_map_->indexToWorld(current_ijk);

        unknown_location.x = current_xyz.x();
        unknown_location.y = current_xyz.y();
        unknown_location.z = current_xyz.z();
        unknown_found = true;

        previously_known = false;

      }

      // Continue stepping along ray
      dda.step(); 
    }

    // If no hit found, use the closest free/unknown boundary, otherwise project to a fixed distance
    if(hit_found==false) {
      if(unknown_found==true) {
        // Use furtherest unknown/free boundary location instead
        tmp_hit_location = unknown_location;
      } else {
        // Juse put it at a point kNoHitDistance distance away
        tmp_hit_location = addPoint(sensor_location, multiply(normalizeVector(direction_vector),kNoHitDistance));
      }
    }

    // Extract the distance
    float distance_sq = normSq(subtract(tmp_hit_location, sensor_location));

    // Remember it if it is better than previous distance
    if(hit_found) { 
      if( !closest_hit_found || distance_sq < closest_hit_distance_sq ) {
        // Hit and it's closer than any previous hit
        closest_hit_distance_sq = distance_sq;
        closest_hit_found = true;
      }
    } else if(unknown_found) {
      if( !closest_hit_found && ( !closest_unknown_found || distance_sq < closest_hit_distance_sq ) ) {
        // Not a hit and no previous hits have happened, and it is better than previous unknown
        closest_hit_distance_sq = distance_sq;
        closest_unknown_found = true;
      }
    }
  }

  // Project the point into the center of the pixels at the minimum distance found

  // Get the center direction (assumed to be the middle element, which should be correct)
  int center_index = (int)floor((float)direction_vectors.size()/2.0);
  Point center_direction_vector = direction_vectors[center_index];

  // Project it at the distance
  float distance;
  if(closest_hit_found || closest_unknown_found) {
    // Hit or unknown found
    distance = sqrt(closest_hit_distance_sq);
  } else {
    // Put it a fixed distance away
    distance = kNoHitDistance;
  }
  hit_location = addPoint(sensor_location, multiply(normalizeVector(center_direction_vector), distance));
  
  // Return true if an actual hit was found
  if(closest_hit_found) {
    return true;
  } else {
    return false;
  }

}


bool MapHandler::readParameters(ros::NodeHandle *nh) {
  // ROS TOPIC 
  if (!nh->getParam("world_frame_id",world_frame_id_)) {
    ROS_ERROR("Cannot read parameter: world_frame_id"); return false;
  }
  if (!nh->getParam("map_voxel_dim",map_voxel_dim_)) {
    ROS_ERROR("Cannot read parameter: map_voxel_dim"); return false;
  }
  if (!nh->getParam("kRaycastMaxRange", kRaycastMaxRange)) {
    ROS_ERROR("Cannot read parameter: kRaycastMaxRange"); return false;
  }
  if (!nh->getParam("kMinTimeBetweenMapDeserialization", kMinTimeBetweenMapDeserialization)) {
    ROS_ERROR("Cannot read parameter: kMinTimeBetweenMapDeserialization"); return false;
  }
  if (!nh->getParam("kNoHitDistance", kNoHitDistance)) {
    ROS_ERROR("Cannot read parameter: kNoHitDistance"); return false;
  }

  return true;
}

MapHandler::MapHandler() : shared_mem_semaphore_(boost::interprocess::open_or_create, "shared_mem_semaphore", 1) {}

bool MapHandler::initializeMapHandler(ros::NodeHandle *nh, ros::NodeHandle *pnh) {

  if (!readParameters(pnh)) return false;

  // init shared memory
  openvdb::initialize();
  initSharedMem();
  deserialization_counter_ = 0;

  // flags
  grid_ptr_initialized_ = false;

  ROS_INFO("MapHandler initialization finished");

  return true;
}



MapHandler::~MapHandler() {
  ROS_INFO("***** Node Shutdown *****");
  // posting in case the node is shutdown before the semaphore is posted
  if (semaphore_count_ == 0) {
    ROS_INFO("Manually Posting the semaphore inside node destructor.");
    shared_mem_semaphore_.post();
    semaphore_count_ += 1;
  }
  else { ROS_INFO("Not required to manually post semaphore inside node destructor."); }
} 

} // namespace object_localization_ns
