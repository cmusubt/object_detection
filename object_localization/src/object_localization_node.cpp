/**************************************************************************
object_localization.cpp
Replaces objdet_mapper for drones
see README.md for details

Graeme Best (bestg@oregonstate.edu)
May 2021

Copyright Carnegie Mellon University (CMU) <2019>

This code is proprietary to the CMU SubT challenge. Do not share or distribute
without express permission of a project lead (Sebastian or Matt).
**************************************************************************/

#include "object_localization/object_localization_node.h"

namespace object_localization_ns {

void ObjectLocalization::callbackDetectionImage(
    const objdet_msgs::DetectionArrayConstPtr& detection_array_msg,
    const sensor_msgs::CameraInfoConstPtr& camera_info_msg,
    const sensor_msgs::ImageConstPtr& image_msg) {

  if(detection_array_msg->detections.size() > 0) {

    // Extract the images
    std::vector<sensor_msgs::ImagePtr> images_with_bounding_boxes;
    std::vector<sensor_msgs::ImagePtr> images_zoomed;

    for (int i = 0; i < detection_array_msg->detections.size(); ++i) {
      // Draw bounding box directly on the image we're sending back.
      const auto cv_image_bb = cv_bridge::toCvCopy(image_msg);
      const auto& detection = detection_array_msg->detections[i];
      bool valid1 = drawDetectionOnImage(cv_image_bb->image, detection);
      if(!valid1) {
        ROS_ERROR_STREAM("localization ignoring detection with label [" << detection.label << "] due to invalid drawDetectionOnImage");
        return;
      }
      images_with_bounding_boxes.push_back(cv_image_bb->toImageMsg());

      // Create a zoomed in image too
      const auto cv_image_zoom = cv_bridge::toCvCopy(image_msg);
      bool valid2 = createZoomedImage(cv_image_zoom->image, detection);
      if(!valid2) {
        ROS_ERROR_STREAM("localization ignoring detection with label [" << detection.label << "] due to invalid createZoomedImage");
        return;
      }
      images_zoomed.push_back(cv_image_zoom->toImageMsg());
    }

    // Process it
    detection_to_world_.preprocessDetection(*detection_array_msg, *camera_info_msg, images_with_bounding_boxes, images_zoomed);

  } else {
    // ROS_INFO("no detections");
  }
}

bool ObjectLocalization::drawDetectionOnImage(cv::Mat& image,
                                           const objdet_msgs::Detection& det) {

  // coordinates normalized to [0, 1], so we need to scale.
  const int width = image.cols;
  const int height = image.rows;
  // const int x1 = det.x1 * width;
  // const int y1 = det.y1 * height;
  // const int x2 = det.x2 * width;
  // const int y2 = det.y2 * height;
  const int x1 = std::max(0, static_cast<int>((det.x1 - 0.05) * width));
  const int y1 = std::max(0, static_cast<int>((det.y1 - 0.05) * height));
  const int x2 = std::min(width-1, static_cast<int>((det.x2 + 0.05) * width));
  const int y2 = std::min(height-1, static_cast<int>((det.y2 + 0.05) * height));

  if(width <= 0 || height <= 0 || det.x1 < 0 || det.y1 < 0 || det.x2 < 0 || det.y2 < 0) {
    ROS_ERROR_STREAM("drawDetectionOnImage " << width << " " << height << " " << det.x1 << " " << det.y1 << " " << det.x2 << " " << det.y2);
    return false;
  }

  const auto& color = BOX_COLORS[(det.id - 1) % NUM_BOX_COLORS];
  cv::rectangle(image, cv::Point2i(x1, y1), cv::Point2i(x2, y2), color, 2);
  // cv::putText(image, det.label, cv::Point2i(x1, y2 - 5),
  //             cv::FONT_HERSHEY_SIMPLEX, 0.5, color, 1);
  if (x2 - x1 > 200) {
    cv::putText(image, det.label, cv::Point2i(x1, y2 - 5),
                cv::FONT_HERSHEY_SIMPLEX, 0.5, color, 1);
  } else if (y2 + 30 < height) {
    cv::putText(image, det.label, cv::Point2i(x1, y2 + 15),
                cv::FONT_HERSHEY_SIMPLEX, 0.5, color, 1);
  } else {
    cv::putText(image, det.label, cv::Point2i(x1, y1 - 5),
                cv::FONT_HERSHEY_SIMPLEX, 0.5, color, 1);
  }

  return true;
}

bool ObjectLocalization::createZoomedImage(cv::Mat& image,
                                           const objdet_msgs::Detection& det) {

  // See https://learnopencv.com/cropping-an-image-using-opencv/
  
  // Figure out the borders
  const float border_frac = 0.05; // add a bit of space around the bounding box 

  const int width = image.cols;
  const int height = image.rows;
  const int x1 = std::max(0, static_cast<int>((det.x1 - border_frac) * width));
  const int y1 = std::max(0, static_cast<int>((det.y1 - border_frac) * height));
  const int x2 = std::min(width-1, static_cast<int>((det.x2 + border_frac) * width));
  const int y2 = std::min(height-1, static_cast<int>((det.y2 + border_frac) * height));

  // Crop it
  image = image(cv::Range(y1,y2),cv::Range(x1,x2)); // Note: height then width

  return true;
}

void ObjectLocalization::processStoredDetections() {
  // Process detections that have been preprocessed but not yet processed
  // And a sufficient delay has passed

  // ROS_INFO("processing stored detections");

  ros::Time current_time = ros::Time::now();

  ros::Duration duration(kProcessingDelayTime);

  for(Localization& localization : localization_aggregator_.localization_array_) {

    // Not processed yet
    if(!localization.processed) {

      // Sufficient time has passed
      if(localization.preprocess_time + duration < current_time) {
        detection_to_world_.processDetection(localization);
      }
    }
  }
}

bool ObjectLocalization::initCameras() {

  ros::NodeHandle *nh = get_private_node_handle();
  
  if (!nh->getParam("camera_names",camera_names_)) {
    ROS_ERROR("Cannot read parameter: camera_names"); return false;
  } 

  ROS_INFO_STREAM("ObjectLocalization num cameras: " << camera_names_.size());

  for(const auto& camera_name : camera_names_) {
    camera_handlers_.emplace_back(new CameraHandler());
    camera_handlers_.back()->initializeCameraHandler(nh, 
                                                    camera_name, 
                                                    boost::bind(&ObjectLocalization::callbackDetectionImage, this, _1, _2, _3));
  }

  return true;
}

const std::array<cv::Scalar, ObjectLocalization::NUM_BOX_COLORS>
    ObjectLocalization::BOX_COLORS = {
        cv::Scalar{230, 25, 75},   cv::Scalar{60, 180, 75},
        cv::Scalar{255, 225, 25},  cv::Scalar{0, 130, 200},
        cv::Scalar{245, 130, 48},  cv::Scalar{145, 30, 180},
        cv::Scalar{70, 240, 240},  cv::Scalar{240, 50, 230},
        cv::Scalar{210, 245, 60},  cv::Scalar{250, 190, 190},
        cv::Scalar{0, 128, 128},   cv::Scalar{230, 190, 255},
        cv::Scalar{170, 110, 40},  cv::Scalar{255, 250, 200},
        cv::Scalar{128, 0, 0},     cv::Scalar{170, 255, 195},
        cv::Scalar{128, 128, 0},   cv::Scalar{255, 215, 180},
        cv::Scalar{0, 0, 128},     cv::Scalar{128, 128, 128},
        cv::Scalar{255, 255, 255}, cv::Scalar{0, 0, 0}};

bool ObjectLocalization::readParameters() {
  ros::NodeHandle *nh = get_private_node_handle();

  if (!nh->getParam("artifact_localization_topic",artifact_localization_topic_)) {
    ROS_ERROR("Cannot read parameter: artifact_localization_topic"); return false;
  }  
  if (!nh->getParam("kAggregatedPublishPeriod",kAggregatedPublishPeriod)) {
    ROS_ERROR("Cannot read parameter: kAggregatedPublishPeriod"); return false;
  }
  if (!nh->getParam("kProcessingDelayTime",kProcessingDelayTime)) {
    ROS_ERROR("Cannot read parameter: kProcessingDelayTime"); return false;
  }

  


  return true;
}

ObjectLocalization::ObjectLocalization(std::string node_name) : BaseNode(node_name) {}

bool ObjectLocalization::initialize() {

  if (!readParameters()) return false;

  ros::NodeHandle *nh = get_node_handle();

  // ROS Init
  if (!initCameras()) return false;

  artifact_localization_pub_ = nh->advertise<objdet_msgs::ArtifactLocalizationArray>(artifact_localization_topic_, 10);

  // Mapping
  map_handler_.initializeMapHandler(nh, get_private_node_handle());

  // Aggregating
  localization_aggregator_.initializeLocalizationAggregator(nh, get_private_node_handle());

  // Initialize detection to world handler
  detection_to_world_.initializeDetectionToWorld(nh, get_private_node_handle(), &map_handler_, &localization_aggregator_, camera_names_);

  ROS_INFO("ObjectLocalization initialization finished");

  return true;
}


bool ObjectLocalization::execute() {

  ROS_INFO("tick");

  // Process unprocessed stored detections
  processStoredDetections();

  // Cluster and publish
  objdet_msgs::ArtifactLocalizationArray output_msg;
  ROS_INFO("aggregating localizations...");
  bool data_exists = localization_aggregator_.extractAggregatedLocalizations(output_msg);
  ROS_INFO_STREAM("localization clusters: " << output_msg.localizations.size());
  if(data_exists) {
    ROS_INFO("publishing localizations");
    artifact_localization_pub_.publish(output_msg);
  }

  return true;
}

ObjectLocalization::~ObjectLocalization() {

  ROS_INFO("***** Node Shutdown *****");
} 

} // namespace object_localization_ns

BaseNode *BaseNode::get() {
  object_localization_ns::ObjectLocalization *node = new object_localization_ns::ObjectLocalization("object_localization_node");
  return node;
}
