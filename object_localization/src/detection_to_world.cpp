/**************************************************************************
detection_to_world.cpp
called from object_localization_node.cpp

Graeme Best (bestg@oregonstate.edu)
May 2021

Copyright Carnegie Mellon University (CMU) <2019>

This code is proprietary to the CMU SubT challenge. Do not share or distribute
without express permission of a project lead (Sebastian or Matt).
**************************************************************************/

#include "object_localization/detection_to_world.h"

namespace object_localization_ns {

void DetectionToWorld::preprocessDetection(const objdet_msgs::DetectionArray& detection_array_msg, 
          const sensor_msgs::CameraInfo& camera_info, const std::vector<sensor_msgs::ImagePtr>& images_bb,
          const std::vector<sensor_msgs::ImagePtr>& images_zoom) {

  // Loop through detections
  for(int i = 0; i < detection_array_msg.detections.size(); ++i) {
  
    const auto& detection = detection_array_msg.detections[i];

    // Extract the sensor location
    geometry_msgs::PointStamped sensor_location_camera_frame;
    sensor_location_camera_frame.header = detection_array_msg.header;
    sensor_location_camera_frame.header.frame_id = detection_array_msg.header.frame_id;
    ros::Time t = ros::Time(sensor_location_camera_frame.header.stamp); // - ros::Duration(0.0);
    sensor_location_camera_frame.header.stamp = t;

    sensor_location_camera_frame.point.x = 0;
    sensor_location_camera_frame.point.y = 0;
    sensor_location_camera_frame.point.z = 0;
    geometry_msgs::PointStamped sensor_location_world_frame;

    try {
      tf_listener_->waitForTransform(world_frame_id_, detection_array_msg.header.frame_id, t, ros::Duration(3.0));
      tf_listener_->transformPoint(world_frame_id_, sensor_location_camera_frame, sensor_location_world_frame);
    } catch (tf::ExtrapolationException e) {
      ROS_WARN_STREAM(e.what());
      try {
        tf_listener_->transformPoint(world_frame_id_, sensor_location_camera_frame, sensor_location_world_frame);
      } catch (tf::ExtrapolationException e) {
        ROS_WARN_STREAM(e.what());
        return;
      }
    } catch (tf::LookupException e) {
      ROS_WARN_STREAM(e.what());
      return;
    }

    Point sensor_location;
    sensor_location.x = sensor_location_world_frame.point.x;
    sensor_location.y = sensor_location_world_frame.point.y;
    sensor_location.z = sensor_location_world_frame.point.z;

    // std::cout << sensor_location.x << " " << sensor_location.y << " " << sensor_location.z << std::endl;

    // Get the direction vector
    std::vector<Point> direction_vectors;
    getDirectionVectors(sensor_location_camera_frame.header, camera_info, detection, sensor_location, direction_vectors);

    // Save the preprocessed information so it can be processed later in processDetection()
    Point hit_location = sensor_location;
    ros::Time preprocess_time = ros::Time::now();
    bool processed = false;
    std_msgs::Header header = detection_array_msg.header;
    header.frame_id = world_frame_id_;
    localization_aggregator_->storeLocalization(header, hit_location, detection, images_bb[i], images_zoom[i], 
          processed, sensor_location, direction_vectors, preprocess_time);
  }

}

bool DetectionToWorld::isFarAwayHelmet(const Point& camera_location, const Point& hit_location, const bool raycast_success, const unsigned int detection_id) {
  // Return true if its a helmet detection and localized far away


  const unsigned int HELMET = 6;

  // Check class
  if(detection_id != HELMET) {
    return false;
  }

  // Check if raycasting was unsuccessful
  if(!raycast_success) {
    // Treat as far away helmet
    return true;
  }

  // Check distance (squared) is far away
  float distance_sq = normSq(subtract(camera_location, hit_location));
  if(distance_sq > kHelmetExclusionDistance_sq) {
    return true;
  }

  // Otherwise, assume not far away helmet
  return false;
}

void DetectionToWorld::processDetection(Localization& localization) {
  // Localization struct defined in localization_aggregator.h

  // Deserialize the map if necessary
  map_handler_->selectiveDeserializeMap();

  // ROS_INFO("doing ray casting");
  Point hit_location;
  bool raycast_success = map_handler_->raycastMultipleMedian(localization.camera_location, localization.direction_vectors, hit_location);

  bool is_far_away_helmet = isFarAwayHelmet(localization.camera_location, hit_location, raycast_success, localization.detection.id);

  if(is_far_away_helmet) {
    // Ignore this detection
    ROS_WARN("Ignoring far away helmet detection!");
    localization.is_excluded = true;
    localization.has_been_aggregated = true;
  } else {
    // Ignore this detection
    localization.is_excluded = false;
  }

  // Update the entry
  localization.location = hit_location;
  localization.processed = true;

  // Output some markers for debugging
  // ROS_INFO("doing markers");
  std_msgs::Header header = localization.header;
  header.frame_id = world_frame_id_;
  bool is_confident = localization.detection.confidence >= kMinClusterConfidenceThreshold;
  publishMarkers(header, localization.camera_location, hit_location, raycast_success, localization.is_excluded, is_confident);
}



void DetectionToWorld::initMarkers() {
  
  marker_sensor_.ns = "sensor";
  marker_sensor_.action = visualization_msgs::Marker::ADD;
  marker_sensor_.pose.orientation.w = 1.0;
  marker_sensor_.id = marker_count_;
  marker_sensor_.type = visualization_msgs::Marker::SPHERE_LIST;
  marker_sensor_.scale.x = 0.3;
  marker_sensor_.scale.y = 0.3;
  marker_sensor_.scale.z = 0.3;
  marker_sensor_.color.a = 1.0;
  marker_sensor_.color.r = 0.5;
  marker_sensor_.color.g = 0.5;
  marker_sensor_.color.b = 1.0;
  
  marker_hit_.ns = "hit";
  marker_hit_.action = visualization_msgs::Marker::ADD;
  marker_hit_.pose.orientation.w = 1.0;
  marker_hit_.id = marker_count_;
  marker_hit_.type = visualization_msgs::Marker::CUBE_LIST;
  marker_hit_.scale.x = 0.3;
  marker_hit_.scale.y = 0.3;
  marker_hit_.scale.z = 0.3;
  marker_hit_.color.a = 1.0;
  marker_hit_.color.r = 0.1;
  marker_hit_.color.g = 0.4;
  marker_hit_.color.b = 0.1;

  marker_hit_confident_.ns = "hit_confident";
  marker_hit_confident_.action = visualization_msgs::Marker::ADD;
  marker_hit_confident_.pose.orientation.w = 1.0;
  marker_hit_confident_.id = marker_count_;
  marker_hit_confident_.type = visualization_msgs::Marker::CUBE_LIST;
  marker_hit_confident_.scale.x = 0.3;
  marker_hit_confident_.scale.y = 0.3;
  marker_hit_confident_.scale.z = 0.3;
  marker_hit_confident_.color.a = 1.0;
  marker_hit_confident_.color.r = 0.4;
  marker_hit_confident_.color.g = 1.0;
  marker_hit_confident_.color.b = 0.4;

  marker_miss_.ns = "miss";
  marker_miss_.action = visualization_msgs::Marker::ADD;
  marker_miss_.pose.orientation.w = 1.0;
  marker_miss_.id = marker_count_;
  marker_miss_.type = visualization_msgs::Marker::SPHERE_LIST;
  marker_miss_.scale.x = 0.3;
  marker_miss_.scale.y = 0.3;
  marker_miss_.scale.z = 0.3;
  marker_miss_.color.a = 1.0;
  marker_miss_.color.r = 1.0;
  marker_miss_.color.g = 0.3;
  marker_miss_.color.b = 0.3;

  marker_excluded_.ns = "excluded";
  marker_excluded_.action = visualization_msgs::Marker::ADD;
  marker_excluded_.pose.orientation.w = 1.0;
  marker_excluded_.id = marker_count_;
  marker_excluded_.type = visualization_msgs::Marker::SPHERE_LIST;
  marker_excluded_.scale.x = 0.3;
  marker_excluded_.scale.y = 0.3;
  marker_excluded_.scale.z = 0.3;
  marker_excluded_.color.a = 1.0;
  marker_excluded_.color.r = 1.0;
  marker_excluded_.color.g = 1.0;
  marker_excluded_.color.b = 1.0;

  marker_line_.ns = "line";
  marker_line_.action = visualization_msgs::Marker::ADD;
  marker_line_.pose.orientation.w = 1.0;
  marker_line_.id = marker_count_;
  marker_line_.type = visualization_msgs::Marker::LINE_LIST;
  marker_line_.scale.x = 0.01;
  marker_line_.scale.y = 0.1;
  marker_line_.color.a = 1.0;
  marker_line_.color.r = 1.0;
  marker_line_.color.g = 1.0;
  marker_line_.color.b = 0.0;
}

void DetectionToWorld::publishMarkers(const std_msgs::Header& header, const Point& sensor_location, const Point& hit_location, const bool raycast_success, const bool is_excluded, const bool is_confident) {

  // Increment the ID so it doesn't overwrite previous message
  marker_sensor_.id = marker_count_;  
  marker_hit_.id = marker_count_;  
  marker_hit_confident_.id = marker_count_; 
  marker_miss_.id = marker_count_;
  marker_line_.id = marker_count_;  
  marker_excluded_.id = marker_count_;
  marker_count_++; // increment ID for next time

  // Update headers
  marker_sensor_.header = header;
  marker_hit_.header = header;
  marker_hit_confident_.header = header;
  marker_miss_.header = header;
  marker_line_.header = header;
  marker_excluded_.header = header;

  // Clear old data
  marker_sensor_.points.clear();
  marker_hit_.points.clear();
  marker_hit_confident_.points.clear();
  marker_miss_.points.clear();
  marker_line_.points.clear();
  marker_excluded_.points.clear();

  // Add the data
  geometry_msgs::Point sensor_location_gm;
  sensor_location_gm.x = sensor_location.x;
  sensor_location_gm.y = sensor_location.y;
  sensor_location_gm.z = sensor_location.z;
  geometry_msgs::Point hit_location_gm;
  hit_location_gm.x = hit_location.x;
  hit_location_gm.y = hit_location.y;
  hit_location_gm.z = hit_location.z;

  marker_sensor_.points.push_back(sensor_location_gm);
  if(is_excluded) {
    marker_excluded_.points.push_back(hit_location_gm);
  } else if(raycast_success) {
    if(is_confident) {
      marker_hit_confident_.points.push_back(hit_location_gm);
    } else {
      marker_hit_.points.push_back(hit_location_gm);
    }
  } else {
    marker_miss_.points.push_back(hit_location_gm);
  }
  marker_line_.points.push_back(sensor_location_gm);
  marker_line_.points.push_back(hit_location_gm);

  // Create single message
  visualization_msgs::MarkerArray marker_array;
  marker_array.markers.push_back(marker_sensor_);
  if(is_excluded) {
    marker_array.markers.push_back(marker_excluded_);
  } else if(raycast_success) {
    if(is_confident) {
      marker_array.markers.push_back(marker_hit_confident_);
    } else {
      marker_array.markers.push_back(marker_hit_);
    }
  } else {
    marker_array.markers.push_back(marker_miss_);
  }
  marker_array.markers.push_back(marker_line_);

  // Ship it!
  markers_pub_.publish(marker_array);
}

void DetectionToWorld::getDirectionVector(const std_msgs::Header& header, const sensor_msgs::CameraInfo& camera_info, 
            const objdet_msgs::Detection& detection, const Point& camera_location, Point& direction_vector) {
  // Return a vector in world coordinates from the odometry to the detection direction

  // Get dimensions of image
  int width = camera_info.width;
  int height = camera_info.height;
  

  // Extract the pixel of interest
  float x = (float)width * (detection.x1 + detection.x2) / 2.0;
  float y = (float)height * (detection.y1 + detection.y2) / 2.0;

  // Get that direction in camera frame
  Eigen::Vector2d img_pixel = {x,y};
  Eigen::Vector3d point_sphere = {0,0,0};
  const std::string camera_name = removeLeadingSlash(camera_info.header.frame_id);
  cameras_[camera_name]->liftProjective(img_pixel, point_sphere);

  // Convert that point world frame
  geometry_msgs::PointStamped point_camera_frame;
  point_camera_frame.header = header;
  point_camera_frame.header.frame_id = camera_info.header.frame_id;
  point_camera_frame.point.x = point_sphere[0];
  point_camera_frame.point.y = point_sphere[1];
  point_camera_frame.point.z = point_sphere[2];
  geometry_msgs::PointStamped point_world_frame;
  tf_listener_->transformPoint(world_frame_id_, point_camera_frame, point_world_frame);

  Point point_location;
  point_location.x = point_world_frame.point.x;
  point_location.y = point_world_frame.point.y;
  point_location.z = point_world_frame.point.z;

  // Convert to direction vector that gets returned
  direction_vector = subtract(point_location, camera_location);
}


void DetectionToWorld::getDirectionVectors(const std_msgs::Header& header, const sensor_msgs::CameraInfo& camera_info, 
            const objdet_msgs::Detection& detection, const Point& camera_location, std::vector<Point>& direction_vectors) {
  // Return a vector in world coordinates from the odometry to the detection direction

  // Get dimensions of image
  int width = camera_info.width;
  int height = camera_info.height;

  // ROS_INFO_STREAM("(x1, x2, y1, y2) = (" << detection.x1 << ", " << detection.x2 << ", " << detection.y1 << ", " << detection.y2 << ")");
  

  // Extract the pixels of interest
  // Grid between the min and max x and y values
  std::vector<float> xs;
  std::vector<float> ys;
  float box_width = detection.x2 - detection.x1;
  float box_height = detection.y2 - detection.y1;
  float inner_box_width = box_width*(1.0-2.0*kBoundingBoxBorder);
  float inner_box_height = box_height*(1.0-2.0*kBoundingBoxBorder);
  float inner_x1 = detection.x1+box_width*kBoundingBoxBorder;
  float inner_y1 = detection.y1+box_height*kBoundingBoxBorder;
  
  for(int i = -kNumRaysPerDirection; i <= kNumRaysPerDirection; i++) {
    
    // Interpolate between the x/y extremes
    float frac;
    if(kNumRaysPerDirection==0) {
      frac = 0.5;
    } else {
      frac = (0.5*(float)i) / ((float)kNumRaysPerDirection) + 0.5;
    }
    xs.push_back((float)width * (box_width*frac + inner_x1));
    ys.push_back((float)height * (box_height*frac + inner_y1));
  }

  // Setup vector of vectors
  direction_vectors.clear();

  // bool print_first = true;

  for(const auto& x : xs) {
    for(const auto& y : ys) {

      // Get that direction in camera frame
      Eigen::Vector2d img_pixel = {x,y};
      Eigen::Vector3d point_sphere = {0,0,0};
      const std::string camera_name = removeLeadingSlash(camera_info.header.frame_id);
      cameras_[camera_name]->liftProjective(img_pixel, point_sphere);

      // if(print_first==true) {
        // ROS_INFO_STREAM("(" << img_pixel[0] << ", " << img_pixel[1] << ") -> (" << point_sphere[0] << ", " << point_sphere[1] << ", " << point_sphere[2] << ")");
        // print_first = false;
      // }
      // Convert that point world frame
      geometry_msgs::PointStamped point_camera_frame;
      point_camera_frame.header = header;
      point_camera_frame.header.frame_id = camera_info.header.frame_id;
      point_camera_frame.point.x = point_sphere[0];
      point_camera_frame.point.y = point_sphere[1];
      point_camera_frame.point.z = point_sphere[2];
      geometry_msgs::PointStamped point_world_frame;
      tf_listener_->transformPoint(world_frame_id_, point_camera_frame, point_world_frame);

      Point point_location;
      point_location.x = point_world_frame.point.x;
      point_location.y = point_world_frame.point.y;
      point_location.z = point_world_frame.point.z;

      // Convert to direction vector that gets returned
      Point direction_vector = subtract(point_location, camera_location);
      direction_vectors.push_back(direction_vector);
    }
  }
}

void DetectionToWorld::initCamera(ros::NodeHandle *nh) {

  // Read in the intrinsics files, one per camera
  std::vector<std::string> ueye_intrinsics;
  if (!nh->getParam("ueye_intrinsics", ueye_intrinsics)) {
    ROS_ERROR("Cannot read parameter: ueye_intrinsics");
  }
  std::string intrinsics_directory;
  if (!nh->getParam("intrinsics_directory", intrinsics_directory)) {
    ROS_ERROR("Cannot read parameter: intrinsics_directory");
  }
  if (ueye_intrinsics.empty()){
    ROS_WARN("Ueye intrinsics file not provided. If this is on the drone it wont be able to localize");
  }
  else{

    if(ueye_intrinsics.size() != camera_names_.size()) {
      ROS_ERROR("Number of intrinsics files does not match number of cameras");
    }
    for(int i = 0; i < ueye_intrinsics.size(); ++i) {
      std::string filename = intrinsics_directory + ueye_intrinsics[i];
      ROS_INFO_STREAM("Creating camera for intrinsics file: " << filename);
      cameras_[camera_names_[i]] = camodocal::CameraFactory::instance()->generateCameraFromYamlFile(filename);
    }
  }
}

bool DetectionToWorld::readParameters(ros::NodeHandle *nh) {

  if (!nh->getParam("markers_topic",markers_topic_)) {
    ROS_ERROR("Cannot read parameter: markers_topic"); return false;
  } 
  if (!nh->getParam("world_frame_id",world_frame_id_)) {
    ROS_ERROR("Cannot read parameter: world_frame_id"); return false;
  }
  if (!nh->getParam("kNumRaysPerDirection", kNumRaysPerDirection)) {
    ROS_ERROR("Cannot read parameter: kNumRaysPerDirection"); return false;
  }
  if (!nh->getParam("kBoundingBoxBorder", kBoundingBoxBorder)) {
    ROS_ERROR("Cannot read parameter: kBoundingBoxBorder"); return false;
  }
  float helmet_exlusion_distance;
  if (!nh->getParam("kHelmetExclusionDistance", helmet_exlusion_distance)) {
    ROS_ERROR("Cannot read parameter: kHelmetExclusionDistance"); return false;
  } 
  kHelmetExclusionDistance_sq = helmet_exlusion_distance * helmet_exlusion_distance;
  if (!nh->getParam("kMinClusterConfidenceThreshold",kMinClusterConfidenceThreshold)) {
    ROS_ERROR("Cannot read parameter: kMinClusterConfidenceThreshold"); return false;
  }

  return true;
}

DetectionToWorld::DetectionToWorld() {}

bool DetectionToWorld::initializeDetectionToWorld(ros::NodeHandle *nh, ros::NodeHandle *pnh, 
        MapHandler* map_handler, LocalizationAggregator* localization_aggregator, 
        const std::vector<std::string>& camera_names) {

  camera_names_ = camera_names;

  if (!readParameters(pnh)) return false;

  // Init map
  map_handler_ = map_handler;
  localization_aggregator_ = localization_aggregator;

  // init markers
  initMarkers();

  // ROS Init 
  markers_pub_ = nh->advertise<visualization_msgs::MarkerArray>(markers_topic_, 1);

  // Camera
  initCamera(pnh);

  // Transform listener
  tf_listener_ = std::unique_ptr<tf::TransformListener>(new tf::TransformListener());

  ROS_INFO("DetectionToWorld initialization finished");

  return true;
}



DetectionToWorld::~DetectionToWorld() {

} 

} // namespace object_localization_ns
