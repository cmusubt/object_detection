/**************************************************************************
localization_aggregator.cpp
called from detection_to_world.cpp

Graeme Best (bestg@oregonstate.edu)
May 2021

Copyright Carnegie Mellon University (CMU) <2019>

This code is proprietary to the CMU SubT challenge. Do not share or distribute
without express permission of a project lead (Sebastian or Matt).
**************************************************************************/

#include "object_localization/localization_aggregator.h"

namespace object_localization_ns {

void LocalizationAggregator::storeLocalization(const std_msgs::Header& header, const Point& location, 
          const objdet_msgs::Detection& detection, const sensor_msgs::ImagePtr& image_bb, const sensor_msgs::ImagePtr& image_zoom,
          const bool processed, const Point& camera_location, const std::vector<Point> direction_vectors,
          const ros::Time preprocess_time) {

  // ROS_INFO("LocAgg storeLocalization");

  Localization tmp_localization;
  localization_array_.push_back(tmp_localization); // push back first to avoid another copy
  Localization& localization = localization_array_.back();

  localization.header = header;
  localization.location = location;
  localization.detection = detection;
  localization.image_bb = image_bb;
  localization.image_zoom = image_zoom;
  localization.has_image = true;
  localization.processed = processed;
  localization.camera_location = camera_location;
  localization.direction_vectors = direction_vectors;
  localization.preprocess_time = preprocess_time;

  localization.has_been_aggregated = false;
}

bool LocalizationAggregator::extractAggregatedLocalizations(objdet_msgs::ArtifactLocalizationArray& output_msg) {
  // based partly on ObjdetMapper::timer_cb()

  ROS_INFO("LocAgg extractAggregatedLocalizations");

  

  // If any detections have been recorded
  if(localization_array_.size() > 0) {

    // Cluster the localizations
    
    ///////////////
    // OLD CLUSTER METHOD
    // PCLCloudPtr pcl_localizations(new PCLCloud);
    // getPCLCloud(pcl_localizations);

    // std::vector<pcl::PointIndices> cluster_indices;
    // clusterLocalizations(pcl_localizations, cluster_indices);
    ///////////////

    std::vector<std::vector<int>> clusters;
    clusterLocalizations(clusters);

    // Get confidences for each cluster
    // Moving this here instead of in the loop
    // So that confidences can be compared between clusters
    std::vector<float> confidences;
    getClusterConfidences(clusters, confidences);

    // Get centroids for each cluster
    // Moving this here instead of in the loop
    // So that centroids can be compared between clusters
    std::vector<Point> centroids;
    getClusterCentroids(clusters, centroids);

    // Convert to msg
    output_msg.header = localization_array_.back().header;

    for(int cluster_idx = 0; cluster_idx < clusters.size(); ++cluster_idx) {

      // Check it's not empty cluster
      // Shouldn't happen
      if(clusters[cluster_idx].size() == 0) {
        continue;
      }

      // Extract which detections have associated images (not deleted)
      std::vector<int> has_images;
      for(int list_idx = 0; list_idx < clusters[cluster_idx].size(); ++list_idx) {

        // Extract the index in localization_array_
        int point_idx = clusters[cluster_idx][list_idx];

        // Remember if it has an image stored
        if(localization_array_[point_idx].has_image) {
          has_images.push_back(list_idx);
        }
      }

      // Don't publish if no images are stored in memory for this cluster
      if(has_images.size() > 0) {

        // Check if the max confidence meets minimum threshold
        if(confidences[cluster_idx] < kMinClusterConfidenceThreshold) {

          // Cluster confidence is not high enough. Do nothing.

          // Save to PCL DetCloud for debugging

          // Fill in the location
          const Point& centroid = centroids[cluster_idx];
          DetPoint pcl_point;

          pcl_point.x = centroid.x;
          pcl_point.y = centroid.y;
          pcl_point.z = centroid.z;

          // Check this is not already in there, to avoid growing list indefinitely
          bool already_there = false;
          for(const auto& p : pcl_det_centers_low_confidence_->points) {
            float x_diff = pcl_point.x - p.x;
            float y_diff = pcl_point.y - p.y;
            float z_diff = pcl_point.z - p.z;
            if(sqrt(x_diff*x_diff + y_diff*y_diff + z_diff*z_diff) < 0.01) {
              already_there = true;
              break;
            }
          }
          if(!already_there) {
            pcl_det_centers_low_confidence_->push_back(pcl_point);
          }
        } else {

          // Cluster confidence is high enough

          // Check if this cluster has one of the top confidences in region around its centroid
          bool is_clutter_in_neighborhood = false; // updated in following function
          bool is_selected_in_neighborhood = true; // updated in following function
          isClusterSelectedInNeighborhood(cluster_idx, centroids, confidences, is_clutter_in_neighborhood, is_selected_in_neighborhood);

          if(!is_selected_in_neighborhood) {

            // Region too cluttered and this detection is worse than other detections in region
            // Do nothing

            // Save to PCL DetCloud for debugging

            // Fill in the location
            const Point& centroid = centroids[cluster_idx];
            DetPoint pcl_point;

            pcl_point.x = centroid.x;
            pcl_point.y = centroid.y;
            pcl_point.z = centroid.z;

            ROS_WARN_STREAM("Skipping due to neighborhood clutter: (" << pcl_point.x << ", "
                                                                            << pcl_point.y << ", "
                                                                            << pcl_point.z << ")");

            // Check this is not already in there, to avoid growing list indefinitely
            bool already_there = false;
            for(const auto& p : pcl_det_centers_clutter_filter_->points) {
              float x_diff = pcl_point.x - p.x;
              float y_diff = pcl_point.y - p.y;
              float z_diff = pcl_point.z - p.z;
              if(sqrt(x_diff*x_diff + y_diff*y_diff + z_diff*z_diff) < 0.01) {
                already_there = true;
                break;
              }
            }
            if(!already_there) {
              pcl_det_centers_clutter_filter_->push_back(pcl_point);
            }
          } else {

            // Passed all checks, proceed to create an AritfactLocalization message from this cluster

            // Setup the new localization cluster
            objdet_msgs::ArtifactLocalization& localization_cluster = output_msg.localizations.emplace_back();

            // Pick which image indices to publish -- not all of them
            std::vector<int> image_indices;
            int step_size = ceil((float)has_images.size() / (float)kMaxNumImagesPerCluster);
            for(int i = 0; i < has_images.size(); ++i) {
              if(has_images.size() <= kMaxNumImagesPerCluster) {
                // Use all of the images
                image_indices.push_back(has_images[i]);
              } else {
                // Pick an evenly spaced subset of stored images
                if(i % step_size == 0) {
                  image_indices.push_back(has_images[i]);
                }
              }
            }

            // Get the confidence (computed above)
            localization_cluster.confidence = confidences[cluster_idx];

            // Extract the data from cluster
            int last_point_idx = 0;
            std::vector<int> class_id_counts(20,0); // 20 labels, counts initialized to 0
            for(int list_idx = 0; list_idx < clusters[cluster_idx].size(); ++list_idx) {
              
              // Extract the index in localization_array_
              int point_idx = clusters[cluster_idx][list_idx];

              // If the image has been selected, append the image
              if(std::find(image_indices.begin(), image_indices.end(), list_idx) != image_indices.end()) {
                if(is_clutter_in_neighborhood) {
                  localization_cluster.images.push_back(*localization_array_[point_idx].image_bb);
                  localization_cluster.images.push_back(*localization_array_[point_idx].image_zoom);
                } else {
                  localization_cluster.images.push_back(*localization_array_[point_idx].image_zoom);
                  localization_cluster.images.push_back(*localization_array_[point_idx].image_bb);
                }
              }

              // Remember the last point in the cluster for below
              last_point_idx = point_idx;

              // Count the id
              class_id_counts.at(localization_array_[point_idx].detection.id)++;
            }

            // Make header match last point in cluster
            localization_cluster.stamp = localization_array_[last_point_idx].header.stamp;
            localization_cluster.valid = true;

            // Set class ID as ID with most counts
            int highest_count = -1;
            int highest_index = 0;
            // Extract the argmax
            for(int class_id_counts_i = 0; class_id_counts_i < class_id_counts.size(); ++class_id_counts_i) {
              if(class_id_counts[class_id_counts_i] > highest_count) {
                highest_count = class_id_counts[class_id_counts_i];
                highest_index = class_id_counts_i;
              }
            }
            localization_cluster.class_id = highest_index;   

            // Fill in the location
            const Point& centroid = centroids[cluster_idx];

            localization_cluster.x = centroid.x;
            localization_cluster.y = centroid.y;
            localization_cluster.z = centroid.z;

            // Check that this hasn't already been published
            LocalizationSummary new_localization_summary = toLocalizationSummary(localization_cluster);

            if(alreadyPublished(new_localization_summary)) {

              // Don't publish it again
              ROS_INFO_STREAM("Skipping publish since already published: (" << new_localization_summary.x << ", "
                                                                            << new_localization_summary.y << ", "
                                                                            << new_localization_summary.z << ")");

              // Remove it from the message
              output_msg.localizations.pop_back();
            } else {

              // Keep it, don't publish again in future
              localization_summaries_.push_back(new_localization_summary);

              // Set the report ID
              localization_cluster.report_id = report_id_count_++; 

              // Save to PCL DetCloud for debugging
              // Create the point
              const Point& centroid = centroids[cluster_idx];
              DetPoint pcl_point;

              pcl_point.x = centroid.x;
              pcl_point.y = centroid.y;
              pcl_point.z = centroid.z;

              // TODO: add color here
              // pcl_point.r = ??
              // pcl_point.g = ??
              // pcl_point.b = ??

              pcl_point.label = localization_cluster.class_id;

              // Append it
              pcl_det_centers_->push_back(pcl_point);
            }
          }
        }
      }
    }

    // Debug publish
    // Setup header
    pcl_det_centers_->header.frame_id = world_frame_id_;
    pcl_conversions::toPCL(ros::Time::now(), pcl_det_centers_->header.stamp);

    // Ship it!
    ROS_INFO_STREAM("det_centers size: " << pcl_det_centers_->size());
    det_centers_pub_.publish(pcl_det_centers_);

    // Setup header
    pcl_det_centers_low_confidence_->header.frame_id = world_frame_id_;
    pcl_conversions::toPCL(ros::Time::now(), pcl_det_centers_low_confidence_->header.stamp);

    // Ship it!
    det_centers_low_confidence_pub_.publish(pcl_det_centers_low_confidence_);

    // Setup header
    pcl_det_centers_clutter_filter_->header.frame_id = world_frame_id_;
    pcl_conversions::toPCL(ros::Time::now(), pcl_det_centers_clutter_filter_->header.stamp);

    // Ship it!
    det_centers_clutter_filter_pub_.publish(pcl_det_centers_clutter_filter_);

    // Remove old localizations
    // !!Currently not functional!!
    // removeOutOfDate(); 

    // Remove old images
    clearOldImages();

    return true;
  } else {

    return false;
  }

}

bool LocalizationAggregator::alreadyPublished(const LocalizationSummary& new_summary) {
  for(const LocalizationSummary& old_summary : localization_summaries_) {
    if(equalSummary(old_summary, new_summary)) {
      return true;
    }
  }
  return false;
} 

/*
void LocalizationAggregator::getPCLCloud(PCLCloudPtr& pcl_cloud) {

  pcl_cloud->clear();

  for(const auto& localization : localization_array_) {

    if(localization.processed && !localization.is_excluded) {
      // Extract and append the point
      pcl_cloud->push_back(PCLPoint(localization.location.x,
                                          localization.location.y,
                                          localization.location.z ));
    } else {
      // Not yet valid, put it somewhere else so it's not clustered with the processed data
      pcl_cloud->push_back(PCLPoint(99999,
                                          99999,
                                          99999 ));
    }
  }
}
*/


/*
void LocalizationAggregator::clusterLocalizations(const PCLCloudPtr& pcl_localizations, std::vector<pcl::PointIndices>& cluster_indices) {
  // based on ObjdetMapper::cluster_det_centroids

  pcl::search::KdTree<PCLPoint>::Ptr localization_tree(
      new pcl::search::KdTree<PCLPoint>);
  localization_tree->setInputCloud(pcl_localizations);

  pcl::EuclideanClusterExtraction<PCLPoint> ec;
  ec.setClusterTolerance(kClusterTolerance);  // meters
  ec.setMinClusterSize(kMinClusterSize);     // number of detections
  ec.setMaxClusterSize(kMaxClusterSize);     // not sure what the default is
  ec.setSearchMethod(localization_tree);
  ec.setInputCloud(pcl_localizations);
  ec.extract(cluster_indices);

}*/

void LocalizationAggregator::clusterLocalizations(std::vector<std::vector<int>>& clusters) {
  // Do greedy clustering, while ensuring the no cluster spreads more than the distance threshold
  //
  // Return a list of mutually exclusive clusters
  // Each cluster is a list of indices into localization_array_

  // Do the clustering twice, once only for localizations that exceed confidence threshold

  // Look at each localization
  for(int localization_index = 0; localization_index < localization_array_.size(); localization_index++) {

    Localization& localization = localization_array_[localization_index];

    // Only add it to a cluster if it's already been "processed" and not "excluded"
    if(localization.processed && !localization.is_excluded) {

      localization.has_been_aggregated = true;

      // Process high confidence detections first
      // Low confidence are processed in following loop
      if(localization.detection.confidence >= kMinClusterConfidenceThreshold) {

        bool cluster_found = false;

        // Look at each existing cluster
        for(int cluster_index = 0; cluster_index < clusters.size(); cluster_index++) {

          // Check if localization can be added to this cluster
          if(canBeAddedToCluster(clusters[cluster_index], localization)) {

            // Add it to the cluster
            clusters[cluster_index].push_back(localization_index);

            // Don't add it to another cluster
            cluster_found = true;
            break;
          }
        }

        // If no cluster found, create a new cluster
        if(!cluster_found) {

          // Create new cluster with this localization
          std::vector<int>& new_cluster = clusters.emplace_back();
          new_cluster.push_back(localization_index);
        }
      }
    }
  }

  // Do the same again, but this time for low confidence clusters
  // Look at each localization
  for(int localization_index = 0; localization_index < localization_array_.size(); localization_index++) {

    Localization& localization = localization_array_[localization_index];

    // Only add it to a cluster if it's already been "processed" and not "excluded"
    if(localization.processed && !localization.is_excluded) {

      localization.has_been_aggregated = true;

      // Process low confidence detections (high confidence are done in previous loop)
      if(localization.detection.confidence < kMinClusterConfidenceThreshold) {

        bool cluster_found = false;

        // Look at each existing cluster
        for(int cluster_index = 0; cluster_index < clusters.size(); cluster_index++) {

          // Check if localization can be added to this cluster
          if(canBeAddedToCluster(clusters[cluster_index], localization)) {

            // Add it to the cluster
            clusters[cluster_index].push_back(localization_index);

            // Don't add it to another cluster
            cluster_found = true;
            break;
          }
        }

        // If no cluster found, create a new cluster
        if(!cluster_found) {

          // Create new cluster with this localization
          std::vector<int>& new_cluster = clusters.emplace_back();
          new_cluster.push_back(localization_index);
        }
      }
    }
  }

  // Remove clusters that are too small
  // https://www.techiedelight.com/remove-elements-vector-inside-loop-cpp/
  auto it = clusters.begin();
  while(it != clusters.end()) {

    // Check if cluster is too small
    if(it->size() < kMinClusterSize) {

      // Remove this cluster
      it = clusters.erase(it);
    } else {

      // Keep cluster, iterate
      ++it;
    }
  }

  // Sort each cluster so their original orders are preserved (so that images will be selected correctly)
  for(std::vector<int>& cluster : clusters) {
    std::sort(cluster.begin(), cluster.end());
  }
}

bool LocalizationAggregator::canBeAddedToCluster(const std::vector<int>& cluster, const Localization& localization) {

  // This localization can be added to this cluster only if it's not too far from *all* of the existing localizations in this cluster
  for(const int& localization_index : cluster) {
  
    // Get distance between two localizations
    float distance = norm(subtract(localization_array_[localization_index].location, localization.location));
    if(distance > kClusterTolerance) {

      // Too far away, don't accept it into cluster
      return false;
    }

  }
  return true;
}

void LocalizationAggregator::getClusterCentroids(const std::vector<std::vector<int>>& clusters, std::vector<Point>& centroids) {

  centroids.clear();

  // Loop through all clusters
  for(int cluster_index = 0; cluster_index < clusters.size(); cluster_index++) {

    // Compute centroid
    Point& centroid = centroids.emplace_back();
    computeCentroid(clusters[cluster_index], centroid);
  }

}

void LocalizationAggregator::computeCentroid(const std::vector<int>& cluster, Point& centroid) {

  centroid.x = 0;
  centroid.y = 0;
  centroid.z = 0;

  if(cluster.empty()) {
    // Invalid case
    return;
  }

  // ROS_WARN("begin computeCentroid()");

  // Sum each axis
  for(const int& idx : cluster) {
    const Point& location = localization_array_[idx].location;

    // ROS_WARN_STREAM("(" << location.x << ", " << location.y << ", " << location.z << ")");

    centroid.x += location.x;
    centroid.y += location.y;
    centroid.z += location.z;
  }

  // Divide to get means
  int s = cluster.size();
  centroid.x /= s;
  centroid.y /= s;
  centroid.z /= s;

  // ROS_WARN_STREAM("centroid: (" << centroid.x << ", " << centroid.y << ", " << centroid.z << ")");
}

void LocalizationAggregator::getClusterConfidences(const std::vector<std::vector<int>>& clusters, std::vector<float>& confidences) {

  confidences.clear();

  // Loop through all clusters
  for(int cluster_idx = 0; cluster_idx < clusters.size(); cluster_idx++) {

    // Compute the confidence as the kClusterConfidenceCountThreshold'th highest confidence 

    // ROS_INFO_STREAM("begin cluster " << cluster_idx);
    
    // Extract all the confidences in cluster
    std::vector<float> this_cluster_confidences;

    // Loop through detections in cluster
    for(int list_idx = 0; list_idx < clusters[cluster_idx].size(); ++list_idx) {
      
      // Extract the index in localization_array_
      int point_idx = clusters[cluster_idx][list_idx];
      
      // Update the confidence (pick the maximum confidence in cluster)
      this_cluster_confidences.push_back(localization_array_[point_idx].detection.confidence);

      // ROS_INFO_STREAM(localization_array_[point_idx].detection.confidence);
    }

    // Sort them in descending order
    std::sort(this_cluster_confidences.begin(), this_cluster_confidences.end(), std::greater<float>());
    
    // ROS_INFO_STREAM("sort:");
    // for(int i = 0; i < this_cluster_confidences.size(); i++) {
    //   ROS_INFO_STREAM(this_cluster_confidences[i]);
    // }


    // Pick the kClusterConfidenceCountThreshold'th highest confidence 
    if(kClusterConfidenceCountThreshold < this_cluster_confidences.size()) {
      // ROS_INFO_STREAM("result: " << this_cluster_confidences[kClusterConfidenceCountThreshold]);
      confidences.push_back(this_cluster_confidences[kClusterConfidenceCountThreshold]);
    } else {
      // Don't exceed bounds, just pick last
      if(this_cluster_confidences.empty()) {
        // Shouldn't happen
        confidences.push_back(0.0);
      } else {
        confidences.push_back(this_cluster_confidences[this_cluster_confidences.size()-1]);
      }
    }
  }
}

void LocalizationAggregator::removeOutOfDate() {

  ROS_INFO("LocAgg removeOutOfDate");/*
  ros::Time current_time = ros::Time::now();
  ros::Duration duration(kAggregatedKeepPeriod);

  // Remove all elements who's times have expired
  auto it = localization_array_.begin();
  while(it != localization_array_.end()) {

    ros::Time localization_time(it->header.stamp);

    // Has enough time passed?
    if(localization_time + duration < current_time) {
      // Remove it
      it = localization_array_.erase(it);
    } else {
      // Iterate
      ++it;
    }
  }*/
  ROS_INFO_STREAM("remaining: " << localization_array_.size());
}


void LocalizationAggregator::isClusterSelectedInNeighborhood(const int this_cluster_idx, const std::vector<Point>& centroids, const std::vector<float>& confidences, bool& is_clutter, bool& is_selected) {

  // Check if this_cluster has a confidence in the top N confidences in a radius around this cluster centroid

  const Point& this_centroid = centroids[this_cluster_idx];
  const float this_confidence = confidences[this_cluster_idx];

  // Count neighbouring clusters that are better than this cluster
  int count_higher = 0;
  int count_neighborhood = 0;

  // Loop through all other clusters
  for(int cluster_idx = 0; cluster_idx < centroids.size(); cluster_idx++) {

    // Skip this_cluster (don't compare to itself)
    if(cluster_idx != this_cluster_idx) {

      // Get distance between the two clusters
      float distance = norm(subtract(this_centroid, centroids[cluster_idx]));

      // If in range, check if cluster has confidence higher than this_cluster
      if(distance <= kClusterNeighborhoodDistance) {

        count_neighborhood++;

        // Check if other cluster has higher confidence than this cluster
        if(confidences[cluster_idx] > this_confidence) {
          count_higher++;
          if(count_higher >= kClusterNeighborhoodCountThreshold) {
            // Return false immediately since there is too much clutter
            is_clutter = true;
            is_selected = false;
            return;
          }
        }
      }
    }
  }

  // Return true if there are fewer than Threshold clusters in neighborhood that have higher confidence
  is_clutter = count_neighborhood >= kClusterNeighborhoodCountThreshold;
  is_selected = count_higher < kClusterNeighborhoodCountThreshold;
  return;
}






// See https://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process

#include "stdlib.h"
#include "stdio.h"
#include "string.h"

int parseLine(char* line){
    // This assumes that a digit will be found and the line ends in " Kb".
    int i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

int getMemoryUsage(){ //Note: this value is in KB!
    FILE* file = fopen("/proc/self/status", "r");
    int result = -1;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmRSS:", 6) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}




void LocalizationAggregator::clearOldImages() {

  // Iterate backwards
  int count_images_stored = 0;
  int count_images_deleted = 0; // debug only
  for(int i = localization_array_.size()-1; i >= 0; --i) {

    auto& localization = localization_array_[i];

    // Check if it has an image stored
    if(localization.has_image) {
      
      // If has been processed, and limit exceeded, remove it
      if( count_images_stored >= kMaxNumImagesInMemory && localization.has_been_aggregated) {

        // Remove it
        localization.has_image = false;
        localization.image_bb.reset(new sensor_msgs::Image); // Smart pointer will hopefully deallocate the image.
        localization.image_zoom.reset(new sensor_msgs::Image); // Smart pointer will hopefully deallocate the image.
        ++count_images_deleted;
      } else {

        // If not exceeded, count it
        ++count_images_stored;
      }
    } else {
      ++count_images_deleted;
    }
  }
  ROS_INFO_STREAM("images stored: " << count_images_stored << "; images deleted: " << count_images_deleted);
  ROS_INFO_STREAM("memory usage: " << getMemoryUsage() << " KB");
}

bool LocalizationAggregator::readParameters(ros::NodeHandle *nh) {

  if (!nh->getParam("det_centers_pcl_topic",det_centers_pcl_topic_)) {
    ROS_ERROR("Cannot read parameter: det_centers_pcl_topic"); return false;
  }
  if (!nh->getParam("det_centers_pcl_low_confidence_topic",det_centers_pcl_low_confidence_topic_)) {
    ROS_ERROR("Cannot read parameter: det_centers_pcl_low_confidence_topic"); return false;
  }
  if (!nh->getParam("det_centers_clutter_filter_topic",det_centers_clutter_filter_topic_)) {
    ROS_ERROR("Cannot read parameter: det_centers_clutter_filter_topic"); return false;
  } 
  if (!nh->getParam("world_frame_id",world_frame_id_)) {
    ROS_ERROR("Cannot read parameter: world_frame_id"); return false;
  }
  if (!nh->getParam("kAggregatedKeepPeriod",kAggregatedKeepPeriod)) {
    ROS_ERROR("Cannot read parameter: kAggregatedKeepPeriod"); return false;
  }
  if (!nh->getParam("kClusterTolerance",kClusterTolerance)) {
    ROS_ERROR("Cannot read parameter: kClusterTolerance"); return false;
  }
  if (!nh->getParam("kMinClusterSize",kMinClusterSize)) {
    ROS_ERROR("Cannot read parameter: kMinClusterSize"); return false;
  }
  if (!nh->getParam("kMaxClusterSize",kMaxClusterSize)) {
    ROS_ERROR("Cannot read parameter: kMaxClusterSize"); return false;
  }
  if (!nh->getParam("kMaxNumImagesPerCluster",kMaxNumImagesPerCluster)) {
    ROS_ERROR("Cannot read parameter: kMaxNumImagesPerCluster"); return false;
  }  
  if (!nh->getParam("kMaxNumImagesInMemory",kMaxNumImagesInMemory)) {
    ROS_ERROR("Cannot read parameter: kMaxNumImagesInMemory"); return false;
  }
  if (!nh->getParam("kMinClusterConfidenceThreshold",kMinClusterConfidenceThreshold)) {
    ROS_ERROR("Cannot read parameter: kMinClusterConfidenceThreshold"); return false;
  }
  if (!nh->getParam("kClusterNeighborhoodDistance",kClusterNeighborhoodDistance)) {
    ROS_ERROR("Cannot read parameter: kClusterNeighborhoodDistance"); return false;
  }  
  if (!nh->getParam("kClusterNeighborhoodCountThreshold",kClusterNeighborhoodCountThreshold)) {
    ROS_ERROR("Cannot read parameter: kClusterNeighborhoodCountThreshold"); return false;
  }
  if (!nh->getParam("kClusterConfidenceCountThreshold",kClusterConfidenceCountThreshold)) {
    ROS_ERROR("Cannot read parameter: kClusterConfidenceCountThreshold"); return false;
  }  
  

  return true;
}

LocalizationAggregator::LocalizationAggregator() {}

bool LocalizationAggregator::initializeLocalizationAggregator(ros::NodeHandle *nh, ros::NodeHandle *pnh) {

  if (!readParameters(pnh)) return false;

  // Debug clouds
  pcl_det_centers_ = DetCloudPtr(new DetCloud);
  pcl_det_centers_low_confidence_ = DetCloudPtr(new DetCloud);
  pcl_det_centers_clutter_filter_ = DetCloudPtr(new DetCloud);

  // ROS Init 
  det_centers_pub_ = nh->advertise<DetCloud>(det_centers_pcl_topic_, 1);
  det_centers_low_confidence_pub_ = nh->advertise<DetCloud>(det_centers_pcl_low_confidence_topic_, 1);
  det_centers_clutter_filter_pub_ = nh->advertise<DetCloud>(det_centers_clutter_filter_topic_, 1);

  ROS_INFO("LocalizationAggregator initialization finished");

  return true;
}



LocalizationAggregator::~LocalizationAggregator() {

} 

} // namespace object_localization_ns
