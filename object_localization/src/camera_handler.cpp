/**************************************************************************
camera_handler.cpp
called from object_localization_node.cpp

based on modules/det_info_images_sub.cpp

Graeme Best (bestg@oregonstate.edu)
June 2021

Copyright Carnegie Mellon University (CMU) <2019>

This code is proprietary to the CMU SubT challenge. Do not share or distribute
without express permission of a project lead (Sebastian or Matt).
**************************************************************************/

#include "object_localization/camera_handler.h"

namespace object_localization_ns {

CameraHandler::CameraHandler() {}

CameraHandler::~CameraHandler() {} 

} // namespace object_localization_ns
