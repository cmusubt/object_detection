#ifndef OBJECT_LOCALIZATION_CAMERA_HANDLER_H
#define OBJECT_LOCALIZATION_CAMERA_HANDLER_H

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/CameraInfo.h>
#include <string>
#include <vector>

#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>

#include <objdet_msgs/DetectionArray.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>

namespace object_localization_ns {
  class CameraHandler;
}

class object_localization_ns::CameraHandler {
 private:

  // ROS subscriber
  message_filters::Subscriber<objdet_msgs::DetectionArray> detection_sub_;
  message_filters::Subscriber<sensor_msgs::CameraInfo> camera_info_sub_;
  message_filters::Subscriber<sensor_msgs::Image> image_sub_;

  using SyncPolicy = message_filters::sync_policies::ExactTime<
      objdet_msgs::DetectionArray, sensor_msgs::CameraInfo, sensor_msgs::Image>;
  typedef message_filters::Synchronizer<SyncPolicy> Sync;
  boost::shared_ptr<Sync> sync_;

  // Functions 
  template <class C> 
  void registerCallback(const C& callback) {
    sync_->registerCallback( callback );
  }

 public:
  explicit CameraHandler();

  template <class C> 
  bool initializeCameraHandler(ros::NodeHandle *nh, const std::string camera_name, const C& callback) {

    // Setup topic names
    std::string detection_topic = "/" + camera_name + "/image_raw/detections";
    std::string camera_info_topic = "/" + camera_name + "/camera_info";
    std::string image_topic = "/" + camera_name + "/image_raw";

    // Setup the subscribers, register callback
    detection_sub_.subscribe(*nh, detection_topic, 30);
    camera_info_sub_.subscribe(*nh, camera_info_topic, 30);
    image_sub_.subscribe(*nh, image_topic, 30);
    sync_.reset(new Sync(SyncPolicy(30), detection_sub_, camera_info_sub_, image_sub_));

    registerCallback(callback);

    return true;
  }

  virtual ~CameraHandler();
};

#endif