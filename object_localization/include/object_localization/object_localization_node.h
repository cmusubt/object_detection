#ifndef OBJECT_LOCALIZATION_OBJECT_LOCALIZATION_H
#define OBJECT_LOCALIZATION_OBJECT_LOCALIZATION_H

#include <base/BaseNode.h>

#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <nav_msgs/Odometry.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <string>
#include <cstdint>
#include <geometry_msgs/Point.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseArray.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <vector>
#include <algorithm>  
#include <thread>

#include <openvdb/io/Stream.h>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/streams/bufferstream.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <objdet_msgs/DetectionArray.h>

#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>

#include <cv_bridge/cv_bridge.h>

#include "object_localization/custom_point.h"
#include "object_localization/map_handler.h"
#include "object_localization/detection_to_world.h"
#include "object_localization/localization_aggregator.h"
#include "object_localization/camera_handler.h"



namespace object_localization_ns {
  class ObjectLocalization;
}

class object_localization_ns::ObjectLocalization : public BaseNode {
 private:

  // ROS publishers, subscribers, etc.
  ros::Publisher artifact_localization_pub_;

  // Constants, parameters, etc.
  std::string artifact_localization_topic_;
  float kAggregatedPublishPeriod, kProcessingDelayTime;

  static const size_t NUM_BOX_COLORS = 22;
  static const std::array<cv::Scalar, NUM_BOX_COLORS> BOX_COLORS;

  // Map setup
  DetectionToWorld detection_to_world_;
  MapHandler map_handler_;
  LocalizationAggregator localization_aggregator_;

  // Cameras
  std::vector<std::string> camera_names_;
  std::vector<CameraHandler*> camera_handlers_;

  void callbackDetectionImage(
    const objdet_msgs::DetectionArrayConstPtr& detection_array_msg,
    const sensor_msgs::CameraInfoConstPtr& camera_info_msg,
    const sensor_msgs::ImageConstPtr& image_msg);

  // Functions
  bool readParameters();
  bool initCameras();
  // void processDetection(const objdet_msgs::DetectionArray& detection_array_msg, const sensor_msgs::CameraInfo& camera_info,
                        // const std::vector<sensor_msgs::ImagePtr>& images_with_bounding_boxes);
  void processStoredDetections();
  bool drawDetectionOnImage(cv::Mat& image, const objdet_msgs::Detection& det);
  bool createZoomedImage(cv::Mat& image, const objdet_msgs::Detection& det);

 public:
  explicit ObjectLocalization(std::string node_name);

  virtual bool initialize();
  virtual bool execute();
  virtual ~ObjectLocalization();
};

#endif