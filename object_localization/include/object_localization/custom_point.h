#ifndef OBJECT_LOCALIZATION_CUSTOM_POINT_H
#define OBJECT_LOCALIZATION_CUSTOM_POINT_H

// Define a Point
typedef struct{
  // Position
  double x;
  double y;
  double z;
}Point;

static inline Point subtract(const Point& p1, const Point& p2) {
  Point p_return;
  p_return.x = p1.x - p2.x;
  p_return.y = p1.y - p2.y;
  p_return.z = p1.z - p2.z;
  return p_return;
}

static inline Point addPoint(const Point& p1, const Point& p2) {
  Point p_return;
  p_return.x = p1.x + p2.x;
  p_return.y = p1.y + p2.y;
  p_return.z = p1.z + p2.z;
  return p_return;
}

static inline Point divide(const Point& p, double factor) {
  Point p_return;
  p_return.x = p.x / factor;
  p_return.y = p.y / factor;
  p_return.z = p.z / factor;
  return p_return;
}

static inline double norm(const Point& p) {
  return sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
}

static inline double normSq(const Point& p) {
  // use instead of norm for efficiency and when you don't need the sqrt
  return (p.x*p.x + p.y*p.y + p.z*p.z);
}

static inline Point multiply(const Point& p, double factor) {
  Point p_return;
  p_return.x = p.x * factor;
  p_return.y = p.y * factor;
  p_return.z = p.z * factor;
  return p_return;
}

static inline Point normalizeVector(const Point& p) {
  Point p_return;
  double factor = norm(p);
  p_return.x = p.x / factor;
  p_return.y = p.y / factor;
  p_return.z = p.z / factor;
  return p_return;
}


#endif