#ifndef OBJECT_LOCALIZATION_DETECTION_TO_WORLD_H
#define OBJECT_LOCALIZATION_DETECTION_TO_WORLD_H

#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <nav_msgs/Odometry.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/CameraInfo.h>
#include <string>
#include <cstdint>
#include <geometry_msgs/Point.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseArray.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <vector>
#include <algorithm>  
#include <thread>


#include <openvdb/io/Stream.h>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/streams/bufferstream.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <vdb_utils/vdb_utils.h>

#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <objdet_msgs/DetectionArray.h>

#include <modules/CameraFactory.h>
#include <modules/Camera.h>

#include "object_localization/custom_point.h"
#include "object_localization/map_handler.h"
#include "object_localization/localization_aggregator.h"

#include <unordered_map>


namespace object_localization_ns {
  class DetectionToWorld;
}

class object_localization_ns::DetectionToWorld {
 private:

  // ROS publishers, subscribers, etc.
  ros::Publisher markers_pub_;

  // Constants, parameters, etc.
  std::string markers_topic_;
  std::string world_frame_id_;
  float kBoundingBoxBorder;
  int kNumRaysPerDirection;  
  std::vector<std::string> camera_names_;
  float kHelmetExclusionDistance_sq;
  float kMinClusterConfidenceThreshold;

  // Variables
  int marker_count_ = 0;
  visualization_msgs::Marker marker_sensor_, marker_hit_, marker_miss_, marker_line_, marker_excluded_, marker_hit_confident_;

  // Map setup
  MapHandler* map_handler_;
  std::unordered_map<std::string, camodocal::CameraPtr> cameras_;
  std::unique_ptr<tf::TransformListener> tf_listener_;

  // Aggregator
  LocalizationAggregator* localization_aggregator_;

  // Functions
  bool readParameters(ros::NodeHandle *nh);
  void initMarkers();
  void initCamera(ros::NodeHandle *nh);
  
  void getDirectionVector(const std_msgs::Header& header, const sensor_msgs::CameraInfo& camera_info, 
            const objdet_msgs::Detection& detection, const Point& camera_location, Point& direction_vector);
  void getDirectionVectors(const std_msgs::Header& header, const sensor_msgs::CameraInfo& camera_info, 
            const objdet_msgs::Detection& detection, const Point& camera_location, std::vector<Point>& direction_vectors);
  
  void publishMarkers(const std_msgs::Header& header, const Point& sensor_location, const Point& hit_location, const bool raycast_success, const bool is_excluded, const bool is_confident);

  bool isFarAwayHelmet(const Point& camera_location, const Point& hit_location, const bool raycast_success, const unsigned int detection_id);


  static inline std::string removeLeadingSlash(const std::string& in) {
    if(in.at(0)=='/') {
      return in.substr(1,std::string::npos);
    } else {
      return in;
    }
  }

 public:
  DetectionToWorld();

  bool initializeDetectionToWorld(ros::NodeHandle *nh, ros::NodeHandle *pnh, 
              MapHandler* map_handler, LocalizationAggregator* localization_aggregator, 
              const std::vector<std::string>& camera_names);

  void preprocessDetection(const objdet_msgs::DetectionArray& detection_array_msg, 
          const sensor_msgs::CameraInfo& camera_info, const std::vector<sensor_msgs::ImagePtr>& images_bb,
          const std::vector<sensor_msgs::ImagePtr>& images_zoom);

  void processDetection(Localization& localization);  

  virtual ~DetectionToWorld();
};

#endif