#ifndef OBJECT_LOCALIZATION_LOCALIZATION_AGGREGATOR_H
#define OBJECT_LOCALIZATION_LOCALIZATION_AGGREGATOR_H

#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <nav_msgs/Odometry.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/CameraInfo.h>
#include <string>
#include <cstdint>
#include <geometry_msgs/Point.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseArray.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <vector>
#include <algorithm>  
#include <thread>


#include <openvdb/io/Stream.h>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/streams/bufferstream.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/common/geometry.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl_ros/point_cloud.h>

#include <vdb_utils/vdb_utils.h>

#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Header.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <objdet_msgs/DetectionArray.h>

#include <modules/CameraFactory.h>
#include <modules/Camera.h>

#include "object_localization/custom_point.h"


namespace object_localization_ns {
  class LocalizationAggregator;

  using PCLPoint = pcl::PointXYZ;
  using PCLCloud = pcl::PointCloud<PCLPoint>;
  using PCLCloudPtr = PCLCloud::Ptr;

  // for debugging only
  using DetPoint = pcl::PointXYZRGBL;
  using DetCloud = pcl::PointCloud<DetPoint>;
  using DetCloudPtr = DetCloud::Ptr;

  // Define a DetectionLocalization
  typedef struct{
    std_msgs::Header header;
    Point location;
    objdet_msgs::Detection detection;
    sensor_msgs::ImagePtr image_bb;
    sensor_msgs::ImagePtr image_zoom;
    bool has_image; // set to false if image is deleted

    bool processed; // set to true after has been processed by raycasting

    bool has_been_aggregated = false;

    Point camera_location;
    std::vector<Point> direction_vectors;
    ros::Time preprocess_time;

    bool is_excluded; // set to true if this localization should be skipped (e.g. far away helmet)
  }Localization;

  // A simpler verion of Localization, used for checking duplicates
  typedef struct{
    ros::Time stamp;
    int class_id;
    float x;
    float y;
    float z;
  }LocalizationSummary;
}


class object_localization_ns::LocalizationAggregator {
 private:

  // ROS publishers, subscribers, etc.
  ros::Publisher det_centers_pub_, det_centers_low_confidence_pub_, det_centers_clutter_filter_pub_;

  // Constants, parameters, etc.
  std::string det_centers_pcl_topic_, det_centers_pcl_low_confidence_topic_, det_centers_clutter_filter_topic_;
  std::string world_frame_id_;
  float kAggregatedKeepPeriod, kClusterTolerance, kMinClusterSize, kMaxClusterSize;
  int kMaxNumImagesPerCluster, kMaxNumImagesInMemory;
  float kMinClusterConfidenceThreshold;
  float kClusterNeighborhoodDistance;
  int kClusterNeighborhoodCountThreshold;
  int kClusterConfidenceCountThreshold;

  // Variables
  int report_id_count_ = 0;

  // Debug cloud
  DetCloudPtr pcl_det_centers_;
  DetCloudPtr pcl_det_centers_low_confidence_;
  DetCloudPtr pcl_det_centers_clutter_filter_;
  
  // Functions
  bool readParameters(ros::NodeHandle *nh);
  void removeOutOfDate();
  // void getPCLCloud(PCLCloudPtr& pcl_cloud);
  // void clusterLocalizations(const PCLCloudPtr& pcl_localizations, std::vector<pcl::PointIndices>& cluster_indices);
  void extractArtifactLocalization(const int index, objdet_msgs::ArtifactLocalization& output_localization);
  void clearOldImages();

  void clusterLocalizations(std::vector<std::vector<int>>& clusters);
  bool canBeAddedToCluster(const std::vector<int>& cluster, const Localization& localization);
  void computeCentroid(const std::vector<int>& cluster, Point& centroid);
  void getClusterConfidences(const std::vector<std::vector<int>>& clusters, std::vector<float>& confidences);
  void getClusterCentroids(const std::vector<std::vector<int>>& clusters, std::vector<Point>& centroids);
  void isClusterSelectedInNeighborhood(const int this_cluster_idx, const std::vector<Point>& centroids, const std::vector<float>& confidences, bool& is_clutter, bool& is_selected);


  // Localization summary stuff for filtering duplicate reports
  std::vector<LocalizationSummary> localization_summaries_;
  static inline LocalizationSummary toLocalizationSummary(const objdet_msgs::ArtifactLocalization& msg){
    LocalizationSummary localization_summary;
    localization_summary.stamp = msg.stamp;
    localization_summary.class_id = msg.class_id;
    localization_summary.x = msg.x;
    localization_summary.y = msg.y;
    localization_summary.z = msg.z;
    return localization_summary;
  }  
  static inline bool equalSummary(const LocalizationSummary& summary1, const LocalizationSummary& summary2) {
    if(summary1.class_id != summary2.class_id) {
      return false;
    }
    if(summary1.stamp != summary2.stamp) {
      return false;
    }
    float x_diff = summary1.x - summary2.x;
    float y_diff = summary1.y - summary2.y;
    float z_diff = summary1.z - summary2.z;

    float dist = sqrt(x_diff*x_diff + y_diff*y_diff + z_diff*z_diff);
    if(dist > 1.0) {
      return false;
    }
    return true;
  }
  bool alreadyPublished(const LocalizationSummary& new_summary);
   


 public:
  LocalizationAggregator();

  std::vector<Localization> localization_array_;

  bool initializeLocalizationAggregator(ros::NodeHandle *nh, ros::NodeHandle *pnh);

  void storeLocalization(const std_msgs::Header& header, const Point& location, 
          const objdet_msgs::Detection& detection, const sensor_msgs::ImagePtr& image_bb, const sensor_msgs::ImagePtr& image_zoom,
          const bool processed, const Point& camera_location, const std::vector<Point> direction_vectors,
          const ros::Time preprocess_time);

  bool extractAggregatedLocalizations(objdet_msgs::ArtifactLocalizationArray& output_localizations);

  virtual ~LocalizationAggregator();
};

#endif