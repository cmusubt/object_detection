#ifndef OBJECT_LOCALIZATION_MAP_HANDLER_H
#define OBJECT_LOCALIZATION_MAP_HANDLER_H

#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <nav_msgs/Odometry.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/CameraInfo.h>
#include <string>
#include <cstdint>
#include <geometry_msgs/Point.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseArray.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <vector>
#include <algorithm>  
#include <thread>

#include <openvdb/io/Stream.h>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/streams/bufferstream.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <vdb_utils/vdb_utils.h>

#include "object_localization/custom_point.h"



namespace object_localization_ns {
  class MapHandler;
}

class object_localization_ns::MapHandler {
 private:

  // ROS publishers, subscribers, etc.

  // Constants, parameters, etc.
  std::string world_frame_id_;
  float kRaycastMaxRange, kMinTimeBetweenMapDeserialization, kNoHitDistance;  

  // Variables

  // Map setup
  boost::interprocess::managed_shared_memory shared_map_;
  boost::interprocess::named_semaphore shared_mem_semaphore_;
  int semaphore_count_ = 1;
  float map_voxel_dim_;
  openvdb::BoolGrid::Ptr global_map_ = NULL;
  openvdb::GridPtrVecPtr grid_ptr_vec_;
  uint8_t deserialization_counter_ = 0;
  bool grid_ptr_initialized_;
  ros::Time time_last_deserialization_;

  // ROS callbacks

  // Functions
  bool readParameters(ros::NodeHandle *nh);
  void initSharedMem();
  bool deserializeMap();
  

 public:
  explicit MapHandler();

  bool initializeMapHandler(ros::NodeHandle *nh, ros::NodeHandle *pnh);

  bool selectiveDeserializeMap();
  bool raycast(const Point& sensor_location, const Point& direction_vector, Point& hit_location);
  bool raycastMultipleMedian(const Point& sensor_location, const std::vector<Point>& direction_vectors, Point& hit_location);
  bool raycastMultipleMinimum(const Point& sensor_location, const std::vector<Point>& direction_vectors, Point& hit_location);


  virtual ~MapHandler();
};

#endif