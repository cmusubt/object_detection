import rosbag
import glob

import numpy as np
import matplotlib.pyplot as plt

count = dict()

# filename = 'bags/run_1/base_station_2021-08-11-17-16-57_0.bag.active'
directory = 'bags/run_4/'

files = glob.glob(directory + "base_station*")

xs = []
ys = []
zs = []

topic_mem = ''
for filename in files:
    print(filename)
    for topic, msg, t in rosbag.Bag(filename).read_messages():
        
        if "artifact_localizations_uncompressed" in topic:
            topic_mem = topic
            #print(topic)
            for localization in msg.localizations:
                report_id = localization.report_id
                x = localization.x
                y = localization.y
                z = localization.z
                xs.append(x)
                ys.append(y)
                zs.append(z)
                if count.has_key(report_id):
                    count[report_id] = count[report_id] + 1
                else:
                    count[report_id] = 1
                print(str(x) + "," + str(y) + "," + str(z))

print("topic: " + topic_mem)

print(count)

num_msgs = 0
for c in count:
    num_msgs = num_msgs + count[c] 

if count.has_key(0):
    street_view = count[0]
else:
    street_view = 0
print("street view: " + str(street_view))

print("num messages: " + str(num_msgs))

num_reports = len(count)       
print("num reports: " + str(num_reports))

num_duplicates = num_msgs-num_reports-street_view+1
print("num duplicates: " + str(num_duplicates))

plt.plot(xs, ys, '.')
plt.show()
