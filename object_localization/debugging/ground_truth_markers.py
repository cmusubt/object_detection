

import rospy
import yaml
from nav_msgs.msg import Odometry
import tf
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point








def run():
	# Read in the yaml files
	with open("anchored_locations.yaml", 'r') as stream:
	    try:
	        anchored_locations = yaml.safe_load(stream)
	    except yaml.YAMLError as exc:
	        print(exc)

	with open("command_post_params.yaml", 'r') as stream:
	    try:
	        command_post_params = yaml.safe_load(stream)
	    except yaml.YAMLError as exc:
	        print(exc)


	# for artifact in anchored_locations:
	# 	print artifact['location_id']
	# 	print artifact['x']
	# 	print artifact['y']
	# 	print artifact['z']



	ids = []
	labels_tmp = []

	for artifact in command_post_params['artifacts']:
		current_id = artifact['location_id']
		ids.append(current_id)
		labels_tmp.append(artifact['category'])

	locations = []
	labels = []
	for artifact in anchored_locations:
		if artifact['location_id'] in ids:
			locations.append([artifact['x'], artifact['y'], artifact['z']])
			idx = ids.index(artifact['location_id'])
			labels.append(labels_tmp[idx])

	print locations

	# Do ROS
	rospy.init_node('ground_truth_markers')
	pub = rospy.Publisher('ground_truth_markers', Marker,queue_size=1000)

	# setup msg
	msg = Marker()
	msg.header.frame_id = 'darpa_map'

	msg.ns = "ground_truth"
	msg.id = 0
	msg.type = Marker.SPHERE_LIST;
	msg.action = Marker.ADD;
	msg.scale.x = 5.0
	msg.scale.y = 5.0
	msg.scale.z = 5.0
	msg.pose.position.x = 0;
	msg.pose.position.y = 0;
	msg.pose.position.z = 0;
	msg.pose.orientation.x = 0.0;
	msg.pose.orientation.y = 0.0;
	msg.pose.orientation.z = 0.0;
	msg.pose.orientation.w = 1.0;

	msg.color.r = 1.0
	msg.color.g = 0.0
	msg.color.b = 0.0
	msg.color.a = 0.7

	

	for loc in locations:
		point = Point(loc[0], loc[1], loc[2])
		msg.points.append(point)

	count = 0
	while not rospy.is_shutdown():
		#msg.header.stamp = 0 #rospy.get_rostime()
		#print(msg.header.stamp)
		print(count)
		count = count + 1
		pub.publish(msg)

		for i in xrange(len(locations)):
			text_marker = Marker()
			text_marker.header.frame_id = 'darpa_map'
			text_marker.ns = "label"
			text_marker.id = i
			text_marker.type = Marker.TEXT_VIEW_FACING;
			text_marker.action = Marker.ADD;
			text_marker.scale.z = 2.0
			text_marker.pose.position.x = locations[i][0];
			text_marker.pose.position.y = locations[i][1];
			text_marker.pose.position.z = locations[i][2];
			text_marker.pose.orientation.x = 0.0;
			text_marker.pose.orientation.y = 0.0;
			text_marker.pose.orientation.z = 0.0;
			text_marker.pose.orientation.w = 1.0;

			text_marker.color.r = 1.0
			text_marker.color.g = 1.0
			text_marker.color.b = 1.0
			text_marker.color.a = 1.0

			text_marker.text = labels[i]

			pub.publish(text_marker)

		rospy.sleep(5.0)
	



if __name__ == '__main__':
	run()

