import rosbag
import glob

import numpy as np

import csv
import rospy
import time
import os

import pkg_resources
pkg_resources.require("cv_bridge==1.13.0")
from cv_bridge import CvBridge, CvBridgeError
import cv2

import sys
print (sys.version)

count = dict()

# filename = 'bags/run_1/base_station_2021-08-11-17-16-57_0.bag.active'
# directory = 'bags/run_4/'
# directory = '/home/graeme/tmp/zoom_in/'
directory = '/home/r2d2/tmp/bags/2021_09_03_drones_bb/canary/'

files = glob.glob(directory + "auto*")
files.sort()
print(files)

output_directory = str(time.time()) + '/'
os.mkdir(output_directory)
print(output_directory)

output_filename = output_directory + 'artifacts' + '.csv'

xs = []
ys = []
zs = []

topic_mem = ''

report_num = 0

with open(output_filename, 'wb') as csvfile:

    csvwriter = csv.writer(csvfile, delimiter=',')

    bridge = CvBridge()

    for filename in files:
        print(filename)
        for topic, msg, t in rosbag.Bag(filename).read_messages():
            
            if "object_localization/artifact_localizations" in topic:
                topic_mem = topic
                #print(topic)
                for localization in msg.localizations:

                    report_id = localization.report_id
                    class_id = localization.class_id
                    x = localization.x
                    y = localization.y
                    z = localization.z
                    xs.append(x)
                    ys.append(y)
                    zs.append(z)
                    if count.has_key(report_id):
                        count[report_id] = count[report_id] + 1
                    else:
                        count[report_id] = 1
                    # print(str(x) + "," + str(y) + "," + str(z))

                    stamps = []
                    frame_ids = []

                    image_num = 0
                    for image in localization.images:
                        stamps.append(image.header.stamp.to_nsec())
                        frame_ids.append(image.header.frame_id)

                        cv_image = bridge.imgmsg_to_cv2(image, "bgr8")
                        image_filename = str(report_num).zfill(3) + "_" + str(report_id).zfill(3) + "_" + str(class_id).zfill(2) + "_" + str(image_num).zfill(3) + '.png'
                        image_full_filename = output_directory + image_filename
                        cv2.imwrite(image_full_filename, cv_image) 
                        #cv2.ShowImage("Image window", cv_image)
                        #cv2.WaitKey(3)
                        image_num = image_num + 1

                    report_num = report_num+1

                    csvwriter.writerow([report_id, class_id, x, y, z, stamps, frame_ids])

print("topic: " + topic_mem)

print(count)

num_msgs = 0
for c in count:
    num_msgs = num_msgs + count[c] 

if count.has_key(0):
    street_view = count[0]
else:
    street_view = 0
print("street view: " + str(street_view))

print("num messages: " + str(num_msgs))

num_reports = len(count)       
print("num reports: " + str(num_reports))

num_duplicates = num_msgs-num_reports-street_view+1
print("num duplicates: " + str(num_duplicates))

print(output_directory)
