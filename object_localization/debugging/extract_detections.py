import rosbag
import glob

import numpy as np

import csv
import rospy
import time
import os

import pkg_resources
pkg_resources.require("cv_bridge==1.13.0")
from cv_bridge import CvBridge, CvBridgeError
import cv2

import sys
print (sys.version)

count = dict()

# filename = 'bags/run_1/base_station_2021-08-11-17-16-57_0.bag.active'
# directory = 'bags/run_4/'
# directory = '/home/graeme/tmp/zoom_in/'
directory = '/home/r2d2/tmp/bags/2021_09_03_drones_bb/canary/'

files = glob.glob(directory + "camera*")
files.sort()
print(files)

output_directory = str(time.time()) + '/'
os.mkdir(output_directory)
print(output_directory)

output_filename = output_directory + 'detections' + '.csv'

xs = []
ys = []
zs = []

topic_mem = ''

report_num = 0
image_num = 0

detections = []

with open(output_filename, 'wb') as csvfile:

    csvwriter = csv.writer(csvfile, delimiter=',')

    bridge = CvBridge()

    for filename in files:
        print(filename)
        for topic, msg, t in rosbag.Bag(filename).read_messages():
            
            # bounding boxes
            if "/camera_right/image_raw/detections" == topic:
                topic_mem = topic
                print(topic)
                
                stamp = msg.header.stamp.to_nsec()

                for detection in msg.detections:

                    x1 = detection.x1
                    y1 = detection.y1
                    x2 = detection.x2
                    y2 = detection.y2
                    label = detection.label

                    data = [stamp, x1, y1, x2, y2, label]
                    detections.append(data)
                    csvwriter.writerow(data)



bridge = CvBridge()

for filename in files:
    print(filename)
    for topic, msg, t in rosbag.Bag(filename).read_messages():

        # images
        if "/camera_right/image_raw" == topic:
            topic_mem = topic
            print(topic)

            stamp = msg.header.stamp.to_nsec()

            # check if in detections list
            is_filter = False
            is_detection = False
            for detection in detections:
                if stamp == detection[0]:
                    label = detection[5]
                    x1 = detection[1]
                    y1 = detection[2]
                    x2 = detection[3]
                    y2 = detection[4]
                    if label == 'cube' or label == 'drill':
                        is_filter = True
                        break
                    is_detection = True

            if is_detection:
                cv_image = bridge.imgmsg_to_cv2(msg, "bgr8")

                shape = cv_image.shape
                width = shape[1]
                height = shape[0]

                

                if is_filter:
                    col = (100,100,255)
                    image_filename = "discard_" + str(stamp) + "_" + str(image_num).zfill(3) + ".png"
                else:
                    col = (100,255,100)
                    image_filename = "keep_" + str(stamp) + "_" + str(image_num).zfill(3) + ".png"

                bb_image = cv2.rectangle(cv_image, (int(x1*width), int(y1*height)), (int(x2*width), int(y2*height)), col, 2);

                # image_filename = str(report_num).zfill(3) + "_" + str(report_id).zfill(3) + "_" + str(class_id).zfill(2) + "_" + str(image_num).zfill(3) + '.png'
                image_full_filename = output_directory + image_filename
                cv2.imwrite(image_full_filename, bb_image) 
                #cv2.ShowImage("Image window", cv_image)
                #cv2.WaitKey(3)
                image_num = image_num + 1

# print("topic: " + topic_mem)

# print(count)

# num_msgs = 0
# for c in count:
#     num_msgs = num_msgs + count[c] 

# if count.has_key(0):
#     street_view = count[0]
# else:
#     street_view = 0
# print("street view: " + str(street_view))

# print("num messages: " + str(num_msgs))

# num_reports = len(count)       
# print("num reports: " + str(num_reports))

# num_duplicates = num_msgs-num_reports-street_view+1
# print("num duplicates: " + str(num_duplicates))

print(output_directory)
