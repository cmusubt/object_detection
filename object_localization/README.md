# object_localization

This code subscribes to visual image artifact detections and publishes (x,y,z) estimates of object locations along with associated data. 
This is intended to replace the existing `objdet_mapper` node. Initially, this is intended for the drones only.
This code is a complete rewrite of `objdet_mapper`, but the main differences are that it uses vdbmaps rather than raw keypose point-clouds, and it processes detections as they arrive rather than doing unnecessary preprocessing that is usually discarded.

## Dependencies

You will need to the following mapping-related packages somewhere in your workspace, in addition to everything in this `object_detection` stack (particularly `modules`):
```bash
git clone git@bitbucket.org:cmusubt/vdbmap.git
git clone git@bitbucket.org:cmusubt/openvdb_catkin.git
git clone git@bitbucket.org:cmusubt/vdb_utils.git
git clone git@bitbucket.org:cmusubt/catkin_simple.git
git clone git@bitbucket.org:cmusubt/common_utils.git
git clone git@bitbucket.org:cmusubt/tictoc_profiler.git
```

## Usage

To run this, run 
```bash
roslaunch object_localization object_localization.launch
```
along with a bag file.

For now, the above launch file also launches a vdbmap. This should not be done if vdbmap node is already launched from elsewhere.

## Implementation details

The node consists of the following cpp files, all compiled together as one ros node.

`object_localization_node.cpp` is the main node. it periodically processes the detections that have been processed internally by `detection_to_world`, clusters them, and publishes localized artifact estimates along with associated data. 

`detection_to_world.cpp` takes as input image object detections, converts them into (x,y,z) locations in the map frame

`map_handler.cpp` deserializes VDBmaps as necessary and contains functions relevant to the map, mainly ray tracing. Called from `detection_to_world`.

`localization_aggregator.cpp` collects the processed detections and periodically clusters them and outputs them as artifact_localization messages. Called from both `object_localization_node` and `detection_to_world`.

## Who do I talk to?
Graeme Best (bestg@oregonstate.edu)
