#include "inference/image_rotator.h"
#include <common/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <boost/bind.hpp>
#include <opencv2/core.hpp>

BaseNode* BaseNode::get() {
  auto* rotator =
      new object_detection::inference::ImageRotator("image_rotator");
  return rotator;
}

namespace object_detection {
namespace inference {
ImageRotator::ImageRotator(std::string node_name)
    : BaseNode(std::move(node_name)), it(*node_handle_) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool ImageRotator::initialize() {
  auto* nh = get_node_handle(0);

  std::vector<std::string> image_topics;
  get_private_node_handle()->getParam("topics", image_topics);

  for (const auto& topic : image_topics) {
    const auto pub_topic = topic + "_rotated";
    disable_transports(nh, pub_topic);
    image_pubs.push_back(it.advertise(pub_topic, 1));
    image_subs.push_back(it.subscribe(
        topic, 1,
        boost::bind(&ImageRotator::image_cb, this, _1, image_pubs.back())));
  }

  if (image_topics.empty()) {
    ROS_FATAL("No topics specified for image rotator");
    return false;
  }

  return true;
}

bool ImageRotator::execute() { return true; }

void ImageRotator::image_cb(const sensor_msgs::ImageConstPtr& image,
                            const image_transport::Publisher& image_pub) {
  monitor.tic("image_cb");

  const auto cv_img = cv_bridge::toCvShare(image, "");
  cv_bridge::CvImage cv_img_rot;
  cv_img_rot.header = cv_img->header;
  cv_img_rot.encoding = cv_img->encoding;

  // -1 flip code means rotate about both axes. No convenient enum provided.
  cv::flip(cv_img->image, cv_img_rot.image, -1);
  image_pub.publish(cv_img_rot.toImageMsg());
  monitor.toc("image_cb");
}

}  // namespace inference
}  // namespace object_detection
