#ifndef OBJECT_DETECTION_INFERENCE_IMAGE_UNDISTORT_H
#define OBJECT_DETECTION_INFERENCE_IMAGE_UNDISTORT_H

#include <base/BaseNode.h>
#include <image_transport/image_transport.h>
#include "modules/FisheyeModel.h"


namespace object_detection {
namespace inference {
class ImageUndistort : public BaseNode {
 public:
  ImageUndistort(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~ImageUndistort() = default;

 private:
  // undistorts image, e.g. fisheye lens using scaramuzza model
  // then publishes undistorted image
  void image_cb(const sensor_msgs::ImageConstPtr& image,
                const image_transport::Publisher& image_pub);
  image_transport::ImageTransport it;
  std::vector<image_transport::Subscriber> image_subs;
  std::vector<image_transport::Publisher> image_pubs;
  FisheyeModel fisheye_model;
  // fisheye_model = FisheyeModel();
};
}  // namespace inference
}  // namespace object_detection

#endif  // OBJECT_DETECTION_INFERENCE_IMAGE_UNDISTORT_H
