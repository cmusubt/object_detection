#ifndef OBJECT_DETECTION_INFERENCE_IMAGE_ROTATOR_H
#define OBJECT_DETECTION_INFERENCE_IMAGE_ROTATOR_H

#include <base/BaseNode.h>
#include <image_transport/image_transport.h>

namespace object_detection {
namespace inference {
class ImageRotator : public BaseNode {
 public:
  ImageRotator(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~ImageRotator() = default;

 private:
  void image_cb(const sensor_msgs::ImageConstPtr& image,
                const image_transport::Publisher& image_pub);
  image_transport::ImageTransport it;
  std::vector<image_transport::Subscriber> image_subs;
  std::vector<image_transport::Publisher> image_pubs;
};
}  // namespace inference
}  // namespace object_detection

#endif  // OBJECT_DETECTION_INFERENCE_IMAGE_ROTATOR_H
