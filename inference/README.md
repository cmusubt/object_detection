# Inference

This package runs inference using either TensorFlow (with optional TensorRT
support) on either CPU or GPU, or uses OpenVINO for optimized inference on x86
platforms.

## Tensorflow Object Detection API

This package runs inference using a model trained with the Tensorflow Object
Detection API.

### Installation

1. Install Tensorflow for GPU
1. Install the API from
   [here](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md)

### Usage

1. The node assumes camera inputs are coming in on the `/camera` channel, and
   will output detections on the `/detections` channel.
1. You can use the `single_inference.launch` file to bring up the inference
   node. It takes the model file and label map as parameters in the launch file.
   These files are provided in the git repo.
1. You can use the `vis_dets.launch` file to bring up a node which will create a
   `/vis_dets` topic that visualizes detections coming from `/detections` on
   images coming from `/camera`. Use RVIZ or similar to view the images.

### Notes

* When replaying a bag file, set `use_sim_time` to `true` if using `--clock`
  with `rosbag play`, e.g. `rosparam set use_sim_time true`.
* Current frame rate is approximately 5 Hz (on my laptop). Since tensorflow is
  lazy, it won't spin up until the first callback, but the network inference is
  fast enough that the node will catch up. You can avoid this by passing in an
  `init_image` parameter, which should be representative of the types of images
  (size, shape, lighting, etc) that will be in general use.
* The `vis_det.py` node doesn't handle time reversing (such as with a bag file
  playing on loop) gracefully. You'll likely need to manually restart the node.

### Models

Use the `export_tf_trt_graph.py` script in the `training/tensorrt` folder to
export the checkpoint files found in `inference/data` as models that are
friendly for TensorRT to load. Note that the models can't be transferred across
machines (with limited exceptions), so it's best to run the optimizer on each
deployed machine.

## OpenVINO API

### Installation

1. Install openvino https://software.intel.com/en-us/openvino-toolkit/choose-download/free-download-linux
1. Setup openvino environment

    ```
    $ source /opt/intel/computer_vision_sdk/bin/setupvars.sh
    ```
    
1.  Install [openCL runtimes](https://software.intel.com/en-us/articles/opencl-drivers)
for intel CPU and GPU (I had symlink problems with the installer for GPU, I 
fixed this by deleting all the .so files that raised errors)

### Usage

The inference node can be run by launching `modules/launch/openvino_inference.launch`.
The node assumes camera inputs are coming in from the `/rs_front/color/image` channel, and will output detections to the `/detections` channel.
The model used to run the node and the input channel can be changed in `modules/launch/openvino_inference.launch`. Simply give the path to the .xml file. NOTE: the .bin file must be included in the same directory.
You can use the `vis_dets.launch` file to bring up a node which will create a `/vis_dets` topic that visualizes detections coming from `/detections` on images coming from `/rs_front/color/image`. Use RVIZ or similar to view the images.

A simulator is available in `modules/launch/sim_bag_openvino.launch`, but requires user to change hardcoded bag file locations.

### Openvino Models:

Some tensorflow models have already been converted to a representation usable by openvino, which consist of an .xml file and a .bin file. These can be found in inference/data/openvino. To create your own xml and bin files from a tensorflow model, run training/openvino/make_ir.sh with a directory of tf models. The tf models must include the pipeline.config file and frozen_inference_graph.pb file.

### Openvino Object Detector:

The object detector is only run on CPU by default. To change this, go to modules/launch/openvino_inference.launch and change the value of device to one of:
CPU, GPU, HETERO:GPU,CPU.
