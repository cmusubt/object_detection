from __future__ import print_function
import sys
import os
from argparse import ArgumentParser
import cv2
import time
import logging as log
from openvino.inference_engine import IENetwork, IEPlugin
from object_detection.utils import label_map_util
import numpy as np
import rospy

class MultiObjectDetector(object):
    def _load_labels(self, path_to_labels):
        return label_map_util.create_category_index_from_labelmap(
            path_to_labels, use_display_name=True
        )

    # change device to "HETERO:GPU,CPU" for GPU and CPU usage
    def __init__(
        self,
        model_xml,
        path_to_labels=None,
        plugin_dir=None,
        device="CPU",
        cpu_extension=os.path.join(
            os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
            "data/openvino/libcpu_extension.so",
        ),
        prob_threshold=0.1,
        batch_size=None,
        c3_resize=False
    ):
        self.model_xml = model_xml
        self.path_to_labels = path_to_labels
        self.plugin_dir = plugin_dir
        self.cpu_extension = cpu_extension
        self.prob_threshold = prob_threshold
        self.init_cnn(model_xml, path_to_labels, plugin_dir, device, cpu_extension, prob_threshold, batch_size, c3_resize)

        """
        Args:
            batch_size: Batch size that the model is trained with. If None,
                arbitrary batch sizes will be handled. If > 0, the model will be
                passed exactly batch_size images at inference time.
        """
    def init_cnn(self,
        model_xml,
        path_to_labels=None,
        plugin_dir=None,
        device="CPU",
        cpu_extension=os.path.join(
            os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
            "data/openvino/libcpu_extension.so",
        ),
        prob_threshold=0.1,
        batch_size=None,
        c3_resize=False
    ):
        '''
        Pulled into separate function to be able to load onto GPU as fallback if necessary
        '''
        self._batch_size = batch_size
        self.c3_resize = c3_resize
        model_full_path = os.path.join(
            model_xml, "bs" + str(batch_size), "frozen_inference_graph"
        )

        # Plugin initialization for specified device and load extensions library if specified
        model_bin = model_full_path + ".bin"
        log.info("Initializing plugin for {} device...".format(device))
        plugin = IEPlugin(device=device, plugin_dirs=plugin_dir)
        if cpu_extension and "CPU" in device:
            plugin.add_cpu_extension(cpu_extension)
        # Read IR
        log.info("Reading IR...")
        net = IENetwork(model=model_full_path + ".xml", weights=model_bin)

        if "HETERO" in device:
            plugin.set_config({"TARGET_FALLBACK": device})
            plugin.set_initial_affinity(net)
            for l in net.layers.values():
                if l.type == "Convolution":
                    l.affinity = "GPU"
                else:
                    l.affinity = "CPU"

        if plugin.device == "CPU":
            supported_layers = plugin.get_supported_layers(net)
            not_supported_layers = [
                l for l in net.layers.keys() if l not in supported_layers
            ]
            if len(not_supported_layers) != 0:
                log.error(
                    "Following layers are not supported by the plugin for specified device {}:\n {}".format(
                        plugin.device, ", ".join(not_supported_layers)
                    )
                )
                log.error(
                    "Please try to specify cpu extensions library path in demo's command line parameters using -l "
                    "or --cpu_extension command line argument"
                )
                sys.exit(1)
        assert (
            len(net.inputs.keys()) == 1
        ), "Demo supports only single input topologies"
        assert (
            len(net.outputs) == 1
        ), "Demo supports only single output topologies"
        self.input_blob = next(iter(net.inputs))
        self.out_blob = next(iter(net.outputs))
        log.info("Loading IR to the plugin...") 

        self.prob_threshold = float(net.layers['DetectionOutput'].params['confidence_threshold'])

        # read and pre-process input image
        self.n, self.c, self.h, self.w = net.inputs[self.input_blob].shape
        if path_to_labels:
            self.category_index = self._load_labels(path_to_labels)
        else:
            self.category_index = None

        self.c3_left_orig_height = None

        try:
            self.exec_net = plugin.load(network=net, num_requests=2)
        except:
            rospy.logwarn("Failed to initially load CNN onto compute stick. Switching to GPU")
            self.init_cnn(self.model_xml, self.path_to_labels, self.plugin_dir, "GPU", self.cpu_extension, self.prob_threshold, self._batch_size, self.c3_resize)

    def _detect_batch(self, images):
        # Assumes that len(images) == self._batch_size, so that the images can
        # be directly passed into the detector.

        output = []

        # pass all the images in
        self.exec_net.start_async(
            request_id=0, inputs={self.input_blob: images}
        )
        # need try because we dont want node to restart and try to use myriad again
        try:
            status_code = self.exec_net.requests[0].wait(-1)
            if status_code == -1:
                rospy.logwarn("Openvino CNN failed to execute. Could be that the compute stick got jostled. Switching to GPU")
                self.init_cnn(self.model_xml, self.path_to_labels, self.plugin_dir, "GPU", self.cpu_extension, self.prob_threshold, self._batch_size, self.c3_resize)                       
        except:
            print('useless')
        
        network_out = self.exec_net.requests[0].outputs[self.out_blob]

        for i in range(len(images)):
            dd = network_out[0, 0][
                np.where(network_out[:, :, :, 0][0][0] == i)[0]
            ]
            dd = dd.reshape(1, 1, dd.shape[0], dd.shape[1])

            output.append(dd)

        return output

    def detect(self, images):
        if self._batch_size is None:  # Pass through for no batch size
            return self._detect_batch(images)
        else:  # Hard coded batch size
            # todo: do this in parallel? does this have to be done
            # or will openvino tensorflow objdet api handle it?
            for i in range(len(images)):
                if (self.c3_resize) and (images[i].shape[0] > 955):
                    # crop canary 3 left image which is not proper size
                    self.c3_left_orig_height = images[i].shape[0]
                    diff = (self.c3_left_orig_height - 955)
                    half_diff = int(diff/2.)
                    images[i] = images[i][half_diff:half_diff+955]

                images[i] = cv2.resize(images[i], (self.w, self.h))
                images[i] = images[i].transpose(
                    (2, 0, 1)
                )  # Change data layout from HWC to CHW
                images[i] = images[i].reshape((1, self.c, self.h, self.w))

            output_dicts = []
            iters = len(images) // self._batch_size
            for i in range(iters):
                batch = images[
                    i * self._batch_size : (i + 1) * self._batch_size
                ]

                output_dicts.extend(self._detect_batch(batch))

            remaining = images[(iters * self._batch_size) :]
            if len(remaining) > 0:
                padding = self._batch_size - len(remaining)
                remaining.extend([images[0]] * padding)
                output = self._detect_batch(remaining)
                output_dicts.extend(output[:-padding])
            return output_dicts

    def detect_single_image(self, image):
        # expanded_image = np.expand_dims(image, axis=0)
        detect([image])

        return out


def build_argparser():
    parser = ArgumentParser()
    parser.add_argument(
        "-m",
        "--model",
        help="Path to an .xml file with a trained model.",
        required=True,
        type=str,
    )
    parser.add_argument(
        "-i",
        "--input",
        help="Path to video file or image. 'cam' for capturing video stream from camera",
        required=True,
        type=str,
    )
    parser.add_argument(
        "-l",
        "--cpu_extension",
        help="MKLDNN (CPU)-targeted custom layers.Absolute path to a shared library with the kernels "
        "impl.",
        type=str,
        default=None,
    )
    parser.add_argument(
        "-pp",
        "--plugin_dir",
        help="Path to a plugin folder",
        type=str,
        default=None,
    )
    parser.add_argument(
        "-d",
        "--device",
        help="Specify the target device to infer on; CPU, GPU, FPGA or MYRIAD is acceptable. Demo "
        "will look for a suitable plugin for device specified (CPU by default)",
        default="CPU",
        type=str,
    )
    parser.add_argument(
        "--labels", help="Labels mapping file", default=None, type=str
    )
    parser.add_argument(
        "-pt",
        "--prob_threshold",
        help="Probability threshold for detections filtering",
        default=0.5,
        type=float,
    )

    return parser


if __name__ == "__main__":
    log.basicConfig(
        format="[ %(levelname)s ] %(message)s",
        level=log.INFO,
        stream=sys.stdout,
    )
    args = build_argparser().parse_args()
    network = MultiObjectDetector(
        args.model, os.path.splitext(args.model)[0] + ".bin", args.plugin_dir
    )
    out = network.detect(args.input, args.labels)
    print(out)
