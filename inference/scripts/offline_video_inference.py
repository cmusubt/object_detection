#!/usr/bin/env python3

import numpy as np
import drawing_utils
import cv2
import multi_object_detector
import argparse
import pathlib


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--path_to_model",
        type=str,
        required=True,
        help="Path to model (.pb file) to use for video export",
    )
    parser.add_argument(
        "--batch_size",
        type=int,
        default=1,
        help="Batch size that the model requires.",
    )
    parser.add_argument(
        "--video_path",
        type=str,
        nargs="+",
        required=True,
        help="Path(s) to arguments",
    )
    parser.add_argument(
        "--output_dir",
        type=str,
        default=None,
        help="Directory to output labeled videos",
    )
    parser.add_argument(
        "--label_map_path",
        type=str,
        default=None,
        help="Path to label map to add human readable output",
    )
    args = parser.parse_args()

    mod = multi_object_detector.MultiObjectDetector(
        args.path_to_model,
        path_to_labels=args.label_map_path,
        batch_size=args.batch_size,
    )

    for input_path in args.video_path:
        input_path = pathlib.Path(input_path)
        if not input_path.exists():
            print("Path %s doesn't exist. Skipping it!", input_path)
            continue

        if args.output_dir is None:
            output_dir = input_path.parent
        else:
            output_dir = pathlib.Path(args.output_dir)
            output_dir.mkdir(parents=True, exist_ok=True)

        cap = cv2.VideoCapture(str(input_path))
        output_path = output_dir / ("labeled_" + input_path.stem + ".mkv")
        fps = int(cap.get(cv2.CAP_PROP_FPS))
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        writer = cv2.VideoWriter(
            str(output_path),
            cv2.VideoWriter_fourcc(*"MJPG"),
            fps,
            (width, height),
        )

        frames = []

        def detect():
            detections = mod.detect(frames)
            for i, det in enumerate(detections):
                draw_detection(
                    frames[i], det, width, height, mod.category_index
                )
                writer.write(cv2.cvtColor(frames[i], cv2.COLOR_RGB2BGR))

        while True:
            ret, frame = cap.read()
            if not ret:
                break

            frames.append(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            if len(frames) == args.batch_size:
                detect()
                frames = []

        if frames:
            detect()

        writer.release()
        cap.release()


def draw_detection(frame, det, width, height, category_index):
    for i in range(det["num_detections"]):
        cls = int(det["detection_classes"][i])
        cls_name = ""
        if category_index is not None:
            cls_name = category_index[cls]["name"]
        box = det["detection_boxes"][i]
        score = det["detection_scores"][i]
        x1 = int(box[1] * width)
        x2 = int(box[3] * width)
        y1 = int(box[0] * height)
        y2 = int(box[2] * height)

        color = drawing_utils.colors[(cls - 1) % len(drawing_utils.colors)]

        cv2.rectangle(frame, (x1, y1), (x2, y2), color, 1)
        # Draw the class label
        cv2.putText(
            frame,
            "[%0.3f] %s" % (score, cls_name),
            (x1, y2 - 5),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            color,
            1,
        )


if __name__ == "__main__":
    main()
