'''
Visualizes the detections from a given bagfile on rgb and depth imagery.
Could help find rgb/depth misalignment.
To get gas info on drones make sure to include auto* bags in list of bags to analyze
'''

# Contact: Bob DeBortoli (debortor@oregonstate.edu)

import rosbag
import pdb
import copy
from image_det_vis import ImageDetVis
import cv_bridge
import drawing_utils
import cv2
import os
import argparse

bagfiles = ['/home/debortor/data/2021-04-09/DS2/auto_flight_2021-04-09-10-25-09_4.bag',
            '/home/debortor/data/2021-04-09/DS2/camera_flight_2021-04-09-10-25-09_10.bag',
            '/home/debortor/data/2021-04-09/DS2/camera_flight_2021-04-09-10-25-44_11.bag']

image_topics = ['/rs_down/color/image',
                '/rs_up/color/image',
                '/rs_front/color/image',
                '/rs_left/color/image',
                '/rs_right/color/image',
                '/rs_back/color/image',
                '/camera/image_raw',
                '/device_0/sensor_1/Color_0/image/data']

gas_topic = '/gas_info'

CV_BRIDGE = cv_bridge.CvBridge()
LAST_IMG_TIME = -1
FREQ_ALPHA = 0.9
FREQ = 0
VIDEO_WRITERS = {}
OUTPUT_FRAMERATE = 10
GAS_LEVEL_DICT = {}

parser = argparse.ArgumentParser(description="Make detection vid from bagfile")
parser.add_argument(
    "--combine",
    action="store_true",
    default=False,
    help="Combine bag files together",
)
args = parser.parse_args()


def write_dets(image_info, det_info, bag_fname):
    '''
    Return false if video already made for this bagfile, otherwise return True
    '''
    global LAST_IMG_TIME, CV_BRIDGE, FREQ, FREQ_ALPHA, VIDEO_WRITERS
    
    image_topic, image = image_info
    det_topic, dets = det_info

    color_image = CV_BRIDGE.imgmsg_to_cv2(image)  # RGB, not BGR
    height, width = color_image.shape[:2]
    for det in dets.detections:
        x1 = int(det.x1 * width)
        y1 = int(det.y1 * height)
        x2 = int(det.x2 * width)
        y2 = int(det.y2 * height)

        color = drawing_utils.colors[
            (det.id - 1) % len(drawing_utils.colors)
        ]
        cv2.rectangle(color_image, (x1, y1), (x2, y2), color, 2)
        # Draw the class label
        cv2.putText(
            color_image,
            det.label,
            (x1, y2 - 5),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            color,
            1,
        )
        # Draw other information
        cv2.putText(
            color_image,
            "[%d]: %0.3f" % (det.id, det.confidence),
            (x1, y2 - 25),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            color,
            1,
        )

    # This will be wrong for a few frames when time jumps backwards when
    # replaying a bag file, but it's correct in the standard case.
    image_time = image.header.stamp
    if LAST_IMG_TIME == -1:
        FREQ = 1
    else:
        FREQ = 1.0 / (image_time - LAST_IMG_TIME).to_sec()  # det rate
        FREQ *= FREQ_ALPHA
        FREQ += (1 - FREQ_ALPHA) * FREQ
    LAST_IMG_TIME = image_time

    drawing_utils.shadow_text(
        color_image,
        "Image Topic: %s" % image_topic,
        (10, 20),
        font_weight=1,
    )
    drawing_utils.shadow_text(
        color_image,
        "Detection Topic: %s" % det_topic,
        (10, 40),
        font_weight=1,
    )
    drawing_utils.shadow_text(
        color_image,
        "Detection Freq: %0.2f Hz" % FREQ,
        (10, 60),
        font_weight=1,
    )
    
    # find the closest gas reading
    gas_time = min(GAS_LEVEL_DICT.keys(), key=lambda x:abs(x-image_time.secs))
    if (abs(gas_time - image_time.secs) < 2):
        gas_val = GAS_LEVEL_DICT[gas_time]
    else:
        gas_val = -1
    
    drawing_utils.shadow_text(
        color_image,
        "CO2 conc.: %0.1f ppm" % gas_val,
        (10, 80),
        font_weight=1,
    )

    if image_topic not in VIDEO_WRITERS.keys():
        out_fname = bag_fname[:-4] + '_' + det_topic[1:].replace('/','_')+\
                                                     '_vis_detects' + '.avi'
        if os.path.exists(out_fname):
            print('Video already writtenfor ',bag_fname, "skipping")
            return False
        VIDEO_WRITERS[image_topic] = cv2.VideoWriter(out_fname, \
                                                     cv2.VideoWriter_fourcc('M','J','P','G'), \
                                                     OUTPUT_FRAMERATE, \
                                                     (width, height))

    VIDEO_WRITERS[image_topic].write(cv2.cvtColor(color_image, cv2.COLOR_BGR2RGB))
    return True


if __name__ == "__main__":
    print('IF RUNNING ON DRONE DATA, MAKE SURE auto* BAGS ARE LISTED FIRST')
    detection_topics, all_topics  = [], []
    video_writers = {}

    for img_topic in image_topics:
        det_topic = img_topic + "/detections"
        detection_topics.append(det_topic)
        all_topics.append(img_topic)
        all_topics.append(det_topic)

    all_topics.append(gas_topic)  
    time_synced_dict = {}

    for bag_fname in bagfiles:
        bag = rosbag.Bag(bag_fname)
        
        if bag_fname.find('auto') == -1:
            gas_topic_present = False
            bag_topics = all_topics 
        else:
            gas_topic_present = True
            bag_topics = gas_topic

        for topic, msg, t in bag.read_messages(topics=bag_topics):
            if (gas_topic_present): # only analyze gas
                if (topic == gas_topic):
                    GAS_LEVEL_DICT[msg.header.stamp.secs] = msg.co2_concentration
                continue

            msg_time = msg.header.stamp

            if msg_time not in time_synced_dict.keys():
                time_synced_dict[msg_time] = {'detection':None,
                                       'image':None}

            t_dict = time_synced_dict[msg_time]

            if topic in image_topics:
                t_dict['image'] = [topic, msg]
            elif topic in detection_topics:
                t_dict['detection'] = [topic, msg]

            if (t_dict['image'] is not None) and \
               (t_dict['detection'] is not None):

                res = write_dets(t_dict['image'], t_dict['detection'], bag_fname)
                if not res:
                    break
        bag.close()
        
        if not (args.combine):
            for vw_key in VIDEO_WRITERS.keys():
                VIDEO_WRITERS[vw_key].release()
            VIDEO_WRITERS = {}

    for vw_key in VIDEO_WRITERS.keys():
        VIDEO_WRITERS[vw_key].release()
    #VIDEO_WRITERS = {}
