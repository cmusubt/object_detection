import rospy
import threading
import functools
import collections


class Subscriber(object):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


class TimeSynchronizer(object):
    def __init__(self, subs, queue_size=10, callback=None):

        self._lock = threading.Lock()
        self._subs = []
        self._stamps = []
        self._msg_caches = []
        self._cb = callback
        self._queue_size = queue_size
        self._last_cb_time = rospy.Time.now()

        for i, sub in enumerate(subs):
            sub.kwargs["callback"] = self._callback
            sub.kwargs["callback_args"] = i

            self._subs.append(rospy.Subscriber(*sub.args, **sub.kwargs))
            self._msg_caches.append({})
            self._stamps.append(collections.deque())

    def _callback(self, msg, i):
        with self._lock:
            current_time = rospy.Time.now()
            if current_time < self._last_cb_time:
                rospy.logwarn("Time went backwards. Clearing queues")
                self._stamps = [collections.deque() for sub in self._subs]

            self._last_cb_time = current_time

            #  print("[%d.%d] Callback %d received message" % (
            #      msg.header.stamp.secs, msg.header.stamp.nsecs, i))
            if len(self._stamps[i]) == self._queue_size:
                remove_stamp = self._stamps[i].popleft()
                del self._msg_caches[i][remove_stamp]

            # Store the order the messages came in, as well as the message
            self._stamps[i].append(msg.header.stamp)
            self._msg_caches[i][msg.header.stamp] = msg

            # This line may be a bit expensive with large queue sizes.
            common = functools.reduce(
                set.intersection, [set(stamp) for stamp in self._stamps]
            )

            for stamp in sorted(common):
                # Remove the stamp from the caches and queues
                messages = [cache.pop(stamp) for cache in self._msg_caches]
                for stamps in self._stamps:
                    stamps.remove(stamp)

                self._cb(*messages)
