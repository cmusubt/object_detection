#!/usr/bin/env python3

import os
import pathlib
import time
import collections
import subprocess
from concurrent.futures import ThreadPoolExecutor
import multiprocessing
import multiprocessing.pool
import threading

GPU_COUNT = 1
gpus = collections.deque(range(GPU_COUNT))

model_paths = [
    (
        "/home/vasu/Documents/projects/SubT/workspaces/objdet/src/object_detection/inference/data/models/rgb_negative_08_06/frozen_trt_graph_4bs_FP16.pb",
        4,
    )
]

video_paths = [
    "/home/vasu/Downloads/models_with_videos/ssd_mnv1_positives_218056/videos/original/2019-08-13-02-44-39_rs_back_color_image_rotated.mkv",
    "/home/vasu/Downloads/models_with_videos/ssd_mnv1_positives_218056/videos/original/2019-08-13-02-44-39_rs_front_color_image_rotated.mkv",
    "/home/vasu/Downloads/models_with_videos/ssd_mnv1_positives_218056/videos/original/2019-08-13-02-44-39_rs_left_color_image_rotated.mkv",
    "/home/vasu/Downloads/models_with_videos/ssd_mnv1_positives_218056/videos/original/2019-08-13-02-44-39_rs_right_color_image_rotated.mkv",
]

label_path = "/home/vasu/Documents/projects/SubT/datasets/tunnel_circuit_vasu/dataset_08_08/label_ignored.pbtxt"


def main():
    pool = multiprocessing.pool.ThreadPool(GPU_COUNT)

    tasks = []
    for model_path, batch_size in model_paths:
        for video_path in video_paths:
            tasks.append((model_path, str(batch_size), video_path))

    pool.starmap(do_thing, tasks)


def do_thing(model_path, batch_size, video_path):
    gpu = gpus.pop()

    output_dir = pathlib.Path(model_path).parent / "videos"
    output_dir.mkdir(exist_ok=True)

    env = os.environ.copy()
    env["CUDA_VISIBLE_DEVICES"] = str(gpu)

    subprocess.run(
        [
            "python3",
            "offline_video_inference.py",
            "--path_to_model",
            model_path,
            "--batch_size",
            batch_size,
            "--video_path",
            video_path,
            "--label_map_path",
            label_path,
            "--output_dir",
            str(output_dir),
        ],
        env=env,
    )

    gpus.append(gpu)


if __name__ == "__main__":
    main()
