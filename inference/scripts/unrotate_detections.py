#!/usr/bin/env python3

import numpy as np
import rospy
from objdet_msgs.msg import Detection, DetectionArray


class Unrotator(object):
    def __init__(self, input_topic, output_topic):
        self._det_sub = rospy.Subscriber(
            input_topic,
            DetectionArray,
            self._unrotate_detections,
            queue_size=10,
        )
        self._det_pub = rospy.Publisher(
            output_topic, DetectionArray, queue_size=10
        )

    def _unrotate_detections(self, msg):

        for det in msg.detections:
            det.x1, det.y1, det.x2, det.y2 = (
                1 - det.x2,
                1 - det.y2,
                1 - det.x1,
                1 - det.y1,
            )
        self._det_pub.publish(msg)


def main():
    rospy.init_node("unrotator_node")
    unrotators = []
    input_topics = rospy.get_param("~input_topics", [])
    output_topics = rospy.get_param("~output_topics", [])

    for i, input_topic in enumerate(input_topics):
        output_topic = (
            input_topic + "_unrotated"
            if i >= len(output_topics)
            else output_topics[i]
        )
        unrotators.append(Unrotator(input_topic, output_topic))

    rospy.spin()


if __name__ == "__main__":
    main()
