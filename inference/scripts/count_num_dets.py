import rospy
from objdet_msgs.msg import Detection, DetectionArray

NUM_DETS = 0

def callback(data):
    global NUM_DETS
    for detection in data.detections:
    	NUM_DETS+=1
    if (NUM_DETS > 0) and ((NUM_DETS % 10) == 0):
    	print(NUM_DETS)
    
if __name__ == "__main__":
    rospy.init_node('count_dets')
    rospy.Subscriber("/camera/image_raw/detections", DetectionArray, callback)
    rospy.spin()