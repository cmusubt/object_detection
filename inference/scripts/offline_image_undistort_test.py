#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError
import time


if __name__ == '__main__':
    rospy.init_node('undistort_test_pub', anonymous=True)
    img_pub = rospy.Publisher('/camera/image_raw', Image, queue_size=10)

    # from: http://rpg.ifi.uzh.ch/software/ocamcalib/undistortFunctions.zip
    img = cv2.imread('/home/bob/Downloads/undistortFunctions/test_fisheye.jpg')
    img_msg = CvBridge().cv2_to_imgmsg(img)

    while not rospy.is_shutdown():
        img_pub.publish(img_msg)
        time.sleep(0.5)




