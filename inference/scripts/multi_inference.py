#!/usr/bin/env python3

import rospy
from object_detection.utils import label_map_util
import yaml
import threading
import functools
from std_msgs.msg import Bool
from sensor_msgs.msg import Image
from objdet_msgs.msg import Detection, DetectionArray
from cv_bridge import CvBridge
import time
import cv2

class RosObjectDetector(object):
    def __init__(
        self,
        topics,
        label_map_path,
        label_remap_path=None,
        batch_wait_time=0.1,
        init_image_path=None,
        **kwargs,
    ):

        if not topics:
            error = "No topics specified in RosObjectDetector"
            rospy.logfatal(error)
            raise ValueError(error)
        self._topics = topics
        self.debug_mode = False
        # c3 left camera is different sensor so it has to be resized
        self.c3_resize = kwargs.get("c3_resize", False)

        
        if self.debug_mode:
            self.inference_times = []

        # This will throw an exception if the path is invalid. Good enough.
        self._category_index = label_map_util.create_category_index_from_labelmap(
            label_map_path
        )

        self._label_remap = dict()
        if label_remap_path is not None:
            with open(label_remap_path, "r") as f:
                self._label_remap = yaml.safe_load(f)

        self._last_batch_time = rospy.Time.now()
        self._batch_wait_time = rospy.Duration(batch_wait_time)
        self._bridge = CvBridge()

        # This is a tensorflow-specific optimization, but there's a chance other
        # frameworks / backends can also benefit from it.
        #
        # In tensorflow, inference is lazy by default, so we'll send a junk
        # image to the network to infer so that we can initialize it and be
        # ready to handle actual images immediately.
        # https://stackoverflow.com/questions/45063489/first-tf-session-run-performs-dramatically-different-from-later-runs-why
        if init_image_path is not None:
            # hacky conversion to RGB
            test_image = cv2.imread(init_image_path)[:, :, ::-1]
            self._detect([test_image])
        else:
            rospy.logwarn(
                "Inference may initialize start faster if you specify an init image"
            )

        # Note that buffer size is set to 1MB in order to ensure an entire image
        # is received, allowing queues to work properly. See here:
        # https://answers.ros.org/question/220502/image-subscriber-lag-despite-queue-1/
        self._image_subs = []
        self._det_pubs = []
        self._images_lock = threading.Lock()
        self._images_cv = threading.Condition(self._images_lock)
        self._images = [None] * len(topics)
        self._image_headers = [None] * len(topics)
        self._images_ready = 0
        self._images_ready_target = 2 ** len(topics) - 1

        for i, topic in enumerate(topics):
            rospy.loginfo("Subscriber %d subscribing to topic %s" % (i, topic))
            self._image_subs.append(
                rospy.Subscriber(
                    topic,
                    Image,
                    functools.partial(self._image_cb, i),
                    queue_size=1,
                    buff_size=1 << 24,
                )
            )
            self._det_pubs.append(
                rospy.Publisher(
                    topic + "/detections", DetectionArray, queue_size=10
                )
            )

        # health publisher to check if we are losing any of the stream
        self._IMAGE_HEALTH_COUNTER_THRESH = 10
        self._image_health_counter = 0
        self._image_health_pub = rospy.Publisher(
            "/image_stream_health", Bool, queue_size=10
        )

        self._detector_thread = threading.Thread(target=self._run_detector)
        self._detector_thread.start()

        rospy.loginfo("Finished initializing RosObjectDetector!")

    def _image_cb(self, i, msg):
        cv_image = self._bridge.imgmsg_to_cv2(msg, "rgb8")
        header = msg.header

        self._images_cv.acquire()
        self._images[i] = cv_image
        self._image_headers[i] = header
        # self._images_ready = inplace or 1 bit shift i places to the left
        self._images_ready |= 1 << i
        # If all of the images are ready, notify the waiting thread. If not,
        # don't do anything, and just release the lock.
        # If the time since last batch exceeds the threshold, send the current
        # batch to detection anyway
        cur_frame_time = rospy.Time.now()
        if cur_frame_time - self._last_batch_time > self._batch_wait_time:
            self._images_ready = self._images_ready_target
        if self._images_ready == self._images_ready_target:
            self._images_cv.notify()
        self._images_cv.release()

    def _run_detector(self):
        while not rospy.is_shutdown():
            self._images_cv.acquire()

            while self._images_ready != self._images_ready_target:
                # todo: is this necessary or can this be tuned?
                self._images_cv.wait(0.25)

                # Need to handle breaking out of this loop
                if rospy.is_shutdown():
                    self._images_cv.release()
                    return

            # We still have the lock, and can modify the data structure.
            # Create new images and headers list for detection
            # Also create images_indices list to keep track of the
            # corresponding indices
            images = []
            headers = []
            images_indices = []
            warn_msgs = []
            for i, image in enumerate(self._images):
                if image is not None:
                    images.append(image)
                    headers.append(self._image_headers[i])
                    images_indices.append(i)
                else:
                    warn_msgs.append(self._topics[i])

            if len(warn_msgs) != 0:
                rospy.logwarn_throttle(
                    1, "Lost stream from %s" % ", ".join(warn_msgs)
                )
                self._image_health_counter += 1
            else:
                self._image_health_counter = 0

            # publish image stream health info
            if self._image_health_counter <= self._IMAGE_HEALTH_COUNTER_THRESH:
                self._image_health_pub.publish(Bool(data=True))
            else:
                self._image_health_pub.publish(Bool(data=False))

            self._images_ready = 0  # Reset images available
            self._images = [None] * len(self._topics)  # Clear self._images
            self._last_batch_time = rospy.Time.now()
            self._images_cv.release()

            try:
                # Made local copies of the data needed for processing. Now,
                # start processing via the object detector.
                det_start = time.time()
                detections = self._detect(images)
                det_end = time.time()

                time_diff = det_end - det_start

                if self.debug_mode:
                    self.inference_times.append(1.0 / time_diff)
                    print(
                        "Mean time:",
                        sum(self.inference_times)
                        / float(len(self.inference_times)),
                    )

                rospy.loginfo_throttle(
                    1,
                    "Detection took %f seconds to run, %f Hz"
                    % (time_diff, 1.0 / time_diff),
                )
            except Exception as e:
                rospy.logwarn("Got exception during detection: %s", e)
                continue

            for i, detection in enumerate(detections):
                try:
                    # This method fills out the class, bounding box, and score
                    det_array = self._parse_detection(detection)
                except Exception as e:
                    rospy.logwarn("Got exception during parsing: %s", e)
                    continue

                det_array.header = headers[i]
                for det in det_array.detections:
                    # Remap the labels
                    # Then add human readable labels
                    det.id = self._label_remap.get(det.id, det.id)
                    if det.id not in self._category_index:
                        rospy.logwarn('det id of %s not in known categories. skipping', det.id)
                        continue
                    det.label = self._category_index[det.id]["name"]

                    # Also, clamp coordinates between 0 and 1
                    det.x1 = min(max(det.x1, 0), 1)
                    det.y1 = min(max(det.y1, 0), 1)
                    det.x2 = min(max(det.x2, 0), 1)
                    det.y2 = min(max(det.y2, 0), 1)

                # We'll publish something whenever inference is over, even
                # if nothing was detected.
                self._det_pubs[images_indices[i]].publish(det_array)

    def _detect(self, images):
        raise NotImplementedError("You need to implement your own detector!")

    def _parse_detection(self, detection):
        raise NotImplementedError("You need to implement your own parser!")


class RosTensorflowObjectDetector(RosObjectDetector):
    def __init__(
        self,
        topics,
        label_map_path,
        model_pb_path,
        batch_size,
        gpu_fraction=0.95,
        **kwargs,
    ):
        # Import inside this init function so that we don't have to import heavy
        # dependencies if we're not using them.
        from multi_object_detector import MultiObjectDetector

        self._detector = MultiObjectDetector(
            model_pb_path, label_map_path, batch_size, gpu_fraction
        )

        super().__init__(topics, label_map_path, **kwargs)

    def _detect(self, images):
        return self._detector.detect(images)

    def _parse_detection(self, detection):
        det_array = DetectionArray()

        for i in range(detection["num_detections"]):
            cls = detection["detection_classes"][i]
            box = detection["detection_boxes"][i]
            score = detection["detection_scores"][i]

            # Detection stores normalized coordinates, same as the network.
            # The network outputs coordinats in (y, x) though, so we need to
            # flip indices accordingly.
            det_msg = Detection()
            det_msg.x1 = box[1]
            det_msg.y1 = box[0]
            det_msg.x2 = box[3]
            det_msg.y2 = box[2]
            det_msg.label = "fill me out pls"
            det_msg.confidence = score
            det_msg.id = cls
            det_array.detections.append(det_msg)

        return det_array


class RosOpenvinoObjectDetector(RosObjectDetector):
    def __init__(
        self,
        topics,
        batch_size,
        label_map_path,
        model_xml_path,
        device,
        **kwargs,
    ):
        # Import inside this init function so that we don't have to import heavy
        # dependencies if we're not using them.
        from multi_object_detector_openvino import MultiObjectDetector
        self.get_img_size(model_xml_path+'/bs'+str(batch_size)+'/frozen_inference_graph.xml')
        # c3 left camera is different sensor
        self.c3_resize = kwargs.get("c3_resize", False)
        self._detector = MultiObjectDetector(
            model_xml_path, device=device, batch_size=batch_size, c3_resize=self.c3_resize
        )


        super().__init__(topics, label_map_path, **kwargs)
        
        self.mask_type = kwargs.get("mask_type", None)
        

    def _detect(self, images):
        return self._detector.detect(images)

    def _parse_detection(self, detection):
        det_array = DetectionArray()

        for obj in detection[0][0]:
            if obj[0] == -1:
                break

            if obj[2] < self._detector.prob_threshold:
                continue

            x1 = obj[3] 
            x2 = obj[5] 
            y1 = obj[4] 
            y2 = obj[6] 

            # remap the c3 left camera coordinates because we cropped before
            # going into CNN
            if (self.c3_resize) and \
                    (self._detector.c3_left_orig_height is not None):
                proper_height = 955
                diff = (self._detector.c3_left_orig_height - proper_height)
                half_diff = int(diff/2.)
                x1 = int((x1*proper_height + half_diff) /  self._detector.c3_left_orig_height)
                x2 = int((x2*proper_height + half_diff) /  self._detector.c3_left_orig_height)


            bbox_center = [(x2-x1)/2.0 + x1, (y2-y1)/2.0 + y1]
            # dont report detections that are from the actual vehicle
            if (self.mask_type == 'small_drone' and \
                    self.inside_small_drone_mask(bbox_center)):
                    continue
            if (self.mask_type == 'canary' and \
                    self.inside_canary_mask(bbox_center)):
                    continue

            det_msg = Detection()
            det_msg.x1 = obj[3]
            det_msg.x2 = obj[5]
            det_msg.y1 = obj[4]
            det_msg.y2 = obj[6]
            det_msg.label = "fill me out pls"
            det_msg.confidence = obj[2]
            det_msg.id = int(obj[1])
            det_array.detections.append(det_msg)

        return det_array

    def get_img_size(self, model_xml_path):
        '''
        Needed for masking
        '''
        import xml.etree.ElementTree as ET
        root = ET.parse(model_xml_path).getroot()
        img_dims = [] 
        for child in root.findall('./layers/layer/output/port/dim')[:4]:
            img_dims.append(int(float(child.text)))
        if len(img_dims) > 0:
            self.img_dims = img_dims
        else:
            rospy.log_warn('Image dims for masking being set to default')
            self.img_dims = [1,3,240,512]

    def inside_small_drone_mask(self, bbox_center):
        '''
        Returns true if detection is inside of mask (i.e. presumably on actual vehicle)
        '''
        if ((bbox_center[0] < 260./1028) or (bbox_center[0] > 730./1028)) and\
            (bbox_center[1] > 400./770):
            return True
        return False

    def inside_canary_mask(self, bbox_center):
        '''
        Returns true if detection is inside of mask (i.e. presumably on actual vehicle)
        '''
        if ((bbox_center[0] > 130./1028) and (bbox_center[0] < 330./1028) and\
                                             (bbox_center[1] > 430./770)) or\
           ((bbox_center[0] > 730./1028) and (bbox_center[0] < 960./1028) and\
                                             (bbox_center[1] > 430./770)):
            return True
        return False




def main():
    rospy.init_node("object_detection")

    private_parameters = rospy.get_param("~")

    backend = private_parameters.get("backend", None)
    if backend is None:
        rospy.logfatal('"backend" parameter not specified!')
        return

    if backend == "tensorflow":
        detector = RosTensorflowObjectDetector(**private_parameters)
    elif backend == "openvino":
        detector = RosOpenvinoObjectDetector(**private_parameters)
    else:
        rospy.logfatal("backend %s is invalid!", backend)
        return

    rospy.spin()


if __name__ == "__main__":
    main()
