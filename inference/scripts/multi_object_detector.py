import tensorflow as tf
import tensorflow.contrib.tensorrt as trt
import numpy as np
from object_detection.utils import label_map_util
import rospy

class MultiObjectDetector(object):
    """Detect objects in multiple images."""

    def _load_graph(self, path_to_model):
        od_graph = tf.Graph()
        od_graph_def = tf.GraphDef()

        with tf.gfile.GFile(path_to_model, "rb") as f:
            od_graph_def.ParseFromString(f.read())

        with od_graph.as_default():
            tf.import_graph_def(od_graph_def, name="")

        return od_graph

    def _load_labels(self, path_to_labels):
        return label_map_util.create_category_index_from_labelmap(
            path_to_labels
        )

    def __init__(
        self,
        path_to_model,
        path_to_labels=None,
        batch_size=None,
        gpu_fraction=0.95,
    ):
        """
        Args:
            path_to_model: Path pointing to .pb file
            path_to_labels: Path poiting to .pbtxt labelmap file. If no labelmap
                is given, self.category_index will be None.
            batch_size: Batch size that the model is trained with. If None,
                arbitrary batch sizes will be handled. If > 0, the model will be
                passed exactly batch_size images at inference time.
            gpu_fraction: Maximum ratio (of 1.0) of gpu memory to allocate
        """
        
        # Load the graph and label map
        try:
            if path_to_model[-3:] != '.pb':
                # we're loading based on batch size
                batch_folder = path_to_model + "bs" + str(batch_size) + '/'
                model_fname = "frozen_trt_graph_" + str(batch_size) + "bs" + "_FP16.pb"
                path_to_model = batch_folder + model_fname
            self._detection_graph = self._load_graph(path_to_model)
        except:
            rospy.logerr("Could not load object detection model!" + \
                         "Check if " + path_to_model + " exists. Object detection will not work.")
        
        if path_to_labels:
            self.category_index = self._load_labels(path_to_labels)
        else:
            self.category_index = None
        self._batch_size = batch_size

        # Note that "detection_masks" is also a valid output in some networks
        # (e.g. mask-rcnn), but we will likely not be using that yet. If
        # necessary in the future, it can be added again.
        self._num_det_name = "num_detections"
        self._det_boxes_name = "detection_boxes"
        self._det_scores_name = "detection_scores"
        self._det_classes_name = "detection_classes"
        self._output_names = [
            self._num_det_name + ":0",
            self._det_boxes_name + ":0",
            self._det_scores_name + ":0",
            self._det_classes_name + ":0",
        ]
        self._outputs = {}

        # Initialize the model
        with self._detection_graph.as_default():
            graph = tf.get_default_graph()

            # This can throw a KeyError, but that's the user's fault if so.
            for output_name in self._output_names:
                self._outputs[output_name] = graph.get_tensor_by_name(
                    output_name
                )

            self._input_tensor = graph.get_tensor_by_name("image_tensor:0")

        # Allowing memory growth is critical to fitting the model into GPU
        # memory on the Xavier, where CPU memory and GPU memory are shared
        # (since TensorFlow will grab all available RAM by default). Also, limit
        # the maximum usage in case that doesn't work.
        self._tf_config = tf.ConfigProto()
        self._tf_config.gpu_options.allow_growth = True
        self._tf_config.gpu_options.per_process_gpu_memory_fraction = (
            gpu_fraction
        )
        # This option may make sense on the Xavier once the model is shrunk.
        # self._tf_config.gpu_options.force_gpu_compatible = True

        self._session = tf.Session(
            graph=self._detection_graph, config=self._tf_config
        )

    def _detect_batch(self, images):
        # Assumes that len(images) == self._batch_size, so that the images can
        # be directly passed into the detector.

        with self._detection_graph.as_default():
            out = self._session.run(
                self._outputs, feed_dict={self._input_tensor: images}
            )

            output_dicts = []
            for i in range(len(images)):
                output_dict = {}
                output_dict[self._num_det_name] = int(
                    out[self._num_det_name + ":0"][i]
                )

                # Only send as many detections as the network actually found
                dets = output_dict[self._num_det_name]
                output_dict[self._det_boxes_name] = out[
                    self._det_boxes_name + ":0"
                ].astype(np.float32)[i, :dets, :]
                output_dict[self._det_scores_name] = out[
                    self._det_scores_name + ":0"
                ].astype(np.float32)[i, :dets]
                output_dict[self._det_classes_name] = out[
                    self._det_classes_name + ":0"
                ].astype(np.int32)[i, :dets]

                output_dicts.append(output_dict)

            return output_dicts

    def detect(self, images):
        if self._batch_size is None:  # Pass through for no batch size
            return self._detect_batch(images)
        else:  # Hard coded batch size
            output_dicts = []
            iters = len(images) // self._batch_size
            for i in range(iters):
                batch = images[
                    i * self._batch_size : (i + 1) * self._batch_size
                ]
                output_dicts.extend(self._detect_batch(batch))

            remaining = images[(iters * self._batch_size) :]
            if len(remaining) > 0:
                padding = self._batch_size - len(remaining)
                remaining.extend([images[0]] * padding)
                output = self._detect_batch(remaining)
                output_dicts.extend(output[:-padding])
            return output_dicts
