#!/usr/bin/env python3
from __future__ import print_function

import glob
import time
import os
import unittest
import numpy as np
import cv2
from multi_object_detector import MultiObjectDetector


class DataLoader(unittest.TestCase):
    def setUp(self):
        self.data_path = os.path.join(
            os.path.dirname(os.path.dirname(os.path.realpath(__file__))), "data"
        )
        self.model_paths = [
            os.path.join(
                self.data_path,
                "ssd_inception_full_309892",
                "frozen_inference_graph.pb",
            ),
            os.path.join(
                self.data_path,
                "ssd_mnv1_positives_218056",
                "frozen_inference_graph.pb",
            ),
        ]
        self.batch_sizes = [4, 4]
        self.label_path = os.path.join(self.data_path, "label.pbtxt")

        self.test_data_path = os.path.join(self.data_path, "test")

        self.test_images = []
        for item in os.listdir(self.test_data_path):
            if item.endswith(".png"):
                image_path = os.path.join(self.test_data_path, item)
                self.test_images.append(cv2.imread(image_path))

        self.eval_set_paths = glob.glob(
            os.path.join(self.test_data_path, "eval-*.record")
        )


class TestLoading(DataLoader):
    def test_loading(self):
        for model in self.model_paths:
            mod = MultiObjectDetector(model)

    def test_loading_with_labelmap(self):
        for model in self.model_paths:
            mod = MultiObjectDetector(model, self.label_path)


class TestInference(DataLoader):
    def setUp(self):
        super(TestInference, self).setUp()

        self.test_images = self.test_images * 2  # Add a few more images

        # https://stackoverflow.com/questions/45063489/first-tf-session-run-performs-dramatically-different-from-later-runs-why
        # Disable autotune for testing purposes
        os.environ["TF_CUDNN_USE_AUTOTUNE"] = "0"

        self.mods = [
            MultiObjectDetector(
                self.model_paths[i], batch_size=self.batch_sizes[i]
            )
            for i in range(len(self.model_paths))
        ]

    def _verify_output_dict_shapes(self, out):
        # Check that the dimensions are as expected
        self.assertNotEqual(type(out["num_detections"]), np.ndarray)
        self.assertEqual(type(out["detection_boxes"]), np.ndarray)
        self.assertEqual(type(out["detection_scores"]), np.ndarray)
        self.assertEqual(type(out["detection_classes"]), np.ndarray)

        self.assertEqual(len(out["detection_boxes"].shape), 2)
        self.assertEqual(len(out["detection_scores"].shape), 1)
        self.assertEqual(len(out["detection_classes"].shape), 1)

        self.assertEqual(out["detection_boxes"].shape[1], 4)

        self.assertEqual(
            out["detection_boxes"].shape[0], out["detection_scores"].shape[0]
        )
        self.assertEqual(
            out["detection_boxes"].shape[0], out["detection_classes"].shape[0]
        )

    def list_inference_helper(self, i, mod):
        # The test images are already a python list
        images = self.test_images[:i]
        out = mod.detect(images)

        self.assertEqual(len(out), i)
        self.assertEqual(type(out), list)

        for d in out:
            self._verify_output_dict_shapes(d)

    def test_list_inference(self):
        for mod in self.mods:
            for i in range(1, len(self.test_images)):
                self.list_inference_helper(i, mod)


#  class TestAccuracy(DataLoader):
#
#      EVAL_BATCH_SIZE = 4
#
#
#      def _parse_image_function(self, example_proto):
#          import tensorflow as tf # Don't want to globally import tensorflow
#          # Decent reference for this stuff:
#          # https://stackoverflow.com/questions/41921746/tensorflow-varlenfeature-vs-fixedlenfeature
#          feature_description = {
#              "image/encoded": tf.FixedLenFeature([], tf.string),
#              "image/object/bbox/xmin": tf.VarLenFeature(tf.float32),
#              "image/object/bbox/ymin": tf.VarLenFeature(tf.float32),
#              "image/object/bbox/xmax": tf.VarLenFeature(tf.float32),
#              "image/object/bbox/ymax": tf.VarLenFeature(tf.float32),
#              "image/object/class/label": tf.VarLenFeature(tf.int64),
#              #  "image/object/class/text": tf.VarLenFeature(tf.string), # TODO
#          }
#
#          return tf.parse_single_example(example_proto, feature_description)
#
#
#      def _evaluate_batch(self, images, ground_truths):
#          # Get a rough sense of execution time. Won't necessarily be super
#          # accurate, but should be close-ish, especially after warmup.
#          inference_start_time = time.time()
#          out = self.mod.detect(images)
#          inference_end_time = time.time()
#          print("Took %f seconds to perform inference" % (
#              inference_end_time - inference_start_time))
#
#          for i, output_dict in enumerate(out):
#              print("Batch image %d" % i)
#              print("Ground truth:", ground_truths[i])
#              print("Output:", output_dict)
#              print("----")
#
#
#      def runTest(self):
#          import tensorflow as tf # Don't want to globally import tensorflow
#          tf.enable_eager_execution() # Necessary to iterate through dataset
#
#          self.mod = MultiObjectDetector(self.model_path, self.label_path)
#          dataset = tf.data.TFRecordDataset(self.eval_set_paths)
#          parsed_dataset = dataset.map(self._parse_image_function)
#
#          images = []
#          ground_truths = []
#          for i, ex in enumerate(parsed_dataset):
#
#              image = ex['image/encoded'].numpy() # encoded string
#              image = tf.image.decode_image(image).numpy()
#              # Make sure to resize all images to the same size, which will be our
#              # final size for inference. The network also requires all images in
#              # the batch to be the same size (or, at least, throws an obscure
#              # ValueError when that's not the case).
#              image = cv2.resize(image, (640, 360))
#              images.append(image)
#
#              xmin = tf.sparse.to_dense(ex['image/object/bbox/xmin']).numpy()
#              ymin = tf.sparse.to_dense(ex['image/object/bbox/ymin']).numpy()
#              xmax = tf.sparse.to_dense(ex['image/object/bbox/xmax']).numpy()
#              ymax = tf.sparse.to_dense(ex['image/object/bbox/ymax']).numpy()
#
#              # The labels are 0 indexed as a result of the
#              # convert_labels_to_records.py script. We'll add a +1 here to
#              # make sure that the ground truth now matches the output of the
#              # network.
#              # TODO: Figure out whether this is being handled correctly.
#              label = tf.sparse.to_dense(
#                      ex['image/object/class/label']).numpy() + 1
#
#              ground_truth = {}
#              ground_truth["num_detections"] = len(label)
#              # https://stackoverflow.com/questions/27513246/combining-vectors-as-column-matrix-in-numpy
#              # Note that the order is (y, x), rather than (x, y), to match
#              # the outputs of the object detection API.
#              boxes = np.concatenate((ymin, xmin, ymax, xmax)).reshape(
#                      (-1, 4), order='F')
#              ground_truth["detection_boxes"] = boxes
#              ground_truth["detection_scores"] = np.ones(len(label),
#                      dtype=np.float32)
#              ground_truth["detection_classes"] = label
#
#              ground_truths.append(ground_truth)
#
#              if len(images) == self.EVAL_BATCH_SIZE:
#                  self._evaluate_batch(images, ground_truths)
#                  images = []
#                  ground_truths = []
#
#          self._evaluate_batch(images, ground_truths)


if __name__ == "__main__":
    unittest.main()
