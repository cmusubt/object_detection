#!/usr/bin/env python3

import time
import rospy
from sensor_msgs.msg import Temperature
from rosgraph_msgs.msg import Clock
import time_synchronizer


def cb(*args):
    print(
        "FIZZBUZZ",
        " ".join(
            [
                "%d.%d" % (msg.header.stamp.secs, msg.header.stamp.nsecs)
                for msg in args
            ]
        ),
    )
    #  print("FIZZBUZZ at %d.%d and %d.%d" % (
    #      msg1.header.stamp.secs, msg1.header.stamp.nsecs,
    #      msg2.header.stamp.secs, msg2.header.stamp.nsecs))


def main():
    rospy.init_node("time_sync_test")

    nums = [2, 3, 7]
    topics = ["test%d" % num for num in nums]

    subs = [
        time_synchronizer.Subscriber(topic, Temperature) for topic in topics
    ]
    ts = time_synchronizer.TimeSynchronizer(subs, callback=cb)

    pubs = [
        rospy.Publisher(topic, Temperature, queue_size=0) for topic in topics
    ]
    clock_pub = rospy.Publisher("clock", Clock, queue_size=1)

    while not rospy.is_shutdown():
        for i in range(100):
            t = rospy.Time(i)
            clock = Clock()
            clock.clock = t
            clock_pub.publish(clock)

            for j, num in enumerate(nums):
                if i % num == 0:
                    msg = Temperature()
                    msg.header.stamp = t
                    msg.temperature = num
                    pubs[j].publish(msg)

            time.sleep(0.02)


if __name__ == "__main__":
    main()
