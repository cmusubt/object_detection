#!/usr/bin/env python3

import drawing_utils
import rospy
import time_synchronizer
import cv_bridge
import cv2
import itertools
import os

from sensor_msgs.msg import Image
from objdet_msgs.msg import DetectionArray

class ImageDetVis(object):
    def __init__(self, image_topic, detection_topic, video_mode, output_vid_fname):
        self._last_image_time = rospy.Time.now()
        self._image_topic = image_topic
        self._image_sub = time_synchronizer.Subscriber(
            image_topic, Image, buff_size=1 << 24
        )

        if detection_topic is not None:
            self._detection_topic = detection_topic
        else:
            self._detection_topic = image_topic + "/detections"

        self.video_mode = video_mode
        if self.video_mode and os.path.exists(output_vid_fname+'_'+\
                                           self._detection_topic[1:].replace('/','_')+'.avi'):
            self.video_writer = None
            self.video_mode = False
            rospy.logwarn('Video already exists for '+self._detection_topic+' so skipping making video of it.')

        self._dets_sub = time_synchronizer.Subscriber(
            self._detection_topic, DetectionArray
        )

        vis_topic = (
            image_topic + "_vis_%s" % self._detection_topic.split("/")[-1]
        )
        self._vis_pub = rospy.Publisher(vis_topic, Image, queue_size=30)
        self._bridge = cv_bridge.CvBridge()

        self._ts = time_synchronizer.TimeSynchronizer(
            [self._image_sub, self._dets_sub],
            queue_size=60,
            callback=self._callback,
        )

        self._freq = 0
        self._freq_alpha = 0.9  # How much of previous freq to keep
        

        if self.video_mode:
            self.vid_write_frame_rate = 15
            self.video_writer = None
            self.output_vid_fname = output_vid_fname

    def _callback(self, image, dets):

        if (not self.video_mode) and self._vis_pub.get_num_connections() == 0:
            return

        color_image = self._bridge.imgmsg_to_cv2(image)  # RGB, not BGR
        height, width = color_image.shape[:2]

        if self.video_mode and self.video_writer is None:
            self.video_writer = cv2.VideoWriter(self.output_vid_fname+'_'+self._detection_topic[1:].replace('/','_')+'.avi', \
                                        cv2.VideoWriter_fourcc('M','J','P','G'), \
                                        self.vid_write_frame_rate, \
                                        (width, height))

        for det in dets.detections:
            x1 = int(det.x1 * width)
            y1 = int(det.y1 * height)
            x2 = int(det.x2 * width)
            y2 = int(det.y2 * height)

            color = drawing_utils.colors[
                (det.id - 1) % len(drawing_utils.colors)
            ]
            cv2.rectangle(color_image, (x1, y1), (x2, y2), color, 2)
            # Draw the class label
            cv2.putText(
                color_image,
                det.label,
                (x1, y2 - 5),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5,
                color,
                1,
            )
            # Draw other information
            cv2.putText(
                color_image,
                "[%d]: %0.3f" % (det.id, det.confidence),
                (x1, y2 - 25),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5,
                color,
                1,
            )

        # This will be wrong for a few frames when time jumps backwards when
        # replaying a bag file, but it's correct in the standard case.
        current_time = rospy.Time.now()
        image_time = image.header.stamp
        lag = current_time - image_time  # How late the image is
        freq = 1.0 / (image_time - self._last_image_time).to_sec()  # det rate
        self._freq *= self._freq_alpha
        self._freq += (1 - self._freq_alpha) * freq
        self._last_image_time = image_time

        drawing_utils.shadow_text(
            color_image,
            "Image Topic: %s" % self._image_topic,
            (10, 20),
            font_weight=1,
        )
        drawing_utils.shadow_text(
            color_image,
            "Detection Topic: %s" % self._detection_topic,
            (10, 40),
            font_weight=1,
        )
        drawing_utils.shadow_text(
            color_image,
            "Detection Lag: %0.3fs" % lag.to_sec(),
            (10, 60),
            font_weight=1,
        )
        drawing_utils.shadow_text(
            color_image,
            "Detection Freq: %0.2f Hz" % self._freq,
            (10, 80),
            font_weight=1,
        )

        if self.video_mode:
            self.video_writer.write(cv2.cvtColor(color_image, cv2.COLOR_BGR2RGB))

        vis_msg = self._bridge.cv2_to_imgmsg(color_image)
        vis_msg.header = image.header
        vis_msg.encoding = image.encoding
        self._vis_pub.publish(vis_msg)


def main():
    rospy.init_node("image_det_vis_node")

    image_topics = rospy.get_param("~topics", [])
    detection_topics = rospy.get_param("~detection_topics", [])
    # video_mode means dont publish the result, write the result out to 
    # a file
    video_mode = rospy.get_param("~video_mode", False)
    output_vid_fname = rospy.get_param("~output_vid_fname", "")

    image_visualizers = []
    for i, topics in enumerate(itertools.zip_longest(image_topics, detection_topics)):
        image_visualizers.append(ImageDetVis(*topics, video_mode, output_vid_fname))

    rate = rospy.Rate(1) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()

    if video_mode:
        for visualizer in image_visualizers:
            if visualizer.video_writer is not None:
                visualizer.video_writer.release()


if __name__ == "__main__":
    main()
    