import rospy
import drawing_utils
import time_synchronizer
import cv_bridge
import cv2
import numpy as np
import time

from sensor_msgs.msg import CameraInfo
from sensor_msgs.msg import Image
from objdet_msgs.msg import DetectionArray
from geometry_msgs.msg import Point
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray


class BoxDetVis(object):

    DS = 4

    def __init__(self):
        self._aligned_depth_sub = time_synchronizer.Subscriber(
            "/aligned_depth", Image, buff_size=1 << 24
        )
        self._aligned_depth_info_sub = time_synchronizer.Subscriber(
            "/aligned_depth/camera_info", CameraInfo
        )
        self._dets_sub = time_synchronizer.Subscriber(
            "/detections", DetectionArray
        )
        self._vis_pub = rospy.Publisher(
            "/det_markers", MarkerArray, queue_size=10
        )

        self._bridge = cv_bridge.CvBridge()
        self._ts = time_synchronizer.TimeSynchronizer(
            [
                self._aligned_depth_sub,
                self._aligned_depth_info_sub,
                self._dets_sub,
            ],
            queue_size=15,
            callback=self._callback,
        )

        self._last_time = rospy.Time.now()

    def _callback(self, aligned_depth, info, dets):
        start = time.time()
        fx = info.K[0]
        cx = info.K[2]
        fy = info.K[4]
        cy = info.K[5]

        marker_array_msg = MarkerArray()
        depth_image = self._bridge.imgmsg_to_cv2(aligned_depth)
        height, width = depth_image.shape[:2]
        for i, det in enumerate(dets.detections):
            x1 = int(det.x1 * width)
            y1 = int(det.y1 * height)
            x2 = int(det.x2 * width)
            y2 = int(det.y2 * height)

            color = drawing_utils.colors[
                (det.id - 1) % len(drawing_utils.colors)
            ]

            # Get the ROI for the detection
            roi = depth_image[y1:y2, x1:x2].astype(float)
            roi_flat = roi.flatten()
            roi_flat /= 1000.0  # Convert mm to m. Default scale is mm.
            roi_valid = (0 < roi_flat) & (
                roi_flat <= 5
            )  # Cut off depth at 5 meters

            normalized_roi = roi / roi.max()
            depth_gradients = cv2.Laplacian(normalized_roi, cv2.CV_64F)
            #  cv2.imshow("ROI: %d" % i, normalized_roi)
            #  cv2.imshow("Gradients: %d" % i, depth_gradients /
            #          depth_gradients.max())

            ys = np.arange(0, len(roi)) + y1
            xs = np.arange(0, len(roi[0])) + x1
            xv, yv = np.meshgrid(xs, ys)

            # Default scale for depth is 1 mm per integer unit. Can be changed.
            valid_roi = roi_flat[roi_valid]
            valid_x = xv.flatten()[roi_valid].astype(float)
            valid_y = yv.flatten()[roi_valid].astype(float)

            x = (valid_x - cx) * valid_roi / fx
            y = (valid_y - cy) * valid_roi / fy
            z = valid_roi

            marker = Marker()
            marker.header = aligned_depth.header  # They're all the same
            marker.header.stamp = rospy.Time()
            marker.ns = "boxes"
            marker.id = i  # Wipe points every time
            marker.type = Marker.POINTS
            marker.action = Marker.ADD
            marker.scale.x = 0.01  # 1 cm
            marker.scale.y = 0.01
            marker.scale.z = 0.01
            marker.color.r = color[0] / 255.0
            marker.color.g = color[1] / 255.0
            marker.color.b = color[2] / 255.0
            marker.color.a = 1.0

            for j in range(0, len(z), self.DS):
                pt = Point()
                pt.x = x[j]
                pt.y = y[j]
                pt.z = z[j]

                marker.points.append(pt)

            marker_array_msg.markers.append(marker)

        self._vis_pub.publish(marker_array_msg)
        end = time.time()

        #  cv2.waitKey(1)
        rospy.loginfo("iteration took %0.3f s", end - start)
