# Object Detection Repo

This monorepo contains a number of different packages to perform object
detection, as well as various hardware drivers and some utilities.

## Prerequisites

If you want to be able to run the neural networks, you'll need to install
git-LFS. See step 2 [here](https://confluence.atlassian.com/bitbucket/use-git-lfs-with-bitbucket-828781636.html#UseGitLFSwithBitbucket-client).
In short, `sudo apt install git-lfs` followed by `git lfs install`. You'll only
need to do this once.

For more information on git LFS, see [here](https://confluence.atlassian.com/bitbucket/use-git-lfs-with-bitbucket-828781636.html#UseGitLFSwithBitbucket-tracking).
The tracking format specifier is the same as .gitignore, which you can find
[here](https://git-scm.com/docs/gitignore#_pattern_format).

You'll probably need librealsense. See `realsense_fork/README.md` for details on installing that.

You'll need the `ceres` library for localizing with the fisheye. Follow the instructions here: http://ceres-solver.org/installation.html. 

## Cloning

Set up your SSH keys to be able to do passwordless clones from BitBucket, by
following [these instructions](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html).
Then, clone the repository into a separate catkin workspace, as follows:

```
$ cd [wherever you want to put your workspaces, e.g. ~/workspaces]
$ mkdir -p objdet/src
$ cd objdet/src
$ git clone git@bitbucket.org:cmusubt/object_detection.git
$ cd object_detection
$ git submodule update --init --recursive 
```

Note the **recursive submodule initialization** at the end of the clone step.
Without that, the code will not build properly.

## Dependencies

Dependencies for Modules/Triangulator:
1. Eigen 3
2. PCL 1.7
3. GTSAM 4.0, follow the installation guide from link(https://github.com/borglab/gtsam/blob/develop/README.md)

This codebase is built using Python3 and C++11. Using Python3 with ROS is a
little difficult (assuming you don't want to recompile it from source), so
there's a bit of a hack to work around that. Namely, from this directory, you'll
need to run the following:

```
$ sudo apt install clang python-catkin-tools python3-catkin-pkg-modules 
$ sudo apt install ros-melodic-pcl-ros ros-melodic-tf2-geometry-msgs
$ sudo apt install ros-melodic-rosmon
$ pip3 install empy gnupg
$ pip3 install rospkg
```

Once you've installed the dependencies from above, you can use the following
script to clone and build the dependencies, as well as configure this workspace
to use python3 instead of python2.7:

```
./setup_deps.sh
```

After you're done with that, you should be able to follow the steps in the next
section to build the code in this workspace.

## Building

Building the code has been consolidated into a single script, for all of the
various types of builds. See the `build.sh` help for more info (`./build.sh
--help`). 

I'd recommend using `clang-debug` builds for testing if the build works,
`clang-release` builds for performance, and `gcc-release` on hardware. Example:

```
./build.sh clang-release
```

## Code Health

### Autoformatting

Code can be autoformatted by installing the following tools:

```
$ sudo apt install clang-format clang-tidy
$ pip3 install black
```

You can then autoformat your code by using the `build.sh` script:

```
./build.sh format
```

### C++ Style

We adhere to the Google Style Guide for C++ source. There is tooling to
automatically format you code to this style, using `clang-format`. You must
ensure that your code meets these style guidelines before merging code into
`master`. There are also a couple of other small points which must be done
manually:

 1. Header files will be given a `.h` extension
 1. Source files will be given a `.cpp` extension
 1. Header guards will be in the form:

```
#ifndef OBJECT_DETECTION_[package] _[filename] _H
#define OBJECT_DETECTION_ [package] _[filename] _H
#endif  // OBJECT_DETECTION_[package]_[filename]_H
```

See the above regarding automatically formatting your code.

### Python Style

Python style is enforced by using the [Black](https://github.com/ambv/black.git)
code formatting tool. If you've installed it via `pip3` (per above), your Python
code will automatically be formatted as well.

### C++ Linting

Linting is performed on a per-package level, rather than across the entire
codebase at once (for now). We use `clang-tidy` to perform linting, which adds a
number of checks to ensure good code health. To use the tool, you'll need to
export compile commands by adding the following line to the package's
`CMakeLists.txt` file.

```
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
```

Then, you can use the `build.sh` script to run `clang-tidy` on the specific
package you care about. For example:

```
./build.sh clang-tidy flir_ros
```
