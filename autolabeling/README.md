# Autolabeling Pipeline

This pipeline is meant to simplify the task of generating and annotating large
amounts of data for training neural networks. 

Note: For all of the below ROS commands (`roslaunch`, `rosrun`, etc), you'll 
want to make sure you have this workspace sourced (`source devel/setup.bash`). 
You will likely want to use a `tmux` session, or otherwise be prepared to open a
few separate terminals concurrently.

Note: Check with all users before changing any conventions (e.g. message types,
folder structures, extensions, etc) as this package is actively being used to
generate data for neural network training.

Note: In places where you need to replace bag file paths, you may find the
following command helpful, which lists the full paths to all `.bag` files in the
current directory.

```
find `pwd` -iname "*.bag"
```

## Data preparation

This section assumes that you've collected some object detection data from the
robot with `/modules/scripts/record_objdet_data.sh`, and that the data is
organized similar to how it is on Perceptron. That is, a flat folder per run,
looking something like this:

```
run_##
├── objdet_2019-06-07-11-14-43_0.bag
├── objdet_2019-06-07-11-16-21_1.bag
├── objdet_2019-06-07-11-17-52_2.bag
└── ...
```

Note that the `objdet_` prefix is hardcoded in certain scripts and is mandatory.

You can use the `/utils/sort_bags.py` script to sort a single directory
containing many runs into separate folders if necessary.

You'll see `[full_path_to_run]` in a few places below. This should be the
absolute path to the `run_##` folder containing your data.

### For data from before 2019-07-01

Bag files collected before 2019-07-01 or so were generated before proper support
for loop closures was added to the data. Since loop closure makes the entire map
globally consistent, it's necessary for projecting ground truth artifacts into
images and point clouds correctly. The following steps detail how to get your
data to playback with loop closures added in post.

In short, you're removing all of the dummy loop closure information (present
both as various topics and within the `/tf` tree), and replacing it with
information coming from the proper loop closure node.

1. Filter out sensor data from the files in your folder, as follows:

```
rosrun autolabeling filter_sensor_data.sh [full_path_to_run]
```
1. Start up a `roscore`
1. In a separate terminal, start recording the output of the proper loop closure
   data, as follows:

```
rosrun autolabeling record_loam_data.sh [full_path_to_run]
```
1. In another separate terminal, run the proper loop closure node. It's possible
   you don't have access to clone this node from BitBucket. If that's the case,
   get in touch with John or Kevin, or borrow it from a robot. Make sure to look
   through the package's README for install instructions.

```
roslaunch online_pose_graph online_pose_graph.launch
```
1. Modify the `launch/loam_data.launch` file (or a copy of it) by replacing the
   paths with paths to your bag files. This launch file replays the topics
   procued by LOAM's odometry stack (namely `/integrated_to_init`,
   `/aft_mapped_to_init`, and `/velodyne_cloud_registered`). Then, in yet
   another separate terminal, run this launch file:

```
roslaunch autolabeling loam_data.launch
```
1. After the bag file has finished playing (or the robot's stopped moving at the
   end of the bag file), stop the recording (`C-c`) and then kill all of the
   other nodes as well. Keep the `roscore` running, or restart it if needed.
1. Finally, remove the LOAM transforms (while keeping anything that didn't come
   from LOAM originally) as follows:

```
rosrun autolabeling remove_loam_tfs.py --folder [full_path_to_run]
```

Congratulations! You should be ready. At this point, your folder structure
should look as follows:

```
run_##
├── labeling
│   ├── loam_data
│   ├── sensor_data
│   └── tf_data
├── objdet_*.bag
└── ...
```

### For data after 2019-07-01

No additional preparation should be needed. The recording code should be updated
such that the correct topics are now being logged.

If you find that loop closures weren't running for some reason, you can follow
the steps in the previous section to correct the data.

## Data playback

After you have your data in the correct format (either following the above
steps, or just using recent data), you'll need to set up a launch file to play
back the necessary topics. Modify the file paths in
`launch/sim_bag_replay_full.launch` (or in a copy) with paths to the correct bag
files. For older bag files (before 2019-07-01), you'll use the bag files
generated in the previous steps. For newer bag files (after 2019-07-01), you can
just replay the bag files directly.

## Ground truth generation

Ground truth can be generated one of two ways - either manually or (semi)
automatically. Here, we'll complete prerequisites for both procedures, and then
work through each option.

### Prerequisites (map and artifacts generation)

1. Run the launch file you've generated, making sure that the `--pause` option
   is enabled in the `rosbag` node. `roslaunch` seems to have some problems with
   these launch files, so I'd advise using `rosmon` instead. Don't hit play yet.

```
mon launch autolabeling sim_bag_replay_full.launch
```
1. If playing back old data (from before 2019-07-01), you'll need to rerun the
   artifact localization stack, as that corrects location estiamtes with loop
   closures. Run the following in a separate terminal:

```
mon launch modules sim_mod.launch
```
1. In a separate terminal, navigate to the `run_##` directory (the flat folder
   where all of your bags are), and then to the `labeling/artifacts` folder 
   (creating it if it doesn't exist). Then, run the artifact writer:

```
rosrun autolabeling artifact_writer
```
1. In another separate terminal, again navigate to the `run_##` directory. Then,
   run the map writer. 

```
rosrun autolabeling map_writer
```
1. Play the bag file back (press space in the terminal with the bag file).
   You'll notice that the artifact writer is constantly writing artifacts,
   whereas the map writer doesn't seem to be doing anything. This is
   intentional, as writing the map takes too long to be done in real time.
1. Pause the bag file when the robot's finished it's path. I'm not sure what
   happens if you just let the bag file finish, so I'd recommend pausing again
   instead. Then, instruct the map writer to save its map:

```
rostopic pub /map_writer_write std_msgs/Bool "data: true"
```

You should now be able to stop all running nodes. Your folder structure should
now look something like this:

```
run_##
├── labeling
│   ├── loam_data
│   ├── sensor_data
│   ├── tf_data
│   ├── artifacts 
│   ├── point_clouds
│   ├── map.pcd 
│   └── map_ds.pcd
├── objdet_*.bag
└── ...
```

Some notes on this data -- the files in `artifacts` are serialized versions of
an `ArtifactLocalization` message, with object coordinates in the fixed frame
(usually `map`). Both valid and invalid artifacts are stored currently, to be
able to know the entire state of the object detection module. This is cleaned up
in the next steps.

There are 2 types of files in `point_clouds` -- `.pose` and `.pcd`. The `.pose` 
files are `geometry_msgs::PoseStamped` serialized messages, and correspond to 
the pose of the corresponding key frame. The pose is also with respect to the 
fixed frame (usually `/map`), rather than the frame the key poses are typically 
given in (usually `/map_rot`). The `.pcd` files themselves are in the key pose
frame -- that is, they're centered on approximately `(0, 0, 0)` rather than
being off in the map somewhere. You can use the `.pose` file to transform the
`.pcd` from key pose frame to the map frame. In fact, this is how the map writer
builds the global map. Finally, note that there isn't necessarily a point cloud
to go along with every key pose, as some clouds are empty for some reason or
another.

### Cleaning up the autogenerated artifacts

As mentioned before, all artifacts, both valid and invalid, are saved to disk.
Remove the invalid ones as they aren't useful for our application. Later stages
in the pipeline will not necessarily handle invalid artifacts well.

```
rosrun autolabeling remove_invalid_artifacts.py \
    --path [full_path_to_run]/labeling/artifacts
```

Due to a weak detector, not all of the artifacts that are generated will be
accurate. You can use the `view_artifacts.py` script to remove the incorrect
artifacts by looking at the images contained inside. You should only be checking
for validity of the detection, not the class. Use spacebar to continue to the
next detection, and `d` to delete the current detection.

```
rosrun autolabeling view_artifact.py --help
rosrun autolabeling view_artifact.py \
    --path [full_path_to_run]/labeling/artifacts
```

At this point, you have a couple of options for generating ground truth 6DOF 3D
bounding boxes for the artifacts.

### Automatic ground truth generation

If you're not too concerned about the accuracy of the data, and just want
something to seed humans when they're manually annotating data later, this
option works well. We're going to trust that the artifacts that were reported by
the artifact detection pipeline are correct, and turn them into ground truth by
keeping their centroid and class, and adding an arbitrary size.

```
rosrun autolabeling make_temp_gt.py \
    --path [full_path_to_run]/labeling/artifacts
```

This will generate a `fake_dets.yaml` file in the `labeling` folder which
contains the "fake" ground truth detections ripped from the detected artifacts.
Again, the accuracy isn't very good, and artifacts are certain to have been
missed, but it's perhaps a good way to seed humans manually annotating data.

### Manual ground truth generation (preferred)

To get good accuracy, you'll want to manually generate the ground truth. You can
start by converting the detected artifacts (which you've validated to be good)
into small, easy to label point clouds, as follows. Note that you'll have to
move the output point clouds yourself.

```
cd [full_path_to_run]/labeling
rosrun make_artifact_clouds --map map.pcd --artifacts artifacts
mkdir -p artifact_clouds
mv artifacts/*.pcd artifact_clouds
```

From each of these individual point clouds, you need to generate a 6DOF bounding
box with the correct class. Details on the recommended pipeline can be found
[here](https://docs.google.com/document/d/1fNbPxA8eS65v6wW-IsOeFsDDC_uSWMiMXbOxIh_7zFY/edit).
If you choose a different tool, make sure the final output matches the format
described in the next section.

Simply completing the above steps means you'll likely have missed artifacts,
however (since you won't be annotating artifacts the robot didn't detect at
all). To deal with this, you'll need to perform the same labeling procedure as
you did above, except on the entire global map (`map.pcd` or `map_ds.pcd`).
Hopefully you saw where various artifacts are in the replay of the data you did.

### Ground truth format

See the `data/example_gt.yaml` file for more details on how the ground truth
should be structured. Note that all coordinates should be in the fixed frame
(usually `/map`).

## Autolabeling

With the ground truth `.yaml` file you have, you can now backproject them into
each of the images by using the image labeler.

1. Play back all data as you did before, in the **Data Playback** section.
   Again, keep it paused.
1. Run the image labeler with parameters pointing to your ground truth and to
   the `CameraInfo` topics for the cameras you want to generate ground truth
   for. See `launch/image_labeler.launch` for more details.
1. Actually play the bag file now, and watch the projected ground truth
   detections appear!

You can then choose what to do with the ground truth (2D) detections. You'll 
likely want to save to some format that's compatible with the `labeler.py` 
script from the `training` package.

Unfortunately, the time synchronization on the data we've generated so far isn't
perfect, and thus proper autolabeling isn't going to be possible (at least, not
fully automated). Once the time synchronization has been improved, this
autolabeling pipeline should work well.
