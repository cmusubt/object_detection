#!/bin/sh

dir=$1
cd $dir
output_dir="labeling/loam_data"
mkdir -p $output_dir 
cd $output_dir

rosbag record --split --duration 2m -b 1024 -o loam \
    /velodyne_cloud_registered \
    /aft_mapped_to_init \
    /integrated_to_init \
    /key_pose_path \
    /map_cloud \
    /relative_pose_to_key_pose \
    /velodyne_cloud_key_pose \
    /key_pose_to_map \
    /loop_closed_to_map \
    /integrated_to_map \
    /tf
