#!/usr/bin/env python3

import rospy
from visualization_msgs.msg import Marker
from objdet_msgs.msg import Detection3D, Detection3DArray
import pprint
import sys
import pathlib
import yaml
import argparse

# https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/
colors = [
    (230, 25, 75),
    (60, 180, 75),
    (255, 225, 25),
    (0, 130, 200),
    (245, 130, 48),
    (145, 30, 180),
    (70, 240, 240),
    (240, 50, 230),
    (210, 245, 60),
    (250, 190, 190),
    (0, 128, 128),
    (230, 190, 255),
    (170, 110, 40),
    (255, 250, 200),
    (128, 0, 0),
    (170, 255, 195),
    (128, 128, 0),
    (255, 215, 180),
    (0, 0, 128),
    (128, 128, 128),
    (255, 255, 255),
    (0, 0, 0),
]

parser = argparse.ArgumentParser()
parser.add_argument(
    "-f",
    "--file",
    type=pathlib.Path,
    required=True,
    help="Path to yaml file containing ground truth",
)

args = parser.parse_args()


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def fill_size(size, artifact):
    size.x = artifact["size"]["x"]
    size.y = artifact["size"]["y"]
    size.z = artifact["size"]["z"]


def fill_pose(pose, artifact):
    pose.position.x = artifact["pose"]["position"]["x"]
    pose.position.y = artifact["pose"]["position"]["y"]
    pose.position.z = artifact["pose"]["position"]["z"]
    pose.orientation.x = artifact["pose"]["orientation"]["x"]
    pose.orientation.y = artifact["pose"]["orientation"]["y"]
    pose.orientation.z = artifact["pose"]["orientation"]["z"]
    pose.orientation.w = artifact["pose"]["orientation"]["w"]


def main():
    if not args.file.exists():
        print("file doesn't exist")
        return 1

    with open(args.file) as f:
        data = f.read()
        artifacts = yaml.load(data, yaml.Loader)

    markers = []
    detections = Detection3DArray()

    for i, artifact in enumerate(artifacts):
        # Create rviz visualizations for each artifact
        marker = Marker()
        marker.header.frame_id = "map"
        marker.header.stamp = rospy.Time()  # Permanent display
        marker.ns = "ground_truth"
        marker.id = i
        marker.type = Marker.CUBE
        marker.action = Marker.ADD
        fill_size(marker.scale, artifact)
        fill_pose(marker.pose, artifact)

        color = colors[artifact["class_id"] - 1]  # 1 indexed
        marker.color.r = color[0] / 255.0
        marker.color.g = color[1] / 255.0
        marker.color.b = color[2] / 255.0
        marker.color.a = 0.5

        markers.append(marker)

        detection = Detection3D()
        detection.id = artifact["class_id"]
        detection.confidence = 1.0
        fill_size(detection, artifact)
        fill_pose(detection.pose, artifact)
        detections.detections.append(detection)

    rospy.init_node("ground_truth_publisher")
    detections.header.stamp = rospy.Time.now()
    detections.header.frame_id = "/map"

    marker_pub = rospy.Publisher("ground_truth_markers", Marker, queue_size=10)
    detections_pub = rospy.Publisher(
        "ground_truth_detections", Detection3DArray, queue_size=10
    )

    r = rospy.Rate(1)
    while not rospy.is_shutdown():
        for marker in markers:
            marker_pub.publish(marker)
        detections_pub.publish(detections)
        try:
            r.sleep()
        except rospy.exceptions.ROSInterruptException:
            break


if __name__ == "__main__":
    main()
