#!/usr/bin/env python3

import yaml
import pathlib
from objdet_msgs.msg import ArtifactLocalization
import argparse

parser = argparse.ArgumentParser(
    description="Convert detected artifacts into a debugging ground truth file"
)
parser.add_argument(
    "-p",
    "--path",
    type=pathlib.Path,
    required=True,
    help="folder to check for .art files",
)

args = parser.parse_args()


def make_gt(artifact):
    gt = {}
    gt["class_id"] = artifact.class_id
    # TODO(vasua): Make this depend on the class?
    gt["size"] = {"x": 0.5, "y": 0.5, "z": 0.5}
    gt["pose"] = {
        "position": {"x": artifact.x, "y": artifact.y, "z": artifact.z},
        "orientation": {"x": 0.0, "y": 0.0, "z": 0.0, "w": 1.0},
    }
    return gt


def main():

    gt = []
    for p in args.path.iterdir():
        if not p.suffix == ".art":
            continue

        with open(p, "rb") as f:
            artifact = ArtifactLocalization()
            artifact.deserialize(f.read())

        if not artifact.valid:
            continue

        gt.append(make_gt(artifact))

    yaml_gt = yaml.dump(gt, Dumper=yaml.Dumper)

    out_file = args.path.parent / "fake_dets.yaml"
    with open(out_file, "w") as f:
        f.write(yaml_gt)


if __name__ == "__main__":
    main()
