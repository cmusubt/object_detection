#!/usr/bin/env python3

# http://answers.ros.org/question/56935/how-to-remove-a-tf-from-a-ros-bag/
# https://gist.github.com/awesomebytes/51470efe54b45045c50263f56d7aec27

import pathlib
import rosbag
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "-f",
    "--folder",
    type=pathlib.Path,
    required=True,
    help="Path to folder containing objdet_*.bag files",
)

args = parser.parse_args()

exclude_pairs = [
    ("/sensor", "/map"),
    ("/map", "/map_rot"),
    ("/map_rot", "/loop_closed"),
    ("/map_rot", "/key_pose"),
]


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def keep_tf(tf):
    parent = tf.header.frame_id
    child = tf.child_frame_id
    for pair in exclude_pairs:
        if (pair[0] == parent and pair[1] == child) or (
            pair[0] == child and pair[1] == parent
        ):
            return False

    return True


def filter_tfs(input_path, output_path):
    with rosbag.Bag(output_path, "w") as out_bag:
        #  print("Out bag path:", out_bag_path)
        print("Opening", input_path)
        for topic, msg, t in rosbag.Bag(input_path).read_messages(
            topics=["/tf"]
        ):
            to_keep = []
            for tf in msg.transforms:
                if keep_tf(tf):
                    to_keep.append(tf)

            if not to_keep == []:
                msg.transforms = to_keep
                out_bag.write(topic, msg, t)


def main():
    if not args.folder.exists() or not args.folder.is_dir():
        eprint("Input folder %s is invalid!" % args.folder)

    # Make the output folder
    out_folder = args.folder / "labeling" / "tf_data"
    out_folder.mkdir(parents=True, exist_ok=True)

    for f in args.folder.iterdir():
        if not f.suffix == ".bag" or not f.name.startswith("objdet"):
            continue

        out_name = f.name.replace("objdet", "tfs")
        out_bag_path = out_folder / out_name

        filter_tfs(str(f.resolve()), str(out_bag_path.resolve()))


if __name__ == "__main__":
    main()
