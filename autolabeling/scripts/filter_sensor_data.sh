#!/bin/sh

dir=$1
cd $dir
output_dir="labeling/sensor_data"
mkdir -p $output_dir 

for f in $(ls objdet*.bag); do
    new_name="${output_dir}/$(echo $f | sed 's/objdet/sensors/')"
    
    echo "Filtering from $f to $new_name"

    rosbag filter $f $new_name \
        "('/rs_front' in topic or 
          '/rs_left' in topic or
          '/rs_right' in topic or
          '/rs_back' in topic or
          topic == '/thermal')"
done
