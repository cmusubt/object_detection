#!/usr/bin/env python3

import argparse
import pathlib
from objdet_msgs.msg import ArtifactLocalization

parser = argparse.ArgumentParser()
parser.add_argument(
    "--path",
    type=pathlib.Path,
    required=True,
    help="folder to check for .art files",
)

args = parser.parse_args()


def main():
    for p in args.path.iterdir():
        if not p.suffix == ".art":
            continue

        with open(p, "rb") as f:
            artifact = ArtifactLocalization()
            artifact.deserialize(f.read())

        if not artifact.valid:
            print("Removing", p)
            p.unlink()


if __name__ == "__main__":
    main()
