#!/usr/bin/env python3

import numpy as np
import cv2
import cv_bridge
import os
import argparse
import pathlib
from objdet_msgs.msg import ArtifactLocalization

description_text = """\
Use this script to view artifacts (.art files) from object detection.

Every image in the artifact will be shown in a separate window. You can press
[space] to continue to the next artifact, [d] to delete the current artifact
(and [y] to confirm), and [q] to quit.
"""

parser = argparse.ArgumentParser(
    description=description_text,
    formatter_class=argparse.RawDescriptionHelpFormatter,
)
parser.add_argument(
    "-p",
    "--path",
    type=pathlib.Path,
    required=True,
    help="folder to check for .art files",
)

args = parser.parse_args()


def shadow_text(frame, text, loc, font_scale=0.5, font_weight=2):
    shadow_color = (0, 0, 0)
    shadow_loc = tuple(np.array(loc) + 2)
    font_color = (255, 255, 255)
    font_type = cv2.FONT_HERSHEY_SIMPLEX

    cv2.putText(
        frame,
        text,
        shadow_loc,
        font_type,
        font_scale,
        shadow_color,
        font_weight,
    )
    cv2.putText(
        frame, text, loc, font_type, font_scale, font_color, font_weight
    )


def confirm_delete():
    window_title = "confirmation"
    window = cv2.namedWindow(window_title)

    confirm_image = np.zeros((360, 640, 3), dtype=np.uint8)
    shadow_text(
        confirm_image,
        "Are you sure you want to delete this artifact? (y/n)",
        (50, 170),
        font_weight=1,
    )

    cv2.imshow(window_title, confirm_image)
    key = cv2.waitKey(0) & 0xFF
    cv2.destroyWindow(window_title)

    return key == ord("y")


def main():

    bridge = cv_bridge.CvBridge()
    black_image = np.zeros((360, 640, 3), dtype=np.uint8)
    open_windows = 0

    for p in sorted(args.path.iterdir()):
        if not p.suffix == ".art":
            continue

        with open(p, "rb") as f:
            artifact = ArtifactLocalization()
            artifact.deserialize(f.read())

        if not artifact.valid:
            continue

        print("Displaying artifact", artifact.report_id)
        for i, img_msg in enumerate(artifact.images):
            cv_image = bridge.imgmsg_to_cv2(img_msg, "bgr8")
            cv_image_rotated = cv_image[::-1, ::-1, :].copy()

            shadow_text(
                cv_image_rotated,
                "Timestamp: %d.%d"
                % (img_msg.header.stamp.secs, img_msg.header.stamp.nsecs),
                (10, 20),
                font_weight=1,
            )

            cv2.imshow("Image %d" % i, cv_image_rotated)
            open_windows = max(i + 1, open_windows)

        while True:
            key = cv2.waitKey(0) & 0xFF
            if key == ord("q"):
                return
            elif key == ord("d"):
                delete = confirm_delete()
                if delete:
                    print("    ---> Deleting artifact", artifact.report_id)
                    p.unlink()
                    break
            elif key == ord(" "):
                break

        # Write over all of the images, in case some artifacts don't have as
        # many images.
        for i in range(open_windows):
            cv2.imshow("Image %d" % i, black_image)


if __name__ == "__main__":
    main()
