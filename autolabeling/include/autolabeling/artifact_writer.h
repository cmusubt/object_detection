#ifndef OBJECT_DETECTION_AUTOLABELING_ARTIFACT_WRITER_H
#define OBJECT_DETECTION_AUTOLABELING_ARTIFACT_WRITER_H

#include <base/BaseNode.h>
#include <objdet_msgs/ArtifactLocalization.h>
#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <unordered_map>

namespace object_detection {
class ArtifactWriter : public BaseNode {
 public:
  ArtifactWriter(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~ArtifactWriter();

 private:
  void artifact_cb(
      const objdet_msgs::ArtifactLocalizationArrayConstPtr& artifact_array);

  std::string output_path = ".";
  ros::Subscriber artifact_sub;
  std::unordered_map<uint16_t, objdet_msgs::ArtifactLocalization> artifacts;
};
}  // namespace object_detection
#endif  // OBJECT_DETECTION_AUTOLABELING_ARTIFACT_WRITER_H
