#ifndef OBJECT_DETECTION_AUTOLABELING_INFO_IMAGES_SUB_H
#define OBJECT_DETECTION_AUTOLABELING_INFO_IMAGES_SUB_H

#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/connection.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>

namespace object_detection {

// Example (default) topics for a realsense:
//
//   /rs_right/color/image/detections
//   /rs_right/color/camera_info
//   /rs_right/color/image
//   /rs_right/aligned_depth_to_color/image
//
// Additional optional topics:
//   /rs_right/color/aligned_lidar/image
//   /rs_right/combined_depth/image
struct InfoImagesSubTopics {
  static const std::string REALSENSE;
  static const std::string REALSENSE_LIDAR;
  static const std::string REALSENSE_COMBINED;

  void set_realsense(const std::string& rs_namespace) {
    det_topic = rs_namespace + "/color/image/gt_detections";
    info_topic = rs_namespace + "/color/camera_info";
    color_topic = rs_namespace + "/color/image";
    depth_topic = rs_namespace + "/aligned_depth_to_color/image";
  }

  void set_realsense_lidar(const std::string& rs_namespace) {
    set_realsense(rs_namespace);
    depth_topic = rs_namespace + "/color/aligned_lidar/image";
  }

  void set_realsense_combined(const std::string& rs_namespace) {
    set_realsense(rs_namespace);
    depth_topic = rs_namespace + "/combined_depth/image";
  }

  std::string det_topic;
  std::string info_topic;
  std::string color_topic;
  std::string depth_topic;
};

class InfoImagesSub {
 public:
  InfoImagesSub(ros::NodeHandle* nh, const InfoImagesSubTopics& topics)
      : it(*nh),
        info_sub(*nh, topics.info_topic, 30),
        color_sub(it, topics.color_topic, 30),
        depth_sub(it, topics.depth_topic, 30),
        sync(SyncPolicy(30), info_sub, color_sub, depth_sub) {}

  template <class C>
  message_filters::Connection registerCallback(C& callback) {
    return sync.registerCallback(callback);
  }

  template <class C>
  message_filters::Connection registerCallback(const C& callback) {
    return sync.registerCallback(callback);
  }

  template <class C, typename T>
  message_filters::Connection registerCallback(C& callback, T* t) {
    return sync.registerCallback(callback, t);
  }

  template <class C, typename T>
  message_filters::Connection registerCallback(const C& callback, T* t) {
    return sync.registerCallback(callback, t);
  }

 private:
  using SyncPolicy = message_filters::sync_policies::ExactTime<
      sensor_msgs::CameraInfo, sensor_msgs::Image, sensor_msgs::Image>;

  image_transport::ImageTransport it;
  message_filters::Subscriber<sensor_msgs::CameraInfo> info_sub;
  image_transport::SubscriberFilter color_sub;
  image_transport::SubscriberFilter depth_sub;
  message_filters::Synchronizer<SyncPolicy> sync;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_AUTOLABELING_INFO_IMAGES_SUB_H
