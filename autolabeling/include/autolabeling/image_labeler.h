#ifndef OBJECT_DETECTION_AUTOLABELING_IMAGE_LABELER_H
#define OBJECT_DETECTION_AUTOLABELING_IMAGE_LABELER_H

#include <base/BaseNode.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <objdet_msgs/Detection3D.h>
#include <objdet_msgs/Detection3DArray.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <map>
#include <memory>
#include <unordered_map>
#include "autolabeling/info_images_sub.h"

namespace object_detection {

class ImageLabeler : public BaseNode {
 public:
  ImageLabeler(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~ImageLabeler() = default;

 private:
  const std::string MAP_FRAME = "map";

  bool load_gt_detections();
  bool load_gt_key_poses();
  bool configure_image_subs();

  // Need to store the input key poses too, in addition to ground truth
  void key_pose_cb(const nav_msgs::OdometryConstPtr& key_pose);

  void info_images_cb(const sensor_msgs::CameraInfoConstPtr& info_msg,
                      const sensor_msgs::ImageConstPtr& color_msg,
                      const sensor_msgs::ImageConstPtr& depth_msg,
                      const ros::Publisher& detection_pub);

  bool get_key_pose_index(const ros::Time& query_time,
                          uint32_t& key_pose_index);

  bool get_gt_map_to_camera_tf(const std::string& image_frame,
                               const ros::Time& query_time,
                               const uint32_t key_pose_index,
                               tf2::Transform& gt_map_to_camera_tf);

  uint32_t project_detection(const objdet_msgs::Detection3D& detection,
                             const tf2::Transform& detection_to_camera_tf,
                             const sensor_msgs::CameraInfoConstPtr& info_msg,
                             std::vector<tf2::Vector3>& camera_pts);

  std::tuple<float, float, float, float> get_bounding_box(
      const std::vector<tf2::Vector3>& camera_pts,
      const sensor_msgs::CameraInfoConstPtr& info_msg);

  bool check_visiblity(const objdet_msgs::Detection3D& detection,
                       const cv::Mat& depth_image,
                       const sensor_msgs::CameraInfoConstPtr& info_msg,
                       const tf2::Transform& camera_to_detection_tf,
                       const float x1, const float x2, const float y1,
                       const float y2);

  objdet_msgs::Detection3DArray gt_detections;
  std::unordered_map<uint32_t, geometry_msgs::Pose> gt_key_poses;
  std::map<uint32_t, ros::Time> gt_key_pose_stamps;
  ros::Duration gt_fudge_factor;

  std::unordered_map<uint32_t, geometry_msgs::Pose> key_poses;
  std::map<uint32_t, ros::Time> key_pose_stamps;

  tf2_ros::Buffer tf_buffer;
  std::unique_ptr<tf2_ros::TransformListener> tf_listener_ptr;

  ros::Subscriber key_pose_sub;
  std::vector<std::unique_ptr<InfoImagesSub>> info_images_subs;
  std::vector<ros::Publisher> ground_truth_publishers;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_AUTOLABELING_IMAGE_LABELER_H
