#ifndef OBJECT_DETECTION_AUTOLABELING_MAP_WRITER_H
#define OBJECT_DETECTION_AUTOLABELING_MAP_WRITER_H

#include <base/BaseNode.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <std_msgs/Bool.h>
#include <tf2_ros/transform_listener.h>
#include <memory>
#include <unordered_map>

namespace object_detection {
using PointType = pcl::PointXYZ;
using PointCloud = pcl::PointCloud<PointType>;

class MapWriter : public BaseNode {
 public:
  MapWriter(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~MapWriter() = default;

 private:
  const std::string MAP_FRAME = "map";

  void key_pose_cb(const nav_msgs::OdometryConstPtr& key_pose);
  void key_pose_cloud_cb(const PointCloud::ConstPtr& cloud);
  void key_pose_path_cb(const nav_msgs::PathConstPtr& key_pose_path);
  void write_cb(const std_msgs::Bool msg);
  PointCloud::Ptr downsample_cloud(const PointCloud::ConstPtr& cloud);
  void write_cloud(const std::string& path, const PointCloud::ConstPtr& cloud);

  std::unordered_map<uint32_t, geometry_msgs::PoseStamped> key_poses;
  std::vector<PointCloud::ConstPtr> key_pose_clouds;

  ros::Subscriber key_pose_sub;
  ros::Subscriber key_pose_cloud_sub;
  ros::Subscriber key_pose_path_sub;
  ros::Subscriber write_sub;

  tf2_ros::Buffer tf_buffer;
  std::unique_ptr<tf2_ros::TransformListener> tf_listener_ptr;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_AUTOLABELING_MAP_WRITER_H
