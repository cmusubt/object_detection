#include "autolabeling/artifact_writer.h"
#include <common/serial.h>
#include <ctime>
#include <iomanip>
#include <sstream>

BaseNode* BaseNode::get() {
  auto* writer = new object_detection::ArtifactWriter("artifact_writer");
  return writer;
}

namespace object_detection {
ArtifactWriter::ArtifactWriter(std::string node_name)
    : BaseNode(std::move(node_name)) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool ArtifactWriter::initialize() {
  artifact_sub = get_node_handle()->subscribe(
      "artifact_localizations", 10, &ArtifactWriter::artifact_cb, this);
  if (!get_private_node_handle()->getParam("output_path", output_path)) {
    ROS_WARN("No output path specified. Logging to current directory.");
  }

  bool timestamped = false;
  get_private_node_handle()->getParam("timestamped", timestamped);
  if (timestamped) {
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    std::ostringstream oss;
    oss << output_path << "/" << std::put_time(&tm, "%Y-%m-%d-%H-%M-%S");
    output_path = oss.str();
  }

  return true;
}

bool ArtifactWriter::execute() {
  monitor.tic("write_artifacts");
  size_t bytes = 0;
  for (const auto& kv : artifacts) {
    std::ostringstream buff;
    buff << output_path << "/artifact_" << std::setfill('0') << std::setw(4)
         << kv.first << ".art";

    bytes += write_msg_to_file(buff.str(), kv.second);
  }

  ROS_INFO("Wrote %lu artifacts for a total of %lu bytes", artifacts.size(),
           bytes);

  monitor.toc("write_artifacts");
  return true;
}

ArtifactWriter::~ArtifactWriter() = default;

void ArtifactWriter::artifact_cb(
    const objdet_msgs::ArtifactLocalizationArrayConstPtr& artifact_array) {
  for (const auto& artifact : artifact_array->localizations) {
    ROS_INFO("Updating artifact %u", artifact.report_id);
    artifacts[artifact.report_id] = artifact;
  }
}

}  // namespace object_detection
