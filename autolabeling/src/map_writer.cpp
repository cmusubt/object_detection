#include "autolabeling/map_writer.h"
#include <common/serial.h>
#include <common/tf2.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <boost/filesystem.hpp>

BaseNode* BaseNode::get() {
  auto* writer = new object_detection::MapWriter("map_writer_node");
  return writer;
}

namespace object_detection {
MapWriter::MapWriter(std::string node_name) : BaseNode(std::move(node_name)) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool MapWriter::initialize() {
  ros::NodeHandle* nh = get_node_handle();
  tf_listener_ptr = std::unique_ptr<tf2_ros::TransformListener>(
      new tf2_ros::TransformListener(tf_buffer));

  key_pose_sub =
      nh->subscribe("key_pose_to_map", 10, &MapWriter::key_pose_cb, this);
  key_pose_cloud_sub = nh->subscribe("velodyne_cloud_key_pose", 10,
                                     &MapWriter::key_pose_cloud_cb, this);
  key_pose_path_sub =
      nh->subscribe("key_pose_path", 10, &MapWriter::key_pose_path_cb, this);
  write_sub = nh->subscribe("map_writer_write", 10, &MapWriter::write_cb, this);

  return true;
}

bool MapWriter::execute() { return true; }

void MapWriter::key_pose_cb(const nav_msgs::OdometryConstPtr& key_pose) {
  const auto key_pose_index =
      static_cast<uint32_t>(key_pose->pose.covariance[0]);
  ROS_INFO_STREAM("got a key pose: " << key_pose_index << " with stamp "
                                     << key_pose->header.stamp);

  // The key poses aren't necessarily in the MAP_FRAME (usually /map) as they're
  // usually in /map_rot. Since a geometry_msgs/Pose message doesn't store any
  // frame information, we must transform the Pose here.
  geometry_msgs::Pose map_frame_key_pose;
  try {
    const auto key_pose_to_map_tf = tf_buffer.lookupTransform(
        MAP_FRAME, sanitize_frame(key_pose->header.frame_id),
        key_pose->header.stamp);
    tf2::doTransform(key_pose->pose.pose, map_frame_key_pose,
                     key_pose_to_map_tf);
  } catch (tf2::TransformException& e) {
    ROS_WARN("Error looking up Key Pose TF: %s", e.what());
    return;
  }

  key_poses[key_pose_index].pose = map_frame_key_pose;
  key_poses[key_pose_index].header.stamp = key_pose->header.stamp;
  key_poses[key_pose_index].header.frame_id = MAP_FRAME;
}

void MapWriter::key_pose_cloud_cb(const PointCloud::ConstPtr& cloud) {
  ROS_INFO("Got a key pose cloud");
  key_pose_clouds.push_back(cloud);
}

void MapWriter::key_pose_path_cb(const nav_msgs::PathConstPtr& key_pose_path) {
  ROS_INFO("Got a key pose path");

  geometry_msgs::TransformStamped key_pose_to_map_tf;
  try {
    key_pose_to_map_tf = tf_buffer.lookupTransform(
        MAP_FRAME, sanitize_frame(key_pose_path->header.frame_id),
        key_pose_path->header.stamp);
  } catch (tf2::TransformException& e) {
    ROS_WARN("Error looking up Key Pose Path TF: %s", e.what());
    return;
  }

  // Update the current key poses, making sure to transform into the MAP_FRAME
  // so that the final output looks right.
  for (size_t i = 0; i < key_pose_path->poses.size(); ++i) {
    // Only keep key poses we actually have, since we need the stamps, which
    // aren't sent over with the key_pose_path.
    if (key_poses.count(i) > 0) {
      tf2::doTransform(key_pose_path->poses[i].pose, key_poses[i].pose,
                       key_pose_to_map_tf);
    }
  }
}

void MapWriter::write_cb(const std_msgs::Bool msg) {  // NOLINT
  ROS_INFO("Writing map to file!");

  // All of the individual point clouds will be stored here. The overall map
  // will be stored in the current directory.
  const std::string cloud_dir = "key_poses";
  boost::filesystem::create_directory(cloud_dir);

  PointCloud::Ptr global_map(new PointCloud);
  const auto reserve = std::accumulate(
      key_pose_clouds.begin(), key_pose_clouds.end(), 0,
      [](size_t i, const decltype(key_pose_clouds)::value_type& cloud) {
        return i + cloud->size();
      });
  global_map->reserve(reserve);

  for (const auto& cloud : key_pose_clouds) {
    // There's a bit of timestamp error since it seems the conversion to / from
    // PCL messages isn't perfect. ROS stores its timestamps as 2 ints, while
    // timestamps returned from PCL are doubles. The conversion is lossy, so
    // exact match with == is impossible.
    const auto stamp = pcl_conversions::fromPCL(cloud->header.stamp);
    const auto it =
        std::find_if(key_poses.begin(), key_poses.end(),
                     [&stamp](const decltype(key_poses)::value_type& kv) {
                       const auto& key_stamp = kv.second.header.stamp;
                       return std::abs((key_stamp - stamp).toSec()) < 0.1;
                     });

    if (it == key_poses.end()) {
      ROS_WARN_STREAM("Didn't find key pose for cloud at time " << stamp);
      continue;
    }

    const auto index = it->first;
    const auto& key_pose = key_poses[index];

    const auto key_pose_to_map_eigen = eigen_tf_from_pose(key_pose.pose);
    PointCloud::Ptr global_key_pose(new PointCloud);
    pcl::transformPointCloud(*cloud, *global_key_pose, key_pose_to_map_eigen);
    *global_map += *global_key_pose;
    ROS_INFO("Global map now contains %lu points", global_map->size());

    // Write out the key pose cloud and the pose associated with it.
    char buff[100];
    snprintf(buff, sizeof(buff), "%s/key_pose_%04u.pcd", cloud_dir.c_str(),
             index);
    write_cloud(buff, cloud);

    // Create a PoseStamped rather than a Pose in order to preserve stamps and
    // frame information.
    snprintf(buff, sizeof(buff), "%s/key_pose_%04u.pose", cloud_dir.c_str(),
             index);
    write_msg_to_file(std::string(buff), key_pose);
  }

  global_map->height = 1;
  global_map->width = global_map->size();
  global_map->is_dense = false;
  write_cloud("map.pcd", global_map);

  const auto downsampled_cloud = downsample_cloud(global_map);
  write_cloud("map_ds.pcd", downsampled_cloud);

  ROS_INFO("Finished writing maps to file!");
}

PointCloud::Ptr MapWriter::downsample_cloud(const PointCloud::ConstPtr& cloud) {
  PointCloud::Ptr downsampled_cloud(new PointCloud);
  pcl::VoxelGrid<PointType> filter;
  filter.setInputCloud(cloud);
  filter.setLeafSize(0.1f, 0.1f, 0.1f);
  filter.filter(*downsampled_cloud);

  ROS_INFO("Downsampled from %lu points to %lu points.", cloud->size(),
           downsampled_cloud->size());

  downsampled_cloud->height = 1;
  downsampled_cloud->width = downsampled_cloud->size();
  downsampled_cloud->is_dense = false;
  return downsampled_cloud;
}

void MapWriter::write_cloud(const std::string& path,
                            const PointCloud::ConstPtr& cloud) {
  try {
    pcl::io::savePCDFileBinary(path, *cloud);
  } catch (const pcl::IOException& e) {
    ROS_ERROR_STREAM("Error while saving to " << path << ": " << e.what());
  } catch (...) {
    ROS_ERROR_STREAM("Unknown error when writing to " << path);
  }
}
}  // namespace object_detection
