#include <common/serial.h>
#include <objdet_msgs/ArtifactLocalization.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

using PointType = pcl::PointXYZRGBA;
using PointCloud = pcl::PointCloud<PointType>;
namespace fs = boost::filesystem;

void parse_args(int argc, char** argv, fs::path& map_path,
                fs::path& artifacts_path) {
  // https://www.boost.org/doc/libs/1_70_0/libs/filesystem/doc/tutorial.html
  // http://www.radmangames.com/programming/how-to-use-boost-program_options
  namespace po = boost::program_options;
  po::options_description desc("Slice small point clouds around artifacts");
  // clang-format off
  desc.add_options()
    ("help", "Print help messages")
    ("map", po::value<fs::path>(&map_path)->required(), "Path to map")
    ("artifacts", po::value<fs::path>(&artifacts_path)->required(),
      "Path to folder containing artifacts");
  // clang-format on

  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
      std::cout << "Insert help here \n" << desc << std::endl;
      exit(0);
    }

    po::notify(vm);
  } catch (const po::error& e) {
    std::cerr << "Error: " << e.what() << "\n\n" << desc << std::endl;
    exit(1);
  }

  try {
    if (!fs::exists(map_path) || !fs::is_regular_file(map_path)) {
      std::cerr << "Map path " << map_path << " is invalid!" << std::endl;
      exit(1);
    }

    if (!fs::exists(artifacts_path) || !fs::is_directory(artifacts_path)) {
      std::cerr << "Artifacts path " << artifacts_path << " is invalid!"
                << std::endl;
      exit(1);
    }
  } catch (const fs::filesystem_error& e) {
    std::cerr << "Filesystem error: " << e.what() << std::endl;
    exit(1);
  }

  std::cout << "Map path: " << map_path << std::endl;
  std::cout << "Artifacts path: " << artifacts_path << std::endl;
}

// Load the map from disk
PointCloud::Ptr load_map(const fs::path& map_path) {
  PointCloud::Ptr map(new PointCloud);

  auto map_canonical = fs::canonical(map_path).string();
  if (pcl::io::loadPCDFile<PointType>(map_canonical, *map) == -1) {
    std::cerr << "Couldn't read cloud from " << map_canonical << std::endl;
    exit(1);
  }

  std::cout << "Successfully read cloud!" << std::endl;
  return map;
}

objdet_msgs::ArtifactLocalization load_artifact(const fs::path& artifact_path) {
  auto canonical = fs::canonical(artifact_path).string();
  return object_detection::load_msg_from_file<
      objdet_msgs::ArtifactLocalization>(canonical);
}

std::vector<std::pair<fs::path, objdet_msgs::ArtifactLocalization>>
load_artifacts(const fs::path& artifacts_path) {
  std::vector<std::pair<fs::path, objdet_msgs::ArtifactLocalization>> artifacts;
  for (const auto& ent : fs::directory_iterator(artifacts_path)) {
    auto extension = fs::extension(ent.path());
    if (extension != ".art") {
      continue;
    }

    artifacts.emplace_back(ent.path(), load_artifact(ent.path()));
    const auto& artifact = artifacts.back().second;
    printf("Artifact at: (%f, %f, %f)\n", artifact.x, artifact.y, artifact.z);
  }

  std::cout << "Successfully read " << artifacts.size() << " artifacts!"
            << std::endl;
  return artifacts;
}

fs::path get_cloud_path(const fs::path& artifact_path) {
  // Determine path for point cloud
  const auto parent = artifact_path.parent_path();
  auto stem = artifact_path.stem().string();
  std::string text("artifact");
  stem.replace(stem.find(text), text.length(), "point_cloud");
  const auto cloud_path = parent / (stem + ".pcd");

  return cloud_path;
}

template <typename T>
inline bool in_box(const T& c, const float r, const T& p) {
  // clang-format off
  return (c.x - r <= p.x && p.x <= c.x + r &&
          c.y - r <= p.y && p.y <= c.y + r &&
          c.z - r <= p.z && p.z <= c.z + r);
  // clang-format on
}

// Sphere look cooler I guess?
template <typename T>
inline bool in_sphere(const T& c, const float r, const T& p) {
  return ((p.x - c.x) * (p.x - c.x) + (p.y - c.y) * (p.y - c.y) +
          (p.z - c.z) * (p.z - c.z)) < r * r;
}

PointCloud::Ptr get_cloud(const PointCloud::Ptr& map,
                          const objdet_msgs::ArtifactLocalization& artifact) {
  const float radius = 2.0f;  // meters
  PointType center;
  center.x = artifact.x;
  center.y = artifact.y;
  center.z = artifact.z;

  PointCloud::Ptr cloud(new PointCloud);
  for (const auto& point : map->points) {
    if (in_sphere(center, radius, point)) {
      cloud->push_back(point);
      cloud->back().r = 0;
      cloud->back().g = 128;
      cloud->back().b = 128;
      cloud->back().a = 50;
    }
  }

  PointCloud::Ptr artifact_cloud(new PointCloud);
  pcl::fromROSMsg(artifact.cloud, *artifact_cloud);

  for (const auto& point : artifact_cloud->points) {
    cloud->push_back(point);
    cloud->back().a = 255;
  }

  return cloud;
}

int main(int argc, char** argv) {
  fs::path map_path;
  fs::path artifacts_path;

  parse_args(argc, argv, map_path, artifacts_path);

  auto map = load_map(map_path);
  auto artifacts = load_artifacts(artifacts_path);

  for (const auto& pair : artifacts) {
    const auto& artifact_path = pair.first;
    const auto& artifact = pair.second;

    const auto cloud = get_cloud(map, artifact);
    std::cout << "There are " << cloud->size() << " points in cloud. ";

    const auto cloud_path = get_cloud_path(artifact_path);
    std::cout << "Writing cloud to " << cloud_path << std::endl;
    pcl::io::savePCDFileBinary(cloud_path.string(), *cloud);
  }
}
