#include "autolabeling/image_labeler.h"
#include <common/math.h>
#include <common/serial.h>
#include <common/tf2.h>
#include <geometry_msgs/PoseStamped.h>
#include <objdet_msgs/DetectionArray.h>
#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <yaml-cpp/yaml.h>
#include <algorithm>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;
using PointType = pcl::PointXYZ;
using PointCloud = pcl::PointCloud<PointType>;

BaseNode* BaseNode::get() {
  auto* labeler = new object_detection::ImageLabeler("image_labeler");
  return labeler;
}

namespace object_detection {
ImageLabeler::ImageLabeler(std::string node_name)
    : BaseNode(std::move(node_name)) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool ImageLabeler::initialize() {
  if (!load_gt_detections()) {
    return false;
  }
  if (!load_gt_key_poses()) {
    return false;
  }
  if (!configure_image_subs()) {
    return false;
  }

  // Remainder of ROS configuration that doesn't make sense to pull into a
  // separate function
  tf_listener_ptr = std::unique_ptr<tf2_ros::TransformListener>(
      new tf2_ros::TransformListener(tf_buffer));
  key_pose_sub = get_node_handle()->subscribe("key_pose_to_map", 10,
                                              &ImageLabeler::key_pose_cb, this);

  return true;
}

bool ImageLabeler::execute() { return true; }

bool ImageLabeler::load_gt_detections() {
  // Load the fudge factor for time sync
  float ff = 0.0f;
  get_private_node_handle()->getParam("gt_fudge_factor", ff);
  ROS_INFO("Fudge factor: %f", ff);
  gt_fudge_factor = ros::Duration(ff);

  // Load the ground truth parameter
  std::string gt_str;
  get_private_node_handle()->getParam("gt_path", gt_str);
  auto gt_path = fs::path(gt_str);
  if (!fs::exists(gt_path) || !fs::is_regular_file(gt_path)) {
    ROS_FATAL_STREAM("Ground truth path " << gt_path << " is invalid!");
    return false;
  }
  ROS_INFO_STREAM("ground truth: " << gt_path);

  auto ground_truth = YAML::LoadFile(fs::canonical(gt_path).string());

  // Convert it into a Detection3DArray
  for (const auto& det : ground_truth) {
    gt_detections.detections.emplace_back();
    auto& detection = gt_detections.detections.back();

    detection.id = det["class_id"].as<uint32_t>();
    detection.confidence = 1.0f;

    detection.x = det["size"]["x"].as<float>();
    detection.y = det["size"]["y"].as<float>();
    detection.z = det["size"]["z"].as<float>();

    detection.pose.position.x = det["pose"]["position"]["x"].as<float>();
    detection.pose.position.y = det["pose"]["position"]["y"].as<float>();
    detection.pose.position.z = det["pose"]["position"]["z"].as<float>();
    detection.pose.orientation.x = det["pose"]["orientation"]["x"].as<float>();
    detection.pose.orientation.y = det["pose"]["orientation"]["y"].as<float>();
    detection.pose.orientation.z = det["pose"]["orientation"]["z"].as<float>();
    detection.pose.orientation.w = det["pose"]["orientation"]["w"].as<float>();
  }

  ROS_INFO("Loaded %lu ground truth artifacts",
           gt_detections.detections.size());
  return true;
}

bool ImageLabeler::load_gt_key_poses() {
  // Load key pose parameter
  std::string key_pose_str;
  get_private_node_handle()->getParam("key_pose_path", key_pose_str);
  auto key_pose_path = fs::path(key_pose_str);
  if (!fs::exists(key_pose_path) || !fs::is_directory(key_pose_path)) {
    ROS_FATAL_STREAM("Key pose path " << key_pose_path << " is invalid!");
    return false;
  }
  ROS_INFO_STREAM("key pose: " << key_pose_path);

  // Load each of the key poses from disk, using the filename to figure out the
  // proper index to insert into.
  for (const auto& ent : fs::directory_iterator(key_pose_path)) {
    auto extension = fs::extension(ent.path());
    if (extension != ".pose") {
      continue;
    }

    const auto canonical = fs::canonical(ent.path()).string();
    const auto key_pose =
        load_msg_from_file<geometry_msgs::PoseStamped>(canonical);

    const auto stem = ent.path().stem().string();
    const auto key_pose_index =
        static_cast<uint32_t>(std::stoi(stem.substr(stem.rfind("_") + 1)));

    gt_key_poses[key_pose_index] = key_pose.pose;
    gt_key_pose_stamps[key_pose_index] = key_pose.header.stamp;
  }

  ROS_INFO("Loaded %lu key poses", gt_key_poses.size());
  return true;
}

bool ImageLabeler::configure_image_subs() {
  std::map<std::string, std::string> sub_topics;
  get_private_node_handle()->getParam("topics", sub_topics);
  if (sub_topics.empty()) {  // Checking for false seems to not work properly.
    ROS_WARN("No topics set for image labeler!");
    return false;
  }

  auto* nh = get_node_handle();
  for (const auto& pair : sub_topics) {
    ROS_INFO("%s registered of type %s", pair.first.c_str(),
             pair.second.c_str());
    InfoImagesSubTopics topics;
    if (pair.second == InfoImagesSubTopics::REALSENSE) {
      topics.set_realsense(pair.first);
    } else if (pair.second == InfoImagesSubTopics::REALSENSE_LIDAR) {
      topics.set_realsense_lidar(pair.first);
    } else if (pair.second == InfoImagesSubTopics::REALSENSE_COMBINED) {
      topics.set_realsense_combined(pair.first);
    } else {
      ROS_ERROR("Type \"%s\" for topic \"%s\" doesn't make sense.",
                pair.second.c_str(), pair.first.c_str());
      continue;
    }

    ground_truth_publishers.emplace_back();
    auto& pub = ground_truth_publishers.back();
    pub = nh->advertise<objdet_msgs::DetectionArray>(topics.det_topic, 10);
    info_images_subs.emplace_back(new InfoImagesSub(nh, topics));
    info_images_subs.back()->registerCallback(
        boost::bind(&ImageLabeler::info_images_cb, this, _1, _2, _3, pub));
  }

  return true;
}

// TODO(vasua): This is nearly identical to the one in objdet_mapper, refactor.
// Refactoring to handle thread safety properly is going to be interesting ...
void ImageLabeler::key_pose_cb(const nav_msgs::OdometryConstPtr& key_pose) {
  // Index encoded as first element of covariance matrix
  const auto key_pose_index =
      static_cast<uint32_t>(key_pose->pose.covariance[0]);
  const auto& stamp = key_pose->header.stamp;
  ROS_INFO("got a key pose: %u", key_pose_index);

  // The key poses aren't necessarily in the MAP_FRAME (usually /map) as they're
  // usually in /map_rot. Since a geometry_msgs/Pose message doesn't store any
  // frame information, we must transform the Pose here.
  geometry_msgs::Pose map_frame_key_pose;
  try {
    const auto key_pose_to_map_tf = tf_buffer.lookupTransform(
        MAP_FRAME, sanitize_frame(key_pose->header.frame_id),
        key_pose->header.stamp);
    tf2::doTransform(key_pose->pose.pose, map_frame_key_pose,
                     key_pose_to_map_tf);
  } catch (tf2::TransformException& e) {
    ROS_WARN("Error looking up Key Pose TF: %s", e.what());
    return;
  }

  key_poses[key_pose_index] = map_frame_key_pose;
  key_pose_stamps[key_pose_index] = stamp;

  assert(std::is_sorted(key_pose_stamps.begin(), key_pose_stamps.end(),
                        [](const decltype(key_pose_stamps)::value_type& left,
                           const decltype(key_pose_stamps)::value_type& right) {
                          return left.second < right.second;
                        }));
}

void ImageLabeler::info_images_cb(
    const sensor_msgs::CameraInfoConstPtr& info_msg,
    const sensor_msgs::ImageConstPtr& color_msg,
    const sensor_msgs::ImageConstPtr& depth_msg,
    const ros::Publisher& detection_pub) {
  const auto image_frame = sanitize_frame(color_msg->header.frame_id);
  const auto query_time = color_msg->header.stamp + gt_fudge_factor;

  uint32_t key_pose_index;
  if (!get_key_pose_index(query_time, key_pose_index)) {
    return;
  }

  tf2::Transform gt_map_to_camera_tf;
  if (!get_gt_map_to_camera_tf(image_frame, query_time, key_pose_index,
                               gt_map_to_camera_tf)) {
    return;
  }

  const auto height = static_cast<float>(info_msg->height);
  const auto width = static_cast<float>(info_msg->width);
  const auto depth_cv_ptr = cv_bridge::toCvShare(depth_msg);

  objdet_msgs::DetectionArray gt_dets;
  gt_dets.header = color_msg->header;  // Copy the header, broken frame and all.

  for (const auto& d : gt_detections.detections) {
    tf2::Transform detection_to_gt_map_tf;
    tf2::fromMsg(d.pose, detection_to_gt_map_tf);
    const auto detection_to_camera_tf =
        gt_map_to_camera_tf * detection_to_gt_map_tf;
    const auto camera_to_detection_tf = detection_to_camera_tf.inverse();

    std::vector<tf2::Vector3> camera_pts;
    if (!project_detection(d, detection_to_camera_tf, info_msg, camera_pts)) {
      continue;
    }

    float x1, x2, y1, y2;
    std::tie(x1, x2, y1, y2) = get_bounding_box(camera_pts, info_msg);

    if (!check_visiblity(d, depth_cv_ptr->image, info_msg,
                         camera_to_detection_tf, x1, x2, y1, y2)) {
      continue;
    }

    objdet_msgs::Detection gt_det;
    gt_det.x1 = clamp(x1 / width, 0.0f, 1.0f);
    gt_det.x2 = clamp(x2 / width, 0.0f, 1.0f);
    gt_det.y1 = clamp(y1 / height, 0.0f, 1.0f);
    gt_det.y2 = clamp(y2 / height, 0.0f, 1.0f);
    gt_det.label = d.label;
    gt_det.id = d.id;
    gt_det.confidence = d.confidence;
    gt_dets.detections.push_back(std::move(gt_det));
  }

  // Publish a ground truth detection every single frame. Even the lack of a
  // detection means something.
  detection_pub.publish(gt_dets);
}

bool ImageLabeler::get_key_pose_index(const ros::Time& query_time,
                                      uint32_t& key_pose_index) {
  // First, match the camera with a key pose from LOAM. Make sure that whatever
  // we find, we also have a corresponding ground truth for.
  auto key_pose_found = false;
  for (auto it = key_pose_stamps.rbegin(); it != key_pose_stamps.rend(); ++it) {
    if (it->second <= query_time) {
      key_pose_index = it->first;
      key_pose_found = true;
      break;
    }
  }

  if (!key_pose_found) {
    ROS_WARN_STREAM("No key pose found for sensor reading at time "
                    << query_time << ". Currently have "
                    << key_pose_stamps.size() << " key poses.");
    return false;
  } else if (gt_key_poses.count(key_pose_index) == 0) {
    ROS_WARN("Found key pose %u but didn't have corresponding ground truth",
             key_pose_index);
    return false;
  }

  return true;
}

bool ImageLabeler::get_gt_map_to_camera_tf(
    const std::string& image_frame, const ros::Time& query_time,
    const uint32_t key_pose_index, tf2::Transform& gt_map_to_camera_tf) {
  const auto& key_pose = key_poses.at(key_pose_index);
  const auto& gt_key_pose = gt_key_poses.at(key_pose_index);

  // Then, get the transform from the camera to that key pose.
  tf2::Transform camera_to_map_tf;
  try {
    const auto camera_to_map = tf_buffer.lookupTransform(
        MAP_FRAME, image_frame, query_time, ros::Duration(.1));
    tf2::fromMsg(camera_to_map.transform, camera_to_map_tf);
  } catch (tf2::TransformException& e) {
    ROS_WARN("%s", e.what());
    return false;
  }

  tf2::Transform key_pose_to_map_tf;
  tf2::fromMsg(key_pose, key_pose_to_map_tf);
  const auto camera_to_key_pose_tf =
      key_pose_to_map_tf.inverse() * camera_to_map_tf;

  // Finally, substitute the corrected key pose.
  tf2::Transform gt_key_pose_to_gt_map_tf;
  tf2::fromMsg(gt_key_pose, gt_key_pose_to_gt_map_tf);
  const auto camera_to_gt_map_tf =
      gt_key_pose_to_gt_map_tf * camera_to_key_pose_tf;
  gt_map_to_camera_tf = camera_to_gt_map_tf.inverse();

  return true;
}

uint32_t ImageLabeler::project_detection(
    const objdet_msgs::Detection3D& detection,
    const tf2::Transform& detection_to_camera_tf,
    const sensor_msgs::CameraInfoConstPtr& info_msg,
    std::vector<tf2::Vector3>& camera_pts) {
  const auto fx = info_msg->K[0];
  const auto fy = info_msg->K[4];
  const auto cx = info_msg->K[2];
  const auto cy = info_msg->K[5];
  const auto height = info_msg->height;
  const auto width = info_msg->width;

  std::vector<tf2::Vector3> pts;
  const auto& d = detection;                       // convenience
  pts.emplace_back(-d.x / 2, -d.y / 2, -d.z / 2);  // 000
  pts.emplace_back(-d.x / 2, -d.y / 2, +d.z / 2);  // 001
  pts.emplace_back(-d.x / 2, +d.y / 2, -d.z / 2);  // 010
  pts.emplace_back(-d.x / 2, +d.y / 2, +d.z / 2);  // 011
  pts.emplace_back(+d.x / 2, -d.y / 2, -d.z / 2);  // 100
  pts.emplace_back(+d.x / 2, -d.y / 2, +d.z / 2);  // 101
  pts.emplace_back(+d.x / 2, +d.y / 2, -d.z / 2);  // 110
  pts.emplace_back(+d.x / 2, +d.y / 2, +d.z / 2);  // 111

  uint32_t points_in_image = 0;
  for (const auto& pt : pts) {
    const auto camera_pt = detection_to_camera_tf * pt;

    // Make sure the point is in front of the camera
    if (camera_pt.z() <= 0) {
      continue;
    }

    const auto u = (fx * camera_pt.x()) / camera_pt.z() + cx;
    const auto v = (fy * camera_pt.y()) / camera_pt.z() + cy;
    camera_pts.emplace_back(u, v, camera_pt.z());

    if (0.0f <= u && u <= width && 0.0f <= v && v < height) {
      ++points_in_image;
    }
  }

  return points_in_image;
}

std::tuple<float, float, float, float> ImageLabeler::get_bounding_box(
    const std::vector<tf2::Vector3>& camera_pts,
    const sensor_msgs::CameraInfoConstPtr& info_msg) {
  const auto height = static_cast<float>(info_msg->height);
  const auto width = static_cast<float>(info_msg->width);
  // Find corners of bounding box
  const auto minmax_x =
      std::minmax_element(camera_pts.begin(), camera_pts.end(),
                          [](const tf2::Vector3& a, const tf2::Vector3& b) {
                            return a.x() < b.x();
                          });

  const auto minmax_y =
      std::minmax_element(camera_pts.begin(), camera_pts.end(),
                          [](const tf2::Vector3& a, const tf2::Vector3& b) {
                            return a.y() < b.y();
                          });
  const auto x1 = clamp(static_cast<float>(minmax_x.first->x()), 0.0f, width);
  const auto x2 = clamp(static_cast<float>(minmax_x.second->x()), 0.0f, width);
  const auto y1 = clamp(static_cast<float>(minmax_y.first->y()), 0.0f, height);
  const auto y2 = clamp(static_cast<float>(minmax_y.second->y()), 0.0f, height);
  // ROS_INFO("Bounding box: (%f, %f), (%f, %f)", x1, y1, x2, y2);

  return {x1, x2, y1, y2};
}

bool ImageLabeler::check_visiblity(
    const objdet_msgs::Detection3D& detection, const cv::Mat& depth_image,
    const sensor_msgs::CameraInfoConstPtr& info_msg,
    const tf2::Transform& camera_to_detection_tf, const float x1,
    const float x2, const float y1, const float y2) {
  const auto fx = info_msg->K[0];
  const auto fy = info_msg->K[4];
  const auto cx = info_msg->K[2];
  const auto cy = info_msg->K[5];
  const auto height = static_cast<float>(info_msg->height);
  const auto width = static_cast<float>(info_msg->width);

  const auto roi_width = static_cast<int32_t>(x2 - x1);
  const auto roi_height = static_cast<int32_t>(y2 - y1);
  const auto roi_rect =
      cv::Rect(static_cast<int32_t>(x1), static_cast<int32_t>(y1), roi_width,
               roi_height);
  const auto depth_box = depth_image(roi_rect);

  std::vector<tf2::Vector3> depth_pts;
  for (int32_t y = 0; y < roi_height; ++y) {
    for (int32_t x = 0; x < roi_width; ++x) {
      const auto depth_raw = depth_box.at<uint16_t>(y, x);
      if (depth_raw == 0) {
        continue;
      }

      // TODO(vasua): Programmatically choose depth scale
      const auto depth = static_cast<float>(depth_raw) / 1000.0f;
      depth_pts.emplace_back((x + x1 - cx) * depth / fx,
                             (y + y1 - cy) * depth / fy, depth);
    }
  }

  // Just check that the points are within 2m or so of the centroid. Since the
  // data isn't time synchronized very well, this is about the best we can do.
  // With better time synchronization, we can check to see if the point is
  // inside the bounding box directly.
  // TODO(vasua): Switch to checking inside the bbox once we have better sync.
  int32_t points_in_bbox = 0;
  const auto det_centroid = tf2::Vector3{detection.x, detection.y, detection.z};
  for (const auto& pt : depth_pts) {
    const auto det_frame_pt = camera_to_detection_tf * pt;
    points_in_bbox += tf2::tf2Distance2(det_centroid, det_frame_pt) <= 4.0f;
  }

  if (depth_pts.size() == 0 ||
      static_cast<float>(points_in_bbox) / depth_pts.size() < .5) {
    return false;
  }

  return true;
}

}  // namespace object_detection
