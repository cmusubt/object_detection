#include "autolabeling/info_images_sub.h"

namespace object_detection {
const std::string InfoImagesSubTopics::REALSENSE = "realsense";
const std::string InfoImagesSubTopics::REALSENSE_LIDAR = "realsense_lidar";
const std::string InfoImagesSubTopics::REALSENSE_COMBINED =
    "realsense_combined";

}  // namespace object_detection
