# SDC30 Gas Sensor ROS Driver
This repository is for the ROS driver for SDC30 gas sensor, which provides 
measurements for CO2 concentration, temperature, and relative humidity.

## Dependencies
* pymodbus

    ```
    sudo pip install pymodbus
    ```

* udev rule

    ```
    sudo cp 11-pticonverter.rules /etc/udev/rules.d
    ```

## Getting Started
1. Connect the sensor to computer with a UART to USB sensor, and check the 
device (`/dev/pti_controller`).
1. Make sure the `SEL` pin of the sensor is connected to `VIN` for the sensor 
to use modbus interface.
1. Launch the rosnode with the following command:

    ```
    roslaunch scd30_ros scd_ros.launch
    ```

    You should be able to see `GasInfo` getting published in rostopic.

1. To perform force calibration, use `scripts/force_calibration.py` 

    ```
    ./force_calibration /dev/pti_controller 400
    ```

## Contact
Henry Zhang (hengruiz@andrew.cmu.edu)
