#!/usr/bin/env python
# SCD30 gas sensor force calibration script

# Contact: Henry Zhang (hengruiz@andrew.cmu.edu)
# Property of Team Explorer

from scd30_driver.scd30_modbus import SCD30_Modbus
import argparse


def main():
    # parse arguments for port name and concentration
    parser = argparse.ArgumentParser(
        description="Force calibrate SCD30 gas" "concentration to ambient level"
    )
    parser.add_argument("port", type=str, help="port name of the sensor")
    parser.add_argument("concentration", type=int, help="ambient CO2 level")
    args = parser.parse_args()

    # setup sensor
    sensor = SCD30_Modbus(args.port)
    sensor.initPort()
    print("sensor initialized!")

    # set continuous measurement mode
    sensor.setContinuousMeasure()

    # set ambient CO2 level, usually 400 ppm
    sensor.setCalibration(args.concentration)
    print("calibrated!")
    print("run driver to check if readings are calibrated")

    return 0


if __name__ == "__main__":
    main()
