# SCD30 gas sensor pymodbus interface

# Contact: Henry Zhang (hengruiz@andrew.cmu.edu)
# Property of Team Explorer

from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from converter import Converter


class SCD30_Modbus:

    SLAVE = 0x61
    REG_MEASINT = 0x25
    REG_READY = 0x27
    REG_READING = 0x28
    REG_FRC = 0x39
    REG_RESET = 0x34
    REG_CONTMES = 0x36
    REG_STOPCONTMES = 0x37

    def __init__(self, port):
        self.port = port
        self.readings = [0.0] * 3
        self.data = [0] * 4

    def initPort(self):
        self.client = ModbusClient(
            method="rtu", port=self.port, timeout=1, baudrate=19200
        )
        self.client.connect()

    def closePort(self):
        self.client.close()

    def softReset(self):
        self.client.write_register(self.REG_RESET, 1, unit=self.SLAVE)

    def setMeasureInterval(self, interval=0x02):
        self.client.write_register(self.REG_MEASINT, interval, unit=self.SLAVE)

    def setCalibration(self, value):
        self.client.write_register(self.REG_FRC, value, unit=self.SLAVE)

    def setContinuousMeasure(self, amb_pres=0):
        self.client.write_register(self.REG_CONTMES, amb_pres, unit=self.SLAVE)

    def setStopContinuousMeasure(self):
        self.client.write_register(self.REG_STOPCONTMES, 1, unit=self.SLAVE)

    def dataReady(self):
        hr = self.client.read_holding_registers(
            self.REG_READY, 1, unit=self.SLAVE
        )
        return hr.registers[0] == 1

    def readMeasurements(self):
        if self.dataReady():

            hr = self.client.read_holding_registers(
                self.REG_READING, 6, unit=self.SLAVE
            )

            if len(hr.registers) == 6:

                word = hr.registers[0]
                self.data[0] = word >> 8
                self.data[1] = word & 0x00FF
                word = hr.registers[1]
                self.data[2] = word >> 8
                self.data[3] = word & 0x00FF
                self.readings[0] = Converter.bytesToFloat(True, self.data)

                word = hr.registers[2]
                self.data[0] = word >> 8
                self.data[1] = word & 0x00FF
                word = hr.registers[3]
                self.data[2] = word >> 8
                self.data[3] = word & 0x00FF
                self.readings[1] = Converter.bytesToFloat(True, self.data)

                word = hr.registers[4]
                self.data[0] = word >> 8
                self.data[1] = word & 0x00FF
                word = hr.registers[5]
                self.data[2] = word >> 8
                self.data[3] = word & 0x00FF
                self.readings[2] = Converter.bytesToFloat(True, self.data)

        return self.readings
