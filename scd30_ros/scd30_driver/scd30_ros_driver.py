# ROS node for SCD30 gas sensor

# Contact: Henry Zhang (hengruiz@andrew.cmu.edu)
# Property of Team Explorer

import rospy
from scd30_modbus import SCD30_Modbus
from base_py import BaseNode
from objdet_msgs.msg import GasInfo


class SCD30ROS(BaseNode):
    def __init__(self):
        super(SCD30ROS, self).__init__("scd30_ros")
        self.port = None
        self.sensor = None
        self.measurement_interval = None
        self.pub = None

    def initialize(self):
        rospy.loginfo("initializing scd30 gas sensor")

        # get private parameters
        self.port = self.get_private_param("port_name")
        self.measurement_interval = self.get_private_param(
            "measurement_interval"
        )

        # create sensor object and set measurement interval
        self.sensor = SCD30_Modbus(self.port)
        self.sensor.initPort()
        self.sensor.setMeasureInterval(self.measurement_interval)

        # ros stuff
        self.pub = rospy.Publisher("gas_info", GasInfo, queue_size=10)

        rospy.loginfo("scd30 gas sensor node initialized")

        return True

    def execute(self):
        readings = self.sensor.readMeasurements()

        # create msg
        msg = GasInfo()
        msg.header.stamp = rospy.get_rostime()
        msg.header.frame_id = "/gas_sensor"
        msg.co2_concentration = readings[0]
        msg.temp = readings[1]
        msg.relative_humidity = readings[2]

        self.pub.publish(msg)
        rospy.loginfo_throttle(
            2, "Gas sensor got reading: %f ppm" % readings[0]
        )

        return True

    def shutdown(self):
        rospy.loginfo("shutting down scd30 gas sensor ros node")
