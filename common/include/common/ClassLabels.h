#ifndef OBJECT_DETECTION_MODULES_CLASS_LABELS
#define OBJECT_DETECTION_MODULES_CLASS_LABELS
#include <ros/console.h>
#include <ros/package.h>
#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>

/* File to load class labels in a consistent manner
   across .cpp files
*/

namespace object_detection {

class ClassLabels {
//  private:
//   std::string labelmap_path = ros::package::getPath("inference") +
//                               "/data/label_maps/finalcomp_label.pbtxt";

 public:
  // ClassLabels() { read_classes(); }
  // tunnel labels
  //     uint8_t BACKPACK_ID = 1;
  //     uint8_t SURVIVOR_ID = 5;

  // urban labels
  // uint8_t BACKPACK_ID;
  // uint8_t PHONE_ID;
  // uint8_t GAS_ID;
  // uint8_t SURVIVOR_ID;

  // cave labels
  uint8_t BACKPACK_ID = 1;
  uint8_t PHONE_ID = 2;
  uint8_t CUBE_ID = 3;
  uint8_t DRILL_ID = 4;
  uint8_t EXTINGUISHER_ID = 5;
  uint8_t HELMET_ID = 6;
  uint8_t ROPE_ID = 7;
  uint8_t SURVIVOR_ID = 8;
  uint8_t VENT_ID = 9;
  uint8_t GAS_ID = 10;  // so the gas_localizer compiles

  // std::ifstream ifs(std::string labelmap_path);
  // std::string content( (std::istreambuf_iterator<char>(ifs) ),
  //                     (std::istreambuf_iterator<char>()    ) );

  // std::fstream ff;
  // ff.open( labelmap_path, ios::in );

  // if( ff.is_open() )
  // {
  //     string s;
  //     while( getline( ff, s ) )
  //     {
  //         cout << s << endl;
  //         // Tokenize s here into columns (probably on spaces)
  //     }
  //     ff.close();
  // }
  // else
  //     cout << "Error opening file " << errno << endl;

  // void read_classes() {
  //   std::fstream ff;
  //   ff.open(labelmap_path, std::ios::in);
  //   std::string s;
  //   std::string filestr = "";
  //   if (ff.is_open()) {
  //     while (getline(ff, s)) {
  //       filestr = filestr + s;
  //     }
  //     ff.close();
  //   } else {
  //     ROS_WARN(
  //         "Error opening file for class ids in c++. Just using default values "
  //         "for tunnel.");
  //     // urban labels
  //     // BACKPACK_ID = 1;
  //     // PHONE_ID = 2;
  //     // GAS_ID = 3;
  //     // VENT_ID = 4;
  //     // SURVIVOR_ID = 5;

  //     // cave labels
  //     BACKPACK_ID = 1;
  //     PHONE_ID = 2;
  //     DRILL_ID = 3;
  //     EXTINGUISHER_ID = 4;
  //     HELMET_ID = 5;
  //     ROPE_ID = 6;
  //     SURVIVOR_ID = 7;
  //     VENT_ID = 8;
  //     GAS_ID = 9;
  //   }

  //   if (filestr.size() > 0) {  // the file has actually been read
  //     std::size_t backpack_ind = filestr.find("backpack");
  //     std::size_t cell_ind = filestr.find("cell");
  //     std::size_t helmet_ind = filestr.find("helmet");
  //     std::size_t rope_ind = filestr.find("rope");
  //     std::size_t survivor_ind = filestr.find("survivor");

  //     if (backpack_ind != std::string::npos || cell_ind != std::string::npos ||
  //         helmet_ind != std::string::npos || rope_ind != std::string::npos ||
  //         survivor_ind != std::string::npos) {
  //       BACKPACK_ID = std::stoi(filestr.substr(backpack_ind - 9, 1));
  //       PHONE_ID = std::stoi(filestr.substr(cell_ind - 9, 1));
  //       HELMET_ID = std::stoi(filestr.substr(helmet_ind - 9, 1));
  //       ROPE_ID = std::stoi(filestr.substr(rope_ind - 9, 1));
  //       SURVIVOR_ID = std::stoi(filestr.substr(survivor_ind - 9, 1));
  //     } else {
  //       // just use default for cave
  //       ROS_WARN(
  //           "Loading class ids not working in c++. Just loading default "
  //           "classes for cave.");

  //       BACKPACK_ID = 1;
  //       PHONE_ID = 2;
  //       HELMET_ID = 3;
  //       ROPE_ID = 4;
  //       SURVIVOR_ID = 5;
  //     }
  //   }
  //   PHONE_ID = 2;
  //   GAS_ID = 9;
  //   VENT_ID = 8;
  //   BACKPACK_ID = 1;
  //   PHONE_ID = 2;
  //   HELMET_ID = 3;
  //   ROPE_ID = 4;
  //   SURVIVOR_ID = 5;
  // }
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_CLASS_LABELS
