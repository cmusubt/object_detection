#!/usr/bin/env python3

"""
Class for reliably loading class_ids from various python
files in the object_detection repo

Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""
import rospkg
import rospy
import re


class ClassLabels:
    def __init__(self):
        # tunnel labels
        # self.BACKPACK_ID = 1
        # self.PHONE_ID = 2
        # self.DRILL_ID = 3
        # self.EXTINGUISHER_ID = 4
        # self.SURVIVOR_ID = 5

        # urban labels
        # self.BACKPACK_ID = 1
        # self.PHONE_ID = 2
        # self.GAS_ID = 3
        # self.VENT_ID = 4
        # self.SURVIVOR_ID = 5

        # cave labels
        # self.BACKPACK_ID = 1
        # self.PHONE_ID = 2
        # self.HELMET_ID = 3
        # self.ROPE_ID = 4
        # self.SURVIVOR_ID = 5

        rospack = rospkg.RosPack()
        label_map_fname = (
            rospack.get_path("inference")
            + "/data/label_maps/finalcomp_label.pbtxt"
        )

        filt_text = self.parse_pbtxt(label_map_fname)

        self.parse_label_map(filt_text)
        self.id_to_txt(filt_text)

    def parse_label_map(self, filt_text):

        try:
            self.BACKPACK_ID = int(
                filt_text[
                    filt_text.find("backpack")
                    - 9 : filt_text.find("backpack")
                    - 9
                    + 1
                ]
            )
            self.PHONE_ID = int(
                filt_text[
                    filt_text.find("cell") - 9 : filt_text.find("cell") - 9 + 1
                ]
            )
            self.HELMET_ID = int(
                filt_text[
                    filt_text.find("helmet")
                    - 9 : filt_text.find("helmet")
                    - 9
                    + 1
                ]
            )
            self.ROPE_ID = int(
                filt_text[
                    filt_text.find("rope") - 9 : filt_text.find("rope") - 9 + 1
                ]
            )
            self.SURVIVOR_ID = int(
                filt_text[
                    filt_text.find("survivor")
                    - 9 : filt_text.find("survivor")
                    - 9
                    + 1
                ]
            )

            rospy.loginfo("Loaded classess successfully")

        except:
            rospy.logwarn(
                "Unable to load class labels properly! Using default values..."
            )

            # cave labels
            self.BACKPACK_ID = 1
            self.PHONE_ID = 2
            self.HELMET_ID = 3
            self.ROPE_ID = 4
            self.SURVIVOR_ID = 5
            self.GAS_ID = 9

    def parse_pbtxt(self, label_map_fname):

        f = open(label_map_fname)
        txt = f.read()
        filt_text = txt.replace("\n", "")
        f.close()
        return filt_text

    def id_to_txt(self, filt_text):

        self.id_to_txt = {}
        self.id_to_txt[0] = "Unknown"

        try:
            indices = [m.start() for m in re.finditer("id:", filt_text)]

            for indice in indices:
                art_id = int(filt_text[indice + 4])
                if filt_text[indice + 4 : indice + 6].isdigit():
                    art_id = int(filt_text[indice + 4 : indice + 6])
                start_text_ind = filt_text[indice:].find("'")
                end_text_ind = filt_text[indice:][start_text_ind + 1 :].find(
                    "'"
                )

                if indice != -1:
                    self.id_to_txt[art_id] = filt_text[indice:][
                        start_text_ind + 1 : start_text_ind + end_text_ind + 1
                    ]

                indice += 1

            rospy.loginfo("Loaded classes texts successfully")

        except:
            # cave labels
            self.id_to_txt = {
                1: "Backpack",
                2: "Cell phone",
                3: "Helmet",
                4: "Rope",
                5: "Survivor",
            }

            rospy.loginfo(
                "Unable to load classs texts properly. Using default values..."
            )
