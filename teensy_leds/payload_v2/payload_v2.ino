#include <Wire.h>

// Set up fixed pinouts for Alice's LED board
constexpr int LED_PIN = 5;       // Pin to pull high to drive LED
constexpr int TRIGGER_PIN = 16;  // Pin where RealSense interrupts come in
constexpr int DAC_PIN = A14;     // Pin to create 1.8V reference voltage at

// Configure the PWM and analog output frequency and resolutions
// TODO(vasua): Consider changing the frequency to be higher to improve LED
// performance at low (< 50%) brightness), at the cost of resolution.
// https://www.pjrc.com/teensy/td_pulse.html
constexpr int OUTPUT_RESOLUTION = 12;    // 12 bit resolution, 0 - 4095
constexpr int OUTPUT_FREQUENCY = 11719;  // Optimal frequency at 12 bits

constexpr int DEFAULT_BRIGHTNESS = 2048;

// I2C and serial buffers. Data can come in from either mechanism (with the same
// payload structure), and will set the output mode and appropriate output.
constexpr int BUF_SIZE = 2;
unsigned char serial_buf[BUF_SIZE];
int serial_buf_count = 0;

// constexpr int I2C_ADDR = 0x42; // Front
// constexpr int I2C_ADDR = 0x43; // Back
constexpr int I2C_ADDR = 0x44;  // Left
// constexpr int I2C_ADDR = 0x45; // Right
unsigned char i2c_buf[BUF_SIZE];
int i2c_buf_count = 0;
int manual_output = 0;

// Globals and constants for getting RealSense triggering to work properly.
constexpr int DAC_OUTPUT = 2234;  // (1.8 / 3.3) * 4096 -> 1.8V output
constexpr unsigned long REALSENSE_TIMEOUT_MILLIS = 1000;
volatile auto realsense_enabled = false;
volatile auto realsense_enable_time = millis();
int realsense_output = DEFAULT_BRIGHTNESS;

// Managing the states.
// TODO(vasua): Consider pulling this (and the decoding function) out into a
// separate file to share between the Teensy and the host.
enum class LedOutputMode : unsigned char {
  MODE_DEFAULT = 0x00,
  MODE_MANUAL = 0x01,
  MODE_REALSENSE = 0x02
};
LedOutputMode output_mode = LedOutputMode::MODE_REALSENSE;

void setup() {
  // Configure pin modes
  pinMode(LED_PIN, OUTPUT);
  pinMode(TRIGGER_PIN, INPUT_PULLDOWN);
  pinMode(DAC_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);  // Builtin LED to debug current state

  // Set up analog output
  analogWriteResolution(OUTPUT_RESOLUTION);
  analogWriteFrequency(LED_PIN, OUTPUT_FREQUENCY);

  // Set up RealSense triggering
  analogWrite(DAC_PIN, DAC_OUTPUT);
  attachInterrupt(digitalPinToInterrupt(TRIGGER_PIN), &handle_trigger, RISING);

  // Set up manual data input (over serial and i2c)
  Serial.begin(9600);  // Number doesn't matter, always runs at 12 Mbps
  Wire.setSDA(18);
  Wire.setSCL(19);
  Wire.begin(I2C_ADDR);
  Wire.onReceive(handle_i2c);
}

// Handle trigger coming from the RealSense
//
// Whenever a trigger is received, mark the RealSense as enabled and store the
// current time. Doesn't need to be particularly precise.
void handle_trigger() {
  realsense_enabled = true;
  realsense_enable_time = millis();
}

// Handle a packet coming over I2C
//
// This is handled nearly identically to the serial line, with the exception
// being that the packet is handled in a callback rather than explicitly checked
// in a loop.
void handle_i2c(int bytes) {
  // Read however many bytes are remaining to fill the buffer.
  for (int i = i2c_buf_count; i < BUF_SIZE; ++i) {
    if (Wire.available()) {
      i2c_buf[i2c_buf_count++] = Wire.read();
    }
  }

  if (i2c_buf_count != BUF_SIZE) {
    return;
  }

  parse_buffer(i2c_buf);
  i2c_buf_count = 0;
}

void loop() {
  // Handle data coming over serial (microusb)
  attempt_serial_read();
  parse_serial_buffer();

  switch (output_mode) {
    case LedOutputMode::MODE_DEFAULT: {
      analogWrite(LED_PIN, DEFAULT_BRIGHTNESS);
      break;
    }
    case LedOutputMode::MODE_MANUAL: {
      analogWrite(LED_PIN, manual_output);
      break;
    }
    case LedOutputMode::MODE_REALSENSE: {
      // disable once enough time has passed
      if ((millis() - realsense_enable_time) > REALSENSE_TIMEOUT_MILLIS) {
        realsense_enabled = false;
      }

      if (realsense_enabled) {
        analogWrite(LED_PIN, realsense_output);
      } else {
        analogWrite(LED_PIN, 0);
      }
      break;
    }
  }

  // This delay is long enough to allow a quick flash to occur on LED_BUILTIN
  // after it's set anywhere in the above loop (e.g. parse_buffer).
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);
}

// Try to read data from the serial line
//
// Serial data read is stuffed into the buffer until enough bytes are received
// to decode a message.
void attempt_serial_read() {
  // Read however many bytes are remaining to fill the buffer.
  for (int i = serial_buf_count; i < BUF_SIZE; ++i) {
    if (Serial.available()) {
      serial_buf[serial_buf_count++] = Serial.read();
    }
  }
}

void parse_serial_buffer() {
  if (serial_buf_count != BUF_SIZE) {
    return;
  }

  parse_buffer(serial_buf);
  serial_buf_count = 0;
}

// Decode message stored in buf
//
// There should be 2 bytes. First 4 bits are the mode, next 12 are the value.
// Note that the count isn't passed in since it's assumed the buffer is full.
void parse_buffer(unsigned char (&buf)[BUF_SIZE]) {
  output_mode = static_cast<LedOutputMode>((buf[0] & 0xF0) >> 4);
  auto output_value =
      static_cast<int>((buf[0] & 0x0F) << 8) | static_cast<int>(buf[1]);

  switch (output_mode) {
    case LedOutputMode::MODE_MANUAL: {
      manual_output = output_value;
      break;
    }
    case LedOutputMode::MODE_REALSENSE: {
      realsense_output = output_value;
      break;
    }
    // Nothing to be done for MODE_DEFAULT
    default: { break; }
  }

  digitalWrite(LED_BUILTIN, HIGH);  // Indicate we received something

  Serial.print("Mode: ");
  Serial.print(static_cast<unsigned char>(output_mode));
  Serial.print(", manual: ");
  Serial.print(manual_output);
  Serial.print(", realsense: ");
  Serial.print(realsense_output);
  Serial.println();  // Also forces a flush
}
