
#include "flasher.h"

constexpr int FRONT_RS_TRIGGER = 3;
constexpr int FRONT_SWITCH = 6;

constexpr int LEFT_RS_TRIGGER = 4;
constexpr int LEFT_SWITCH = 15;

constexpr int BACK_RS_TRIGGER = 7;
constexpr int BACK_SWITCH = 5;

constexpr int RIGHT_RS_TRIGGER = 8;
constexpr int RIGHT_SWITCH = 14;

Flasher<FRONT_RS_TRIGGER, FRONT_SWITCH> front_flasher(
    false);  // Keep this one on all the time
Flasher<LEFT_RS_TRIGGER, LEFT_SWITCH> left_flasher(
    false);  // The rest will just stay off
Flasher<BACK_RS_TRIGGER, BACK_SWITCH> back_flasher(false);
Flasher<RIGHT_RS_TRIGGER, RIGHT_SWITCH> right_flasher(false);

const int LED_PIN = 13;

void setup() {
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(9600);

  pinMode(A14, OUTPUT);
  analogWriteResolution(12);
  analogWrite(A14, 2234);
}

void loop() {
  // Have a blink going just to maintain status
  digitalWrite(LED_PIN, HIGH);
  delay(1000);
  digitalWrite(LED_PIN, LOW);
  delay(1000);
  Serial.println("This is Vasu's Teensy, don't touch");
}
