constexpr int LED_PIN = 5;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  analogWriteFrequency(LED_PIN, 2929);
  Serial.begin(9600);
  analogWrite(LED_PIN, 128);
}

void loop() {
  //  // put your main code here, to run repeatedly:
  //  for (int i = 0; i < 256; i += 25) {
  //    Serial.println(i);
  //    analogWrite(LED_PIN, i);
  //    delay(1000);
  //  }

  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}
