#ifndef FLASHER_H
#define FLASHER_H

template <int trigger_pin, int switch_pin>
class Flasher {
 private:
  static constexpr unsigned int FRAME_RATE =
      15;  // Maybe this should be passed in to the flasher? Should this be
           // dynamic?

  static constexpr unsigned int FRAME_TIME_US = (1000 * 1000) / FRAME_RATE;
  static constexpr unsigned int LINE_DELAY_US =
      100;  // Amount of downtime after exposure end
  static constexpr unsigned int PULSE_WIDTH_US = 2500;  // 1ms of flash time
  static constexpr unsigned int THRESHOLD_US =
      FRAME_TIME_US - PULSE_WIDTH_US - LINE_DELAY_US;

  static volatile byte timer_call_count;
  static unsigned int last_frame_time;
  static IntervalTimer timer;

  static void turn_off_leds() {
    digitalWrite(switch_pin, LOW);  // leave switch closed
  }

  static void turn_on_leds() {
    digitalWrite(switch_pin, HIGH);  // flip switch to disconnected
  }

  static void handle_trigger() {
    // This is called slightly after the end of a frame on RealSense.
    // Since we know the frame exposure is done, we can turn LEDs off.
    turn_off_leds();

    const unsigned int current_frame_time = micros();
    const unsigned int frame_time_us = current_frame_time - last_frame_time;
    const unsigned int threshold_us =
        frame_time_us - PULSE_WIDTH_US - LINE_DELAY_US;
    last_frame_time = current_frame_time;

    // Then, we can figure out how long to delay before starting the timer.
    //    timer.begin(&handle_timer, THRESHOLD_US);
    timer_call_count = 0;
    timer.begin(&handle_timer, threshold_us);
    timer.priority(255);
  }

  static void handle_timer() {
    //    timer.end();
    turn_on_leds();

    timer_call_count++;
    // If the timer gets called a second time, that means it didn't get reset by
    // the trigger, and it should turn off the LED.
    if (timer_call_count > 1) {
      timer.end();
      turn_off_leds();
    }
  }

 public:
  explicit Flasher(boolean always_on = false) {
    pinMode(trigger_pin, INPUT_PULLDOWN);
    pinMode(switch_pin, OUTPUT);

    turn_off_leds();

    if (always_on) {
      turn_on_leds();
    } else {
      attachInterrupt(digitalPinToInterrupt(trigger_pin), &handle_trigger,
                      RISING);
    }
  }
};

// Need to declare where the timer goes, too.
template <int trigger_pin, int switch_pin>
IntervalTimer Flasher<trigger_pin, switch_pin>::timer;

template <int trigger_pin, int switch_pin>
unsigned int Flasher<trigger_pin, switch_pin>::last_frame_time;

template <int trigger_pin, int switch_pin>
volatile byte Flasher<trigger_pin, switch_pin>::timer_call_count;

#endif  // FLASHER_H
