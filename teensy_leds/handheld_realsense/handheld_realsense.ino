
#include "flasher.h"

constexpr int TRIGGER_PIN = 16;
constexpr int LED_PIN = 5;

Flasher<TRIGGER_PIN, LED_PIN> front_flasher(false);

void setup() {
  //  pinMode(LED_PIN, OUTPUT);
  Serial.begin(9600);

  pinMode(A14, OUTPUT);
  analogWriteResolution(12);
  analogWrite(A14, 2234);
}

void loop() {
  delay(1000);
  Serial.println("This is Vasu's Teensy, don't touch");
}
