#!/usr/bin/env python3

# This script is provided for example purposes for communication over Xavier i2c
# with the LED board. Not marked executable since it hasn't been tested. See the
# serial example for different mode and value examples.

# https://www.pololu.com/docs/0J73/15.9
import smbus2

bus = smbus2.SMBus(8)  # Uses pins 3 (SDA) and pins 5 (SCL) on the Xavier devkit
addr = 0x42  # Teensy i2c address

# Create an i2c msg and write it out to the bus
bus.i2c_rdwr(smbus2.i2c_msg.write(addr, [0x1A, 0x00]))

bus.close()
