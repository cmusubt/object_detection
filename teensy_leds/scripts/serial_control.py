#!/usr/bin/env python3

# This script is provided for example purposes for communication over serial
# with the LED board. Not marked as executable since it hasn't been tested.

import serial
import time

ser = serial.Serial("/dev/ttyACM0")


# Default mode
ser.write([0x00, 0x00])
print(ser.readline().decode("ascii"))
time.sleep(1)


# Cycle through a few values of brightness
ser.write([0x14, 0x00])  # about 25% brightness
print(ser.readline().decode("ascii"))
time.sleep(1)

ser.write([0x16, 0x00])  # roughly where it stops jittering
print(ser.readline().decode("ascii"))
time.sleep(1)

ser.write([0x1A, 0x00])  # probably a reasonable sustained brightness
print(ser.readline().decode("ascii"))
time.sleep(1)

ser.write([0x1F, 0xFF])  # full beans
print(ser.readline().decode("ascii"))
time.sleep(1)


# Switch to RealSense triggering
ser.write([0x2A, 0x00])  # should be reasonably bright if realsense is on
print(ser.readline().decode("ascii"))
time.sleep(1)

ser.write([0x20, 0x00])  # won't light up even if RealSense is on
print(ser.readline().decode("ascii"))
time.sleep(1)


ser.close()
