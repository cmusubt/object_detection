/*
Monitors the lidar_renderer and if it doesn't hear from it in
a while, it publishes a fake lidar image
in order to ensure the artifact localization pipeline continues
operating.


Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer
*/
#include "modules/fake_lidar_renderer.h"
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include "opencv2/imgproc/imgproc.hpp"

BaseNode* BaseNode::get() {
  auto* fake_lidar_renderer =
      new object_detection::FakeLidarRenderer("fake_lidar_renderer");
  return fake_lidar_renderer;
}

namespace object_detection {
FakeLidarRenderer::FakeLidarRenderer(std::string node_name)
    : BaseNode(std::move(node_name)), it(*node_handle_) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool FakeLidarRenderer::initialize() {
  const auto& node_name = ros::this_node::getName() + "/";
  auto* nh = get_node_handle();

  get_private_node_handle()->getParam("wait_time", wait_time);
  get_private_node_handle()->getParam("camera_topics", camera_topics);
  get_private_node_handle()->getParam("default_depth", default_depth);

  if (camera_topics.empty()) {
    ROS_ERROR(
        "\nNo camera topics have been specified for fake lidar renderer\n");
  }

  if (default_depth != 1.0) {
    ROS_WARN(
        "Default depth being too large or too small can affect localization "
        "results!");
  }

  for (std::size_t i = 0; i < camera_topics.size(); ++i) {
    // 360,640 default values, rs_depth_cb will take care of resize
    cv::Mat image = cv::Mat(360, 640, CV_16UC1, cv::Scalar(default_depth));
    default_imgs.push_back(
        cv_bridge::CvImage(std_msgs::Header(), "mono16", image).toImageMsg());

    lidar_img_subs.push_back(it.subscribe(
        camera_topics[i] + "/color/aligned_lidar/image", 1,
        boost::bind(&FakeLidarRenderer::lidar_img_cb, this, _1, i)));

    lidar_img_times.push_back(ros::Time::now().toSec());

    detection_subs.push_back(nh->subscribe<objdet_msgs::DetectionArray>(
        camera_topics[i] + "/color/image/detections", 1,
        boost::bind(
            &FakeLidarRenderer::detection_cb, this, _1, i,
            it.advertise(camera_topics[i] + "/color/aligned_lidar/image", 1))));

    rs_depth_img_subs.push_back(it.subscribe(
        camera_topics[i] + "/aligned_depth_to_color/image", 1,
        boost::bind(&FakeLidarRenderer::rs_depth_cb, this, _1, i)));
  }

  return true;
}

bool FakeLidarRenderer::execute() {
  // todo: do I need this to keep the subscribers running?
  // this entire class is subscriber-run so no need to
  // have anything substantial here
  ros::spin();
  return true;
}

void FakeLidarRenderer::rs_depth_cb(const sensor_msgs::ImageConstPtr& msg,
                                    const std::size_t& topic_index) {
  // change the fake lidar image size to the image size of the depth image
  int rows, cols;
  cv_bridge::CvImageConstPtr cv_ptr;

  cv_ptr = cv_bridge::toCvShare(msg);
  rows = cv_ptr->image.rows;
  cols = cv_ptr->image.cols;

  // resize the default lidar depth image to match the rs_depth image dims
  if (default_imgs.size() > topic_index) {
    cv::Mat image = cv::Mat(rows, cols, CV_16UC1, cv::Scalar(default_depth));
    default_imgs[topic_index] =
        cv_bridge::CvImage(std_msgs::Header(), "mono16", image).toImageMsg();
  } else {
    ROS_WARN(
        "Something went wrong with keeping track of default lidar depth "
        "images.");
  }

  // we have the image dims so no need to subscriber to this again for this
  // topic_index (camera)
  if (rs_depth_img_subs.size() > topic_index) {
    rs_depth_img_subs[topic_index].shutdown();
  } else {
    ROS_WARN(
        "Something went wrong with the fake lidar renderer rs_depth "
        "subscribers");
  }
}

void FakeLidarRenderer::lidar_img_cb(const sensor_msgs::ImageConstPtr& msg,
                                     const std::size_t& topic_index) {
  cv_bridge::CvImageConstPtr cv_ptr;
  cv_ptr = cv_bridge::toCvShare(msg);

  if (default_imgs.size() <= topic_index) {
    ROS_WARN(
        "Something went wrong with keeping track of default lidar depth "
        "images.");
    return;
  }

  if (lidar_img_times.size() <= topic_index) {
    ROS_WARN(
        "Something went wrong with keeping track of default lidar depth image "
        "times.");
    return;
  }

  // if its justthe default image, dont count this as having received something
  // from the lidar renderer recently

  // todo: can we compare these like this? seems to work...
  if (msg->data == default_imgs[topic_index]->data) {
    return;
  }

  lidar_img_times[topic_index] = ros::Time::now().toSec();
}

void FakeLidarRenderer::detection_cb(
    const objdet_msgs::DetectionArrayConstPtr& dets_msg,
    const std::size_t& topic_index,
    const image_transport::Publisher& lidar_pub) {
  if (lidar_img_times.size() <= topic_index) {
    ROS_WARN(
        "Something went wrong with keeping track of default lidar depth image "
        "times.");
    return;
  }

  if (default_imgs.size() <= topic_index) {
    ROS_WARN(
        "Something went wrong with keeping track of default lidar depth "
        "images.");
    return;
  }

  if (ros::Time::now().toSec() - lidar_img_times[topic_index] > wait_time) {
    ROS_WARN_THROTTLE(
        1, "Lidar renderer may be down, publishing fake lidar depth image");
    // todo most efficient way to do this?
    sensor_msgs::Image lidar_depth_img;
    lidar_depth_img = *default_imgs[topic_index];
    lidar_depth_img.header.stamp = dets_msg->header.stamp;
    lidar_pub.publish(lidar_depth_img);
  }
}

}  // namespace object_detection
