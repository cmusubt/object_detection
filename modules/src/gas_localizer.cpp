#include "modules/gas_localizer.h"
#include <common/tf2.h>
#include <image_transport/image_transport.h>
#include <math.h>
#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <sensor_msgs/Image.h>
#include <algorithm>
#include <cmath>
#include <exception>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include "opencv2/imgproc/imgproc.hpp"

BaseNode* BaseNode::get() {
  auto* localizer = new object_detection::GasLocalizer("gas_localizer");
  return localizer;
}

namespace object_detection {
GasLocalizer::GasLocalizer(std::string node_name)
    : BaseNode(std::move(node_name)), it(*node_handle_) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool GasLocalizer::initialize() {
  const auto& node_name = ros::this_node::getName() + "/";
  auto* nh = get_node_handle();

  std::vector<std::string> topics;

  get_private_node_handle()->getParam("gas_topics", topics);
  for (const auto& topic : topics) {
    gas_reading_subs.push_back(nh->subscribe<objdet_msgs::GasInfo>(
        topic, 10, boost::bind(&GasLocalizer::gas_reading_cb, this, _1)));
  }

  if (gas_reading_subs.empty()) {
    ROS_WARN("No topics set for gas  localizer!");
    return false;
  }

  get_private_node_handle()->getParam("save_gas_imgs", save_imgs);
  get_private_node_handle()->getParam("adaptive_thresholding",
                                      adaptive_thresholding);
  get_private_node_handle()->getParam("adaptive_thresholding_ugv",
                                      adaptive_thresholding_ugv);
  get_private_node_handle()->getParam("img_streams", img_topics);
  get_private_node_handle()->getParam("vehicle_type", vehicle_type);

  if (save_imgs && (img_topics.empty())) {
    ROS_WARN("\nYou want to save images but no topics have been specified!\n");
  }

  if (vehicle_type.empty()) {
    ROS_WARN(
        "\n\nNo vehicle type has been set. The ground vertical location for "
        "the gas artifact localization will be the same as the velodyne\n\n");
  } else if (vehicle_type.compare("drone") == 0) {
    // we need a way to determine height off of ground

    get_private_node_handle()->getParam("downward_depth_topic",
                                        downward_depth_topic);

    if (downward_depth_topic.empty()) {
      ROS_WARN(
          "\nNo downward facing camera topic has been set. For the drone, the "
          "ground vertical location for the gas artifact localization will be "
          "the same as the velodyne\n");
    } else {
      depth_img_sub =
          it.subscribe(downward_depth_topic, 1,
                       boost::bind(&GasLocalizer::depth_img_cb, this, _1));
      last_depth_img_save_time = ros::Time::now().toSec();
    }
  }

  if (adaptive_thresholding) {
    in_air_sub = nh->subscribe<std_msgs::Bool>(
        "/in_air", 10, boost::bind(&GasLocalizer::in_air_cb, this, _1));
  }
  // save the most recent images in order to associate images with the
  // concentration readings
  recent_imgs.resize(img_topics.size());

  for (std::size_t i = 0; i < img_topics.size(); ++i) {
    img_subs.push_back(it.subscribe(
        img_topics[i], 1, boost::bind(&GasLocalizer::rgb_img_cb, this, _1, i)));

    // so we only save each image stream image once per
    // specified period (e.g. 1 Hz)
    last_img_save_time.push_back(ros::Time::now().toSec());
  }

  artifact_localizations_publisher =
      nh->advertise<objdet_msgs::ArtifactLocalizationArray>(
          "artifact_localizations", 10);

  delay_subscriber.initialize(nh);
  key_pose_listener.initialize(nh, &tf_buffer);
  tf_listener_ptr = std::unique_ptr<tf2_ros::TransformListener>(
      new tf2_ros::TransformListener(tf_buffer));

  return true;
}

bool GasLocalizer::execute() {
  ROS_INFO("Have readings for %lu gas clouds", gas_readings.size());

  Eigen::Vector3f position;
  for (auto& gas_reading : gas_readings) {
    ROS_INFO_STREAM("    Cloud " << gas_reading.id << " has "
                                 << gas_reading.readings.size() << " readings");

    // Need to convert the readings from key pose frame to map frame.
    std::vector<signal_solvers::ReadingFromPosition> solver_readings;
    solver_readings.reserve(gas_reading.readings.size());
    std::transform(
        gas_reading.readings.begin(), gas_reading.readings.end(),
        std::back_inserter(solver_readings), [&](const auto& reading) {
          const auto& key_pose =
              key_pose_listener.get_key_pose_by_index(reading.key_pose_index);
          tf2::Transform key_pose_to_map_tf;
          tf2::fromMsg(key_pose, key_pose_to_map_tf);
          const auto robot_position_map =
              key_pose_to_map_tf * reading.robot_position;

          signal_solvers::ReadingFromPosition r;
          r.robot_position = {static_cast<float>(robot_position_map.x()),
                              static_cast<float>(robot_position_map.y()),
                              static_cast<float>(robot_position_map.z())};
          r.raw_reading = reading.concentration;
          return r;
        });

    for (std::size_t i = 0; i < gas_reading.readings.size(); ++i) {
      gas_reading.readings[i].reading_pos_map_frame[0] =
          solver_readings[i].robot_position[0];
      gas_reading.readings[i].reading_pos_map_frame[1] =
          solver_readings[i].robot_position[1];
      gas_reading.readings[i].reading_pos_map_frame[2] =
          solver_readings[i].robot_position[2];
    }

    int max_rssi_index = 0;
    // TODO(@debortor): change to something that also does door detection
    // not just something that finds the max
    if (!signal_solvers::find_artifact_max_rssi(solver_readings, position,
                                                max_rssi_index)) {
      continue;
    }

    // check if the id has been checked
    // if it has not,

    // Find images to go along with the artifacts
    if (save_imgs && !(img_topics.empty())) {
      cached_artifacts[gas_reading.id].update_imgs(
          gas_reading.readings[max_rssi_index].images);
    }

    // Update all of the Concentration/position pairs we have for this device
    cached_artifacts[gas_reading.id].update_readings(solver_readings);

    // Artifact now contains something useful.
    const auto updated =
        cached_artifacts[gas_reading.id].update_position(position);
    if (updated) {
      // ROS_INFO_STREAM("Updated position for address " << mac_address);
    }
  }

  objdet_msgs::ArtifactLocalizationArray artifacts;
  artifacts.header.stamp = ros::Time::now();
  artifacts.header.frame_id = MAP_FRAME;
  for (auto& cached_artifact : cached_artifacts) {
    // if (!cached_artifact.get_dirty()) {
    //  continue;
    //}

    artifacts.localizations.emplace_back(cached_artifact.get_artifact());
    cached_artifact.clean();
  }

  // Only publish actual updates
  if (!artifacts.localizations.empty()) {
    artifact_localizations_publisher.publish(artifacts);
  }

  return true;
}

int GasLocalizer::gen_new_cloud() {
  /* Returns id of new cloud
   */
  uint32_t id = gas_readings.size();
  GasLocalizer::GasReadings new_gas_reading_set;

  new_gas_reading_set.id = id;
  gas_readings.push_back(new_gas_reading_set);
  cached_artifacts.emplace_back(id);  // So that we can directly access it
  cached_artifacts.back().update_valid(true);       // Always valid
  cached_artifacts.back().update_confidence(1.0f);  // We know it's gas
  cached_artifacts.back().update_class_id(class_labels.GAS_ID);
  cached_artifacts.back().clean();

  return id;
}

void GasLocalizer::gas_reading_cb(const objdet_msgs::GasInfoConstPtr& reading) {
  monitor.tic("gas_reading_cb");

  if (adaptive_thresholding) {
    // todo (@debortor): make this an average rather than the last level?
    steady_state_gas = reading->co2_concentration;
  }

  else if (adaptive_thresholding_ugv){
    float orig_min_concentration = MIN_CONCENTRATION;
    MIN_CONCENTRATION = reading->co2_concentration + gas_buffer;
    ROS_WARN("MIN_CONCENTRATION for gas detections for ugv changed from %f to: %f",
             orig_min_concentration, MIN_CONCENTRATION);
    adaptive_thresholding_ugv = false; // dont reset threshold again
  }

  // we'll always get readings of ambient co2 concentrations,
  // let's make sure it actually is some gas source
  if (reading->co2_concentration < MIN_CONCENTRATION) {
    // ROS_INFO_STREAM("Concentration too low: " << reading->co2_concentration);
    return;
  }

  const auto query_time = //ros::Time::now();
                           reading->header.stamp - delay_subscriber.get_delay_duration();
                           //- sensor_detection_time;

  // Get key pose
  const auto key_pose_index =
      key_pose_listener.get_key_pose_index_at_time(query_time);
  if (!key_pose_index) {
    // ROS_INFO_STREAM("Key pose no good!");
    return;
  }
  const auto& key_pose =
      key_pose_listener.get_key_pose_by_index(*key_pose_index);
  tf2::Transform key_pose_to_map_tf;
  tf2::fromMsg(key_pose, key_pose_to_map_tf);

  // Get robot (gas reading) position in key frame
  tf2::Transform gas_reading_to_map_tf;
  try {
    const auto gas_reading_to_map = tf_buffer.lookupTransform(
        MAP_FRAME,
        "sensor",
        // sanitize_frame(reading->header.frame_id),
        query_time,
        ros::Duration(.1));
    tf2::fromMsg(gas_reading_to_map.transform, gas_reading_to_map_tf);
  } catch (tf2::TransformException& e) {
    ROS_WARN("%s", e.what());
    return;
  }
  const auto gas_reading_to_key_pose_tf =
      key_pose_to_map_tf.inverse() * gas_reading_to_map_tf;

  ReadingFromKeyPosePosition gas_reading;
  gas_reading.robot_position = gas_reading_to_key_pose_tf.getOrigin();
  gas_reading.robot_position[1] =
      gas_reading.robot_position[1] -
      find_ground(gas_reading);  // gas localization point is on the floor
  gas_reading.key_pose_index = *key_pose_index;
  gas_reading.concentration = reading->co2_concentration;

  // put the most recent images captured in gas reading
  gas_reading.images = recent_imgs;

  // for our first reading
  if (gas_readings.empty()) {
    uint32_t id = gen_new_cloud();
    gas_readings[id].readings.push_back(gas_reading);
    return;
  }

  // determine which cloud, if any the reading belongs to
  // if it belongs to none, make a new cloud

  // convert the position to the map frame
  tf2::fromMsg(key_pose, key_pose_to_map_tf);
  const auto robot_position_map =
      key_pose_to_map_tf * gas_reading.robot_position;

  GasLocalizer::ClosestReading closest_reading;
  closest_reading = find_closest_reading(robot_position_map);

  // if there are no other readings, or no other readings that are close
  if ((closest_reading.id == -1) || closest_reading.dist > CLUSTER_SPLIT_DIST) {
    uint32_t id = gen_new_cloud();
    gas_readings[id].readings.push_back(gas_reading);
  } else {
    gas_readings[closest_reading.id].readings.push_back(gas_reading);
  }

  monitor.toc("gas_reading_cb");
}

float GasLocalizer::find_ground(const ReadingFromKeyPosePosition& gas_reading) {
  // gas localization pt is the ground, so we need to
  // find the ground in the local frame

  // z offset should be positive

  float z_offset = 0.0;

  if (vehicle_type.empty()) {
    return z_offset;
  }

  if (vehicle_type.compare("ugv") == 0) {
    z_offset = UGV_VELODYNE_HEIGHT;
  } else if (vehicle_type.compare("drone") == 0) {
    z_offset = get_drone_height();
  }

  return z_offset;
}

GasLocalizer::ClosestReading GasLocalizer::find_closest_reading(
    const tf2::Vector3& new_reading_pos) {
  /*
  Finds the closest co2 reading to determine if this is a cloud we have seen
  before. Everything done in loop-closed map frame
  */
  GasLocalizer::ClosestReading closest_reading;
  closest_reading.id = -1;

  for (const auto& cloud : gas_readings) {
    for (const auto& gas_reading : cloud.readings) {
      // if closest_reading hasn't been set yet
      if (closest_reading.id == -1) {
        closest_reading.id = cloud.id;
        closest_reading.dist =
            new_reading_pos.distance(gas_reading.reading_pos_map_frame);
      } else {
        float dist =
            new_reading_pos.distance(gas_reading.reading_pos_map_frame);

        if (dist < 1E-6) {
          // its the same reading
          continue;
        }

        if (dist < closest_reading.dist) {
          closest_reading.id = cloud.id;
          closest_reading.dist = dist;
        }
      }
    }
  }

  if (closest_reading.id == -1) {
    ROS_INFO_STREAM("\nCould not find a closest reading!!\n");
  }

  return closest_reading;
}

float GasLocalizer::euclid_dist(const tf2::Vector3& point_a,
                                const tf2::Vector3& point_b) {
  double x = point_a.getX() - point_b.getX();
  double y = point_a.getY() - point_b.getY();
  double z = point_a.getZ() - point_b.getZ();

  float dist =
      pow(x, 2) + pow(y, 2) + pow(z, 2);  // calculating Euclidean distance
  dist = sqrt(dist);

  return dist;
}

void GasLocalizer::rgb_img_cb(const sensor_msgs::ImageConstPtr& msg,
                              const std::size_t& topic_index) {
  if (save_imgs && (ros::Time::now().toSec() - last_img_save_time[topic_index] >
                    img_save_period)) {
    if (topic_index < recent_imgs.size()) {
      recent_imgs[topic_index] = msg;
    }
    last_img_save_time[topic_index] = ros::Time::now().toSec();
  }
}

void GasLocalizer::depth_img_cb(const sensor_msgs::ImageConstPtr& msg) {
  // save depth image, can be used later to determine height above floor for
  // drone
  if (vehicle_type.compare("drone") == 0 &&
      (ros::Time::now().toSec() - last_depth_img_save_time >
       depth_img_save_period)) {
    downward_depth_img = msg;
    last_depth_img_save_time = ros::Time::now().toSec();
  }
}

float GasLocalizer::get_drone_height() {
  // get drone height from median of downward-facing depth image
  // height is w.r.t. velodyne

  if (downward_depth_img == sensor_msgs::ImageConstPtr()) {
    ROS_WARN(
        "Drone depth image isn't populated. Using velodyne height as gas "
        "sensor height");
    return 0.0;
  }

  // convert to a cv image
  cv_bridge::CvImageConstPtr downward_depth_cv_ptr;
  downward_depth_cv_ptr = cv_bridge::toCvShare(
      downward_depth_img);  //, sensor_msgs::image_encodings::BGR8);
  cv::Mat center_patch;
  center_patch =
      downward_depth_cv_ptr->image.colRange(250, 350).rowRange(130, 230);

  // https://stackoverflow.com/questions/30078756/super-fast-median-of-matrix-in-opencv-as-fast-as-matlab
  // COMPUTE HISTOGRAM OF SINGLE CHANNEL MATRIX
  int max_range, n_bins;
  max_range = 10000.0;  // must be in mm
  n_bins = 1000;

  float range[] = {0, static_cast<float>(max_range)};
  const float* histRange = {range};
  bool uniform = true;
  bool accumulate = false;
  cv::Mat hist;
  cv::calcHist(&center_patch, 1, 0, cv::Mat(), hist, 1, &n_bins, &histRange,
               uniform, accumulate);

  // COMPUTE CUMULATIVE DISTRIBUTION FUNCTION (CDF)
  cv::Mat cdf;
  hist.copyTo(cdf);
  for (int i = 1; i <= n_bins - 1; i++) {
    cdf.at<float>(i) += cdf.at<float>(i - 1);
  }
  cdf /= center_patch.total();

  // // COMPUTE MEDIAN
  double medianVal;
  for (int i = 0; i <= n_bins - 1; i++) {
    if (cdf.at<float>(i) >= 0.5) {
      medianVal = i;
      break;
    }
  }

  // convert to m from mm
  return medianVal /
         (static_cast<float>(n_bins) / static_cast<float>(max_range) * 1000.0);
}

void GasLocalizer::adaptive_threshold_timer_cb(const ros::TimerEvent& event) {
  in_air_sub.shutdown();
  float orig_min_concentration = MIN_CONCENTRATION;
  MIN_CONCENTRATION = steady_state_gas + gas_buffer;
  ROS_WARN("MIN_CONCENTRATION for gas detections changed from %f to: %f",
           orig_min_concentration, MIN_CONCENTRATION);
  adaptive_gas_timer.stop();
}

void GasLocalizer::in_air_cb(const std_msgs::BoolConstPtr& msg) {
  if (msg->data && !gas_timer_waiting) {
    auto* nh = get_node_handle();
    adaptive_gas_timer = nh->createTimer(
        ros::Duration(5.0f), &GasLocalizer::adaptive_threshold_timer_cb, this);
    gas_timer_waiting = true;  // so we dont just keep creating timers
  }
}

}  // namespace object_detection
