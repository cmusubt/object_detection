#include "modules/state_estimate_delay_estimator.h"

BaseNode* BaseNode::get() {
  auto* estimator = new object_detection::StateEstimateDelayEstimator(
      "state_estimate_delay_estimator_node");
  return estimator;
}

namespace object_detection {
StateEstimateDelayEstimator::StateEstimateDelayEstimator(std::string node_name)
    : BaseNode(std::move(node_name)) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool StateEstimateDelayEstimator::initialize() {
  ros::NodeHandle* nh = get_node_handle();
  auto* pnh = get_private_node_handle();

  pnh->param<decltype(counter_step)>("counter_step", counter_step,
                                     counter_step);
  pnh->param<decltype(max_delay)>("max_delay", max_delay, max_delay);
  pnh->param<decltype(min_delay)>("max_delay", min_delay, min_delay);

  sensor_pose_sub = nh->subscribe("/integrated_to_map", 1,
                                  &StateEstimateDelayEstimator::delay_cb, this);
  health_pub = nh->advertise<std_msgs::Bool>("state_estimate_delay_health", 10);
  delay_pub = nh->advertise<std_msgs::Float64>("state_estimate_delay", 10);

  ROS_INFO("state estimation delay estimator successfully initialized!");
  return true;
}

bool StateEstimateDelayEstimator::execute() { return true; }

void StateEstimateDelayEstimator::delay_cb(
    const nav_msgs::OdometryConstPtr& sensor_pose) {
  // add a counter so that we only recalculate at 10hz instead of 200hz
  if ((counter % counter_step) != 0) {
    ++counter;
    return;
  }
  counter = 0;  // set counter back to 0 to avoid potential overflow
  ++counter;

  const auto delay = ros::Time::now() - sensor_pose->header.stamp;
  const auto delay_secs = delay.toSec() * (1 - sensor_pose_delay_alpha);
  sensor_pose_delay = sensor_pose_delay * sensor_pose_delay_alpha + delay_secs;
  delay_pub_msg.data = sensor_pose_delay;

  // check to see if the sensor pose delay is abnormal
  if (min_delay <= sensor_pose_delay && sensor_pose_delay <= max_delay) {
    ROS_INFO_STREAM_THROTTLE(1, "Sensor pose delay: " << sensor_pose_delay);
    delay_is_valid_msg.data = true;
  } else {
    ROS_WARN_THROTTLE(1, "Sensor pose delay of %f is out of bounds [%f, %f]",
                      sensor_pose_delay, min_delay, max_delay);
    delay_is_valid_msg.data = false;
  }

  delay_pub.publish(delay_pub_msg);
  health_pub.publish(delay_is_valid_msg);

  return;
}
}  // namespace object_detection
