#include "modules/point_cloud_renderer.h"
#include <common/tf2.h>
#include <pcl/common/transforms.h>
#include <ros/console.h>
#include <stdlib.h>
#include <algorithm>
#include <iostream>

namespace object_detection {

PointCloudRenderer::PointCloudRenderer() : timer("CPU Renderer") {
  std::string ueye_intrinsics;
  nhPriv.getParam("ueye_intrinsics", ueye_intrinsics);
  if (ueye_intrinsics.empty()) {
    ROS_WARN(
        "Ueye intrinsics file not provided. If this is on the drone it wont be "
        "able to localize");
  } else {
    // even if the ueye model is not used, there is not an issue loading this
    // camera model
    camera = camodocal::CameraFactory::instance()->generateCameraFromYamlFile(
        ueye_intrinsics);
  }
}

void PointCloudRenderer::set_cloud(const PointCloudConstPtr& new_cloud_ptr) {
  cloud_ptr = new_cloud_ptr;
}

cv::Mat PointCloudRenderer::render(
    const sensor_msgs::CameraInfoConstPtr& camera_info_ptr,
    const tf2::Transform& map_to_camera_tf, const int inflate_const) {
  timer.tic("overall render");

  timer.tic("mat creation");
  const auto height = camera_info_ptr->height;
  const auto width = camera_info_ptr->width;
  cv::Mat mat =
      cv::Mat::zeros(camera_info_ptr->height, camera_info_ptr->width, CV_16UC1);
  timer.toc("mat creation");

  // Transform the map to the camera frame
  timer.tic("transform cloud");
  PointCloudPtr map_camera_frame(new PointCloud);
  const auto map_to_camera_eigen = eigen_tf_from_tf2(map_to_camera_tf);
  pcl::transformPointCloud(*cloud_ptr, *map_camera_frame, map_to_camera_eigen);
  timer.toc("transform cloud");

  const auto fx = camera_info_ptr->K[0];
  const auto fy = camera_info_ptr->K[4];
  const auto cx = camera_info_ptr->K[2];
  const auto cy = camera_info_ptr->K[5];

  timer.tic("project points");
  for (const auto& point : *map_camera_frame) {
    if (point.z <= 0.2) {  // point behind the camera, ignore.
      continue;
    }

    const auto u = static_cast<int>((fx * point.x) / point.z + cx);
    const auto v = static_cast<int>((fy * point.y) / point.z + cy);

    int inflate = static_cast<int>(floor(inflate_const / point.z / 2));
    // inflate = inflate > 16 ? 16 : inflate;

    if (u + inflate >= 0 or u - inflate < width or v + inflate >= 0 or
        v - inflate < height) {  // Ensure coordinates are within image
      const auto new_depth = static_cast<uint16_t>(point.z * 1000.0);
      for (int i = std::max(v - inflate, 0);
           i <= std::min(v + inflate, static_cast<int>(height - 1)); ++i) {
        for (int j = std::max(u - inflate, 0);
             j <= std::min(u + inflate, static_cast<int>(width - 1)); ++j) {
          auto& current_depth = mat.at<uint16_t>(i, j);
          if (current_depth == 0 || new_depth < current_depth) {
            current_depth = new_depth;
          }
        }
      }
    }
  }
  timer.toc("project points");
  timer.toc("overall render");

  return mat;
}

cv::Mat PointCloudRenderer::render_fisheye(
    const sensor_msgs::CameraInfoConstPtr& camera_info_ptr,
    const tf2::Transform& map_to_camera_tf, const int inflate_const) {
  timer.tic("overall render");

  timer.tic("dst_persp creation");
  const auto height = camera_info_ptr->height;
  const auto width = camera_info_ptr->width;
  cv::Mat mat =
      cv::Mat::zeros(camera_info_ptr->height, camera_info_ptr->width, CV_16UC1);
  timer.toc("dst_persp creation");

  // Transform the map to the camera frame
  timer.tic("transform cloud");
  PointCloudPtr map_camera_frame(new PointCloud);
  const auto map_to_camera_eigen = eigen_tf_from_tf2(map_to_camera_tf);
  pcl::transformPointCloud(*cloud_ptr, *map_camera_frame, map_to_camera_eigen);
  timer.toc("transform cloud");

  timer.tic("project points");
  // double img_pixel[2] = {0,0};
  // double point_arr[3] = {0,0,0};
  Eigen::Vector2d img_pixel = {0, 0};
  Eigen::Vector3d point_arr = {0, 0, 0};

  for (const auto& point : *map_camera_frame) {
    if (point.z <= 0) {  // point behind the camera, ignore.
      continue;
    }
    point_arr[0] = point.x;
    point_arr[1] = point.y;
    point_arr[2] = point.z;
    camera->spaceToPlane(point_arr, img_pixel);
    const auto u = static_cast<int>(img_pixel[0]);
    const auto v = static_cast<int>(img_pixel[1]);
    // int inflate = static_cast<int>(ceil(inflate_const / point.z / 4));
    int inflate = 4;
    if (point.z < 0.2) {
      continue;
    }
    if (point.z < 0.5) {
      inflate = 8;
    }

    if (0 + inflate <= u and u < width - inflate and u < width and
        0 + inflate <= v and v < height - inflate and
        v < height) {  // Ensure coordinates are within image
      const auto new_depth = static_cast<uint16_t>(point.z * 1000.0);
      for (int i = -inflate; i <= inflate; ++i) {
        for (int j = -inflate; j <= inflate; ++j) {
          auto& current_depth = mat.at<uint16_t>(v + i, u + j);
          if (current_depth == 0 || new_depth < current_depth) {
            current_depth = new_depth;
          }
        }
      }
    }
  }

  timer.toc("project points");
  timer.toc("overall render");

  return mat;
}

void PointCloudRenderer::prLiftProjective(const Eigen::Vector2d& p,
                                          Eigen::Vector3d& P) {
  camera->liftProjective(p, P);
}
}  // namespace object_detection
