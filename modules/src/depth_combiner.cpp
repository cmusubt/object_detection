#include "modules/depth_combiner.h"
#include <common/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <boost/bind.hpp>
#include <sstream>

BaseNode* BaseNode::get() {
  auto* combiner = new object_detection::DepthCombiner("depth_combiner_node");
  return combiner;
}

namespace object_detection {
DepthCombiner::DepthCombiner(std::string node_name)
    : BaseNode(std::move(node_name)), it(*node_handle_) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool DepthCombiner::initialize() {
  ros::NodeHandle* nh = get_node_handle();

  std::map<std::string, float> camera_topics_depth;
  get_private_node_handle()->getParam("camera_topics_depth",
                                      camera_topics_depth);
  if (camera_topics_depth.empty()) {
    ROS_WARN("No camera topics and depth specified for depth combiner!");
  }

  for (const auto& topic_depth : camera_topics_depth) {
    const auto camera_topic = topic_depth.first;
    const float threshold_depth = topic_depth.second;
    const uint16_t threshold_depth_scaled =
        static_cast<uint16_t>(threshold_depth * 1000.0f);
    ROS_INFO_STREAM("topic name: " << camera_topic
                                   << "/ depth: " << threshold_depth);

    const auto depth_topic = camera_topic + "/aligned_depth_to_color/image";
    const auto lidar_topic = camera_topic + "/color/aligned_lidar/image";
    const auto pub_topic = camera_topic + "/combined_depth/image";

    disable_transports(nh, pub_topic);
    combined_pubs.push_back(it.advertise(pub_topic, 1));

    depth_subs.emplace_back(new DepthLidarSub(nh, depth_topic, lidar_topic));
    depth_subs.back()->registerCallback(
        boost::bind(&DepthCombiner::combine_cb, this, combined_pubs.back(),
                    threshold_depth_scaled, _1, _2));
  }

  return true;
}

bool DepthCombiner::execute() { return true; }

void DepthCombiner::combine_cb(const image_transport::Publisher& image_pub,
                               const uint16_t& thresh_depth_scaled,
                               const sensor_msgs::ImageConstPtr& depth_msg,
                               const sensor_msgs::ImageConstPtr& lidar_msg) {
  monitor.tic("combine_cb");
  const auto depth_bridge = cv_bridge::toCvShare(depth_msg);
  const auto lidar_bridge = cv_bridge::toCvShare(lidar_msg);
  const auto depth_image = depth_bridge->image;
  const auto lidar_image = lidar_bridge->image;

  if (depth_image.rows != lidar_image.rows ||
      depth_image.cols != lidar_image.cols) {
    std::ostringstream out;
    out << "Image sizes don't match: "
        << "Depth = (" << depth_image.cols << ", " << depth_image.rows << ") "
        << "Lidar = (" << lidar_image.cols << ", " << lidar_image.rows << ")";
    fail(out.str());
    return;
  }

  if (depth_image.depth() != CV_16U || lidar_image.depth() != CV_16U) {
    std::ostringstream out;
    out << "Image depths don't match: "
        << "Depth = " << depth_image.depth() << ", "
        << "Lidar = " << lidar_image.depth();
    fail(out.str());
    return;
  }

  if (!depth_image.isContinuous() || !lidar_image.isContinuous()) {
    std::ostringstream out;
    out << "Images aren't continuous: "
        << "Depth = " << depth_image.isContinuous() << ", "
        << "Lidar = " << lidar_image.isContinuous();
    fail(out.str());
    return;
  }

  cv_bridge::CvImage combined_bridge;
  combined_bridge.header = depth_msg->header;
  combined_bridge.encoding = depth_bridge->encoding;
  combined_bridge.image =
      cv::Mat(depth_image.rows, depth_image.cols, depth_image.type());

  const size_t pixels = depth_image.rows * depth_image.cols;
  auto depth_ptr = reinterpret_cast<const uint16_t* const>(depth_image.data);
  auto lidar_ptr = reinterpret_cast<const uint16_t* const>(lidar_image.data);
  auto combined_ptr =
      reinterpret_cast<uint16_t* const>(combined_bridge.image.data);

  monitor.tic("filtering");
  for (size_t i = 0; i < pixels; ++i) {
    const auto depth_pixel = depth_ptr[i];
    const auto lidar_pixel = lidar_ptr[i];

    // Take the LIDAR depth past the limit (or where there's no depth), and
    // keep the RealSense depth otherwise.
    if (depth_pixel == 0 || depth_pixel > thresh_depth_scaled) {
      *combined_ptr++ = lidar_pixel;
    } else {
      *combined_ptr++ = depth_pixel;
    }
  }
  monitor.toc("filtering");

  image_pub.publish(combined_bridge.toImageMsg());
  monitor.toc("combine_cb");
}
}  // namespace object_detection