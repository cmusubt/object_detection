#include "modules/artifact_filter.h"
#include <common/math.h>
#include <common/ranges.h>
#include <pcl/common/centroid.h>
#include <pcl/kdtree/kdtree.h>
#include <algorithm>
#include <boost/bind.hpp>
#include <numeric>
#include <unordered_set>

BaseNode *BaseNode::get()
{
  auto *filter = new object_detection::ArtifactFilter("artifact_filter");
  return filter;
}

namespace object_detection
{
  ArtifactFilter::ArtifactFilter(std::string node_name)
      : BaseNode(std::move(node_name))
  {
    // Actually using the execute function this time!
    get_private_node_handle()->setParam("execute_target", 1);
  }

  bool ArtifactFilter::initialize()
  {

    artifact_centroids = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
    std::vector<std::string> artifact_topics;
    get_private_node_handle()->getParam("topics", artifact_topics);
    if (artifact_topics.empty())
    {
      ROS_WARN("No topics specified for artifact filter");
      return false;
    }

    float min_match_dist = -1;
    if (get_private_node_handle()->getParam("match_threshold", min_match_dist))
    {
      if (min_match_dist >= 0)
      {
        MATCH_THRESHOLD = min_match_dist;
        MATCH_THRESHOLD_SQ = MATCH_THRESHOLD * MATCH_THRESHOLD;
        ROS_INFO_STREAM("Setting Match Radius " << MATCH_THRESHOLD);
      }
    }

    float cluster_tolerance = -1;
    if (get_private_node_handle()->getParam("cluster_tolerance", cluster_tolerance))
    {
      if (cluster_tolerance >= 0)
      {
        ROS_INFO_STREAM("Setting Cluster Tolerance " << cluster_tolerance);
        CLUSTER_TOLERANCE = cluster_tolerance;
      }
      else
      {
        ROS_ERROR_STREAM("Can't have a negative value using default tolerance of " << CLUSTER_TOLERANCE);
      }
    }

    bool ignore_cell_visual = true;
    if (get_private_node_handle()->getParam("ignore_visual_cell_phones", ignore_cell_visual))
    {
      ROS_INFO_STREAM("Should ignore visual cell phones? " << ignore_cell_visual);
      ignore_visual_cell_phones = ignore_cell_visual;
    }

    bool bypass_filter = false;
    if (get_private_node_handle()->getParam("bypass_filter", bypass_filter))
    {
      ROS_INFO_STREAM("BYPASS: " << bypass_filter);
      bypass_artifact_filter = bypass_filter;
    }

    auto *nh = get_node_handle();


  /// Initialize the report id...                                                                                                                                                                                                                                                  
  std::string report_file("/home/developer/deploy_ws/filter_node");
  report_file.append("_report.bin");

  std::ifstream report_file_stream(report_file.c_str(), std::ios::in | std::ios::binary);

  if (report_file_stream.good()) {
    report_file_stream >> last_report_id;
    ROS_INFO_STREAM("Previous reports exists....starting at " << last_report_id);
  } else {
    if (report_file_stream){
      ROS_INFO_STREAM("not good but can we open???");
      report_file_stream >> last_report_id;
      ROS_INFO_STREAM("Previous reports exists....starting at " << last_report_id);

    }
    ROS_INFO_STREAM("No reports exist....starting at " << last_report_id);
  }

  report_file_stream.close();
  report_id_stream.open(report_file, std::ios::out | std::ios::binary);
  report_id_stream.seekp(0);
  report_id_stream << last_report_id;
          //report_id_stream.write(reinterpret_cast<const char *>(&last_report_id), sizeof(uint16_t));
  report_id_stream.flush();

    // Generate callbacks for each of the topics by giving each topic a separate
    // map to fill out. Note that the map is bound directly to the callback rather
    // than passing in a string (or similar) to avoid a lookup every time.

    //all_artifacts.reserve(artifact_topics.size());
    for (const auto &artifact_topic : artifact_topics)
    {
      //all_artifacts.emplace_back();
      ROS_INFO_STREAM("Artifact filter subscribing to " << artifact_topic);
      artifact_subs.push_back(nh->subscribe(artifact_topic, 1000, &ArtifactFilter::artifact_cb, this));
    }

    artifact_localizations_publisher =
        nh->advertise<objdet_msgs::ArtifactLocalizationArray>(
            "artifact_localizations", 10);


    return true;
  }

  bool ArtifactFilter::execute()
  {
    return true;
  }

  std::vector<pcl::PointIndices> ArtifactFilter::cluster_points(
      const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud)
  {
    std::vector<pcl::PointIndices> cluster_indices;
    // Creating the tree fails if the input cloud is empty, so check that
    // manually.
    if (cloud->empty())
    {
      return cluster_indices;
    }

    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(
        new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud(cloud);

    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance(CLUSTER_TOLERANCE);
    ec.setMinClusterSize(MIN_CLUSTER_SIZE);
    ec.setMaxClusterSize(MAX_CLUSTER_SIZE);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud);
    ec.extract(cluster_indices);

    return cluster_indices;
  }

  objdet_msgs::ArtifactLocalization ArtifactFilter::createDummyArtifact(const objdet_msgs::ArtifactLocalization& newArtifact){
    objdet_msgs::ArtifactLocalization dummyArtifact;
    dummyArtifact.stamp = newArtifact.stamp;
    dummyArtifact.valid = newArtifact.valid;
    dummyArtifact.x = newArtifact.x;
    dummyArtifact.y = newArtifact.y;
    dummyArtifact.z = newArtifact.z;
    dummyArtifact.confidence = newArtifact.confidence;
    dummyArtifact.class_id = newArtifact.class_id;
    dummyArtifact.report_id = newArtifact.report_id;
    dummyArtifact.device_code = newArtifact.device_code;
    dummyArtifact.readings = newArtifact.readings;
    
    return dummyArtifact;
  }

  std::unordered_map<uint16_t, std::vector<sensor_msgs::Image>> ArtifactFilter::update_artifacts(const objdet_msgs::ArtifactLocalizationArray &new_artifacts)
  {
    //   std::vector<objdet_msgs::ArtifactLocalization>& new_artifacts) {
    // Similar to the update_artifacts function in modules/objdet_mapper.cpp

    // Match current artifacts to new artifacts
    //  std::unordered_set<size_t> unmatched_indices;
    //for (size_t i = 0; i < current_artifacts.size(); ++i) {
    //unmatched_indices.insert(i);
    // }

    uint16_t dirt_count_replace = 0;
    uint16_t dirt_count_new = 0;
    std::unordered_map<uint16_t, std::vector<sensor_msgs::Image>> outputImages;
    for (size_t i = 0; i < new_artifacts.localizations.size(); ++i)
    {
      auto min_index = -1; // index into current_artifacts
      auto min_distance = MATCH_THRESHOLD_SQ;

      //   for (const auto unmatched_index : unmatched_indices) {
      for (size_t unmatched_index = 0; unmatched_index < current_artifacts.size(); ++unmatched_index)
      {

        auto dist = artifact_distance_sq(current_artifacts[unmatched_index],
                                         new_artifacts.localizations[i]);

        bool distanceCheck = dist < min_distance;
        bool isDupeCell = check_duplicate_artifact(current_artifacts[unmatched_index], new_artifacts.localizations[i]);

        if (isDupeCell)
        {
          /// Two Cell Phones, same device --> PASS Regardless of distance....
          min_index = unmatched_index;
          min_distance = dist;
        }
        else if (distanceCheck && current_artifacts[unmatched_index].class_id == new_artifacts.localizations[i].class_id)
        {
          /// Close artifacts AND the same class...
          min_index = unmatched_index;
          min_distance = dist;
        }
      }

      auto new_artifact = new_artifacts.localizations[i];
      if (min_index >= 0)
      {
        auto &current_artifact = current_artifacts[min_index];
        new_artifact.stamp = current_artifact.stamp;
        new_artifact.report_id = current_artifact.report_id;
        ROS_INFO_STREAM("Old Report " << new_artifact.report_id << " Current confidence = " << current_artifact.confidence << " " << new_artifact.confidence);
        current_artifacts_dirty[min_index] =
            !compare_artifacts(current_artifact, new_artifact);

        outputImages[min_index] = new_artifact.images;
        new_artifact.images.clear();
        current_artifact = createDummyArtifact(new_artifact);
        dirt_count_replace++;
      }
      else
      {                
        last_report_id++;
        new_artifact.report_id = last_report_id;//current_artifacts.size();                                                                                                                                                                                                        
        //if (last_report_id % 5 == 0){
        if (true){
          report_id_stream.seekp(0);
          report_id_stream << last_report_id;
          //report_id_stream.write(reinterpret_cast<const char *>(&last_report_id), sizeof(uint16_t));
          report_id_stream.flush();
          ROS_INFO_STREAM("Storing report " << last_report_id);
        }


        current_artifacts_dirty.push_back(true);

        outputImages[current_artifacts.size()] = new_artifact.images;
        new_artifact.images.clear();
        current_artifacts.push_back(createDummyArtifact(new_artifact));
        
        dirt_count_new++;
      }
    }

    ROS_INFO_STREAM("Incoming! : " << new_artifacts.localizations.size() <<  " Dirt New= " << dirt_count_new << " Dirt Replace: " << dirt_count_replace);


    // Everything left unmatched is now invalidated.
    //for (const auto unmatched_index : unmatched_indices) {
    // If valid, we want to mark as invalid and set as dirty.
    //  current_artifacts_dirty[unmatched_index] =
    //      current_artifacts[unmatched_index].valid;
    //  current_artifacts[unmatched_index].valid = false;
    //}

    // Everything should've been moved already, so clear the vector.
    /// new_artifacts.clear();
    return outputImages;
  }

  bool ArtifactFilter::check_duplicate_artifact(const objdet_msgs::ArtifactLocalization &a, const objdet_msgs::ArtifactLocalization &b)
  {

    if (a.class_id != b.class_id)
    {
      return false;
    }

    return !a.device_code.empty() && !b.device_code.empty() && (a.device_code.compare(b.device_code) == 0);
  }

  float ArtifactFilter::artifact_distance_sq(
      const objdet_msgs::ArtifactLocalization &a,
      const objdet_msgs::ArtifactLocalization &b)
  {
    return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) +
           (a.z - b.z) * (a.z - b.z);
  }

  bool ArtifactFilter::compare_artifacts(
      const objdet_msgs::ArtifactLocalization &a,
      const objdet_msgs::ArtifactLocalization &b)
  {
    // This condition should never trip for the arguments that are given to the
    // function in this node, but added here for completeness.
    if (a.valid != b.valid)
    {
      return false;
    }

    // Compare coordinates, allowing for slight error.
    if (!(almost_equal(a.x, b.x) && almost_equal(a.y, b.y) &&
          almost_equal(a.z, b.z)))
    {
      return false;
    }

    // Artifact reclassification matters too.
    if (a.class_id != b.class_id)
    {
      return false;
    }

    // Cheap check if the image size has differed. We check the image timestamps
    // and frame_ids later on in this function
    if (a.images.size() != b.images.size())
    {
      return false;
    }

    // compare the raw readings lengths, we want all raw readings to
    // get back to rviz for visualization to the human operator
    if (a.readings.size() != b.readings.size())
    {
      return false;
    }

    // compare the readings elementwise, in case of loop closure changing
    // positions
    for (size_t i = 0; i < a.readings.size(); ++i)
    {
      if (!compare_readings(a.readings[i], b.readings[i]))
      {
        return false;
      }
    }

    // Compare images by looking at their frame and timestamps. Since there
    // shouldn't be 2 images in the same frame with the same timestamp, this is a
    // cheap way of checking image equality. Can't use pointer comparisons because
    // we've copied the data already.
    for (size_t i = 0; i < a.images.size(); ++i)
    {
      // Could compare header directly, but we don't need to compare seq.
      if ((a.images[i].header.stamp != b.images[i].header.stamp) ||
          (a.images[i].header.frame_id != b.images[i].header.frame_id))
      {
        return false;
      }
    }

    return true;
  }

  bool ArtifactFilter::compare_readings(objdet_msgs::StampedReading a,
                                        objdet_msgs::StampedReading b)
  {
    // return false if the readings are different
    if (!(almost_equal(a.pose.position.x, b.pose.position.x) &&
          almost_equal(a.pose.position.y, b.pose.position.y) &&
          almost_equal(a.pose.position.z, b.pose.position.z)))
    {
      return false;
    }

    // if they're in the same position, check if the rssi value has changed
    if (std::abs(a.reading - b.reading) > 1E-3)
    {
      return false;
    }

    return true;
  }

  void ArtifactFilter::publish_artifacts()
  {
    objdet_msgs::ArtifactLocalizationArray msg;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = map_frame;

    for (size_t i = 0; i < current_artifacts.size(); ++i)
    {
      if (!current_artifacts_dirty[i])
      {
        continue;
      }

      msg.localizations.push_back(current_artifacts[i]);
    }

    if (!msg.localizations.empty())
    {
      artifact_localizations_publisher.publish(msg);
    }
  }

  void ArtifactFilter::artifact_cb(const objdet_msgs::ArtifactLocalizationArray &incoming_artifacts)
  {
    if (map_frame == "")
    {
      map_frame = incoming_artifacts.header.frame_id;
    }
    else
    {
      if (map_frame != incoming_artifacts.header.frame_id)
      {
        ROS_WARN_STREAM("Received artifacts in frame "
                        << incoming_artifacts.header.frame_id
                        << " which doesn't match current map frame "
                        << map_frame);
        return;
      }
    }

    if (bypass_artifact_filter)
    {
      objdet_msgs::ArtifactLocalizationArray msg;
      msg.header.stamp = ros::Time::now();
      msg.header.frame_id = map_frame;
      for (auto &tempArt : incoming_artifacts.localizations)
      {
        ROS_INFO_STREAM(bypass_counter << " Publishing Artifact at location: " << tempArt.x << "," << tempArt.y << "," << tempArt.z << " type= " << tempArt.class_id << " Size = " << ros::serialization::serializationLength(tempArt) << " " << tempArt.images.size());
        msg.localizations.push_back(tempArt);
        msg.localizations.back().report_id = bypass_counter++;
      }

      if (!msg.localizations.empty())
      {
        artifact_localizations_publisher.publish(msg);
      }
      return;
    }

    // for (const auto& artifact : incoming_artifacts.localizations) {
    // Remove the artifact if it is no longer valid. It must necessarily have
    // been valid at a previous time.A
    /*if (!artifact.valid) {
      if (artifacts.count(artifact.report_id)) {
        artifacts.erase(artifact.report_id);
      } else {
        ROS_WARN("Told to remove artifact %u which wasn't stored",
                 artifact.report_id);
      }

      continue;
      }*/

    //if (ignore_visual_cell_phones && artifact.device_code.empty() && artifact.class_id==2){
    //ROS_WARN_STREAM("IGNORING Visual Cell Phone detection! report=" << artifact.report_id << " Device:" << artifact.device_code << " Class:" << artifact.class_id);
    // continue;
    // }

    // Otherwise, store the valid artifact.
    //artifacts[artifact.report_id] = artifact;

    //current_artifacts.push_back(artifact);
    //artifact_centroids->points.emplace_back(artifact.x, artifact.y, artifact.z);
    //}

    // const auto cluster_indices = cluster_points(artifact_centroids);
    //auto new_artifacts = make_artifacts_from_clusters(cluster_indices, artifact_centroids);
    std::unordered_map<uint16_t, std::vector<sensor_msgs::Image>> artifact_images = update_artifacts(incoming_artifacts);

    objdet_msgs::ArtifactLocalizationArray msg;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = map_frame;

    for (size_t i = 0; i < current_artifacts.size(); ++i)
    {
      if (!current_artifacts_dirty[i])
      {
        continue;
      }

      if (ignore_visual_cell_phones && current_artifacts[i].device_code.empty() && current_artifacts[i].class_id == 2)
      {
        ROS_WARN_STREAM("IGNORING Visual Cell Phone detection! report=" << current_artifacts[i].report_id << " Device:" << current_artifacts[i].device_code << " Class:" << current_artifacts[i].class_id);
        continue;
      }

      current_artifacts_dirty[i] = false;

      objdet_msgs::ArtifactLocalization tempArt = current_artifacts[i];
      tempArt.images = artifact_images[i];
      ROS_INFO_STREAM(i << " Publishing Artifact at location: " << tempArt.x << "," << tempArt.y << "," << tempArt.z << " type= " << tempArt.class_id << " Size = " << ros::serialization::serializationLength(tempArt) << " " << tempArt.images.size() << " in Vect " << ros::serialization::serializationLength(current_artifacts[i]));
      msg.localizations.push_back(tempArt);
      ROS_INFO_STREAM("Append artifact " << msg.localizations.back().report_id);

      //current_artifacts[i].images.clear();
    }

    if (!msg.localizations.empty())
    {
      artifact_localizations_publisher.publish(msg);
    }
  }

} // namespace object_detection
