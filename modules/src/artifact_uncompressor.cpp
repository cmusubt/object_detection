#include "modules/artifact_uncompressor.h"
#include <cv_bridge/cv_bridge.h>
#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <objdet_msgs/CompressedArtifactAck.h>
#include <opencv2/opencv.hpp>

BaseNode* BaseNode::get() {
  auto* compressor =
      new object_detection::ArtifactUncompressor("artifact_uncompressor");
  return compressor;
}

namespace object_detection {

ArtifactUncompressor::ArtifactUncompressor(std::string node_name)
    : BaseNode(std::move(node_name)) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool ArtifactUncompressor::initialize() {
  ros::NodeHandle* nh = get_node_handle();
  artifact_sub = nh->subscribe("artifact_localizations_compressed", 10,
                               &ArtifactUncompressor::artifact_cb, this);
  artifact_pub = nh->advertise<objdet_msgs::ArtifactLocalizationArray>(
      "artifact_localizations_uncompressed", 10);
  acknowledge_pub = nh->advertise<objdet_msgs::CompressedArtifactAck>(
      "artifact_localizations_compressed_ack", 10);

  return true;
}

bool ArtifactUncompressor::execute() { return true; }

ArtifactUncompressor::~ArtifactUncompressor() = default;

void ArtifactUncompressor::artifact_cb(
    const objdet_msgs::CompressedArtifactLocalizationArrayConstPtr& msg) {
  monitor.tic("uncompress_all");

  // Acknowledge receipt of the localization array
  objdet_msgs::CompressedArtifactAck ack;
  ack.stamp = msg->header.stamp;
  acknowledge_pub.publish(ack);

  objdet_msgs::ArtifactLocalizationArray artifact_array;
  artifact_array.header = msg->header;

  artifact_array.localizations.reserve(msg->localizations.size());
  for (const auto& compressed : msg->localizations) {
    artifact_array.localizations.push_back(uncompress_artifact(compressed));
  }

  artifact_pub.publish(artifact_array);
  monitor.toc("uncompress_all");
}

objdet_msgs::ArtifactLocalization ArtifactUncompressor::uncompress_artifact(
    const objdet_msgs::CompressedArtifactLocalization& compressed) {
  objdet_msgs::ArtifactLocalization artifact;
  artifact.stamp = compressed.stamp;
  artifact.valid = compressed.valid;
  artifact.x = compressed.x;
  artifact.y = compressed.y;
  artifact.z = compressed.z;
  artifact.confidence = compressed.confidence;
  artifact.class_id = compressed.class_id;
  artifact.report_id = compressed.report_id;
  artifact.readings = compressed.readings;

  artifact.images.reserve(compressed.images.size());
  for (const auto& compressed_image : compressed.images) {
    artifact.images.push_back(uncompress_image(compressed_image));
  }

  return artifact;
}

sensor_msgs::Image ArtifactUncompressor::uncompress_image(
    const sensor_msgs::CompressedImage& compressed) {
  monitor.tic("uncompress_image");

  // For some reason, there's no compressed_imgmsg_to_cv2 function in c++?
  cv_bridge::CvImage cv_image;
  cv_image.header = compressed.header;
  cv_image.encoding = "rgb8";  // TODO(vasua): Maybe handle 16 bit images?
  cv_image.image = cv::imdecode(compressed.data, CV_LOAD_IMAGE_COLOR);
  monitor.toc("uncompress_image");

  // I think this is safe?
  return *cv_image.toImageMsg();
}
}  // namespace object_detection
