#include "modules/objdet_mapper.h"
#include <common/image_transport.h>
#include <common/tf2.h>
#include <pcl/common/common.h>
#include <pcl/common/distances.h>
#include <pcl/common/geometry.h>
#include <pcl/common/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <visualization_msgs/Marker.h>
#include <algorithm>
#include <boost/bind.hpp>
#include <limits>
#include <numeric>
#include <unordered_set>
#include "modules/color_filter.h"
#include "modules/load_camera_info.h"
#include "modules/size_filter.h"
#include "objdet_msgs/ArtifactLocalization.h"
#include "objdet_msgs/ArtifactLocalizationArray.h"

BaseNode *BaseNode::get() {
  auto *mapper = new object_detection::ObjdetMapper("objdet_mapper_node");
  return mapper;
}

namespace object_detection {

ObjdetMapper::ObjdetMapper(std::string node_name)
    : BaseNode(std::move(node_name), false, 2, 1, 1), it(*node_handle_) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool ObjdetMapper::initialize() {
  // Run everything related to receiving detections and tfs on global queue
  ros::NodeHandle *nh = get_node_handle(0);
  tf_listener_ptr = std::unique_ptr<tf2_ros::TransformListener>(
      new tf2_ros::TransformListener(tf_buffer));

  // Put delay subscriber on a separate thread.
  delay_subscriber.initialize(get_node_handle(1));

  // Namespace published topics with current name to allow multiple objdet
  // mappers to run concurrently
  const auto &node_name = ros::this_node::getName() + "/";

  key_pose_sub =
      nh->subscribe("key_pose_to_map", 10, &ObjdetMapper::key_pose_cb, this);
  key_pose_path_sub =
      nh->subscribe("key_pose_path", 10, &ObjdetMapper::key_pose_path_cb, this);
  sensing_range_publisher =
      nh->advertise<visualization_msgs::Marker>(node_name + "objdet_range", 1);
  local_map_publisher = nh->advertise<DetCloud>(node_name + "local_det_map", 1);

  std::map<std::string, float> min_dists;
  get_private_node_handle()->getParam("min_dists", min_dists);

  get_private_node_handle()->getParam("topics", sub_topics);
  if (sub_topics.empty()) {  // Checking for false seems to not work properly.
    ROS_WARN("No topics set for objdet mapper!");
    return false;
  }

  float confidence_threshold;
  if (nh->getParam("/confidence_threshold", confidence_threshold)) {
    CONFIDENCE_THRESHOLD = confidence_threshold;
  }
  bool logging_debug_info;
  if (get_private_node_handle()->getParam("logging_debug_info", logging_debug_info)) {
    LOGGING_DEBUG_INFO = logging_debug_info;
  }
  bool publish_debug_msgs;
  if (get_private_node_handle()->getParam("publish_debug_msgs", publish_debug_msgs)) {
    PUBLISH_DEBUG_MSGS = publish_debug_msgs;
  }

  std::map<std::string, bool> settings;
  get_private_node_handle()->getParam("settings", settings);
  if (settings.empty()) {
    ROS_WARN("Use default value for settings");
  }
  {
    auto search_itr = settings.find("use_color_filter");
    if (search_itr != settings.end()) {
      USE_COLOR = search_itr->second;
    }
    search_itr = settings.find("force_drop");
    if (search_itr != settings.end()) {
      COLOR_FORCE_FILTER = search_itr->second;
    }
    search_itr = settings.find("use_median");
    if (search_itr != settings.end()) {
      USE_MEDIAN = search_itr->second;
    }
    search_itr = settings.find("use_size_filter");
    if (search_itr != settings.end()) {
      USE_SIZE = search_itr->second;
    }
    search_itr = settings.find("use_depth_filter");
    if (search_itr != settings.end()) {
      USE_DEPTH_FILTER = search_itr->second;
    }
  }
  std::map<std::string, double> float_settings;
  get_private_node_handle()->getParam("float_settings", float_settings);
  {
    auto search_itr = float_settings.find("size_threshold");
    if (search_itr != float_settings.end()) {
      SIZE_THRESHOLD = search_itr->second;
    }
  }
  std::map<std::string, int> int_settings;
  get_private_node_handle()->getParam("int_settings", int_settings);
  {
    auto search_itr = int_settings.find("minimum_pixels");
    if (search_itr != int_settings.end()) {
      MINIMUM_PIXELS = search_itr->second;
    }
  }

  std::map<std::string, int> step_size;
  get_private_node_handle()->getParam("step_size", step_size);
  for (const auto &pair : step_size) {
    clouds_from_dets_params[pair.first].step_size = pair.second;
  }
  std::map<std::string, int> max_depth;
  get_private_node_handle()->getParam("max_depth", max_depth);
  for (const auto &pair : max_depth) {
    clouds_from_dets_params[pair.first].max_depth = pair.second;
  }

  for (const auto &pair : sub_topics) {
    std::string filename;
    get_private_node_handle()->getParam(pair.first, filename);
    if (filename.length() > 0) {
      camera_info_list[pair.first] =
          static_cast<sensor_msgs::CameraInfoConstPtr>(
              loadCameraInfo(filename));
    } else {
      std::string info_topic;
      if (pair.second == DetDepthSubTopics::FLIR_LIDAR_DROP or
          pair.second == DetDepthSubTopics::FLIR_LIDAR or
          pair.second == DetDepthSubTopics::REALSENSE_LIDAR and
              pair.second.find("camera") != std::string::npos) {
        info_topic = pair.first + "/camera_info";
      } else {
        info_topic = pair.first + "/color/camera_info";
      }
      ROS_WARN(
          "objdet_mapper: Did not find camera info file for %s. Will listen to "
          "its topic for "
          "camera info",
          info_topic.c_str());
      camera_info_list[pair.first] =
          ros::topic::waitForMessage<sensor_msgs::CameraInfo>(
              info_topic, ros::Duration(10.0));
    }
  }

  float min_topic_dist;
  for (const auto &pair : sub_topics) {
    DetDepthSubTopics topics;
    if (pair.second == DetDepthSubTopics::REALSENSE) {
      topics.set_realsense(pair.first);
    } else if (pair.second == DetDepthSubTopics::REALSENSE_LIDAR) {
      topics.set_realsense_lidar(pair.first);
    } else if (pair.second == DetDepthSubTopics::REALSENSE_COMBINED) {
      topics.set_realsense_combined(pair.first);
    } else if (pair.second == DetDepthSubTopics::FLIR_LIDAR_DROP) {
      topics.set_flir_lidar_drop(pair.first);
    } else if (pair.second == DetDepthSubTopics::FLIR_LIDAR) {
      topics.set_flir_lidar(pair.first);
    } else {
      ROS_ERROR("Type \"%s\" for topic \"%s\" doesn't make sense.",
                pair.second.c_str(), pair.first.c_str());
      continue;
    }

    det_image_subs.emplace_back(new DetDepthSub(nh, topics));

    // avoiding false positives from detection on robot itself
    if ((!min_dists.empty()) &&
        (min_dists.find(pair.first) != min_dists.end())) {
      min_topic_dist = min_dists[pair.first];
    } else {
      min_topic_dist = 0;
    }

    const auto pub_topic = pair.first + "/color_filtered/image";
    disable_transports(nh, pub_topic);
    color_filtered_image_pubs.push_back(it.advertise(pub_topic, 1));

    det_image_subs.back()->registerCallback(boost::bind(
        &ObjdetMapper::det_images_cb, this, _1, _2, _3,
        color_filtered_image_pubs.back(),
        clouds_from_dets_params.at(pair.second), min_topic_dist, pair.first));
  }

  // Keeping all of the following callbacks in the map_nh node handle is
  // important to maintain thread safety for various flags and variables.
  // Generate the global map on a separate thread / queue
  ros::NodeHandle *map_nh = get_node_handle(2);
  global_map_gen_timer =
      map_nh->createTimer(ros::Duration(1.0f), &ObjdetMapper::timer_cb, this);
  global_map_publisher = map_nh->advertise<DetCloud>(node_name + "det_map", 1);
  global_centroid_publisher =
      map_nh->advertise<DetCloud>(node_name + "det_centers", 1);
  cluster_publisher = map_nh->advertise<DetCloud>(node_name + "clusters", 1);
  artifact_localizations_publisher =
      map_nh->advertise<objdet_msgs::ArtifactLocalizationArray>(
          node_name + "artifact_localizations", 10);

  ROS_INFO("Mapper successfully initialized!");

  return true;
}

bool ObjdetMapper::execute() { return true; }

ObjdetMapper::~ObjdetMapper() = default;

void ObjdetMapper::key_pose_cb(const nav_msgs::OdometryConstPtr &key_pose) {
  monitor.tic("key_pose");

  // Index encoded as first element of covariance matrix
  const auto key_pose_index =
      static_cast<uint32_t>(key_pose->pose.covariance[0]);
  const auto &stamp = key_pose->header.stamp;
  // ROS_INFO("got a key pose: %u", key_pose_index);

  // The key poses aren't necessarily in the MAP_FRAME (usually /map) as they're
  // usually in /map_rot. Since a geometry_msgs/Pose message doesn't store any
  // frame information, we must transform the Pose here.
  geometry_msgs::Pose map_frame_key_pose;
  try {
    const auto key_pose_to_map_tf = tf_buffer.lookupTransform(
        MAP_FRAME, sanitize_frame(key_pose->header.frame_id),
        key_pose->header.stamp);
    tf2::doTransform(key_pose->pose.pose, map_frame_key_pose,
                     key_pose_to_map_tf);
  } catch (tf2::TransformException &e) {
    ROS_WARN("Error looking up Key Pose TF: %s", e.what());
    return;
  }

  {
    std::lock_guard<std::mutex> lock(key_poses_mutex);
    monitor.tic("key_pose_critical");
    key_pose_ptrs[key_pose_index] = std::make_shared<KeyPose>();
    key_pose_ptrs[key_pose_index]->pose = map_frame_key_pose;

    // Use the fact that key poses are necessarily temporally sorted.
    key_pose_timestamps[key_pose_index] = stamp;

    // Sanity check that key pose timestamps are sorted. If not, something's
    // gone wrong. Perhaps system time has jumped around too far?
    assert(std::is_sorted(
        key_pose_timestamps.begin(), key_pose_timestamps.end(),
        [](const decltype(key_pose_timestamps)::value_type &left,
           const decltype(key_pose_timestamps)::value_type &right) {
          return left.second < right.second;
        }));

    // ROS_INFO("Currently have %lu key poses", key_pose_ptrs.size());

    monitor.toc("key_pose_critical");
  }

  monitor.toc("key_pose");
}

void ObjdetMapper::key_pose_path_cb(
    const nav_msgs::PathConstPtr &key_pose_path) {
  // ROS_INFO_STREAM("Got a key pose path with size "
  //                 << key_pose_path->poses.size());

  geometry_msgs::TransformStamped key_pose_to_map_tf;
  try {
    key_pose_to_map_tf = tf_buffer.lookupTransform(
        MAP_FRAME, sanitize_frame(key_pose_path->header.frame_id),
        key_pose_path->header.stamp);
  } catch (tf2::TransformException &e) {
    ROS_WARN("Error looking up Key Pose Path TF: %s", e.what());
    return;
  }

  {
    std::lock_guard<std::mutex> lock(key_poses_mutex);
    monitor.tic("key_pose_path_crit");

    // Key poses are indexed by the position in the array.
    for (size_t i = 0; i < key_pose_path->poses.size(); ++i) {
      if (key_pose_ptrs.count(i) > 0) {  // Key pose exists in map
        auto &key_pose = key_pose_ptrs.at(i);
        double distance = distancesquare(key_pose->pose.position, key_pose_path->poses[i].pose.position);
        if (distance < MATCH_THRESHOLD_SLAM_SQ)
        {
          std::lock_guard<std::mutex> key_pose_lock(key_pose->mutex);
          tf2::doTransform(key_pose_path->poses[i].pose, key_pose->pose,
                          key_pose_to_map_tf);
        }
      }
    }
    monitor.toc("key_pose_path_crit");
  }
}

void ObjdetMapper::det_images_cb(
    const objdet_msgs::DetectionArrayConstPtr &dets_msg,
    const sensor_msgs::ImageConstPtr &color_msg,
    const sensor_msgs::ImageConstPtr &aligned_msg,
    const image_transport::Publisher &image_pub,
    const CloudsFromDetsParams &params, const float &min_topic_dist,
    const std::string &topic) {
  monitor.tic("det_info_images");
  if (camera_info_list[topic] == nullptr) {
    std::string info_topic;
    if (sub_topics[topic] == DetDepthSubTopics::FLIR_LIDAR_DROP or
        sub_topics[topic] == DetDepthSubTopics::FLIR_LIDAR or
        sub_topics[topic] == DetDepthSubTopics::REALSENSE_LIDAR and
            sub_topics[topic].find("camera") != std::string::npos) {
      info_topic = topic + "/camera_info";
    } else {
      info_topic = topic + "/color/camera_info";
    }
    ROS_WARN(
        "objdet_mapper:Did not receive camera info for topic %s. Will listen "
        "to its topic for "
        "camera info",
        info_topic.c_str());
    camera_info_list[topic] =
        ros::topic::waitForMessage<sensor_msgs::CameraInfo>(
            info_topic, ros::Duration(10.0));
    return;
  }
  const sensor_msgs::CameraInfoConstPtr info_msg = camera_info_list[topic];

  publish_sensor_range_marker(info_msg, params);

  if (dets_msg->detections.empty()) {
    monitor.toc("det_info_images");
    return;
  }

  const auto det_frame = sanitize_frame(color_msg->header.frame_id);

  std::vector<DetCloud::Ptr> det_points;
  DetCloud::Ptr det_centroids(new DetCloud);
  std::vector<int> detection_index = get_clouds_from_dets(dets_msg, info_msg, color_msg, aligned_msg, image_pub,
                       params, det_points, det_centroids, min_topic_dist);

  if (det_points.empty()) {  // There might not be anything in range
    if (LOGGING_DEBUG_INFO) {  
      ROS_WARN("No 3D points found, detection in %s may be out of range!",
              det_frame.c_str());
    }
    monitor.toc("det_info_images");
    return;
  }

  // Adjust the time from the images a bit to account for USB delay, as well as
  // state estimation timestamp delay.
  // TODO(vasua): Remove this once timestamping is more precise.
  const auto query_time =
      color_msg->header.stamp - delay_subscriber.get_delay_duration();

  std::shared_ptr<KeyPose> key_pose_ptr = get_key_pose_at_time(query_time);
  if (key_pose_ptr == nullptr) {
    ROS_WARN_STREAM("Unable to find key pose at time " << query_time);
    monitor.toc("det_info_images");
    return;
  }

  {
    std::lock_guard<std::mutex> lock(key_pose_ptr->mutex);
    monitor.tic("det_info_images_crit");

    // Convert the map to key pose to a transform message
    tf2::Transform camera_to_map_tf;
    tf2::Transform key_pose_to_map_tf;
    tf2::fromMsg(key_pose_ptr->pose, key_pose_to_map_tf);

    try {
      // Will block for up to .1 seconds before continuing. However, the buffers
      // for the detection information (sized to 30) should handle it.
      //
      // TODO(vasua): Figure out why this function call takes ~17ms on average.
      // Time measured on my laptop. This shouldn't be a problem _yet_, but
      // may be a problem in the future. If so, remove the ros::Duration (or set
      // to 0) and just live with the fact that not all detections will be used.
      const auto camera_to_map = tf_buffer.lookupTransform(
          MAP_FRAME, det_frame, query_time, ros::Duration(0.1));
      tf2::fromMsg(camera_to_map.transform, camera_to_map_tf);
    } catch (tf2::TransformException &e) {
      ROS_WARN("%s", e.what());
      return;
    }

    if (!camera_moved_enough(det_frame, camera_to_map_tf)) {
      if (LOGGING_DEBUG_INFO) {  
        ROS_WARN_STREAM("Camera not moved enough to produce unique detection");
      }
      return;
    }

    auto camera_to_key_pose_tf =
        key_pose_to_map_tf.inverse() * camera_to_map_tf;
    auto camera_to_key_pose_eigen = eigen_tf_from_tf2(camera_to_key_pose_tf);

    // Add the centroids to the key pose
    DetCloud::Ptr det_centroids_key_frame(new DetCloud);
    pcl::transformPointCloud(*det_centroids, *det_centroids_key_frame,
                             camera_to_key_pose_eigen);
    if (key_pose_ptr->det_centroids) {
      *key_pose_ptr->det_centroids += *det_centroids_key_frame;
    } else {
      key_pose_ptr->det_centroids = det_centroids_key_frame;
    }

    // Add the det clouds to the key pose
    for (const auto &det_points_ptr : det_points) {
      DetCloud::Ptr det_points_key_frame_ptr(new DetCloud);
      pcl::transformPointCloud(*det_points_ptr, *det_points_key_frame_ptr,
                               camera_to_key_pose_eigen);
      key_pose_ptr->det_points.push_back(det_points_key_frame_ptr);
      local_map_publisher.publish(*det_points_ptr);
    }

    // Add the rgb det image ptrs to the key pose
    for (size_t i = 0; i < det_points.size(); ++i) {

      // Draw bounding box directly on the image we're sending back.
      const auto cv_image = cv_bridge::toCvCopy(color_msg);
      const auto &detection = dets_msg->detections[detection_index[i]];
      draw_detection_on_image(cv_image->image, detection, topic);

      // Also create a cropped image
      const auto cv_image_cropped = cv_bridge::toCvCopy(color_msg);
      create_zoomed_image(cv_image_cropped->image, detection);

      // Store both images (det_images is a list of lists)
      key_pose_ptr->det_images.emplace_back(
          std::initializer_list<std::pair<sensor_msgs::ImageConstPtr, sensor_msgs::ImageConstPtr>>{
              {cv_image_cropped->toImageMsg(),
              cv_image->toImageMsg()}});
    }

    if ((key_pose_ptr->det_points.size() !=
         key_pose_ptr->det_centroids->size()) ||
        (key_pose_ptr->det_points.size() != key_pose_ptr->det_images.size())) {
      ROS_FATAL_STREAM("Messed up somewhere: "
                       << key_pose_ptr->det_points.size() << ", "
                       << key_pose_ptr->det_centroids->size() << ", "
                       << key_pose_ptr->det_images.size());
    }

    monitor.toc("det_info_images_crit");
  }

  monitor.toc("det_info_images");
}

void ObjdetMapper::publish_sensor_range_marker(
    const sensor_msgs::CameraInfoConstPtr &info_msg,
    const CloudsFromDetsParams &params) {
  // https://stackoverflow.com/questions/39992968/how-to-calculate-field-of-view-of-the-camera-from-camera-intrinsic-matrix
  const auto hfov =
      2 * atan2(static_cast<double>(info_msg->width), 2 * info_msg->K[0]);
  const auto offset = -(hfov / 2) + (3.14159 / 2);

  visualization_msgs::Marker marker;
  marker.header.frame_id = info_msg->header.frame_id;
  marker.header.stamp = ros::Time::now();
  marker.ns = info_msg->header.frame_id;  // Unique string
  marker.id = 0;
  marker.type = visualization_msgs::Marker::LINE_STRIP;
  marker.action = visualization_msgs::Marker::ADD;
  marker.lifetime = ros::Duration(0);  // Forever
  marker.frame_locked = true;          // Follow the camera frame around

  const size_t segments = 10;
  const auto step = hfov / segments;

  marker.points.emplace_back();  // Default construct a point at (0, 0, 0);
  for (size_t i = 0; i <= segments; ++i) {
    const auto angle = i * step + offset;

    geometry_msgs::Point p;
    p.x = cos(angle) * params.max_depth;
    p.y = 0;
    p.z = sin(angle) * params.max_depth;
    marker.points.push_back(p);
  }
  marker.points.emplace_back();  // Default construct a point at (0, 0, 0);

  marker.color.a = 1.0f;
  marker.color.r = 1.0f;
  marker.color.g = 0.0f;
  marker.color.b = 0.2f;
  marker.scale.x = .02f;

  sensing_range_publisher.publish(marker);
}

bool ObjdetMapper::camera_moved_enough(const std::string &camera_frame,
                                       const tf2::Transform &camera_to_map_tf) {
  // TODO(vasua): Also consider rotation
  // Note the default construction with the unordered_map
  const auto &last_detection_tf = last_detection_tfs[camera_frame];
  const auto &last_detection_position = last_detection_tf.getOrigin();
  const auto &camera_to_map_position = camera_to_map_tf.getOrigin();
  const float position_delta_sq =
      tf2::tf2Distance2(camera_to_map_position, last_detection_position);
  if (position_delta_sq < MIN_CAMERA_MOVEMENT_SQ) {
    // ROS_INFO(
    //     "%s camera only moved by %f meters, instead of at least %f. "
    //     "Ignoring detection.",
    //     camera_frame.c_str(), std::sqrt(position_delta_sq),
    //     MIN_CAMERA_MOVEMENT);
    return false;
  }

  last_detection_tfs[camera_frame] = camera_to_map_tf;
  return true;
}

// Return pointer to key pose which corresponds to the specified timestamp.
// This method is threadsafe.
std::shared_ptr<KeyPose> ObjdetMapper::get_key_pose_at_time(
    const ros::Time &time) {
  std::lock_guard<std::mutex> lock(key_poses_mutex);
  monitor.tic("get_key_pose_crit");

  // Since we're typically going to be looking at the most recent key pose or
  // two, prefer a reverse linear search over a binary search. The first one
  // found will be the correct one when searching in reverse.
  for (auto it = key_pose_timestamps.rbegin(); it != key_pose_timestamps.rend();
       ++it) {
    if (it->second <= time) {
      monitor.toc("get_key_pose_crit");
      return key_pose_ptrs.at(it->first);  // Return a copy, not a reference
    }
  }

  ROS_WARN_STREAM("No key pose found for sensor reading at time "
                  << time << ". Currently have " << key_pose_timestamps.size()
                  << " key poses.");
  monitor.toc("get_key_pose_crit");
  return nullptr;
}

// Get point cloud from detections with all points within bounding box mapped to
// a 3d point using the depth image, in the sensor frame. This point cloud will
// still need to be transformed into the key frame coordinates.
std::vector<int> ObjdetMapper::get_clouds_from_dets(
    const objdet_msgs::DetectionArrayConstPtr &dets_msg,
    const sensor_msgs::CameraInfoConstPtr &info_msg,
    const sensor_msgs::ImageConstPtr &color_msg,
    const sensor_msgs::ImageConstPtr &aligned_msg,
    const image_transport::Publisher &image_pub,
    const CloudsFromDetsParams &params, std::vector<DetCloud::Ptr> &det_clouds,
    DetCloud::Ptr &det_centroids, const float &min_topic_dist) {
  monitor.tic("get_clouds_from_dets");
  std::vector<int> detection_index;

  std::size_t found = info_msg->header.frame_id.find("rs");
  std::size_t thermal_found = info_msg->header.frame_id.find("thermal");
  bool is_ueye =
      (found == std::string::npos && thermal_found == std::string::npos);

  // dealing with realsense cam matrix
  const auto fx = info_msg->K[0];
  const auto cx = info_msg->K[2];
  const auto fy = info_msg->K[4];
  const auto cy = info_msg->K[5];
  // defined for ueye cameras
  Eigen::Vector2d img_pixel = {0, 0};
  Eigen::Vector3d point_arr = {0, 0, 0};

  // This can throw, but it shouldn't.
  const auto color_cv_ptr = cv_bridge::toCvShare(color_msg);
  const auto aligned_cv_ptr = cv_bridge::toCvShare(aligned_msg);
  const auto height = aligned_cv_ptr->image.rows;
  const auto width = aligned_cv_ptr->image.cols;

  cv::Mat filtered_image;
  if (PUBLISH_DEBUG_MSGS) {
    filtered_image = color_cv_ptr->image.clone();
  }

  int index_count = 0;
  // Go through all detections
  for (const auto &det : dets_msg->detections) {
    if (det.confidence < CONFIDENCE_THRESHOLD) {
      continue;
    }
    DetCloud::Ptr det_cloud(new DetCloud);
    det_cloud->header.frame_id = color_msg->header.frame_id;
    pcl_conversions::toPCL(color_msg->header.stamp, det_cloud->header.stamp);
    // We require that (x1, y1) and (x2, y2) are within the image.
    const auto x1 = static_cast<int32_t>(det.x1 * width);
    const auto y1 = static_cast<int32_t>(det.y1 * height);
    const auto x2 = static_cast<int32_t>(det.x2 * width);
    const auto y2 = static_cast<int32_t>(det.y2 * height);

    const auto roi_width = x2 - x1;
    const auto roi_height = y2 - y1;
    const auto roi_rect = cv::Rect(x1, y1, roi_width, roi_height);
    const auto color_origin_box = color_cv_ptr->image(roi_rect);
    auto color_box = color_origin_box.clone();
    const auto aligned_box = aligned_cv_ptr->image(roi_rect);
    cv::Mat filter_box;
    if (PUBLISH_DEBUG_MSGS) {
      filter_box = filtered_image(roi_rect);
      draw_detection_on_image(filtered_image, det, "");
    }
    

    // Do color filtering
    bool apply_color_filter = false;
    bool drop_image = false;
    if (USE_COLOR) {
      const auto itr = color_filter::color_map.find(det.label);
      if (itr != color_filter::color_map.end()) {
        apply_color_filter = true;
        cv::Mat dst_mat, dst_mat2, mask;
        if (is_ueye) {
          cv::cvtColor(color_box, dst_mat, cv::COLOR_BGR2HSV);
        } else {
          cv::cvtColor(color_box, dst_mat, cv::COLOR_RGB2HSV);
        }
        for (const auto &threshold : itr->second) {
          cv::Mat temp_mask;
          cv::inRange(
              dst_mat,
              cv::Scalar(threshold.h_min, threshold.s_min, threshold.v_min),
              cv::Scalar(threshold.h_max, threshold.s_max, threshold.v_max),
              temp_mask);
          if (mask.empty()) {
            mask = temp_mask.clone();
          } else {
            cv::bitwise_or(mask, temp_mask, mask);
          }
        }
        // erode and dilate to make the mask more continuous
        cv::Mat element =
            cv::getStructuringElement(0, cv::Size(3, 3), cv::Point(0, 0));
        cv::morphologyEx(mask, mask, cv::MORPH_CLOSE, element,
                         cv::Point(-1, -1), 1);
        cv::morphologyEx(mask, mask, cv::MORPH_OPEN, element, cv::Point(-1, -1),
                         1);

        cv::bitwise_and(dst_mat, dst_mat, dst_mat2, mask);

        cv::Mat hsv_channels[3];
        cv::split(dst_mat2, hsv_channels);
        double filtered_count =
            static_cast<double>(cv::countNonZero(hsv_channels[2]));
        double total_count = static_cast<double>(dst_mat2.cols * dst_mat2.rows);

        cv::cvtColor(dst_mat2, color_box, cv::COLOR_HSV2RGB);
        if (PUBLISH_DEBUG_MSGS) {
          color_box.copyTo(filter_box);
        }

        if (filtered_count / total_count < color_filter::MIN_PERCENTAGE) {
          drop_image = true;
        }
      }

      // only discard the image when we use color filter and choose the force
      // filter option if the percentage is not enough we discard this
      // detection otherwise we use the original image to do localization
      if (drop_image) {
        if (COLOR_FORCE_FILTER) {
          if (LOGGING_DEBUG_INFO) {
             ROS_WARN("Use color filter to filter out detections: %s", det.label.c_str());
          }
          continue;
        } else {
          color_box = color_origin_box.clone();
        }
      }
    }

    double depth_min = 0.0;
    double depth_max = 0.0;
    std::vector<double> depth_list;
    for (int y = 0; y < roi_height; y += params.step_size) {
      for (int x = 0; x < roi_width; x += params.step_size) {
        const auto depth_raw = aligned_box.at<uint16_t>(y, x);
        const auto &rgb = color_box.at<cv::Vec3b>(y, x);
        if (depth_raw == 0) {
          continue;
        }
        if (USE_COLOR and apply_color_filter and cv::norm(rgb) < 1) {
          continue;
        }
        depth_list.emplace_back(static_cast<double>(depth_raw) / 1000.0);
      }
    }
    int pixel_count = depth_list.size();
    if (pixel_count < MINIMUM_PIXELS) {
      if (LOGGING_DEBUG_INFO) {  
        ROS_WARN("Too few pixels! label: %s", det.label.c_str());}
      continue;
    }
    // do depth filtering
    bool apply_depth_filter = false;
    if (USE_DEPTH_FILTER) {
      const auto itr = size_filter::size_map_max.find(det.label);
      if (itr != size_filter::size_map_max.end()) {
        apply_depth_filter = true;
        double radius = itr->second;
        auto begin = depth_list.begin();
        auto end = depth_list.end();
        const std::size_t size = std::distance(begin, end);
        const std::size_t mid = size / 2;
        std::nth_element(begin, begin + mid, end);
        auto median_depth = begin[mid];
        depth_min = median_depth - radius;
        depth_max = median_depth + radius;

        for (const auto &i : depth_list) {
          if (i > depth_max or i < depth_min) {
            pixel_count--;
          }
        }
      }
      if (pixel_count < MINIMUM_PIXELS) {
         if (LOGGING_DEBUG_INFO) {  
          ROS_WARN("Too few pixels After size filter! label: %s",
                  det.label.c_str());
         }
        continue;
      }
    }

    pcl::CentroidPoint<DetPoint> centroid_point;

    for (int y = 0; y < roi_height; y += params.step_size) {
      for (int x = 0; x < roi_width; x += params.step_size) {
        const auto depth_raw = aligned_box.at<uint16_t>(y, x);
        const auto &rgb = color_box.at<cv::Vec3b>(y, x);
        if (depth_raw == 0) {
          continue;
        }

        // discard the pixel if we use color filter and
        // this ROI has minimum number of desired color pixels and this pixel is
        // black
        if (USE_COLOR and apply_color_filter and cv::norm(rgb) < 1) {
          continue;
        }

        // TODO(vasua): Programmatically choose depth scale
        const auto depth = static_cast<float>(depth_raw) / 1000.0f;
        if (depth > params.max_depth) {
          if (LOGGING_DEBUG_INFO) {  
            ROS_WARN("Detection farther than %s meters, so not localizing!",
                    std::to_string(static_cast<int>(params.max_depth)).c_str());
          }
          continue;
        }

        if (USE_DEPTH_FILTER and apply_depth_filter and (depth > depth_max or depth < depth_min))
        {
          continue;
        }

        DetPoint point;

        if (is_ueye) {
          img_pixel[0] = x + x1;
          img_pixel[1] = y + y1;
          pc_rend.prLiftProjective(img_pixel, point_arr);

          point.x = point_arr[0] * depth / point_arr[2];
          point.y = point_arr[1] * depth / point_arr[2];
          point.z = depth;
        } else {
          point.x = (x + x1 - cx) * depth / fx;
          point.y = (y + y1 - cy) * depth / fy;
          point.z = depth;
        }

        point.a = 255;
        point.r = rgb[0];
        point.g = rgb[1];
        point.b = rgb[2];
        point.label = det.id;
        det_cloud->push_back(point);
        centroid_point.add(point);
      }
    }

    // Early termination of the loop above could mean no points get added, so
    // check to make sure that there are nonzero points before adding things.
    if (centroid_point.getSize() > 0u) {
      DetPoint centroid;
      centroid_point.get(centroid);
      // don't add if too close (e.g. on the robot)
      if (std::sqrt((centroid.x * centroid.x) + (centroid.y * centroid.y) +
                    (centroid.z * centroid.z)) > min_topic_dist) {
        // use size_filter to eliminate out the artifacts that is too large.
        if (USE_SIZE) {
          const auto itr1 = size_filter::size_map_max.find(det.label);
          const auto itr2 = size_filter::size_map_min.find(det.label);
          if (itr1 != size_filter::size_map_max.end() and itr2 != size_filter::size_map_min.end()) {
            double radius_max = itr1->second;
            double radius_min = itr2->second;
            int count_large = 0;
            int count_small = 0;
            int threshold_num_large = static_cast<int>(
                std::floor(det_cloud->size() * SIZE_THRESHOLD));
            int threshold_num_small = static_cast<int>(det_cloud->size() - threshold_num_large);
            bool size_flag = false;
            bool size_flag_small = false;
            for (const auto &p : *det_cloud) {
              if (pcl::euclideanDistance(centroid, p) > radius_max) {
                count_large += 1;
                if (count_large > threshold_num_large) {
                  size_flag = true;
                  break;
                }
              }
              if (pcl::euclideanDistance(centroid, p) < radius_min) {
                count_small += 1;
                if (count_small > threshold_num_small) {
                  size_flag_small = true;
                  break;
                }
              }
            }
            if (size_flag or size_flag_small) {
              if (LOGGING_DEBUG_INFO) {  
                ROS_WARN("Artifact size is too %s! label is %s",
                        (size_flag ? "large" : "small"),
                        det.label.c_str());
                DetPoint p1, p2;
                std::cout << pcl::getMaxSegment(*det_cloud, p1, p2) << std::endl;
              }
              continue;
            }
          }
        }
        det_centroids->push_back(centroid);
        det_clouds.push_back(det_cloud);
        detection_index.push_back(index_count);
      } else {
        if (LOGGING_DEBUG_INFO) {  
          ROS_WARN("Detection too close, removing!!");
        }
        continue;
      }
    }
    else{
      if (LOGGING_DEBUG_INFO) {  
        ROS_WARN("No centroids is got! label is %s",
                det.label.c_str());
      }
      continue;
    }

    index_count ++;
  }
  if (PUBLISH_DEBUG_MSGS) {  
    cv_bridge::CvImage filtered_img;
    filtered_img.header = color_msg->header;
    filtered_img.encoding = sensor_msgs::image_encodings::RGB8;
    filtered_img.image = filtered_image;
    image_pub.publish(filtered_img.toImageMsg());
  }

  monitor.toc("get_clouds_from_dets");
  return detection_index;
}

void ObjdetMapper::timer_cb(const ros::TimerEvent &event) {  // NOLINT
  monitor.tic("timer_cb");

  // Make a copy of the key pose pointers, to allow this function to work in a
  // separate thread on the local map data.
  std::vector<std::shared_ptr<KeyPose>> key_poses;
  {
    std::lock_guard<std::mutex> lock(key_poses_mutex);
    monitor.tic("timer_cb_crit");
    key_poses.reserve(key_pose_ptrs.size());
    for (const auto &pair : key_pose_ptrs) {
      key_poses.push_back(pair.second);
    }
    monitor.toc("timer_cb_crit");
  }

  // Transform all det clouds and centroids into global frame
  std::vector<DetCloud::Ptr> global_det_clouds;
  DetCloud::Ptr global_det_centroids(new DetCloud);
  // Also, grab all of the image pointers. We _might_ be able to just store
  // references to avoid expensive atomic operations, but copy for now.
  DetImagePairsList global_det_images;

  for (auto &key_pose : key_poses) {
    std::lock_guard<std::mutex> lock(key_pose->mutex);
    if (key_pose->det_points.empty()) {
      continue;
    }

    tf2::Transform key_pose_to_map_tf;
    tf2::fromMsg(key_pose->pose, key_pose_to_map_tf);
    const auto key_pose_to_map_eigen = eigen_tf_from_tf2(key_pose_to_map_tf);

    // Transform the det clouds one by one
    for (const auto &det_points : key_pose->det_points) {
      DetCloud::Ptr global_det_cloud_key_pose(new DetCloud);
      pcl::transformPointCloud(*det_points, *global_det_cloud_key_pose,
                               key_pose_to_map_eigen);
      global_det_clouds.push_back(global_det_cloud_key_pose);
    }

    // Transform the centroids as a cloud
    DetCloud::Ptr global_det_centroids_key_pose(new DetCloud);
    pcl::transformPointCloud(*key_pose->det_centroids,
                             *global_det_centroids_key_pose,
                             key_pose_to_map_eigen);
    *global_det_centroids += *global_det_centroids_key_pose;

    // Copy the image pointers
    global_det_images.insert(global_det_images.end(),
                             key_pose->det_images.begin(),
                             key_pose->det_images.end());
  }

  // Publish global centroid map for debugging
  global_det_centroids->header.frame_id = MAP_FRAME;
  pcl_conversions::toPCL(ros::Time::now(), global_det_centroids->header.stamp);
  global_centroid_publisher.publish(global_det_centroids);

  // Break early if there are no centroids
  if (global_det_centroids->empty()) {
    return;
  }

  // Cluster, match, and publish the centroids
  auto cluster_indices = cluster_det_centroids(global_det_centroids);
  auto artifact_labels = 
      group_labels_by_indices(cluster_indices, global_det_centroids);
  auto artifact_images =
      group_images_by_indices(cluster_indices, global_det_images);
  auto artifact_clouds =
      group_det_clouds_by_indices(cluster_indices, global_det_clouds);

  DetCloud::Ptr artifact_centroids;
  if (USE_MEDIAN) {
    artifact_centroids = get_median_from_clouds(artifact_clouds);
  } else {
    artifact_centroids = get_centroids_from_clouds(artifact_clouds);
  }

  update_artifact_locations(artifact_centroids, artifact_images,
                            artifact_clouds, artifact_labels);
  publish_artifact_locations();

  // Debugging publishers
  if (PUBLISH_DEBUG_MSGS) {  
    cluster_publisher.publish(artifact_centroids);
  }
  publish_global_det_cloud(global_det_clouds);
  const auto elapsed = monitor.toc("timer_cb") / 1000;
  ROS_INFO(
      "[%lu ms] Objdet mapper has %lu locations, %lu clusters, %d valid "
      "locations, %d dirty locations",
      elapsed, artifact_locations.size(), artifact_centroids->size(),
      std::accumulate(begin(artifact_locations), end(artifact_locations), 0,
                      [](int i, const ClusteredDetPoint &point) {
                        return i + point.get_valid();
                      }),
      std::accumulate(begin(artifact_locations), end(artifact_locations), 0,
                      [](int i, const ClusteredDetPoint &point) {
                        return i + point.get_dirty();
                      }));
}

std::vector<pcl::PointIndices> ObjdetMapper::cluster_det_centroids(
    const DetCloud::Ptr &centroids) {
  pcl::search::KdTree<DetPoint>::Ptr centroid_tree(
      new pcl::search::KdTree<DetPoint>);
  centroid_tree->setInputCloud(centroids);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<DetPoint> ec;
  ec.setClusterTolerance(CLUSTER_TOLERANCE);  // meters
  ec.setMinClusterSize(MIN_CLUSTER_SIZE);     // number of detections
  ec.setMaxClusterSize(MAX_CLUSTER_SIZE);     // not sure what the default is
  ec.setSearchMethod(centroid_tree);
  ec.setInputCloud(centroids);
  ec.extract(cluster_indices);

  return cluster_indices;
}

std::vector<std::vector<int>> ObjdetMapper::group_labels_by_indices(
    const std::vector<pcl::PointIndices> &cluster_indices,
    const DetCloud::Ptr &global_det_centroids) {
  std::vector<std::vector<int>> label_lists;
  for (const auto &point_indices : cluster_indices) {
    std::vector<int> labels;
    for (const auto index : point_indices.indices) {
      labels.push_back(global_det_centroids->at(index).label);
    }
    label_lists.emplace_back(labels);
  }
  return label_lists;
}

DetImagePairsList ObjdetMapper::group_images_by_indices(
    const std::vector<pcl::PointIndices> &cluster_indices,
    DetImagePairsList &images) {
  DetImagePairsList all_cluster_images;
  for (const auto &point_indices : cluster_indices) {
    DetImagePairs cluster_images;
    for (const auto index : point_indices.indices) {
      // Note the move from the shared pointer here.
      cluster_images.insert(cluster_images.end(),
                            std::make_move_iterator(images[index].begin()),
                            std::make_move_iterator(images[index].end()));
      images[index].clear();
    }

    // Technically cluster_images could have duplicates, but probably not?
    all_cluster_images.push_back(cluster_images);
  }

  images.clear();  // Clearing vector because we moved from all shared_ptrs
  return all_cluster_images;
}

std::vector<DetCloud::Ptr> ObjdetMapper::group_det_clouds_by_indices(
    const std::vector<pcl::PointIndices> &cluster_indices,
    const std::vector<DetCloud::Ptr> &global_det_clouds) {
  std::vector<DetCloud::Ptr> grouped_clouds;
  for (const auto &indices : cluster_indices) {
    DetCloud::Ptr group(new DetCloud);
    for (const auto index : indices.indices) {
      *group += *global_det_clouds[index];
    }

    group->header.frame_id = MAP_FRAME;
    pcl_conversions::toPCL(ros::Time::now(), group->header.stamp);
    grouped_clouds.push_back(group);
  }

  return grouped_clouds;
}

DetCloud::Ptr ObjdetMapper::get_centroids_from_clouds(
    const std::vector<DetCloud::Ptr> &clouds) {
  DetCloud::Ptr centroid_cloud(new DetCloud);
  for (const auto &cloud : clouds) {
    pcl::CentroidPoint<DetPoint> centroid_point;
    for (const auto &point : cloud->points) {
      centroid_point.add(point);
    }

    DetPoint centroid;
    centroid_point.get(centroid);
    centroid_cloud->push_back(centroid);
  }

  centroid_cloud->header.frame_id = MAP_FRAME;
  pcl_conversions::toPCL(ros::Time::now(), centroid_cloud->header.stamp);
  return centroid_cloud;
}

DetCloud::Ptr ObjdetMapper::get_median_from_clouds(
    const std::vector<DetCloud::Ptr> &clouds) {
  DetCloud::Ptr median_cloud(new DetCloud);
  for (const auto &cloud : clouds) {
    DetPoint median;
    median.label = cloud->points[0].label;
    auto begin = cloud->begin();
    auto end = cloud->end();
    const std::size_t size = std::distance(begin, end);
    const std::size_t mid = size / 2;
    if (size % 2 == 0) {  // Even number of values
      std::nth_element(
          begin, begin + (mid - 1), end,
          [](const auto &a, const auto &b) -> bool { return a.x < b.x; });
      median.x = (begin[mid - 1].x +
                  (std::min_element(begin + mid, end,
                                    [](const auto &a, const auto &b) -> bool {
                                      return a.x < b.x;
                                    }))
                      ->x) /
                 2.0;
      std::nth_element(
          begin, begin + (mid - 1), end,
          [](const auto &a, const auto &b) -> bool { return a.y < b.y; });
      median.y = (begin[mid - 1].y +
                  (std::min_element(begin + mid, end,
                                    [](const auto &a, const auto &b) -> bool {
                                      return a.x < b.x;
                                    }))
                      ->y) /
                 2.0;
      std::nth_element(
          begin, begin + (mid - 1), end,
          [](const auto &a, const auto &b) -> bool { return a.z < b.z; });
      median.z = (begin[mid - 1].z +
                  (std::min_element(begin + mid, end,
                                    [](const auto &a, const auto &b) -> bool {
                                      return a.x < b.x;
                                    }))
                      ->z) /
                 2.0;
    } else {  // Odd number of values
      std::nth_element(
          begin, begin + mid, end,
          [](const auto &a, const auto &b) -> bool { return a.x < b.x; });
      median.x = begin[mid].x;
      std::nth_element(
          begin, begin + mid, end,
          [](const auto &a, const auto &b) -> bool { return a.y < b.y; });
      median.y = begin[mid].y;
      std::nth_element(
          begin, begin + mid, end,
          [](const auto &a, const auto &b) -> bool { return a.z < b.z; });
      median.z = begin[mid].z;
    }
    median_cloud->push_back(median);
  }
  median_cloud->header.frame_id = MAP_FRAME;
  pcl_conversions::toPCL(ros::Time::now(), median_cloud->header.stamp);
  return median_cloud;
}

void ObjdetMapper::update_artifact_locations(
    const DetCloud::Ptr &artifact_centroids,
    const DetImagePairsList &artifact_images,
    const std::vector<DetCloud::Ptr> &artifact_clouds,
    const std::vector<std::vector<int>> &label_lists) {
  // Try to match each point in the cluster cloud to an existing point, updating
  // artifact_locations as we go.

  // ROS_INFO("all_cluster_images is size %lu", all_cluster_images.size());
  // for (size_t i = 0; i < all_cluster_images.size(); ++i) {
  //   ROS_INFO("Cluster %lu has %lu images", i, all_cluster_images[i].size());
  // }

  // Mark all current artifacts as unmatched
  std::unordered_set<size_t> unmatched_indices;
  for (size_t i = 0; i < artifact_locations.size(); ++i) {
    unmatched_indices.insert(i);
  }

  for (size_t i = 0; i < artifact_centroids->size(); ++i) {
    const auto &cluster_point = artifact_centroids->points[i];
    // Find the matching point, if any
    auto min_index = -1;
    auto min_distance = MATCH_THRESHOLD_SAMELABEL_SQ;
    for (const auto unmatched_index : unmatched_indices) {
      const auto &reported_point =
          artifact_locations[unmatched_index].get_point();
      const auto distance =
          pcl::geometry::squaredDistance(cluster_point, reported_point);
      if ((distance < MATCH_THRESHOLD_SAMELABEL_SQ and cluster_point.label == reported_point.label) or
          (distance < MATCH_THRESHOLD_DIFFLABEL_SQ and cluster_point.label != reported_point.label))
      {
        if (cluster_point.label == reported_point.label)
        {
        ROS_WARN("MATCHED! distance: %f match threshold SQ: %f", distance, MATCH_THRESHOLD_SAMELABEL_SQ);
        ROS_WARN("Label: %d", cluster_point.label);
        }
        if (distance < min_distance) {
          min_index = unmatched_index;
          min_distance = distance;
        }
      }
      
    }

    // ROS_INFO("Min index is %d", min_index);

    // Update matching point, or add new point
    const auto &cluster_images = artifact_images[i];
    const auto &cluster_cloud = artifact_clouds[i];
    const auto &label_list = label_lists[i];
    if (min_index >= 0) {  // Matched an existing point
      auto &matched_point = artifact_locations[min_index];
      matched_point.set_point(cluster_point);
      matched_point.set_valid(true);
      matched_point.update_images(cluster_images, label_list);
      matched_point.update_cloud(cluster_cloud);
      unmatched_indices.erase(min_index);
    } else {
      // Automatically marked as valid and dirty
      artifact_locations.emplace_back(cluster_point, ros::Time::now());
      artifact_locations.back().update_images(cluster_images, label_list);
      artifact_locations.back().update_cloud(cluster_cloud);
    }
  }

  // For all unmatched points, mark them as invalid.
  for (const auto unmatched_index : unmatched_indices) {
    artifact_locations[unmatched_index].set_valid(false);
  }
}

void ObjdetMapper::publish_artifact_locations() {
  auto msg = objdet_msgs::ArtifactLocalizationArray();
  msg.header.stamp = ros::Time::now();
  msg.header.frame_id = MAP_FRAME;

  for (size_t i = 0; i < artifact_locations.size(); ++i) {
    auto &artifact_location = artifact_locations[i];

    // Only send new points so we don't flood the network.
    if (!artifact_location.get_dirty()) {
      continue;
    }

    auto localization_msg = objdet_msgs::ArtifactLocalization();
    const auto &point = artifact_location.get_point();

    // valid needs to be set in every case
    localization_msg.valid = artifact_location.get_valid();  // NOLINT

    // These two items also need to be set, as they're used to track artifacts
    // for compression and by the GUI.
    localization_msg.report_id = i;
    localization_msg.stamp = artifact_location.get_stamp();

    // The rest of this is artifact specific data that isn't (and shouldn't) be
    // used in bookkeeping by anything.
    if (localization_msg.valid) {
      localization_msg.class_id = point.label;
      localization_msg.x = point.x;
      localization_msg.y = point.y;
      localization_msg.z = point.z;
      localization_msg.confidence = 0.5;  // TODO(vasua)

      const auto &images = artifact_location.get_best_images();
      for (const auto &image : images) {
        // This might be an unnecessary copy?
        localization_msg.images.push_back(*image);
      }

      // Need to convert the pcl point cloud into a ROS message.
      pcl::toROSMsg(*artifact_location.get_cloud(), localization_msg.cloud);
    }

    msg.localizations.push_back(localization_msg);

    // Mark the point as clean
    artifact_location.set_dirty(false);
  }

  if (!msg.localizations.empty()) {
    artifact_localizations_publisher.publish(msg);
  }
}

void ObjdetMapper::publish_global_det_cloud(
    const std::vector<DetCloud::Ptr> &global_det_clouds) {
  // Generate the full global point cloud and publish that
  DetCloud::Ptr global_det_cloud(new DetCloud);
  global_det_cloud->header.frame_id = MAP_FRAME;
  pcl_conversions::toPCL(ros::Time::now(), global_det_cloud->header.stamp);
  for (const auto &cloud : global_det_clouds) {
    *global_det_cloud += *cloud;
  }
  global_map_publisher.publish(global_det_cloud);
}

void ObjdetMapper::draw_detection_on_image(cv::Mat &image,
                                           const objdet_msgs::Detection &det, 
                                           const std::string &topic) {
  // coordinates normalized to [0, 1], so we need to scale.
  const int width = image.cols;
  const int height = image.rows;
  // const int x1 = det.x1 * width;
  // const int y1 = det.y1 * height;
  // const int x2 = det.x2 * width;
  // const int y2 = det.y2 * height;
  const int x1 = std::max(0, static_cast<int>((det.x1 - 0.05) * width));
  const int y1 = std::max(0, static_cast<int>((det.y1 - 0.05) * height));
  const int x2 = std::min(width, static_cast<int>((det.x2 + 0.05) * width));
  const int y2 = std::min(height, static_cast<int>((det.y2 + 0.05) * height));

  const auto &color = BOX_COLORS[(det.id - 1) % NUM_BOX_COLORS];
  // cv::putText(image, topic, cv::Point2i(width-100, 15),
  //             cv::FONT_HERSHEY_SIMPLEX, 0.5, color, 1);
  cv::rectangle(image, cv::Point2i(x1, y1), cv::Point2i(x2, y2), color, 2);
  if (x2 - x1 > 200) {
    cv::putText(image, det.label, cv::Point2i(x1, y2 - 5),
                cv::FONT_HERSHEY_SIMPLEX, 0.5, color, 1);
  } else if (y2 + 30 < height) {
    cv::putText(image, det.label, cv::Point2i(x1, y2 + 15),
                cv::FONT_HERSHEY_SIMPLEX, 0.5, color, 1);
  } else {
    cv::putText(image, det.label, cv::Point2i(x1, y1 - 5),
                cv::FONT_HERSHEY_SIMPLEX, 0.5, color, 1);
  }
}

void ObjdetMapper::create_zoomed_image(cv::Mat& image,
                                       const objdet_msgs::Detection& det) {

  // See https://learnopencv.com/cropping-an-image-using-opencv/
  
  // Figure out the borders
  const float border_frac = 0.05; // add a bit of space around the bounding box 

  const int width = image.cols;
  const int height = image.rows;
  const int x1 = std::max(0, static_cast<int>((det.x1 - border_frac) * width));
  const int y1 = std::max(0, static_cast<int>((det.y1 - border_frac) * height));
  const int x2 = std::min(width-1, static_cast<int>((det.x2 + border_frac) * width));
  const int y2 = std::min(height-1, static_cast<int>((det.y2 + border_frac) * height));

  // Crop it
  image = image(cv::Range(y1,y2),cv::Range(x1,x2)); // Note: height then width
}

double ObjdetMapper::distancesquare(const geometry_msgs::Point& p1, const geometry_msgs::Point& p2)
{
  return (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y) + (p1.z - p2.z)*(p1.z - p2.z);
}

const std::array<cv::Scalar, ObjdetMapper::NUM_BOX_COLORS>
    ObjdetMapper::BOX_COLORS = {
        cv::Scalar{230, 25, 75},   cv::Scalar{60, 180, 75},
        cv::Scalar{255, 225, 25},  cv::Scalar{0, 130, 200},
        cv::Scalar{245, 130, 48},  cv::Scalar{145, 30, 180},
        cv::Scalar{70, 240, 240},  cv::Scalar{240, 50, 230},
        cv::Scalar{210, 245, 60},  cv::Scalar{250, 190, 190},
        cv::Scalar{0, 128, 128},   cv::Scalar{230, 190, 255},
        cv::Scalar{170, 110, 40},  cv::Scalar{255, 250, 200},
        cv::Scalar{128, 0, 0},     cv::Scalar{170, 255, 195},
        cv::Scalar{128, 128, 0},   cv::Scalar{255, 215, 180},
        cv::Scalar{0, 0, 128},     cv::Scalar{128, 128, 128},
        cv::Scalar{255, 255, 255}, cv::Scalar{0, 0, 0}};

}  // namespace object_detection
