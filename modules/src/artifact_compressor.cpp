#include "modules/artifact_compressor.h"
#include <cv_bridge/cv_bridge.h>
#include <objdet_msgs/CompressedArtifactLocalizationArray.h>
#include <algorithm>
#include <boost/core/null_deleter.hpp>
#include <numeric>

BaseNode* BaseNode::get() {
  auto* compressor =
      new object_detection::ArtifactCompressor("artifact_compressor");
  return compressor;
}

namespace object_detection {

ArtifactCompressor::ArtifactCompressor(std::string node_name)
    : BaseNode(std::move(node_name)) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool ArtifactCompressor::initialize() {
  ros::NodeHandle* nh = get_node_handle();
  acknowledge_sub = nh->subscribe(COMPRESSED_TOPIC + "_ack", 10,
                                  &ArtifactCompressor::acknowledge_cb, this);
  artifact_sub = nh->subscribe("artifact_localizations", 10,
                               &ArtifactCompressor::artifact_cb, this);
  retransmit_sub = nh->subscribe("radio_command", 10,
                                 &ArtifactCompressor::retransmit_cb, this);
  artifact_pub =
      nh->advertise<objdet_msgs::CompressedArtifactLocalizationArray>(
          COMPRESSED_TOPIC, 10);
  return true;
}

bool ArtifactCompressor::execute() {
  const auto current_time = ros::Time::now();
  TransmitAttempt attempt;
  attempt.stamp = current_time;

  auto msg = objdet_msgs::CompressedArtifactLocalizationArray();
  msg.header.stamp = current_time;
  msg.header.frame_id = "/map";  // Maybe pull this from incoming arrays?

  for (auto& pair : artifacts) {
    auto report_id = pair.first;
    auto& to_transmit = pair.second;

    // Only send dirty artifacts
    if (!to_transmit.dirty) {
      continue;
    }

    // Make sure enough time has elapsed since the last update, so we don't
    // send things while the artifact is still being updated.
    if (current_time - to_transmit.last_update < DEBOUNCE_DURATION) {
      continue;
    }

    // If we decided to send the artifact, but we've sent the artifact recently,
    // don't try to send it.
    if (current_time - to_transmit.last_transmit_attempt <=
        ros::Duration(to_transmit.retransmit_delay)) {
      continue;
    } else {
      // If we did decide to send it, increment the retransmit time somewhat,
      // capping at some value as well.
      to_transmit.retransmit_delay += 5.0;
      to_transmit.retransmit_delay =
          std::min(to_transmit.retransmit_delay, 30.0);
    }

    // Once we do decide to send it, i
    msg.localizations.push_back(to_transmit.artifact);
    to_transmit.last_transmit_attempt = msg.header.stamp;
    attempt.last_update_times.emplace_back(report_id, to_transmit.last_update);

    if (msg.localizations.size() >= MAX_ARTIFACTS_PER_ATTEMPT) {
      break;
    }
  }

  ROS_INFO(
      "Compressor has %lu artifacts, %lu valid, and %lu dirty. %lu "
      "artifacts to be sent.",
      artifacts.size(),
      std::accumulate(artifacts.begin(), artifacts.end(), 0lu,
                      [](size_t i, decltype(artifacts)::value_type compressed) {
                        return i + compressed.second.artifact.valid;
                      }),
      std::accumulate(artifacts.begin(), artifacts.end(), 0lu,
                      [](size_t i, decltype(artifacts)::value_type compressed) {
                        return i + compressed.second.dirty;
                      }),
      msg.localizations.size());

  // If there's nothing to transmit, return. This skips sending (and
  // bookkeeping) blank artifacts.
  if (msg.localizations.empty()) {
    return true;
  }

  artifact_pub.publish(msg);

  // Keep the size of the transmit history bounded.
  if (transmit_attempts.size() >= MAX_TRANSMIT_HISTORY) {
    transmit_attempts.pop_front();
  }
  transmit_attempts.push_back(attempt);

  return true;
}

ArtifactCompressor::~ArtifactCompressor() = default;

void ArtifactCompressor::acknowledge_cb(
    const objdet_msgs::CompressedArtifactAckConstPtr& ack) {
  auto it =
      std::find_if(std::begin(transmit_attempts), std::end(transmit_attempts),
                   [&ack](const TransmitAttempt& attempt) {
                     return ack->stamp == attempt.stamp;
                   });

  if (it == transmit_attempts.end()) {
    ROS_WARN_STREAM("Ack timestamp "
                    << ack->stamp << " didn't get found. "
                    << "You may need to increase the MAX_TRANSMIT_HISTORY.");
    return;
  }

  for (const auto& pair : it->last_update_times) {
    if (artifacts[pair.first].last_update == pair.second) {
      artifacts[pair.first].dirty = false;
    }
  }
}

void ArtifactCompressor::artifact_cb(
    const objdet_msgs::ArtifactLocalizationArrayConstPtr& artifact_array) {
  monitor.tic("compress_all");
  for (const auto& artifact : artifact_array->localizations) {
    // We're not told what's been updated, just that something changed.
    // ROS_INFO("Updating artifact %u", artifact.report_id);
    // Note the default construction happening here
    artifacts[artifact.report_id].artifact = compress_artifact(artifact);
    artifacts[artifact.report_id].last_update = ros::Time::now();
    artifacts[artifact.report_id].retransmit_delay = 0.0;
    artifacts[artifact.report_id].dirty = true;
  }
  monitor.toc("compress_all");
}

void ArtifactCompressor::retransmit_cb(
    const basestation_msgs::RadioConstPtr& radio_msg) {
  if (radio_msg->message_type ==
      basestation_msgs::Radio::MESSAGE_TYPE_RESEND_ALL_ARTIFACTS) {
    ROS_WARN("Retransmitting all artifacts!");
    for (auto& pair : artifacts) {
      pair.second.dirty = true;
      pair.second.retransmit_delay = 0.0;
    }
  }
}

objdet_msgs::CompressedArtifactLocalization
ArtifactCompressor::compress_artifact(
    const objdet_msgs::ArtifactLocalization& artifact) {
  objdet_msgs::CompressedArtifactLocalization compressed;

  // For now, we're just compressing the images, so copy the rest of the
  // fields over as they are.
  compressed.stamp = artifact.stamp;
  compressed.valid = artifact.valid;
  compressed.x = artifact.x;
  compressed.y = artifact.y;
  compressed.z = artifact.z;
  compressed.confidence = artifact.confidence;
  compressed.class_id = artifact.class_id;
  compressed.report_id = artifact.report_id;
  compressed.readings = artifact.readings;

  compressed.images.reserve(artifact.images.size());
  for (const auto& image : artifact.images) {
    compressed.images.push_back(compress_image(image));
  }

  return compressed;
}

sensor_msgs::CompressedImage ArtifactCompressor::compress_image(
    const sensor_msgs::Image& image) {
  monitor.tic("compress_image");
  sensor_msgs::CompressedImage compressed;
  compressed.header = image.header;
  compressed.format = "jpeg";  // jpeg or png

  // https://answers.ros.org/question/196697/get-constptr-from-message/
  boost::shared_ptr<const sensor_msgs::Image> image_ptr(&image,
                                                        boost::null_deleter());
  const auto cv_image = cv_bridge::toCvShare(image_ptr);
  // We could theoretically use the cv_bridge toCompressedImageMsg() function
  // for this rather than compressing the image ourselves, but that doesn't give
  // us control over the compression params.
  if (compressed.format == "jpeg") {
    cv::imencode(".jpeg", cv_image->image, compressed.data, JPEG_PARAMS);
  } else if (compressed.format == "png") {
    cv::imencode(".png", cv_image->image, compressed.data, PNG_PARAMS);
  } else {
    fail("Invalid compression specified");
  }

  const auto prev_size = cv_image->image.total() * cv_image->image.elemSize();
  const auto current_size = compressed.data.size();
  // ROS_INFO("Shrank image from %lu to %lu bytes (%02.2f%%)", prev_size,
  //          current_size, static_cast<float>(current_size) / prev_size *
  //          100.0f);

  monitor.toc("compress_image");
  return compressed;
}
}  // namespace object_detection
