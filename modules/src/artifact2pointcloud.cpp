#include <pcl/common/common.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <ros/ros.h>
#include <vector>
#include "objdet_msgs/ArtifactLocalizationArray.h"

ros::Publisher global_publisher;

void cb(const objdet_msgs::ArtifactLocalizationArray &msg) {
  pcl::PointCloud<pcl::PointXYZRGBL> new_clouds;
  for (const auto &l : msg.localizations) {
    pcl::PointXYZRGBL new_point;
    new_point.x = l.x;
    new_point.y = l.y;
    new_point.z = l.z;
    new_point.label = l.class_id;
    new_clouds.push_back(new_point);
  }
  global_publisher.publish(new_clouds);
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "artifact2pointcloud");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/artifact_localizations", 1000, cb);
  global_publisher = n.advertise<pcl::PointCloud<pcl::PointXYZRGBL>>(
      "artifact_pointclouds", 1);

  while (ros::ok()) {
    ros::spin();
  }
  return 0;
}