#include "modules/det_images_sub.h"

namespace object_detection {
const std::string DetDepthSubTopics::REALSENSE = "realsense";
const std::string DetDepthSubTopics::REALSENSE_LIDAR = "realsense_lidar";
const std::string DetDepthSubTopics::REALSENSE_COMBINED = "realsense_combined";
const std::string DetDepthSubTopics::FLIR_LIDAR_DROP = "flir_lidar_drop";
const std::string DetDepthSubTopics::FLIR_LIDAR = "flir_lidar";

}  // namespace object_detection
