#include "modules/local_lidar_renderer.h"
#include <common/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <pcl/common/geometry.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <boost/bind.hpp>
#include "modules/load_camera_info.h"

BaseNode *BaseNode::get() {
  auto *writer =
      new object_detection::LocalLidarRenderer("local_lidar_renderer_node");
  return writer;
}

namespace object_detection {
LocalLidarRenderer::LocalLidarRenderer(std::string node_name)
    : BaseNode(std::move(node_name), false, 1, 1),
      it(*node_handle_),
      voxel_cloud(VOXEL_SIZE, VOXEL_HALF_WIDTH, FILTER_SIZE),
      tf_listener(tf_buffer) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool LocalLidarRenderer::initialize() {
  ros::NodeHandle *nh = get_node_handle(0);
  ros::NodeHandle *timesync_delay_nh = get_node_handle(1);

  delay_subscriber.initialize(timesync_delay_nh);
  registered_cloud_sub = nh->subscribe("velodyne_cloud_registered", 10,
                                       &LocalLidarRenderer::cloud_cb, this);
  highrate_odom_sub = nh->subscribe(
      "integrated_to_map", 5, &LocalLidarRenderer::highRateOdomHandler, this);
  
  float confidence_threshold;
  if (nh->getParam("/confidence_threshold", confidence_threshold)) {
    CONFIDENCE_THRESHOLD = confidence_threshold;
  }
  bool logging_debug_info;
  if (get_private_node_handle()->getParam("logging_debug_info", logging_debug_info)) {
    LOGGING_DEBUG_INFO = logging_debug_info;
  }
  bool publish_debug_msgs;
  if (get_private_node_handle()->getParam("publish_debug_msgs", publish_debug_msgs)) {
    PUBLISH_DEBUG_MSGS = publish_debug_msgs;
  }

  std::vector<std::string> det_topics;
  get_private_node_handle()->getParam("det_topics", det_topics);
  if (det_topics.empty()) {  // Checking for false seems to not work properly.
    ROS_WARN("Detection topics not set for lidar renderer!");
    return false;
  }

  for (const auto &topic : det_topics) {
    const auto base_topic = topic.substr(1, topic.find("/", 1) - 1);
    std::string filename;
    get_private_node_handle()->getParam(base_topic, filename);
    if (filename.length() > 0) {
      camera_info_list[topic] = static_cast<sensor_msgs::CameraInfoConstPtr>(
          loadCameraInfo(filename));
    } else {
      const auto info_topic =
          topic.substr(0, topic.rfind("/", topic.rfind("/") - 1)) +
          "/camera_info";
      ROS_WARN(
          "lidar_renderer: Did not find camera info file for %s. Will listen "
          "to its topic for "
          "camera info",
          info_topic.c_str());
      camera_info_list[topic] =
          ros::topic::waitForMessage<sensor_msgs::CameraInfo>(
              info_topic, ros::Duration(10.0));
    }
  }

  for (const auto &pair : camera_info_list) {
    ROS_INFO_STREAM("New subscriber for topic " << pair.first);

    const auto pub_topic = get_pub_topic(pair.first);
    disable_transports(nh, pub_topic);
    image_pubs.push_back(it.advertise(pub_topic, 1));

    // when the render function is run, to avoid callbacks lagging. In any case,
    // the queue_size is set to 1 to ignore all but the most recent message.
    info_msg_subs.emplace_back(nh->subscribe<objdet_msgs::DetectionArray>(
        pair.first, 10,
        boost::bind(&LocalLidarRenderer::info_cb, this, _1, image_pubs.back(),
                    pair.first)));
  }

  voxel_grid_pub =
      nh->advertise<pcl::PointCloud<pcl::PointXYZI>>("voxel_pointcloud", 1);
  return true;
}

bool LocalLidarRenderer::execute() { return true; }

void LocalLidarRenderer::cloud_cb(const PointCloud::ConstPtr &msg) {
  monitor.tic("cloud_cb");
  // TODO(vasua): Automatically determine the header frame
  // Using the PCL version of the callback seems to reset the frame info?
  // voxel_cloud_frame = msg->header.frame_id;
  voxel_cloud_frame = "sensor_init_rot";

  // Only add the point if robot move more than a threshold.
  auto moved_enough = [this]() mutable {
    auto dis = pcl::geometry::squaredDistance(robot_pos, robot_last_pos);
    robot_last_pos = robot_pos;
    return dis > MIN_MOVE_DISTANCE_SQ;
  };

  if (moved_enough()) {
    voxel_cloud.add(msg, robot_pos);
  }

  // Auto is actually useful here, since the return type of voxel_cloud::get
  // can change depending on the data manager used.
  const auto new_cloud_ptr = voxel_cloud.get();
  if (PUBLISH_DEBUG_MSGS) {
    COUNT++;
    if (COUNT == COUNT_MAX) {
      COUNT = 0;
      auto cloud_msg = voxel_cloud.get_colored_voxel();
      cloud_msg->header = msg->header;
      voxel_grid_pub.publish(cloud_msg);
    }
  }
  
  cloud_renderer.set_cloud(new_cloud_ptr);
  monitor.toc("cloud_cb");
}

void LocalLidarRenderer::info_cb(
    const objdet_msgs::DetectionArrayConstPtr &dets_msg,
    const image_transport::Publisher &image_pub, const std::string &topic) {
  monitor.tic("info_cb");
  if (voxel_cloud_frame == "") {
    ROS_WARN_STREAM("No point clouds received, but asked to render frame.");
    return;
  }
  if (camera_info_list[topic] == nullptr) {
    const auto info_topic =
        topic.substr(0, topic.rfind("/", topic.rfind("/") - 1)) +
        "/camera_info";
    ROS_WARN(
        "lidar_renderer: Did not receive camera info for topic %s. Will listen "
        "to its topic for "
        "camera info",
        info_topic.c_str());
    camera_info_list[topic] =
        ros::topic::waitForMessage<sensor_msgs::CameraInfo>(
            info_topic, ros::Duration(10.0));
    return;
  }
  if (camera_info_list[topic]->header.frame_id == "") {
    ROS_ERROR_THROTTLE(1, "[%s] Frame ID is not set in header.", topic.c_str());
    return;
  }

  if (dets_msg->detections.empty()) {
    return;
  }

  bool reach_confidence = false;
  for (const auto& det : dets_msg->detections) {
    if (det.confidence > CONFIDENCE_THRESHOLD) {
      reach_confidence = true;
      break;
    }
  }
  if (not reach_confidence) {
    if (LOGGING_DEBUG_INFO) {
      ROS_WARN_STREAM("Detections not reach minimum confidence level!");
    }
    return;
  }

  // adjust the timestamp based on timesync_delay_estimator to find
  // a better corresponding tf
  const auto query_time =
      dets_msg->header.stamp - delay_subscriber.get_delay_duration();

  monitor.tic("wait_for_tf");
  tf2::Transform map_to_camera_tf;
  try {
    // block for up to 100ms to wait for sensor pose tfs
    const auto map_to_camera = tf_buffer.lookupTransform(
        camera_info_list[topic]->header.frame_id, voxel_cloud_frame, query_time,
        ros::Duration(.1));
    tf2::fromMsg(map_to_camera.transform, map_to_camera_tf);
  } catch (tf2::TransformException &e) {
    monitor.toc("wait_for_tf");
    ROS_WARN("%s", e.what());
    return;
  }
  monitor.toc("wait_for_tf");

  monitor.tic("initialize");
  cv_bridge::CvImage bridge_depth;
  bridge_depth.header.frame_id =
      camera_info_list[topic]->header.frame_id;  // copy frame and timestamp
  bridge_depth.header.stamp = dets_msg->header.stamp;
  bridge_depth.encoding = "16UC1";
  monitor.toc("initialize");

  monitor.tic("render");
  // Render the image, and inflate each reading by 4 in each dimension.
  std::size_t found = camera_info_list[topic]->header.frame_id.find("rs");
  std::size_t thermal_found =
      camera_info_list[topic]->header.frame_id.find("thermal");

  if (found == std::string::npos && thermal_found == std::string::npos) {
    // ROS_WARN("Not using normal render function, using fisheye");
    bridge_depth.image = cloud_renderer.render_fisheye(
        camera_info_list[topic], map_to_camera_tf,
        static_cast<int>(cloud_renderer.kInflateConst * FILTER_SIZE));
  } else {
    // ROS_WARN("Using normal render function, for realsenses");
    bridge_depth.image = cloud_renderer.render(
        camera_info_list[topic], map_to_camera_tf,
        static_cast<int>(cloud_renderer.kInflateConst * FILTER_SIZE));
  }
  monitor.toc("render");

  monitor.tic("publish");
  image_pub.publish(bridge_depth.toImageMsg());
  monitor.toc("publish");
  // ROS_INFO_STREAM_THROTTLE(1, "Published rendered image!");
}

void LocalLidarRenderer::highRateOdomHandler(
    const nav_msgs::Odometry::ConstPtr &odom) {
  robot_pos = PointType(odom->pose.pose.position.x, odom->pose.pose.position.y,
                        odom->pose.pose.position.z);
}

inline std::string LocalLidarRenderer::get_pub_topic(const std::string &topic) {
  const auto base_topic =
      topic.substr(0, topic.rfind("/", topic.rfind("/") - 1));
  return base_topic + "/aligned_lidar/image";
}
}  // namespace object_detection
