#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include "modules/point_cloud_renderer_cuda.h"

#define CHECK_GPU(ans) \
  { gpu_assert((ans), __FILE__, __LINE__); }
#define CHECK_GPU_SAFE(ans) \
  { gpu_assert((ans), __FILE__, __LINE__, false); }

namespace object_detection {

// https://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
inline void gpu_assert(cudaError_t code, const char* file, int line,
                       bool abort = true) {
  if (code != cudaSuccess) {
    fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file,
            line);
    if (abort) {
      exit(code);
    }
  }
}

struct CudaPoint {
  float x;
  float y;
  float z;
  float padding;  // pad to 16 bytes to match PointXYZ
};

struct CudaTransform {
  float rot[3][3];
  float trans[3];

  // Need to convert to something that can be used on the device, since the
  // host functions .getBasis() and .getOrigin() don't work. So, we just copy
  // the data over in the same format.
  CudaTransform(const tf2::Transform& tf) {
    const auto& basis = tf.getBasis();
    const auto& origin = tf.getOrigin();

    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) {
        rot[i][j] = basis[i][j];
      }
    }

    for (int i = 0; i < 3; ++i) {
      trans[i] = origin[i];
    }
  }
};

struct CudaInfo {
  int width;
  int height;
  float fx;
  float fy;
  float cx;
  float cy;

  // Can't call accessors directly on the CameraInfo (boost gets in the way), so
  // copy out the relevant parts into our own struct.
  CudaInfo(const sensor_msgs::CameraInfoConstPtr& info_ptr)
      : width(info_ptr->width),
        height(info_ptr->height),
        fx(info_ptr->K[0]),
        fy(info_ptr->K[4]),
        cx(info_ptr->K[2]),
        cy(info_ptr->K[5]) {}
};

void* cuda_malloc(const size_t n) {
  void* device_points;
  CHECK_GPU(cudaMalloc(&device_points, n));
  return device_points;
}

void cuda_memcpy_to_device(void* dst, const void* src, const size_t n) {
  CHECK_GPU(cudaMemcpy(dst, src, n, cudaMemcpyHostToDevice));
}

void cuda_free(void* ptr) { CHECK_GPU_SAFE(cudaFree(ptr)); }

// Renders the image on the GPU. No particular optimizations applied.
//
// This kernel stores the rendered image as a uint32_t in the
// image_scratch_space.
__global__ void render_kernel(const CudaPoint* device_points,
                              const size_t num_points,
                              const CudaInfo camera_info,
                              const CudaTransform transform,
                              const int inflate_const,
                              uint32_t* image_scratch_space) {
  const auto idx = static_cast<size_t>(blockIdx.x * blockDim.x + threadIdx.x);
  if (idx >= num_points) {
    return;
  }

  // Transform the point into the camera frame.
  const auto& rot = transform.rot;
  const auto& trans = transform.trans;
  const float x = rot[0][0] * device_points[idx].x +
                  rot[0][1] * device_points[idx].y +
                  rot[0][2] * device_points[idx].z + trans[0];
  const float y = rot[1][0] * device_points[idx].x +
                  rot[1][1] * device_points[idx].y +
                  rot[1][2] * device_points[idx].z + trans[1];
  const float z = rot[2][0] * device_points[idx].x +
                  rot[2][1] * device_points[idx].y +
                  rot[2][2] * device_points[idx].z + trans[2];
  if (z <= 0) {  // ignore points behind the camera
    return;
  }

  // Project into image space
  // It's _possible_ to get something higher than INT_MAX, and the value for
  // static_cast becomes undefined at that point. So, make sure to clip the
  // value as a float before casting it to an int.
  auto u_full = (camera_info.fx * x) / z + camera_info.cx;
  auto v_full = (camera_info.fy * y) / z + camera_info.cy;

  const int width = camera_info.width;
  const int height = camera_info.height;

  // Consider width and height when converting u and v to ensure we're within
  // bounds of float / int conversion.
  const int u =
      u_full < 0 ? -1 : u_full >= width ? width : static_cast<int>(u_full);
  const int v =
      v_full < 0 ? -1 : v_full >= height ? height : static_cast<int>(v_full);

  const int inflate = static_cast<int>(ceil(inflate_const / point.z / 2));

  if ((0 + inflate) <= u && u < (width - inflate) && (0 + inflate) <= v &&
      v < (height - inflate)) {  // in bounds
    // Clamp the depth value we're putting in to close to the limit of a
    // uint16_t. We could go to like 65000 but meh. Don't want to be exact since
    // you might run into floating point rounding issues.
    const float new_depth_full = z * 1000.0f;  // convert to mm
    const uint32_t new_depth = new_depth_full < 60000.0f
                                   ? static_cast<uint32_t>(new_depth_full)
                                   : 60000u;
    for (int i = -inflate; i <= inflate; ++i) {
      for (int j = -inflate; j <= inflate; ++j) {
        const auto image_index = (v + i) * width + (u + j);
        atomicMin(image_scratch_space + image_index, new_depth);
      }
    }
  }
}

// Converts (copy) uint32_t image_scratch_space to uint16_t image_buffer
//
// Any points which are UINT_MAX in the image_scratch_space will be converted to
// 0 when put into the image_buffer.
__global__ void fill_image_buffer_kernel(const uint32_t* image_scratch_space,
                                         uint16_t* image_buffer,
                                         const size_t num_points) {
  const int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i >= num_points) {
    return;
  }

  const auto current = image_scratch_space[i];
  if (current == 0xFFFFFFFFu) {
    image_buffer[i] = 0;
  } else {
    // This conversion is known to be safe since the value is constrained to be
    // between 0 and 60000, based on the render step.
    image_buffer[i] = static_cast<uint16_t>(current);
  }
}

PointCloudRendererCuda::PointCloudRendererCuda() : timer("CUDA Renderer") {
  print_cuda_info();
  set_image_buffers(DEFAULT_WIDTH, DEFAULT_HEIGHT);
}

PointCloudRendererCuda::~PointCloudRendererCuda() {
  cuda_free(image_buffer);
  cuda_free(image_scratch_space);
}

void PointCloudRendererCuda::set_cloud(
    const std::deque<std::pair<void*, size_t>>* new_clouds) {
  clouds = new_clouds;
}

cv::Mat PointCloudRendererCuda::render_fisheye(
    const sensor_msgs::CameraInfoConstPtr& camera_info_ptr,
    const tf2::Transform& map_to_camera_tf, const int inflate) {
  const auto height = camera_info.height;
  const auto width = camera_info.width;
  cv::Mat mat = cv::Mat::zeros(height, width, CV_16UC1);

  fprintf(stderr, "ERROR Fisheye CUDA support is not implemented!\n");
  return mat;
}

cv::Mat PointCloudRendererCuda::render(
    const sensor_msgs::CameraInfoConstPtr& camera_info_ptr,
    const tf2::Transform& map_to_camera_tf, const int inflate) {
  timer.tic("overall render v2");

  timer.tic("mat creation");
  const auto height = camera_info_ptr->height;
  const auto width = camera_info_ptr->width;
  cv::Mat mat = cv::Mat::zeros(height, width, CV_16UC1);
  timer.toc("mat creation");

  timer.tic("set image buffers");
  set_image_buffers(width, height);
  timer.toc("set image buffers");

  const dim3 threads_per_block(256);  // Reasonable value in general

  timer.tic("render to temp");
  // Render the points for each input cloud
  for (const auto& pair : *clouds) {
    if (pair.second == 0) {
      // Empty cloud. This probably shouldn't happen.
      fprintf(stderr, "Empty cloud found!\n");
      continue;
    }
    const dim3 num_blocks((pair.second / threads_per_block.x) + 1);
    render_kernel<<<num_blocks, threads_per_block>>>(
        reinterpret_cast<const CudaPoint*>(pair.first), pair.second,
        CudaInfo(camera_info_ptr), CudaTransform(map_to_camera_tf), inflate,
        image_scratch_space);
    CHECK_GPU(cudaPeekAtLastError());
  }
  timer.toc("render to temp");

  timer.tic("fill final image");
  const dim3 num_blocks(((width * height) / threads_per_block.x) + 1);
  fill_image_buffer_kernel<<<num_blocks, threads_per_block>>>(
      image_scratch_space, image_buffer, width * height);
  CHECK_GPU(cudaPeekAtLastError());
  timer.toc("fill final image");

  timer.tic("final copy");
  // Could also use mat.elemSize() * mat.total()
  CHECK_GPU(cudaMemcpy(mat.data, image_buffer,
                       width * height * sizeof(uint16_t),
                       cudaMemcpyDeviceToHost));
  timer.toc("final copy");

  timer.toc("overall render v2");
  return mat;
}

// Code borrowed from CMU 15418
void PointCloudRendererCuda::print_cuda_info() {
  int deviceCount = 0;
  CHECK_GPU(cudaGetDeviceCount(&deviceCount));

  fprintf(stderr, "--------------------------------------------------------\n");
  fprintf(stderr, "Found %d CUDA devices\n", deviceCount);

  for (int i = 0; i < deviceCount; i++) {
    cudaDeviceProp deviceProps;
    CHECK_GPU(cudaGetDeviceProperties(&deviceProps, i));
    fprintf(stderr, "Device %d: %s\n", i, deviceProps.name);
    fprintf(stderr, "   SMs:        %d\n", deviceProps.multiProcessorCount);
    fprintf(stderr, "   Global mem: %.0f MB\n",
            static_cast<float>(deviceProps.totalGlobalMem) / (1024 * 1024));
    fprintf(stderr, "   CUDA Cap:   %d.%d\n", deviceProps.major,
            deviceProps.minor);
  }
  fprintf(stderr, "--------------------------------------------------------\n");
}

void PointCloudRendererCuda::set_image_buffers(const size_t width,
                                               const size_t height) {
  if (scratch_elems < width * height) {
    // Reallocate the scratch spaces
    cuda_free(image_scratch_space);
    cuda_free(image_buffer);
    scratch_elems = width * height;
    image_scratch_space = reinterpret_cast<uint32_t*>(
        cuda_malloc(scratch_elems * sizeof(uint32_t)));
    image_buffer = reinterpret_cast<uint16_t*>(
        cuda_malloc(scratch_elems * sizeof(uint16_t)));
  }

  // And then set it to UINT32_MAX = 0xFFFFFFFF
  // Note that we're only setting width * height elements, not the full thing.
  CHECK_GPU(
      cudaMemset(image_scratch_space, 0xFF, width * height * sizeof(uint32_t)));
  // Leave the image_buffer uninitialized, it gets set every frame.
}

}  // namespace object_detection
