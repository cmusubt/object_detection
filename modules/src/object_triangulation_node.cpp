/**
 * @file Object_triangulation_node.cpp
 *
 * @brief Object_triangulation_node creates a node receiving images and artifact
 * detection, looking up TF tree to get the location of the camera and then use
 * the above information to do triangulation. Triangulation result is sent to
 * artifact_filter.
 *
 * @author Chenfeng Tu and Tian Liu
 * Contact: {chenfent,tianliu}@andrew.cmu.edu
 *
 */

#include "../include/triangulator/object_triangulation_node.h"

using namespace std;
using namespace cv;

BaseNode *BaseNode::get() {
  auto *object_triangulator =
      new object_detection::object_triangulation::ObjectTriangulationNode(
          "object_triangulator");
  return object_triangulator;
}

namespace object_detection {
namespace object_triangulation {
ObjectTriangulationNode::ObjectTriangulationNode(std::string node_name)
    : BaseNode(std::move(node_name)) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool ObjectTriangulationNode::initialize() {
  report_id_ = 0;

  ros::NodeHandle *nh = get_node_handle();

  can_do_new_triangulation_ = std::vector<bool>(10, false);

  // Load parameters
  std::vector<std::string> image_topics;
  std::vector<std::string> image_detection_topics;
  std::vector<double> front_camera_intrinsics;
  std::vector<double> back_camera_intrinsics;
  std::vector<double> left_camera_intrinsics;
  std::vector<double> right_camera_intrinsics;

  std::vector<double> intrinsics;

  get_private_node_handle()->getParam("image_topics", image_topics);
  num_cameras_ = image_topics.size();

  get_private_node_handle()->getParam("image_detection_topics",
                                      image_detection_topics);

  get_private_node_handle()->getParam("intrinsics", intrinsics);

  get_private_node_handle()->getParam("optimize", optimize_);
  get_private_node_handle()->getParam("use_ransac", use_ransac_);
  get_private_node_handle()->getParam("num_observs_thresh",
                                      num_observs_thresh_);

  get_private_node_handle()->getParam("is_debug", is_debug_);

  get_private_node_handle()->getParam("cluster_mean_k", cluster_mean_k_);
  get_private_node_handle()->getParam("cluster_std_mul_th",
                                      cluster_std_mul_th_);
  get_private_node_handle()->getParam("counter_for_no_detection_threshold",
                                      counter_for_no_detection_threshold_);
  get_private_node_handle()->getParam("debug_files_path", debug_files_path_);
  get_private_node_handle()->getParam("nearest_neighbor_k",
                                      nearest_neighbor_k_);
  get_private_node_handle()->getParam("max_matched_keyframe_distance",
                                      max_matched_keyframe_distance_);
  get_private_node_handle()->getParam("min_translation_between_two_keyframes",
                                      min_translation_between_two_keyframes_);
  get_private_node_handle()->getParam("pose_delay_offset", pose_delay_offset_);
  get_private_node_handle()->getParam("label_map_path", label_map_path_);
  // get the number of object types
  {
    std::fstream ff;
    ff.open(label_map_path_, ios::in);

    if (ff.is_open()) {
      string s;
      while (getline(ff, s)) {
        // id line
        if (s.find("id:") != std::string::npos) {
          int temp_num = std::stoi(s.substr(s.find(": ") + 1).c_str());
          num_object_types_ =
              num_object_types_ > temp_num ? num_object_types_ : temp_num;
        }
      }
      ff.close();
    } else
    ROS_WARN_STREAM("Error opening label map file " << errno);
  }

  if (is_debug_) {
    ROS_WARN_STREAM("debug_files_path = " << debug_files_path_);
  }
  triangulator_.LoadParameters(get_private_node_handle());

  orb_detector_ = ORB::create(50, 1.2f, 8, 31, 0, 2, ORB::HARRIS_SCORE, 31, 20);

  // Initialize Calibration
  for (int i = 0; i < num_cameras_; i++) {
    sharedCals_.push_back(boost::make_shared<gtsam::Cal3_S2>(
        intrinsics[0 + i * 4], intrinsics[1 + i * 4], 0, intrinsics[2 + i * 4],
        intrinsics[3 + i * 4]));
  }

  // message filter synchronizers registration
  for (int i = 0; i < num_cameras_; i++) {
    tf_buffers_.push_back(make_shared<tf2_ros::Buffer>());
    tf_listener_ptrs_.push_back(std::unique_ptr<tf2_ros::TransformListener>(
        new tf2_ros::TransformListener(*tf_buffers_[i])));
  }

  image_transport::ImageTransport it_(*nh);

  for (int i = 0; i < num_cameras_; i++) {
    image_subs_.push_back(make_shared<image_transport::SubscriberFilter>());
    bounding_box_subs_.push_back(
        make_shared<
            message_filters::Subscriber<objdet_msgs::DetectionArray>>());
    syncs_.push_back(
        make_shared<message_filters::TimeSynchronizer<
            sensor_msgs::Image, objdet_msgs::DetectionArray>>(10000));
  }

  for (int i = 0; i < num_cameras_; i++) {
    image_subs_[i]->subscribe(it_, image_topics[i], 10000);
    bounding_box_subs_[i]->subscribe(*nh, image_detection_topics[i], 10000);
  }

  for (int i = 0; i < num_cameras_; i++) {
    syncs_[i]->connectInput(*image_subs_[i], *bounding_box_subs_[i]);
    syncs_[i]->registerCallback(boost::bind(
        &ObjectTriangulationNode::image_detection_callback, this, _1, _2, i));
  }

  for (int i = 0; i < num_cameras_; i++) {
    counters_for_no_detection_.push_back(counter_for_no_detection_threshold_);
  }

  const auto &node_name = ros::this_node::getName() + "/";
  artifact_localizations_publisher =
      nh->advertise<objdet_msgs::ArtifactLocalizationArray>(
          node_name + "artifact_localizations", 10);

  // for debugging, publish triangulated point to show and check in Rviz
  if (is_debug_) {
    ROS_WARN("object triangulation node topic established! ");

    std::string triangulated_object_keypoints_topic(
        "/triangulated_object_keypoints");
    triangulator_object_pointcloud_pub_ =
        nh->advertise<sensor_msgs::PointCloud>(
            triangulated_object_keypoints_topic, 100);

    std::string filtered_triangulated_object_keypoints_topic(
        "/filtered_triangulated_object_keypoints");
    triangulator_filtered_object_pointcloud_pub_ =
        nh->advertise<sensor_msgs::PointCloud>(
            filtered_triangulated_object_keypoints_topic, 100);

    std::string averaged_triangulated_object_keypoints_topic(
        "/averaged_triangulated_object_keypoints");
    triangulator_averaged_object_pointcloud_pub_ =
        nh->advertise<sensor_msgs::PointCloud>(
            averaged_triangulated_object_keypoints_topic, 100);

    std::string triangulation_ground_truth_topic("/triangulation_ground_truth");
    triangulation_ground_truth_pub_ = nh->advertise<sensor_msgs::PointCloud>(
        triangulation_ground_truth_topic, 100);

    std::string artifact_localizations_topic("/artifact_localizations");
    artifact_localizations_sub_.subscribe(*nh, artifact_localizations_topic,
                                          100);
    artifact_localizations_sub_.registerCallback(boost::bind(
        &ObjectTriangulationNode::objectLocalizationCallback, this, _1));
  }

  ROS_WARN("Triangulation initialization Finished!");
  return true;
}

void ObjectTriangulationNode::image_detection_callback(
    const sensor_msgs::ImageConstPtr &img_msg,
    const objdet_msgs::DetectionArrayConstPtr &detection_array, int camera) {
  if (camera == 0) {
    process_callback("front", counters_for_no_detection_[camera],
                     *tf_buffers_[camera], sharedCals_[camera], img_msg,
                     detection_array);
  }

  if (camera == 1) {
    process_callback("back", counters_for_no_detection_[camera],
                     *tf_buffers_[camera], sharedCals_[camera], img_msg,
                     detection_array);
  }

  if (camera == 2) {
    process_callback("left", counters_for_no_detection_[camera],
                     *tf_buffers_[camera], sharedCals_[camera], img_msg,
                     detection_array);
  }

  if (camera == 3) {
    process_callback("right", counters_for_no_detection_[camera],
                     *tf_buffers_[camera], sharedCals_[camera], img_msg,
                     detection_array);
  }

  if (camera == 4) {
    process_callback("up", counters_for_no_detection_[camera],
                     *tf_buffers_[camera], sharedCals_[camera], img_msg,
                     detection_array);
  }

  if (camera == 5) {
    process_callback("down", counters_for_no_detection_[camera],
                     *tf_buffers_[camera], sharedCals_[camera], img_msg,
                     detection_array);
  }
}

// This function process images, when artifacts are detected:
// 1. get the bounding box and detect orb features in the bounding box
// 2. add keyframe if the distance to the previous keyframe is long enough(to
// ensure triangulation quality)
// 3. do feature matching with neighbour keyframes
void ObjectTriangulationNode::process_callback(
    const std::string &camera_name, int &counter_for_no_detection,
    tf2_ros::Buffer &tf_buffer,
    const boost::shared_ptr<gtsam::Cal3_S2> &shared_cal_,
    const sensor_msgs::ImageConstPtr &img_msg,
    const objdet_msgs::DetectionArrayConstPtr &detection_array) {
  cv_bridge::CvImageConstPtr img_ptr =
      cv_bridge::toCvCopy(img_msg, sensor_msgs::image_encodings::BGR8);
  cv::Mat image = img_ptr->image;
  int rows = image.rows;
  int cols = image.cols;

  // only do triangulation when object is detected
  if (detection_array->detections.size() > 0) {
    counter_for_no_detection = 0;

    int object_id = detection_array->detections[0].id;
    cv::Mat gray;
    cv::cvtColor(image, gray, COLOR_BGR2GRAY);

    // if bounding box is around the boundary, discard the frame
    float x1 = -1, y1 = -1, x2 = -1, y2 = -1, mid_x, mid_y;
    for (const auto &bounding_box : detection_array->detections) {
      x1 = bounding_box.x1 * cols;
      y1 = bounding_box.y1 * rows;
      x2 = bounding_box.x2 * cols;
      y2 = bounding_box.y2 * rows;
      float mid_x = (x1 + x2) * 0.5;
      float mid_y = (y1 + y2) * 0.5;

      if (x1 <= 1 || x2 >= cols - 1) return;

      if (is_debug_) {
        cv::Point2f pt1(x1, y1);
        cv::Point2f pt2(x2, y2);
        cv::Point2f pt_mid(mid_x, mid_y);

        cv::circle(image, pt_mid, 4, cv::Scalar(0, 0, 255), -1);
        cv::rectangle(image, pt1, pt2, cv::Scalar(0, 0, 255), 2, cv::LINE_8, 0);
      }
    }

    geometry_msgs::TransformStamped transform_stamped;
    try {
      auto lookup_time = img_msg->header.stamp;
      lookup_time.sec -= pose_delay_offset_;
      transform_stamped = tf_buffer.lookupTransform(
          "map", "rs_" + camera_name + "/camera_color_optical_frame",
          lookup_time);
    } catch (tf2::TransformException &ex) {
      ROS_WARN("%s", ex.what());
      ros::Duration(1.0).sleep();
      return;  // if failed, then wait for the next sync
    }

    // get current pose
    Eigen::Vector3d t;
    Eigen::Quaterniond q;
    t[0] = transform_stamped.transform.translation.x;
    t[1] = transform_stamped.transform.translation.y;
    t[2] = transform_stamped.transform.translation.z;
    q.x() = transform_stamped.transform.rotation.x;
    q.y() = transform_stamped.transform.rotation.y;
    q.z() = transform_stamped.transform.rotation.z;
    q.w() = transform_stamped.transform.rotation.w;

    gtsam::Rot3 rotation = gtsam::Rot3::Quaternion(q.w(), q.x(), q.y(), q.z());
    gtsam::Point3 translation(t);
    gtsam::Pose3 current_pose(rotation, translation);

    // Keypoints Detection and Descriptors Extraction
    std::vector<cv::KeyPoint> key_points;
    cv::Mat mask = cv::Mat::zeros(gray.size(), CV_8U);
    cv::Rect roi(x1, y1, x2 - x1, y2 - y1);
    mask(roi).setTo(255);

    cv::Mat descriptors;
    orb_detector_->detect(gray, key_points, mask);
    if (key_points.empty()) {
      return;
    }
    orb_detector_->compute(gray, key_points, descriptors);

    // create new keyframe
    int new_keyframe_id = triangulator_.object_keyframes_[object_id].size();
    KeyFrame<gtsam::Cal3_S2> new_keyframe(object_id, new_keyframe_id,
                                          current_pose, shared_cal_, key_points,
                                          descriptors);

    // store image for publishing, 3 is the number of proof images
    if (cached_images_keypoint_nums_.size() < 3) {
      cached_images_.push_back(*img_msg);
      cached_images_keypoint_nums_.push_back(key_points.size());
    } else {
      // replace the one that with fewer keypoints
      if (key_points.size() >
          *std::min_element(cached_images_keypoint_nums_.begin(),
                            cached_images_keypoint_nums_.end())) {
        int pos =
            std::distance(cached_images_keypoint_nums_.begin(),
                          min_element(cached_images_keypoint_nums_.begin(),
                                      cached_images_keypoint_nums_.end()));
        cached_images_keypoint_nums_[pos] = key_points.size();
        cached_images_[pos] = *img_msg;
      }
    }

    // save keyframe image if debug
    if (is_debug_) {
      string keyframe_img_name = debug_files_path_ + "keyframes/object_" +
                                 ZeroPadNumber(object_id) + "/keyframe_" +
                                 ZeroPadNumber(new_keyframe_id) + ".jpg";
      ROS_WARN_STREAM("keyframe_img_name  = " << keyframe_img_name);
      cv::imwrite(keyframe_img_name, image);
    }

    int object_keyframes_size =
        triangulator_.object_keyframes_[object_id].size();

    if (object_keyframes_size > 0) {
      const auto &last_keyframe =
          triangulator_.object_keyframes_[object_id].back();
      double distance_to_last_keyframe =
          last_keyframe.GetDistance(new_keyframe);
      if (distance_to_last_keyframe > min_translation_between_two_keyframes_) {
        double squared_max_distance_threshold =
            max_matched_keyframe_distance_ * max_matched_keyframe_distance_;

        std::vector<int> indices_search;
        std::vector<float> squared_distance_search;
        int num_neighbors = triangulator_.KNearestSearchKeyFrame(
            object_id, new_keyframe, nearest_neighbor_k_, indices_search,
            squared_distance_search);
        for (int i = 0; i < indices_search.size(); i++) {
          if (squared_distance_search[i] < squared_max_distance_threshold) {
            int old_keyframe_id = indices_search[i];
            KeyFrame<gtsam::Cal3_S2> &old_keyframe =
                triangulator_.object_keyframes_[object_id][old_keyframe_id];
            ros::WallTime start_time, end_time;
            start_time = ros::WallTime::now();
            triangulator_.MatchTwoKeyframe(old_keyframe, new_keyframe);
            can_do_new_triangulation_[object_id] = true;
            can_do_new_triangulation_any_ = true;
            end_time = ros::WallTime::now();
            if (is_debug_) {
              double execution_time = (end_time - start_time).toNSec() * 1e-6;
              ROS_INFO_STREAM(
                  "Time spent on matchTwoKeyframe (ms): " << execution_time);
            }
          }
        }
        triangulator_.InsertNewKeyFrame(object_id, new_keyframe);
      }

    } else {
      triangulator_.InsertNewKeyFrame(object_id, new_keyframe);
    }

    if (is_debug_) {
      drawKeypoints(image, key_points, image, Scalar::all(-1),
                    DrawMatchesFlags::DEFAULT);
      string keyframe_debug_img_name = debug_files_path_ +
                                       "keyframes_debug/object_" +
                                       ZeroPadNumber(object_id) + "/keyframe_" +
                                       ZeroPadNumber(new_keyframe_id) + ".jpg";
      cv::imwrite(keyframe_debug_img_name, image);
    }
  } else {
    // when there is no artifact detection, the counter will always accumulate
    // in execute(), here is for preventing the counter from overflow
    if (counter_for_no_detection > counter_for_no_detection_threshold_) {
      counter_for_no_detection = counter_for_no_detection_threshold_;
    }
  }
  if (is_debug_) {
    cv::namedWindow(camera_name, cv::WINDOW_AUTOSIZE);
    cv::imshow(camera_name, image);
    cv::waitKey(1);
  }
}

/** Periodically check if triangulation can be made and do triangulation
 */
bool ObjectTriangulationNode::execute() {
  // accumulate the counter
  double et;
  get_private_node_handle()->getParam("execute_target", et);
  for (int i = 0; i < num_cameras_; i++) {
    counters_for_no_detection_[i] += et * 1000;  // 1000 to convert to mseconds
  }
  counter_for_no_detection_front_ += et * 1000;  // 1000 to convert to mseconds
  counter_for_no_detection_back_ += et * 1000;
  counter_for_no_detection_left_ += et * 1000;
  counter_for_no_detection_right_ += et * 1000;
  // prevent overflow
  for (int i = 0; i < num_cameras_; i++) {
    counters_for_no_detection_[i] = counter_for_no_detection_threshold_;
  }

  if (counter_for_no_detection_front_ > counter_for_no_detection_threshold_) {
    counter_for_no_detection_front_ = counter_for_no_detection_threshold_;
  }
  if (counter_for_no_detection_back_ > counter_for_no_detection_threshold_) {
    counter_for_no_detection_back_ = counter_for_no_detection_threshold_;
  }
  if (counter_for_no_detection_left_ > counter_for_no_detection_threshold_) {
    counter_for_no_detection_left_ = counter_for_no_detection_threshold_;
  }
  if (counter_for_no_detection_right_ > counter_for_no_detection_threshold_) {
    counter_for_no_detection_right_ = counter_for_no_detection_threshold_;
  }

  // when can do triangulation and all of the cameras are not seeing artifact
  // for a certain period, this could potentially delay the triangulation when
  // there are more than 1 artifacts being observed by different cameras.
  bool counter_flag = true;
  for (int i = 0; i < num_cameras_; i++) {
    counter_flag &=
        (counters_for_no_detection_[i] >= counter_for_no_detection_threshold_);
  }
  if (can_do_new_triangulation_any_ && counter_flag) {
    if (is_debug_) {
      ROS_WARN("========== can do triangulation ===============");
    }

    for (int object_id = 0; object_id <= num_object_types_; object_id++) {
      if (can_do_new_triangulation_[object_id]) {
        if (is_debug_) {
          ROS_WARN_STREAM("will do new triangulation for object : ");
        }

        ros::WallTime start_time, end_time;
        start_time = ros::WallTime::now();
        triangulator_.TriangulateObject(object_id, 1e-9, optimize_, use_ransac_,
                                        num_observs_thresh_);
        end_time = ros::WallTime::now();
        double execution_time = (end_time - start_time).toNSec() * 1e-6;
        publish_results(object_id);
        triangulator_.ClearObject(object_id);
        can_do_new_triangulation_[object_id] = false;
      }
    }

    can_do_new_triangulation_any_ = false;
  }
  return true;
}

void ObjectTriangulationNode::publish_results(int object_id) {
  sensor_msgs::PointCloud triangulated_object_pointcloud;
  sensor_msgs::PointCloud filtered_triangulated_object_pointcloud;
  sensor_msgs::PointCloud averaged_triangulated_object_pointcloud;
  triangulated_object_pointcloud.header.frame_id = "map";
  filtered_triangulated_object_pointcloud.header.frame_id = "map";
  averaged_triangulated_object_pointcloud.header.frame_id = "map";

  // create pcl and apply StatisticalOutlierRemoval filter
  pcl::PointCloud<pcl::PointXYZ>::Ptr landmark_cloud(
      new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud(
      new pcl::PointCloud<pcl::PointXYZ>);
  for (const auto &landmark : triangulator_.objects_[object_id]) {
    geometry_msgs::Point32 p;
    if (landmark.CheckTriangulation()) {
      Point3 landmark_location = landmark.GetTriangulation();
      p.x = (float)landmark_location.x();
      p.y = (float)landmark_location.y();
      p.z = (float)landmark_location.z();
      triangulated_object_pointcloud.points.push_back(p);

      landmark_cloud->push_back(pcl::PointXYZ(
          landmark_location.x(), landmark_location.y(), landmark_location.z()));
    }
  }
  if (landmark_cloud->empty()) {
    return;
  }

  objdet_msgs::ArtifactLocalizationArray artifacts;
  artifacts.header.stamp = ros::Time::now();
  artifacts.header.frame_id = "map";
  objdet_msgs::ArtifactLocalization artifact;

  pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
  sor.setInputCloud(landmark_cloud);
  sor.setMeanK(cluster_mean_k_);
  sor.setStddevMulThresh(cluster_std_mul_th_);
  sor.filter(*filtered_cloud);
  Eigen::Vector4f centroid;

  if (filtered_cloud->size() > 0) {
    for (const auto &point : filtered_cloud->points) {
      geometry_msgs::Point32 p;
      p.x = (float)point.x;
      p.y = (float)point.y;
      p.z = (float)point.z;
      filtered_triangulated_object_pointcloud.points.push_back(p);
    }
    pcl::compute3DCentroid(*filtered_cloud, centroid);
    if (is_debug_) {
      ROS_WARN_STREAM(" The predicted location for object ["
                      << object_id << "] is: " << centroid);
    }

    geometry_msgs::Point32 p;
    p.x = centroid(0);
    p.y = centroid(1);
    p.z = centroid(2);
    averaged_triangulated_object_pointcloud.points.push_back(p);

    artifact.stamp = artifacts.header.stamp;
    artifact.valid = true;
    artifact.x = p.x;
    artifact.y = p.y;
    artifact.z = p.z;
    artifact.confidence = 0.5;
    artifact.class_id = object_id;

    for (const auto &image : cached_images_) {
      artifact.images.push_back(image);
    }
    cached_images_.clear();
    cached_images_keypoint_nums_.clear();

    artifacts.localizations.emplace_back(artifact);
  }

  if (is_debug_) {
    triangulator_object_pointcloud_pub_.publish(triangulated_object_pointcloud);
    triangulator_filtered_object_pointcloud_pub_.publish(
        filtered_triangulated_object_pointcloud);
    triangulator_averaged_object_pointcloud_pub_.publish(
        averaged_triangulated_object_pointcloud);

    triangulator_.PrintTriangulationResults();
  }

  // Only publish actual updates
  if (!artifacts.localizations.empty()) {
    artifact_localizations_publisher.publish(artifacts);
  }
}

/** debug function to publish triangulated point cloud
 */
void ObjectTriangulationNode::objectLocalizationCallback(
    const objdet_msgs::ArtifactLocalizationArrayConstPtr &localization_array) {
  // TODO: cannot recognize and triangulate different instances of the same type
  // of artifact right now
  int class_id = localization_array->localizations[0].class_id;
  latest_artifact_locations_[class_id].push_back(
      localization_array->localizations[0].x);
  latest_artifact_locations_[class_id].push_back(
      localization_array->localizations[0].y);
  latest_artifact_locations_[class_id].push_back(
      localization_array->localizations[0].z);

  sensor_msgs::PointCloud ground_truth_point_cloud;
  geometry_msgs::Point32 p;
  ground_truth_point_cloud.header = localization_array->header;
  p.x = localization_array->localizations[0].x;
  p.y = localization_array->localizations[0].y;
  p.z = localization_array->localizations[0].z;
  ground_truth_point_cloud.points.push_back(p);
  triangulation_ground_truth_pub_.publish(ground_truth_point_cloud);
}

}  // namespace object_triangulation
}  // namespace object_detection
