#include "modules/signal_localizer.h"
#include <common/tf2.h>
#include <image_transport/image_transport.h>
#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <sensor_msgs/Image.h>
#include <algorithm>
#include <cmath>
#include <exception>

BaseNode* BaseNode::get() {
  auto* localizer = new object_detection::SignalLocalizer("signal_localizer");
  return localizer;
}

namespace object_detection {
SignalLocalizer::SignalLocalizer(std::string node_name)
    : BaseNode(std::move(node_name)), it(*node_handle_) {
  get_private_node_handle()->setParam("execute_target", 1);
}

bool SignalLocalizer::initialize() {
  const auto& node_name = ros::this_node::getName() + "/";
  auto* nh = get_node_handle();

  // Subscribe to all of the topics, setting the signal type appropriately.
  std::vector<std::string> topics;

  get_private_node_handle()->getParam("wifi_topics", topics);
  for (const auto& topic : topics) {
    signal_subs.push_back(
        nh->subscribe<objdet_msgs::WirelessSignalReadingArray>(
            topic, 10,
            boost::bind(&SignalLocalizer::signal_cb, this, _1,
                        SignalType::WIFI)));
  }

  topics.clear();
  get_private_node_handle()->getParam("bluetooth_topics", topics);
  for (const auto& topic : topics) {
    signal_subs.push_back(
        nh->subscribe<objdet_msgs::WirelessSignalReadingArray>(
            topic, 10,
            boost::bind(&SignalLocalizer::signal_cb, this, _1,
                        SignalType::BLUETOOTH)));
  }

  if (signal_subs.empty()) {
    ROS_WARN("No topics set for signal localizer!");
    return false;
  }

  get_private_node_handle()->getParam("save_cell_phone_imgs", save_imgs);
  get_private_node_handle()->getParam("img_streams", img_topics);

  if (save_imgs && (img_topics.empty())) {
    ROS_WARN("\nYou want to save images but no topics have been specified!\n");
  }

  // save the most recent images in order to associate images with the
  // rssi readings
  recent_imgs.resize(img_topics.size());

  for (std::size_t i = 0; i < img_topics.size(); ++i) {
    img_subs.push_back(it.subscribe(
        img_topics[i], 1, boost::bind(&SignalLocalizer::img_cb, this, _1, i)));

    // so we only save each image stream image once per
    // specified period (e.g. 1 Hz)
    last_img_save_time.push_back(ros::Time::now().toSec());
  }

  artifact_localizations_publisher =
      nh->advertise<objdet_msgs::ArtifactLocalizationArray>(
          node_name + "artifact_localizations", 10);

  delay_subscriber.initialize(nh);
  key_pose_listener.initialize(nh, &tf_buffer);
  tf_listener_ptr = std::unique_ptr<tf2_ros::TransformListener>(
      new tf2_ros::TransformListener(tf_buffer));

  return true;
}

bool SignalLocalizer::execute() {
  ROS_INFO("Have readings for %lu devices", device_readings.size());

  Eigen::Vector3f position;
  for (const auto& [mac_address, signal_reading] : device_readings) {
    // ROS_INFO_STREAM("    Device " << mac_address << " has "
    //                               << signal_reading.readings.size()
    //                               << " readings");

    // Need to convert the readings from key pose frame to map frame.
    std::vector<signal_solvers::ReadingFromPosition> solver_readings;
    solver_readings.reserve(signal_reading.readings.size());
    std::transform(
        signal_reading.readings.begin(), signal_reading.readings.end(),
        std::back_inserter(solver_readings), [&](const auto& reading) {
          const auto& key_pose =
              key_pose_listener.get_key_pose_by_index(reading.key_pose_index);
          tf2::Transform key_pose_to_map_tf;
          tf2::fromMsg(key_pose, key_pose_to_map_tf);
          const auto robot_position_map =
              key_pose_to_map_tf * reading.robot_position;

          signal_solvers::ReadingFromPosition r;
          r.robot_position = {static_cast<float>(robot_position_map.x()),
                              static_cast<float>(robot_position_map.y()),
                              static_cast<float>(robot_position_map.z())};
          r.raw_reading = reading.rssi;
          return r;
        });

    int max_rssi_index = 0;
    if (!signal_solvers::find_artifact_max_rssi(solver_readings, position,
                                                max_rssi_index)) {
      continue;
    }

    // Find images to go along with the artifacts
    if (save_imgs && !(img_topics.empty())) {
      cached_artifacts[signal_reading.id].update_imgs(
          signal_reading.readings[max_rssi_index].images);
    }

    // Update all of the RSSI/position pairs we have for this device
    cached_artifacts[signal_reading.id].update_readings(solver_readings);
    cached_artifacts[signal_reading.id].device_code = mac_address;
    // Artifact now contains something useful.
    const auto updated =
        cached_artifacts[signal_reading.id].update_position(position);
    if (updated) {
      ROS_INFO_STREAM("Updated position for address " << mac_address);
    }
  }

  objdet_msgs::ArtifactLocalizationArray artifacts;
  artifacts.header.stamp = ros::Time::now();
  artifacts.header.frame_id = MAP_FRAME;
  for (auto& cached_artifact : cached_artifacts) {
    if (!cached_artifact.get_dirty()) {
      continue;
    }

    artifacts.localizations.emplace_back(cached_artifact.get_artifact());
    cached_artifact.clean();
  }

  // Only publish actual updates
  if (!artifacts.localizations.empty()) {
    artifact_localizations_publisher.publish(artifacts);
  }

  return true;
}

void SignalLocalizer::signal_cb(
    const objdet_msgs::WirelessSignalReadingArrayConstPtr& readings,
    const SignalType signal_type) {
  monitor.tic("signal_cb");

  if (readings->readings.empty()) {
    // Not worth trying to lookup the transform if there's nothing nearby
    return;
  }

  const auto query_time = //ros::Time::now();
       readings->header.stamp - delay_subscriber.get_delay_duration();

  // Get key pose
  const auto key_pose_index =
      key_pose_listener.get_key_pose_index_at_time(query_time);
  if (!key_pose_index) {
    return;
  }
  const auto& key_pose =
      key_pose_listener.get_key_pose_by_index(*key_pose_index);
  tf2::Transform key_pose_to_map_tf;
  tf2::fromMsg(key_pose, key_pose_to_map_tf);

  // Get robot (signal) position in key frame
  tf2::Transform signal_to_map_tf;
  try {
    const auto signal_to_map = tf_buffer.lookupTransform(
        MAP_FRAME, 
        "sensor", // sanitize_frame(readings->header.frame_id),
        query_time,
        ros::Duration(.1));
    tf2::fromMsg(signal_to_map.transform, signal_to_map_tf);
  } catch (tf2::TransformException& e) {
    ROS_WARN("%s", e.what());
    return;
  }
  const auto signal_to_key_pose_tf =
      key_pose_to_map_tf.inverse() * signal_to_map_tf;

  ReadingFromKeyPosePosition device_reading;
  device_reading.robot_position = signal_to_key_pose_tf.getOrigin();
  device_reading.key_pose_index = *key_pose_index;

  /// BUNK Value
  uint8_t incomingArtifactClass = 250;

  for (const auto& reading : readings->readings) {
    ROS_INFO_STREAM("Parsing reading....");
    if (signal_type == SignalType::WIFI) {
      // For WiFi, we know the SSID will be set to "PhoneArtifactXX", so use
      // that to filter out the other access points (and our own nodes)
      if (reading.name.find("PhoneArtifact") == std::string::npos) {
        continue;
      } else {
        ROS_INFO_STREAM("WIFI SIGNAL! " << reading.name);
        incomingArtifactClass = class_labels.PHONE_ID;
      }

    } else if (signal_type == SignalType::BLUETOOTH) {
      // For Bluetooth, just keep everything. Potentially filter in the future
      // by comparing MAC address to a WiFi one.
      ROS_INFO_STREAM("BLUE SIGNAL! " << reading.name);
      if (reading.name.find("PhoneArtifact") != std::string::npos) {
        incomingArtifactClass = class_labels.PHONE_ID;  
      } else if (reading.name.find("CubeArtifact") != std::string::npos) {
        incomingArtifactClass = class_labels.CUBE_ID;   
      }
    }

    if (incomingArtifactClass == 250){
      /// Reading for an irrelevant device....
      continue;
    }

    if (device_readings.count(reading.name) == 0) {

      // Can't do this in one line since it'll use the size of the map after
      // default constructing the new element instead of before.
      uint32_t id = device_readings.size();
      device_readings[reading.name].id = id;
      
      cached_artifacts.emplace_back(id);  // So that we can directly access it
      cached_artifacts.back().update_valid(true);       // Always valid
      cached_artifacts.back().update_confidence(1.0f);  // We know it's a phone
      cached_artifacts.back().update_class_id(incomingArtifactClass);
      cached_artifacts.back().clean();
    }

    device_reading.rssi = reading.rssi;
    // put the most recent images captured in device reading
    device_reading.images = recent_imgs;

    device_readings[reading.name].readings.push_back(device_reading);
  }
  monitor.toc("signal_cb");
}

void SignalLocalizer::img_cb(const sensor_msgs::ImageConstPtr& msg,
                             const std::size_t& topic_index) {
  if (save_imgs && (ros::Time::now().toSec() - last_img_save_time[topic_index] >
                    img_save_period)) {
    if (topic_index < recent_imgs.size()) {
      recent_imgs[topic_index] = msg;
    }
    last_img_save_time[topic_index] = ros::Time::now().toSec();
  }
}

}  // namespace object_detection
