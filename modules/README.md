## Dependencies

Depends on CUDA for optimal performance. A non-CUDA build is in progress.

Dependencies for Triangulator:
1. Eigen 3
2. PCL 1.8
3. GTSAM 4.0, follow the installation guide from link(https://github.com/borglab/gtsam/blob/develop/README.md)

## Quick start

1. Build object detection repository (ideally in release mode)
1. Follow installation instructions for Tensorflow inference in the `/inference`
   folder
1. Modify `launch/sim_bag.launch` in this folder to have paths to your bag files
   and run it (`roslaunch sim_bag.launch`). It starts paused by default so
   you'll have to press spacebar. You'll want to modify the `rosbag play` node
   at the end of the file. You'll also probably want to modify the path to the
   frozen inference graph being used in `tfod_multi_inference.py`.
1. Run `launch/sim_mod.launch` as is. You may want to change the launch file to
   change which cameras are being included for mapping.
1. Launch base station, telling it to use real artifact detections rather than
   simulated ones.

## Chrony node setup

Assumes chrony is set up and working properly.

1. Figure out your username (`echo $USER`)
1. Run `sudo visudo` (will launch `nano`)
1. Add this line at the end (with the username from above)

    username ALL=(ALL) NOPASSWD: /usr/bin/chronyc makestep

## Tegrastats node setup

1. Run `sudo visudo` (will launch `nano`)
1. Add these lines to the end

    %sudo ALL=(ALL) NOPASSWD: /usr/bin/tegrastats
    %sudo ALL=(ALL) NOPASSWD: /usr/bin/jetson_clocks

## Usage of Triangulation module

1. Enabling/Disabling Triangulation module: toggling comment of `object_triangulation_node` in `mk_1.launch`, triangulation module should not affect other modules when disabled.
2. Refer to `docs/Triangulation framework.png` for general pipeline of Triangulation system framework
3. In testing, to visualize use of result of Triangulation module, uncomment `<node name="rviz" pkg="rviz" type="rviz" output="screen" args = "-d $(find modules)/rviz/triangulation_visualization.rviz" />` in `mk_1.launch` 

## Notes
1. If you encounter `No kernel image is available for execution on device`
error, you need to add the CUDA computablity as a flag to NVCC.

    Use the `deviceQuery` tool in CUDA sample or this 
    [link](https://developer.nvidia.com/cuda-gpus) to check the computablity of 
    your CUDA enabled GPU. Then add it under the `CUDA_NVCC_FLAGS` in 
    CMakeLists.txt.
