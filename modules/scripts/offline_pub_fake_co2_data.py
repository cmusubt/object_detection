#!/usr/bin/env python3

"""
Publishes fake co2 data at pre-specified locations


Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""
import rospy
from objdet_msgs.msg import GasInfo
from nav_msgs.msg import Odometry
import math
from visualization_msgs.msg import Marker
import numpy as np


def odom_cb(msg, args):
    """
    If the robot is getting close to a fake source,
    publish a corresponding reading
    """
    global concentration_msg, last_odom_msg, marker_id

    gas_pub, fake_locs = args

    in_range_of_source = False

    cumulative_concentration = 0

    for loc in fake_locs:
        concentration = get_strength(msg.pose.pose.position, loc)
        cumulative_concentration += concentration

        if cumulative_concentration > 0:
            concentration_msg = GasInfo()
            concentration_msg.header.stamp = rospy.Time.now()
            concentration_msg.header.frame_id = "/velodyne"
            concentration_msg.co2_concentration = cumulative_concentration

            in_range_of_source = True

            last_odom_msg = msg

    if not in_range_of_source:
        concentration_msg = GasInfo()
        concentration_msg.header.stamp = rospy.Time.now()
        concentration_msg.header.frame_id = "/map"
        concentration_msg.co2_concentration = cumulative_concentration


def pub_concentration_msg(gas_pub, marker_pub):
    global concentration_msg, last_odom_msg, marker_id
    gas_pub.publish(concentration_msg)

    # publish rviz marker that is the localization fo the reading
    marker = Marker()
    marker.header.frame_id = last_odom_msg.header.frame_id

    marker.type = marker.CUBE
    marker.action = marker.ADD

    t = rospy.Duration()
    marker.lifetime = t
    marker.pose.position.x = last_odom_msg.pose.pose.position.x
    marker.pose.position.y = last_odom_msg.pose.pose.position.y
    marker.pose.position.z = last_odom_msg.pose.pose.position.z
    marker.pose.orientation.w = 1
    marker.scale.x = 1.2
    marker.scale.y = 1.2
    marker.scale.z = 0.3

    marker.color.a = 0.5
    marker.color.r = 1
    marker.color.b = 1

    marker.id = marker_id
    marker_id += 1

    marker_pub.publish(marker)


def get_strength(robot_pos, fake_source_loc):
    """
    Return a fake gas concentration based in gaussian model
    """
    stddev = 3.0
    variance = stddev ** 2

    dist = math.sqrt(
        (robot_pos.x - fake_source_loc[0]) ** 2
        + (robot_pos.y - fake_source_loc[1]) ** 2
        + (robot_pos.z - fake_source_loc[2]) ** 2
    )

    syn_concentration = (
        1.0
        / (variance * math.sqrt(2 * math.pi))
        * np.exp((-1.0 / 2.0) * np.power((dist / variance), 2.0))
        * 157e3
    )

    if syn_concentration < 10:
        return 0

    return syn_concentration


if __name__ == "__main__":

    rospy.init_node("fake_co2_pub")
    pub_frequency = 0.5  # in Hz
    rate = rospy.Rate(pub_frequency)

    fake_locs = [[79.083, 10.482, 0.25], [103.290, -55.271, 0.000]]

    gas_pub = rospy.Publisher("gas_info", GasInfo, queue_size=10)

    marker_pub = rospy.Publisher("gas_marks", Marker, queue_size=10)

    signal_pos_sub = rospy.Subscriber(
        "/integrated_to_map", Odometry, odom_cb, (gas_pub, fake_locs)
    )

    global concentration_msg, last_odom_msg, marker_id
    concentration_msg = GasInfo()
    last_odom_msg = Odometry()
    marker_id = 0

    while not rospy.is_shutdown():
        pub_concentration_msg(gas_pub, marker_pub)
        rate.sleep()
