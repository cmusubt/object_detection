#!/usr/bin/env python  
import rospy

import math
import tf2_ros
import geometry_msgs.msg
import turtlesim.srv

if __name__ == '__main__':
    rospy.init_node('tf2_listener')

    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)
    br = tf2_ros.TransformBroadcaster()

    rate = rospy.Rate(100.0)
    while not rospy.is_shutdown():
        try:
            trans = tfBuffer.lookup_transform('sensor_init', 'sensor', rospy.Time())
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rate.sleep()
            continue

        t = trans #geometry_msgs.msg.TransformStamped()

        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "sensor_init"
        t.child_frame_id = "sensor_adj"
        # print(t.transform.translation.x, t.transform.translation.y, t.transform.translation.z)
        # t.transform.translation.x = msg.x
        # t.transform.translation.y = msg.y
        # t.transform.translation.z = 0.0
        # q = tf_conversions.transformations.quaternion_from_euler(0, 0, msg.theta)
        # t.transform.rotation.x = q[0]
        # t.transform.rotation.y = q[1]
        # t.transform.rotation.z = q[2]
        # t.transform.rotation.w = q[3]
        # print('\nsending transform\n')
        br.sendTransform(t)

        rate.sleep()