#!/usr/bin/env python3

import datetime
import rosbag
import rospy
import time_synchronizer
import tf2_ros

from sensor_msgs.msg import CameraInfo
from sensor_msgs.msg import Image


class SaveInfoImages(object):
    def __init__(self, base_topic):
        self._color_image_topic = base_topic + "/color/image"
        self._color_info_topic = base_topic + "/color/camera_info"
        self._aligned_depth_image_topic = (
            base_topic + "/aligned_depth_to_color/image"
        )
        self._aligned_depth_info_topic = (
            base_topic + "/aligned_depth_to_color/camera_info"
        )

        self._subs = [
            time_synchronizer.Subscriber(
                self._color_image_topic, Image, buff_size=1 << 24
            ),
            time_synchronizer.Subscriber(self._color_info_topic, CameraInfo),
            time_synchronizer.Subscriber(
                self._aligned_depth_image_topic, Image, buff_size=1 << 24
            ),
            time_synchronizer.Subscriber(
                self._aligned_depth_info_topic, CameraInfo
            ),
        ]

        self._ts = time_synchronizer.TimeSynchronizer(
            self._subs, queue_size=10, callback=self.callback
        )

        self._tf_buffer = tf2_ros.Buffer()
        self._tf_listener = tf2_ros.TransformListener(self._tf_buffer)
        self._saved = False

    def callback(self, color, color_info, aligned_depth, aligned_depth_info):
        if self._saved:
            return

        timestamp = color.header.stamp
        try:
            map_to_camera_tf = self._tf_buffer.lookup_transform(
                color.header.frame_id, "map", timestamp, rospy.Duration(0.5)
            )
        except (
            tf2_ros.LookupException,
            tf2_ros.ConnectivityException,
            tf2_ros.ExtrapolationException,
        ) as e:
            rospy.logwarn("Error during transform extraction: %s", e)
            return

        output_filename = datetime.datetime.now().strftime(
            "/tmp/example_%Y-%m-%d-%H-%M-%S.bag"
        )
        with rosbag.Bag(output_filename, "w") as bag:
            bag.write(self._color_image_topic, color, timestamp)
            bag.write(self._color_info_topic, color_info, timestamp)
            bag.write(self._aligned_depth_image_topic, aligned_depth, timestamp)
            bag.write(
                self._aligned_depth_info_topic, aligned_depth_info, timestamp
            )
            bag.write("/map_to_camera_tf", map_to_camera_tf, timestamp)

        rospy.loginfo("Saved an example!")
        self._saved = True


def main():
    rospy.init_node("save_info_images")
    base_topic = rospy.get_param("~base_topic", None)
    if base_topic is None:
        rospy.logerr("No base topic specified!")

    saver = SaveInfoImages(base_topic)
    rospy.spin()


if __name__ == "__main__":
    main()
