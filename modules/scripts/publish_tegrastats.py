#!/usr//bin/env python3

import re
import rospy
import subprocess
import signal
from objdet_msgs.msg import Tegrastats


class TegrastatsParser(object):
    def __init__(self):
        # TODO(vasua): Make this less fragile.
        # The temperatures and power consumption metrics displayed seem to vary
        # somewhat based on the hardware connected (e.g. iwlwifi not being
        # present when no wifi adapter is hooked up). This, plus the message
        # type, needs to be made robust to these failures somehow. Maybe just
        # keep the few things we actually care about instead of everything?
        self._stats_regex = re.compile(
            r"RAM "
            r"(?P<ram>\d+)/\d+MB "
            #  r"\(lfb (?P<lfb>\d+)x4MB\) "
            r".*"  # This changes units and we don't _really_ care
            r"SWAP (?P<swap>\d+)/\d+MB \(cached (?P<swap_cached>\d+)MB\) "
            r"CPU \[(?P<cpu>.*)\] "
            r"EMC_FREQ (?P<emc_percentage>\d+)% "
            r"GR3D_FREQ (?P<gpu_percentage>\d+)% "
            r"AO@(?P<ao_temp>[0-9.]+)C "
            r"GPU@(?P<gpu_temp>[0-9.]+)C "
            # r"iwlwifi@(?P<iwlwifi_temp>[0-9.]+)C "
            r".*"  # Remove iwlwifi temp since it's not present everywhere.
            r"Tboard@(?P<tboard_temp>[0-9.]+)C "
            r"Tdiode@(?P<tdiode_temp>[0-9.]+)C "
            r"AUX@(?P<aux_temp>[0-9.]+)C "
            r"CPU@(?P<cpu_temp>[0-9.]+)C "
            r"thermal@(?P<thermal_temp>[0-9.]+)C "
            r"PMIC@(?P<pmic_temp>[0-9.]+)C "
            r"GPU (?P<gpu_current_power>\d+)/(?P<gpu_average_power>\d+) "
            r"CPU (?P<cpu_current_power>\d+)/(?P<cpu_average_power>\d+) "
            r"SOC (?P<soc_current_power>\d+)/(?P<soc_average_power>\d+) "
            r"CV (?P<cv_current_power>\d+)/(?P<cv_average_power>\d+) "
            r"VDDRQ (?P<vddrq_current_power>\d+)/(?P<vddrq_average_power>\d+) "
            r"SYS5V (?P<sys5v_current_power>\d+)/(?P<sys5v_average_power>\d+)"
        )

    def parse(self, output):
        m = self._stats_regex.match(output)

        stats = Tegrastats()

        stats.ram = int(m.group("ram"))
        #  stats.lfb = int(m.group("lfb"))
        stats.swap = int(m.group("swap"))
        stats.swap_cached = int(m.group("swap_cached"))

        for cpu in m.group("cpu").split(","):
            cpu_percentage, cpu_frequency = cpu.split("%@")
            stats.cpu_percentage.append(int(cpu_percentage))
            stats.cpu_frequency.append(int(cpu_frequency))

        stats.emc_percentage = int(m.group("emc_percentage"))
        # stats.emc_frequency = int(m.group("emc_frequency"))
        stats.gpu_percentage = int(m.group("gpu_percentage"))
        # stats.gpu_frequency = int(m.group("gpu_frequency"))
        # stats.audio_frequency = int(m.group("audio_frequency"))
        # stats.fg_percentage = int(m.group("fg_percentage"))
        # stats.bg_percentage = int(m.group("bg_percentage"))
        stats.ao_temp = float(m.group("ao_temp"))
        stats.gpu_temp = float(m.group("gpu_temp"))
        #  stats.iwlwifi_temp = float(m.group("iwlwifi_temp"))
        stats.tboard_temp = float(m.group("tboard_temp"))
        stats.tdiode_temp = float(m.group("tdiode_temp"))
        stats.aux_temp = float(m.group("aux_temp"))
        stats.cpu_temp = float(m.group("cpu_temp"))
        stats.thermal_temp = float(m.group("thermal_temp"))
        stats.pmic_temp = float(m.group("pmic_temp"))
        stats.gpu_current_power = int(m.group("gpu_current_power"))
        stats.gpu_average_power = int(m.group("gpu_average_power"))
        stats.cpu_current_power = int(m.group("cpu_current_power"))
        stats.cpu_average_power = int(m.group("cpu_average_power"))
        stats.soc_current_power = int(m.group("soc_current_power"))
        stats.soc_average_power = int(m.group("soc_average_power"))
        stats.cv_current_power = int(m.group("cv_current_power"))
        stats.cv_average_power = int(m.group("cv_average_power"))
        stats.vddrq_current_power = int(m.group("vddrq_current_power"))
        stats.vddrq_average_power = int(m.group("vddrq_average_power"))
        stats.sys5v_current_power = int(m.group("sys5v_current_power"))
        stats.sys5v_average_power = int(m.group("sys5v_average_power"))

        return stats


class TegrastatsPublisher(object):
    def __init__(self, interval):
        self._interval = str(interval)
        self._stats_pub = rospy.Publisher(
            "tegrastats", Tegrastats, queue_size=1
        )
        self._parser = TegrastatsParser()

    # https://stackoverflow.com/a/38341327
    def start(self):
        tegrastats = subprocess.Popen(
            ["sudo", "/usr/bin/tegrastats", "--interval", self._interval],
            stdout=subprocess.PIPE,
        )

        for line in tegrastats.stdout:
            output = line.decode("ascii").strip()
            try:
                stats = self._parser.parse(output)
                stats.header.stamp = rospy.Time.now()
            except Exception as e:
                rospy.logwarn("Unable to parse tegrastats: %s", e)
                continue

            if rospy.is_shutdown():
                return

            self._stats_pub.publish(stats)


def main():
    rospy.init_node("tegrastats_publisher")

    interval = rospy.get_param("~interval", 1000)
    pub = TegrastatsPublisher(interval)
    pub.start()


if __name__ == "__main__":
    main()
