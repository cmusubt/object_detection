#!/usr/bin/env python3

import rospy
import cv2
import cv_bridge
from objdet_msgs.msg import ArtifactLocalizationArray
from basestation_msgs.msg import WifiDetection


class ArtifactTranslator(object):
    def __init__(self, robot_id, input, output, rotate_color):
        self._rotate_color = rotate_color
        self._artifact_localization_sub = rospy.Subscriber(
            input, ArtifactLocalizationArray, self._translate_cb
        )
        self._wifi_detection_pub = rospy.Publisher(
            output, WifiDetection, queue_size=10
        )
        self._bridge = cv_bridge.CvBridge()
        self.robot_id = robot_id

    def _translate_cb(self, localization_msg):
        for localization in localization_msg.localizations:
            wifi_msg = WifiDetection()
            self._fill_images(wifi_msg, localization)
            self._fill_artifact_data(wifi_msg, localization)
            self._wifi_detection_pub.publish(wifi_msg)

    def _fill_images(self, wifi_msg, localization):
        if not self._rotate_color:
            wifi_msg.imgs = localization.images
            return

        # We might need to go and rotate color images (but not thermal)
        for image in localization.images:
            if "rs_" in image.header.frame_id:
                cv_image = self._bridge.imgmsg_to_cv2(image, "rgb8")
                cv_image_rotated = cv_image[::-1, ::-1, :]
                img_msg = self._bridge.cv2_to_imgmsg(cv_image_rotated, "rgb8")
                img_msg.header = image.header
                wifi_msg.imgs.append(img_msg)
            else:
                wifi_msg.imgs.append(image)

    def _fill_artifact_data(self, wifi_msg, localization):
        wifi_msg.artifact_stamp = localization.stamp
        wifi_msg.artifact_robot_id = self.robot_id
        wifi_msg.artifact_report_id = localization.report_id

        wifi_msg.artifact_type = localization.class_id
        if not localization.valid:
            wifi_msg.artifact_type = wifi_msg.ARTIFACT_REMOVE

        wifi_msg.artifact_x = localization.x
        wifi_msg.artifact_y = localization.y
        wifi_msg.artifact_z = localization.z

        wifi_msg.readings = localization.readings


def main():
    rospy.init_node("artifact_translator")

    translators = []
    robot_id = 0
    for topic in rospy.get_param("~topics", []):
        translators.append(ArtifactTranslator(robot_id, **topic))
        robot_id += 1

    if not translators:
        rospy.logwarn("No topics specified to be translated!")
        return

    rospy.spin()


if __name__ == "__main__":
    main()
