import rosbag

off_list = ['/rosout', '/rosout_agg', '/tf', '/camera/image_raw/compressedDepth/parameter_descriptions', '/ueye_cam_nodelet/parameter_updates', '/camera/image_raw/theora/parameter_descriptions', '/camera/image_raw/compressed/parameter_descriptions', '/diagnostics', '/camera/timeout_count', '/rosmon_1608410922921029815/state', '/uav4/cloud_nodelet/parameter_updates', '/uav4/cloud_nodelet/parameter_descriptions', '/camera/image_raw/compressedDepth/parameter_updates', '/camera/image_raw/theora/parameter_updates', '/camera/image_raw/compressed/parameter_updates', '/ueye_cam_nodelet/parameter_descriptions', '/imu/data', '/uav4/velodyne_packets', '/camera/image_raw/compressed', '/camera/image_raw', '/velodyne_points', '/camera/image_raw/theora', '/camera/camera_info', '/uav4/velodyne_nodelet_manager/bond', '/nodelet_manager/bond', '/velodyne_cloud_2', '/laser_cloud_less_sharp', '/laser_cloud_less_flat', '/laser_cloud_sharp', '/laser_cloud_flat', '/superodom/feature_info', '/laser_cloud_surround', '/laser_cloud_map', '/velodyne_cloud_registered_imu', '/velodyne_cloud_registered', '/aft_mapped_to_init', '/aft_mapped_to_init_imu', '/velodyne_rawcloud_registered', '/aft_mapped_path', '/super_odometry_stats', '/integrated_to_init', '/integrated_to_init2', '/imu_loam/imu/path']

with rosbag.Bag('/home/subt-nuc-02/Downloads/gates_2_tf_adjusted.bag', 'w') as outbag:
    for topic, msg, t in rosbag.Bag('/home/subt-nuc-02/Downloads/gates2_superodom.bag').read_messages():
        # This also replaces tf timestamps under the assumption 
        # that all transforms in the message share the same timestamp
        # print(t.secs)
        # if t.secs > 1611000000:
        #     if topic not in off_list:
        #         off_list.append(topic)
        #     print(off_list)
        outbag.write(topic, msg, t)
        # if topic == "/tf" and msg.transforms:
        #     outbag.write(topic, msg, msg.transforms[0].header.stamp)
        #     print("T: ", t.secs, msg.transforms[0].header.stamp)
        # else:
        #     if msg._has_header:
        #         stamp = msg.header.stamp
        #         # if stamp.secs < 1609738730:
        #         #     stamp.secs += 1326303
        #         print(stamp.secs)
        #         outbag.write(topic, msg, stamp)
        #     else:
        #         stamp = t
        #         print("Not writing: ", topic)
                