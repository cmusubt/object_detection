#!/usr/bin/env python3

"""
Publishes markers that have been colormapped to designate
the strength of the rssi reading and/or gas cocnentration 
level at various locations

Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""
import rospy
import threading
from visualization_msgs.msg import MarkerArray, Marker
from nav_msgs.msg import Odometry
from std_msgs.msg import ColorRGBA
from basestation_msgs.msg import WifiDetection
import math
from common import ClassLabels as CL
from matplotlib import cm


class RawReadingVisualizer(object):
    def __init__(self):

        self.detection_topics = rospy.get_param(
            "/visualize_raw_readings/detection_topics"
        )

        # Display amt: amount of colormapped markers to display_amt
        # low = display only high-value rssi readings
        # med = display everything at a low-density
        # high = display everything
        self.wifi_display_amt = rospy.get_param(
            "/visualize_raw_readings/wifi_display_amt"
        )

        self.gas_display_amt = rospy.get_param(
            "/visualize_raw_readings/gas_display_amt"
        )

        # array of signal positions/rssis
        for topic in self.detection_topics:
            rospy.logfatal(topic)
            self.signal_pos_sub = rospy.Subscriber(
                topic, WifiDetection, self.detection_cb
            )

        self.wifi_marker_pub = rospy.Publisher(
            "wifi_signal_markers", MarkerArray, queue_size=10
        )

        self.gas_marker_pub = rospy.Publisher(
            "gas_concentration_markers", MarkerArray, queue_size=10
        )

        # dictionary key is "robot_id/report_id", values is StampedReading
        self.signal_pos_readings = {}
        self.concentration_pos_readings = {}
        self.class_labels = CL.ClassLabels()

    def dist(self, reading_a, reading_b):
        """
        Determines euclidean distance between two readings
        """
        return math.sqrt(
            (reading_a.pose.position.x - reading_b.pose.position.x) ** 2
            + (reading_a.pose.position.y - reading_b.pose.position.y) ** 2
            + (reading_a.pose.position.z - reading_b.pose.position.z) ** 2
        )

    def detection_cb(self, detection_msg):
        """
        Receive an WifiDetection message and publish the corresponding 
        readings
        """

        # if its not a cell phone, return
        # even invalid artifacts may have reading info
        if detection_msg.artifact_type not in [
            -1,
            self.class_labels.PHONE_ID,
            self.class_labels.GAS_ID,
        ]:
            return

        # negative values also indicate cell phone
        if (
            detection_msg.artifact_type == self.class_labels.PHONE_ID
            or self.sum_readings(detection_msg) < 0
        ):
            self.update_phone_markers(detection_msg)

        else:
            self.update_gas_markers(detection_msg)

    def sum_readings(self, detection_msg):
        """
        Sum the readings, if they're positiive its a co2 detection, 
        else its a cell phone
        """
        value_sum = 0
        for reading in detection_msg.readings:
            value_sum += reading.reading

        return value_sum

    def artifact_robot_id_to_map_topic(self, artifact_robot_id):
        return self.detection_topics[artifact_robot_id].replace(
            "/wifi_detection", "_map"
        )

    def update_phone_markers(self, detection_msg):

        # overwrite readings in case of loop closure

        self.signal_pos_readings[
            str(detection_msg.artifact_robot_id)
            + "/"
            + str(detection_msg.artifact_report_id)
        ] = detection_msg.readings

        map_topic = self.artifact_robot_id_to_map_topic(
            detection_msg.artifact_robot_id
        )
        self.publish_markers(
            self.signal_pos_readings, [-100, -50, -30], "wifi", map_topic
        )

    def update_gas_markers(self, detection_msg):

        # overwrite readings in case of loop closure

        print("gas detection!", len(detection_msg.readings))

        self.concentration_pos_readings[
            str(detection_msg.artifact_robot_id)
            + "/"
            + str(detection_msg.artifact_report_id)
        ] = detection_msg.readings

        map_topic = self.artifact_robot_id_to_map_topic(
            detection_msg.artifact_robot_id
        )
        self.publish_markers(
            self.concentration_pos_readings,
            [1000, 1500, 2000],
            "gas",
            map_topic,
        )

    def get_marker_color(self, reading, thresholds, reading_type):

        color_info = ColorRGBA()

        if reading_type == "wifi":
            # funky math because rssi is negative...
            normalized_val = (abs(thresholds[0]) - abs(reading)) / (
                abs(thresholds[0]) - abs(thresholds[-1])
            )
            return cm.hot(normalized_val)
        elif reading_type == "gas":
            normalized_val = abs(thresholds[0] - reading) / abs(
                thresholds[0] - thresholds[-1]
            )
            return cm.winter(normalized_val)

    def get_text_marker(self, reading, reading_type, map_topic):

        marker = Marker()

        marker.header.frame_id = map_topic
        marker.header.frame_id = reading.header.frame_id

        marker.type = marker.TEXT_VIEW_FACING
        marker.action = marker.ADD
        if reading_type == "wifi":
            marker.text = "PHONE READING"
        elif reading_type == "gas":
            marker.text = "GAS READING"

        marker.lifetime = rospy.Duration()
        marker.pose.position.x = reading.pose.position.x
        marker.pose.position.y = reading.pose.position.y
        marker.pose.position.z = reading.pose.position.z + 0.5

        marker.pose.orientation.w = 1
        marker.scale.z = 0.2

        marker.color.a = 1
        marker.color.r = 52 / 255.0
        marker.color.g = 235.0 / 255
        marker.color.b = 229.0 / 255

        return marker

    def publish_markers(
        self, reading_dict, thresholds, reading_type, map_topic
    ):

        markerArray = MarkerArray()
        marker_id = 0

        prev_reading = None

        if reading_type == "wifi":
            disp_amt = self.wifi_display_amt
        elif reading_type == "gas":
            disp_amt = self.gas_display_amt

        for report_id in reading_dict:
            stamped_readings = reading_dict[report_id]

            for stamped_reading in stamped_readings:

                # only display high-strength responses
                if disp_amt == "low" and (
                    (
                        prev_reading != None
                        and self.dist(stamped_reading, prev_reading) < 2.0
                        and stamped_reading.reading < thresholds[1]
                    )
                    or stamped_reading.reading < thresholds[0]
                ):

                    continue

                # display everything at a reduced density
                elif (
                    disp_amt == "med"
                    and prev_reading != None
                    and self.dist(stamped_reading, prev_reading) < 2.5
                    and stamped_reading.reading < thresholds[1]
                ):

                    continue

                marker = Marker()

                stamped_reading.header.frame_id = map_topic
                marker.header.frame_id = stamped_reading.header.frame_id

                marker.type = marker.CYLINDER
                marker.action = marker.ADD

                marker.lifetime = rospy.Duration()
                marker.pose.position.x = stamped_reading.pose.position.x
                marker.pose.position.y = stamped_reading.pose.position.y
                marker.pose.position.z = stamped_reading.pose.position.z
                marker.pose.orientation.w = 1
                marker.scale.x = 0.7
                marker.scale.y = 0.7
                marker.scale.z = 0.7

                marker.id = marker_id
                marker_id += 1

                (
                    marker.color.r,
                    marker.color.g,
                    marker.color.b,
                    marker.color.a,
                ) = self.get_marker_color(
                    stamped_reading.reading, thresholds, reading_type
                )

                markerArray.markers.append(marker)

                text_marker = self.get_text_marker(
                    stamped_reading, reading_type, map_topic
                )
                text_marker.id = marker_id
                marker_id += 1

                markerArray.markers.append(text_marker)

                prev_reading = stamped_reading

        if len(markerArray.markers) > 0:
            if reading_type == "wifi":
                self.wifi_marker_pub.publish(markerArray)
            elif reading_type == "gas":
                self.gas_marker_pub.publish(markerArray)


def main():
    rospy.init_node("raw_reading_visualizer")

    wv = RawReadingVisualizer()

    rospy.spin()


if __name__ == "__main__":
    main()
