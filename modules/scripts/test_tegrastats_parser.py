#!/usr/bin/env python3

import unittest
from publish_tegrastats import TegrastatsParser


class TestParser(unittest.TestCase):
    def setUp(self):
        self._parser = TegrastatsParser()

    def test_single_line(self):
        tegrastats = "RAM 4311/15698MB (lfb 35x4MB) SWAP 0/23849MB (cached 0MB) CPU [11%@1111,5%@1111,0%@1112,0%@1111,2%@1112,0%@1112,2%@1112,0%@1112] EMC_FREQ 0% GR3D_FREQ 0% AO@31.5C GPU@31C Tboard@32C Tdiode@32.5C AUX@31.5C CPU@32.5C thermal@31.65C PMIC@100C GPU 284/284 CPU 1044/1044 SOC 1517/1517 CV 0/0 VDDRQ 94/94 SYS5V 2813/2813"
        stats = self._parser.parse(tegrastats)

    def test_multiple_lines(self):
        tegrastats = """RAM 4311/15698MB (lfb 35x4MB) SWAP 0/23849MB (cached 0MB) CPU [11%@1111,5%@1111,0%@1112,0%@1111,2%@1112,0%@1112,2%@1112,0%@1112] EMC_FREQ 0% GR3D_FREQ 0% AO@31.5C GPU@31C Tboard@32C Tdiode@32.5C AUX@31.5C CPU@32.5C thermal@31.65C PMIC@100C GPU 284/284 CPU 1044/1044 SOC 1517/1517 CV 0/0 VDDRQ 94/94 SYS5V 2813/2813
RAM 4218/15698MB (lfb 117x4MB) SWAP 0/23849MB (cached 0MB) CPU [4%@1190,0%@1190,0%@1190,0%@1190,0%@1190,1%@1190,0%@1190,0%@1190] EMC_FREQ 0% GR3D_FREQ 0% AO@31C GPU@30.5C Tboard@32C Tdiode@32.5C AUX@31C CPU@32C thermal@31C PMIC@100C GPU 285/285 CPU 379/379 SOC 1138/1138 CV 0/0 VDDRQ 0/0 SYS5V 2652/2652
RAM 1411/15698MB (lfb 3377x4MB) SWAP 0/23849MB (cached 0MB) CPU [1%@1190,0%@1190,0%@1190,0%@1190,0%@1190,0%@1190,0%@1190,0%@1190] EMC_FREQ 0% GR3D_FREQ 0% AO@34C GPU@35.5C iwlwifi@41C Tboard@36C Tdiode@37.75C AUX@34C CPU@35.5C thermal@34.9C PMIC@100C GPU 0/0 CPU 357/357 SOC 1073/1073 CV 0/0 VDDRQ 0/0 SYS5V 3818/3818
RAM 1411/15698MB (lfb 3376x4MB) SWAP 0/23849MB (cached 0MB) CPU [2%@1190,0%@1190,0%@1190,0%@1190,0%@1190,0%@1190,0%@1190,0%@1190] EMC_FREQ 0% GR3D_FREQ 0% AO@34C GPU@35.5C iwlwifi@41C Tboard@36C Tdiode@38C AUX@34C CPU@35.5C thermal@35.1C PMIC@100C GPU 0/0 CPU 357/357 SOC 1073/1073 CV 0/0 VDDRQ 0/0 SYS5V 3818/3818"""

        for line in tegrastats.splitlines():
            stats = self._parser.parse(line)

    # def test_another_broken_line(self):
    #     tegrastats = "RAM 5103/15700MB (lfb 1x256kB) SWAP 0/16000MB (cached 0MB) CPU [63%@2265,60%@2265,62%@2265,60%@2265,100%@2265,54%@2265,59%@2265,100%@2265] EMC_FREQ 8%@2133 GR3D_FREQ 4%@1377 APE 150 MTS fg 2% bg 7% AO@38C GPU@38.5C Tboard@37C Tdiode@38.75C AUX@38C CPU@42.5C thermal@39.5C PMIC@100C GPU 2061/1975 CPU 9456/8712 SOC 3277/3199 CV 0/0 VDDRQ 655/627 SYS5V 3651/3613"

    #     stats = self._parser.parse(tegrastats)


if __name__ == "__main__":
    unittest.main()
