#!/usr/bin/env python3

"""
Monitors the lidar_renderer and if it doesn't hear from it in
a while, it publishes a fake lidar image
in order to ensure the artifact localization pipeline continues
operating.


Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""

import rospy
from sensor_msgs.msg import Image
from objdet_msgs.msg import DetectionArray
from functools import partial
from cv_bridge import CvBridge
import cv2
import numpy as np

import pdb


def lidar_img_cb(msg, i):
    """
    Keep track of when we heard from the lidar renderer last
    """
    global lidar_img_times, default_imgs

    bridge = CvBridge()
    cv_img = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")
    # dont' count this as a most recent publish
    # if its the default depth image
    if (cv_img == default_imgs[i]).min():
        return

    lidar_img_times[i] = rospy.get_time()


def detection_cb(msg, args):
    """
    Publishes a fake lidar depth image if we havent heard from the lidar_renderer
    in a while
    """
    global lidar_img_times, wait_time, default_imgs

    i, lidar_pub = args

    if rospy.get_time() - lidar_img_times[i] > wait_time:
        rospy.logwarn_throttle(
            1, "Lidar renderer may be down, publishing fake lidar depth image"
        )
        bridge = CvBridge()
        lidar_depth_img = bridge.cv2_to_imgmsg(
            default_imgs[i], encoding="passthrough"
        )
        lidar_depth_img.header = msg.header

        lidar_pub.publish(lidar_depth_img)


def rs_depth_cb(msg, i):
    """
    Should be called only once. This is the get the size of the lidar
    depth image, which must match the realsense depth image exactly
    to be used in the depth combiner
    """
    global default_imgs, rs_depth_img_subs
    bridge = CvBridge()
    cv_img = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")
    # np.ones so it (hopefully) doesnt get caught up in a min_dist issue
    # also will probably be more accurate than just 0 as default
    default_imgs[i] = np.ones((cv_img.shape[0], cv_img.shape[1]), np.uint16)
    rs_depth_img_subs[i].unregister()


if __name__ == "__main__":

    rospy.init_node("fake_lidar_pub")
    rate = rospy.Rate(1)

    global lidar_img_times, wait_time, camera_topics, default_imgs, rs_depth_img_size
    global rs_depth_sub, rs_depth_img_subs
    lidar_img_times, default_imgs, rs_depth_img_subs = [], [], []
    lidar_img_subs, detection_subs = [], []

    wait_time = rospy.get_param("/fake_lidar_pub/wait_time")
    camera_topics = rospy.get_param("/fake_lidar_pub/camera_topics")

    for i, camera_topic in enumerate(camera_topics):
        lidar_img_subs.append(
            rospy.Subscriber(
                camera_topic + "/color/aligned_lidar/image",
                Image,
                lidar_img_cb,
                i,
            )
        )

        lidar_img_times.append(
            rospy.get_time()
        )  # the last time we hear from various topics

        detection_subs.append(
            rospy.Subscriber(
                camera_topic + "/color/image/detections",
                DetectionArray,
                detection_cb,
                (
                    i,
                    rospy.Publisher(
                        camera_topic + "/color/aligned_lidar/image",
                        Image,
                        queue_size=10,
                    ),
                ),
            )
        )
        # default size, should adapt based on first realsense depth image received
        # np.ones so it (hopefully) doesnt get caught up in a min_dist issue
        # also will probably be more accurate than just 0 as default
        default_imgs.append(np.ones((360, 640), np.uint16))
        rs_depth_img_subs.append(
            rospy.Subscriber(
                camera_topic + "/aligned_depth_to_color/image",
                Image,
                rs_depth_cb,
                i,
            )
        )

    while not rospy.is_shutdown():
        rate.sleep()
