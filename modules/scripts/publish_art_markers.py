#!/usr/bin/env python3

import copy
import functools
import rospy
from visualization_msgs.msg import Marker, MarkerArray
from objdet_msgs.msg import ArtifactLocalizationArray
from common import ClassLabels as CL


class ArtifactMarkerPublisher(object):
    # https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/
    colors = [
        (60, 180, 75),
        (255, 225, 25),
        (0, 130, 200),
        (230, 25, 75),
        (245, 130, 48),
        (145, 30, 180),
        (70, 240, 240),
        (240, 50, 230),
        (210, 245, 60),
        (250, 190, 190),
        (0, 128, 128),
        (230, 190, 255),
        (170, 110, 40),
        (255, 250, 200),
        (128, 0, 0),
        (170, 255, 195),
        (128, 128, 0),
        (255, 215, 180),
        (0, 0, 128),
        (128, 128, 128),
        (255, 255, 255),
        (0, 0, 0),
    ]

    def __init__(self, topics):
        self._topics = topics
        self._topic_subs = []
        self._marker_pubs = []
        for i, topic in enumerate(topics):
            self._topic_subs.append(
                rospy.Subscriber(
                    topic,
                    ArtifactLocalizationArray,
                    functools.partial(self._artifact_cb, i),
                    queue_size=10,
                )
            )
            self._marker_pubs.append(
                rospy.Publisher(topic + "/markers", MarkerArray, queue_size=10)
            )

        self._artifacts = [dict() for i in range(len(topics))]

        self.class_labels = CL.ClassLabels()

    # Update the current list of artifacts
    def _artifact_cb(self, i, msg):
        invalids = []
        for artifact in msg.localizations:
            if not artifact.valid:
                try:
                    self._artifacts[i].pop(artifact.report_id)
                    invalids.append(artifact.report_id)
                except KeyError:
                    rospy.logwarn(
                        "Tried to invalidate artifact %d but it didn't exist"
                        % artifact.report_id
                    )
                continue

            self._artifacts[i][artifact.report_id] = artifact

        markers = MarkerArray()
        texts = MarkerArray()
        shadow_texts = MarkerArray()

        # Remove the invalid markers
        for invalid in invalids:
            remove_marker = Marker()
            remove_marker.ns = self._topics[i]
            remove_marker.id = invalid
            remove_marker.action = Marker.DELETE
            markers.markers.append(remove_marker)

            remove_text = Marker()
            remove_text.ns = remove_marker.ns + "_text"
            remove_text.id = invalid
            remove_text.action = Marker.DELETE
            texts.markers.append(remove_text)

            remove_shadow_text = Marker()
            remove_shadow_text.ns = remove_text.ns + "_shadow"
            remove_shadow_text.id = invalid
            remove_shadow_text.action = Marker.DELETE
            shadow_texts.markers.append(remove_shadow_text)

        # Convert artifacts into markers
        for report_id, artifact in self._artifacts[i].items():
            marker = Marker()
            marker.header.frame_id = msg.header.frame_id
            marker.header.stamp = rospy.Time()  # Display forever
            marker.ns = self._topics[i]
            marker.id = report_id
            # marker.type = Marker.CUBE
            marker.type = 1 if i < 2 else 2
            marker.action = Marker.ADD
            marker.scale.x = 0.5 if i != 3 else 0.55
            marker.scale.y = 0.5 if i != 3 else 0.55
            marker.scale.z = 0.5 if i != 3 else 0.55
            marker.pose.position.x = artifact.x
            marker.pose.position.y = artifact.y
            marker.pose.position.z = artifact.z
            marker.color.r = self.colors[i][0] / 255.0
            marker.color.g = self.colors[i][1] / 255.0
            marker.color.b = self.colors[i][2] / 255.0
            # marker.color.a = 0.5
            marker.color.a = 0.8

            text = Marker()
            text.header = marker.header
            text.ns = marker.ns + "_text"
            text.id = marker.id
            text.type = Marker.TEXT_VIEW_FACING
            if artifact.class_id in self.class_labels.id_to_txt:
                text.text = self.class_labels.id_to_txt[artifact.class_id]
            else:
                print(artifact.class_id)
                text.text = str(artifact.class_id)
            text.color = marker.color
            text.scale.z = 0.4
            text.pose.position.x = marker.pose.position.x
            text.pose.position.y = marker.pose.position.y
            text.pose.position.z = marker.pose.position.z + 0.5

            shadow_text = copy.deepcopy(text)
            shadow_text.ns += "_shadow"
            shadow_text.scale.z = 0.4
            shadow_text.color.r = 1.0
            shadow_text.color.g = 1.0
            shadow_text.color.b = 1.0

            markers.markers.append(marker)
            texts.markers.append(text)
            shadow_texts.markers.append(shadow_text)

        self._marker_pubs[i].publish(markers)
        self._marker_pubs[i].publish(texts)
        self._marker_pubs[i].publish(shadow_texts)


def main():
    rospy.init_node("art_markers_publisher")

    topics = rospy.get_param("~topics", [])
    if topics == []:
        rospy.logwarn("No topics specified for artifact marker publisher!")

    pub = ArtifactMarkerPublisher(topics)

    rospy.spin()


if __name__ == "__main__":
    main()
