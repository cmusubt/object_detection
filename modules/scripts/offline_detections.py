#!/usr/bin/env python3

"""
 Publish with appropriate image size


Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""
import rospy
from sensor_msgs.msg import Image
from objdet_msgs.msg import DetectionArray

def image_cb(msg, i):
    """
    Publish with appropriate image size
    """

    dets = DetectionArray()
    dets.header.stamp = msg.header.stamp
    pubs[i].publish(dets)

if __name__ == "__main__":
    global adjusted_pub, subs, pubs
    rospy.init_node("fake_detections")
    camera_topics = rospy.get_param("/spot_fake_detections/camera_topics")

    subs, pubs = [], []

    for i, camera_topic in enumerate(camera_topics):
        sub = rospy.Subscriber(camera_topic+"/color/image", Image, image_cb, i)
        pub = rospy.Publisher(camera_topic+"/color/image/detections", DetectionArray, queue_size=10)
        subs.append(sub)
        pubs.append(pub)

    while not rospy.is_shutdown():
        rospy.spin()
