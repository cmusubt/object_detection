#!/usr/bin/env python3

import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import time
import os
import numpy as np
import pdb

class image_converter:

  def __init__(self, exp_time):
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/camera/aligned_lidar/image", Image, self.lidar_callback)
    self.rgb_sub = rospy.Subscriber("/camera/image_raw", Image, self.rgb_callback)
    self.exp_time = exp_time # time experiment was run
    self.exp_dir = "/home/subt-nuc-02/Downloads/lidar_rend_experiment_" + exp_time + "/"
    if not os.path.exists(self.exp_dir):
        os.makedirs(self.exp_dir)
    self.rgb_img, self.lidar_img = None, None
    self.frame_cnt = 0

  def lidar_callback(self, data):
    cv_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
    cv_image = (cv_image/float(cv_image.max()+1E-5)) * 255# still in cm format
    self.lidar_img = cv2.cvtColor(cv_image.astype('uint8'), cv2.COLOR_GRAY2BGR)

    if self.rgb_img is not None:

        # put lidar and rgb images next to each other and draw bboxes randomly to 
        # qualitatively look at correspondence
        circle_radius = 50
        color_green = (0,255,0)
        color_red = (255,0,0)
        circle_thickness = 10

        cv2.rectangle(self.rgb_img, (300,250), (400,400), color_green, thickness=circle_thickness)
        cv2.rectangle(self.lidar_img, (300,250), (400,400), color_green, thickness=circle_thickness)

        cv2.rectangle(self.rgb_img, (550,300), (620,450), color_red, thickness=circle_thickness)
        cv2.rectangle(self.lidar_img, (550,300), (620,450), color_red, thickness=circle_thickness)

        # cv2.circle(self.rgb_img, (200, 200), circle_radius, circle_color, thickness=circle_thickness)
        # cv2.circle(self.lidar_img, (200, 200), circle_radius, circle_color, thickness=circle_thickness)

        # cv2.circle(self.rgb_img, (500, 500), circle_radius, circle_color, thickness=circle_thickness)
        # cv2.circle(self.lidar_img, (500, 500), circle_radius, circle_color, thickness=circle_thickness)
        
        combined_img = np.concatenate((self.rgb_img, self.lidar_img), axis=1)
        cv2.imwrite(self.exp_dir + str(self.frame_cnt) + ".png", combined_img)
        self.frame_cnt += 1

        # print(self.lidar_img.shape)
        # cv2.imwrite(self.exp_dir + str(time.time()) + "color.png", self.rgb_img)
        # cv2.imwrite(self.exp_dir + str(time.time()) + "lidar.png", self.lidar_img)

  def rgb_callback(self, data):
    cv_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
    self.rgb_img =  cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
    cv2.imwrite("/home/subt-nuc-02/Downloads/thing.png", self.rgb_img)


def main(args):
  ic = image_converter(str(time.time()))
  rospy.init_node('image_converter', anonymous=True)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)