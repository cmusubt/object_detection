#!/bin/sh

if [ -z "$1" ]; then
    echo "You need to specify an input directory: $0 [directory]"
    exit
fi

playlist=$(find $1 -iname "*.mp3")
mplayer -loop 0 -shuffle $playlist
