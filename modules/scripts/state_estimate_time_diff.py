#!/usr/bin/env python3

import rospy
import rostopic
import functools
from std_msgs.msg import Float32
from sensor_msgs.msg import Image
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from objdet_msgs.msg import DetectionArray
from sensor_msgs.msg import PointCloud2


class StateEstimateTimeDiff(object):
    def __init__(self):

        self.subs = []
        for topic, topic_type in [
            ("/integrated_to_init", Odometry),
            ("/integrated_to_map", Odometry),
            ("/key_pose_to_map", Odometry),
            ("/rs_front/color/image", Image),
            ("/rs_front/color/image/detections", DetectionArray),
            ("/velodyne_points", PointCloud2),
            ("/velodyne_cloud_key_pose", PointCloud2),
            ("/velodyne_cloud_registered", PointCloud2),
            ("/imu/data", Imu),
        ]:

            pub = rospy.Publisher(topic + "_diff", Float32, queue_size=1)
            self.subs.append(
                rospy.Subscriber(
                    topic,
                    topic_type,
                    functools.partial(self.time_diff_cb, pub, topic),
                )
            )

    def time_diff_cb(self, pub, topic, msg):
        current_time = rospy.Time.now()
        msg_time = msg.header.stamp
        diff = current_time - msg_time

        pub.publish(diff.to_sec())

        #  print("[%s] Current time: %i.%i, Msg time: %i.%i, Diff: %i.%i" % (
        #      topic, msg_time.secs, msg_time.nsecs, current_time.secs,
        #      current_time.nsecs, diff.secs, diff.nsecs))


def main():
    rospy.init_node("state_estimate_time_diff")

    node = StateEstimateTimeDiff()
    rospy.spin()


if __name__ == "__main__":
    main()
