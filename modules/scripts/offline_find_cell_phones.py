#!/usr/bin/env python3

"""
Goes through bag files and finds where cell phones
were detected while scanning wifi


Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""
import rosbag
import glob


if __name__ == "__main__":
    top_level_folder = "/media/dstest/XavierSSD500/2019-08-18"

    for bagfile in sorted(glob.glob(top_level_folder + "/*.bag")):
        bag = rosbag.Bag(bagfile)
        found_phone = False
        for topic, msg, t in bag.read_messages(topics=["/wifi_info"]):
            for reading in msg.readings:
                if "Phone" in reading.name and found_phone == False:
                    print("phone in ", bagfile)
                    found_phone = True
            # print msg
        bag.close()
