#!/usr/bin/env python3
import rospy
import tf2_ros
from objdet_msgs.msg import WirelessSignalReadingArray


class SignalLocalizer:
    def __init__(self):
        self._tf_buffer = tf2_ros.Buffer()
        self._tf_listener = tf2_ros.TransformListener(self._tf_buffer)

        self.bluetooth_sub = rospy.Subscriber(
            "bluetooth_rssi",
            WirelessSignalReadingArray,
            self._bluetooth_cb,
            queue_size=10,
        )

    def _bluetooth_cb(self, msg):
        rospy.loginfo("got bluetooth")
        tf = self._tf_buffer.lookup_transform(
            msg.header.frame_id, "map", msg.header.stamp
        )
        rospy.loginfo("Map transform: ", tf)


def main():
    rospy.init_node("signal_localizer")

    node = SignalLocalizer()
    rospy.spin()


if __name__ == "__main__":
    main()
