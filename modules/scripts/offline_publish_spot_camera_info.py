#!/usr/bin/env python3

"""
 Publish with appropriate image size


Contact: Bob DeBortoli (debortor@oregonstate.edu)
Property of Team Explorer 
"""
import rospy
import time
from sensor_msgs.msg import CameraInfo


def camera_info_cb(msg):
    """
    Publish with appropriate image size
    """

    # adj_msg = CameraInfo()
    # adj_msg.header.seq = msg.header.seq
    # adj_msg.header.stamp = msg.header.stamp
    # adj_msg.header.frame_id = "camera/camera_link"
    # adj_msg.height = 770
    # adj_msg.width = 1028
    adj_msg = msg
    adj_msg.header.frame_id = "camera"
    adjusted_pub.publish(adj_msg)

if __name__ == "__main__":
    rospy.init_node("fake_camera_info")
    #adjusted_pub = rospy.Publisher("/camera/camera_info_adj", CameraInfo, queue_size=10)
    adjusted_pub = rospy.Publisher("/spot_front/color/camera_info", CameraInfo, queue_size=10)
    orig_camera_info_pub = rospy.Subscriber("/camera/camera_info", CameraInfo, camera_info_cb)
    while not rospy.is_shutdown():
        adj_msg = CameraInfo()
        adj_msg.header.frame_id = "spot_front/camera_link"
        adj_msg.header.stamp = rospy.Time.now()
        adj_msg.width = 624
        adj_msg.height = 520
        adjusted_pub.publish(adj_msg)
        print("PubCAM")
        time.sleep(1)
        #rospy.spin()
   
