#!/usr/bin/env python3

import unittest
from chrony_status import ChronyStatusNode


class TestOutputProcessing(unittest.TestCase):
    def test_synced(self):
        s1 = """\
Reference ID    : C0A8010A (nuc)
Stratum         : 11
Ref time (UTC)  : Thu Mar 07 05:17:19 2019
System time     : 0.000000000 seconds fast of NTP time
Last offset     : +0.812181532 seconds
RMS offset      : 0.812181532 seconds
Frequency       : 54.439 ppm slow
Residual freq   : +0.800 ppm
Skew            : 0.504 ppm
Root delay      : 0.000259584 seconds
Root dispersion : 0.000095983 seconds
Update interval : 0.0 seconds
Leap status     : Normal"""

        s2 = """\
Reference ID    : C0A8010A (nuc)
Stratum         : 11
Ref time (UTC)  : Thu Mar 07 04:43:42 2019
System time     : 0.000015314 seconds slow of NTP time
Last offset     : -0.000032613 seconds
RMS offset      : 0.000032613 seconds
Frequency       : 53.420 ppm slow
Residual freq   : -0.197 ppm
Skew            : 0.604 ppm
Root delay      : 0.000506965 seconds
Root dispersion : 0.000019288 seconds
Update interval : 64.0 seconds
Leap status     : Normal"""

        strings = [s1, s2]
        for s in strings:
            self.assertTrue(ChronyStatusNode.process_chrony_tracking_output(s))

    def test_not_synced(self):
        s1 = """\
Reference ID    : 00000000 ()
Stratum         : 0
Ref time (UTC)  : Thu Jan 01 00:00:00 1970
System time     : 0.000000001 seconds slow of NTP time
Last offset     : +0.000000000 seconds
RMS offset      : 0.000000000 seconds
Frequency       : 54.439 ppm slow
Residual freq   : +0.000 ppm
Skew            : 0.000 ppm
Root delay      : 1.000000000 seconds
Root dispersion : 1.000000000 seconds
Update interval : 0.0 seconds
Leap status     : Not synchronised"""

        s2 = """\
Reference ID    : 00000000 ()
Stratum         : 0
Ref time (UTC)  : Thu Jan 01 00:00:00 1970
System time     : 0.000000006 seconds slow of NTP time
Last offset     : +0.000000000 seconds
RMS offset      : 0.000000000 seconds
Frequency       : 53.411 ppm slow
Residual freq   : +0.000 ppm
Skew            : 0.000 ppm
Root delay      : 1.000000000 seconds
Root dispersion : 1.000000000 seconds
Update interval : 0.0 seconds
Leap status     : Not synchronised"""

        strings = [s1, s2]
        for s in strings:
            self.assertFalse(ChronyStatusNode.process_chrony_tracking_output(s))

    def test_successful_tracking(self):
        self.assertTrue(
            ChronyStatusNode.process_chrony_tracking_output(
                ChronyStatusNode.get_chrony_tracking_output()
            )
        )


if __name__ == "__main__":
    unittest.main()
