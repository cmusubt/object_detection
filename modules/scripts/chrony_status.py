#!/usr/bin/env python3

import rospy
import subprocess
import std_msgs


class ChronyStatusNode(object):
    def __init__(self):
        self.timer = rospy.Timer(rospy.Duration(1), self.timer_callback)
        self.status_pub = rospy.Publisher(
            "chrony_status", std_msgs.msg.Bool, queue_size=1
        )

    @staticmethod
    def get_chrony_tracking_output():
        try:
            return subprocess.check_output(["chronyc", "tracking"]).decode(
                "ascii"
            )
        except subprocess.CalledProcessError as e:
            rospy.logwarn("Chrony error: %s", e)

        return None

    @staticmethod
    def process_chrony_tracking_output(output):
        lines = output.splitlines()
        lines = {
            tuple(map(lambda x: str(x).strip(), line.split(":")))
            for line in lines
        }
        lines = {s[0]: s[1] for s in lines}

        stratum = int(lines["Stratum"])
        return (stratum > 0) and lines["Leap status"] == "Normal"

    @staticmethod
    def force_chrony_sync():
        subprocess.check_call(["sudo", "chronyc", "makestep"])

    def timer_callback(self, event):
        output = ChronyStatusNode.get_chrony_tracking_output()
        if output is not None:
            status = ChronyStatusNode.process_chrony_tracking_output(output)
        else:
            status = False

        rospy.loginfo("Chrony status: %s", status)

        msg = std_msgs.msg.Bool()
        msg.data = status
        self.status_pub.publish(msg)

        #  if status == False:
        #      try:
        #          rospy.loginfo("Attempting to force sync")
        #          subprocess.check_call(["ping", "-c", "1", "nuc"])
        #          ChronyStatusNode.force_chrony_sync()
        #      except subprocess.CalledProcessError as e:
        #          rospy.logwarn("Unable to force sync, nuc is likely not on!")


def main():
    rospy.init_node("chrony_status")

    node = ChronyStatusNode()
    rospy.spin()


if __name__ == "__main__":
    main()
