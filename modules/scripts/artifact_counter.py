'''
Count how many distinct artifact are there published by objdet_mapper/object_ledger
'''

from objdet_msgs.msg import ArtifactLocalization, ArtifactLocalizationArray
import rospy

class_id = (1, 3, 4, 5, 6, 7, 8, 9)


class artifactCounter:
    def __init__(self, topic_name):

        self.count = 0
        self.sub = rospy.Subscriber(
            topic_name,
            ArtifactLocalizationArray,
            self.artifact_cb
        )
        self.topic_name = topic_name
        self.artifacts = {}
        self.artifacts = {}

    def __str__(self):
        return self.topic_name + ": " + str(len(self.artifacts)) + '/' + str(self.count)

    def artifact_cb(self, msg):
        for l in msg.localizations:
            if l.class_id not in class_id:
                continue
            if l.report_id not in self.artifacts:
                self.artifacts[l.report_id] = l
                # print("Adding new artifacts! Label: " + str(l.class_id))
            else:
                if not self.artifacts[l.report_id].class_id == l.class_id:
                    print(self.topic_name, "\nChanging report type from", self.artifacts[l.report_id].class_id,
                          "to", l.class_id, "For report ID", l.report_id)
                self.artifacts[l.report_id] = l
                # print("Renew artifact with label " + str(l.class_id))
            self.count += 1


if __name__ == "__main__":
    rospy.init_node("artifact_counter")
    rate = rospy.Rate(0.1)
    pub = [artifactCounter("/artifact_localizations"),
           artifactCounter("/previous_artifact_localizations"),
           artifactCounter("/objdet_mapper/artifact_localizations")]
    while not rospy.is_shutdown():
        print('\n'.join([str(i) for i in pub]))
        rate.sleep()
