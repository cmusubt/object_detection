#include "modules/point_cloud_renderer.h"
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/TransformStamped.h>
#include <gtest/gtest.h>
#include <pcl/io/pcd_io.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/Image.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "modules/point_cloud_renderer_cuda.h"

namespace object_detection {

const std::vector<std::pair<std::string, std::string>> bags = {
    {"example_1.bag", "map1.pcd"}, {"example_2.bag", "map2.pcd"}};

class DepthRendererTest
    : public ::testing::TestWithParam<std::pair<std::string, std::string>> {
 protected:
  using PointType = pcl::PointXYZ;
  using PointCloud = pcl::PointCloud<PointType>;
  using PointCloudPtr = PointCloud::Ptr;

  const std::string color_image_topic = "/rs_front/color/image";
  const std::string color_info_topic = "/rs_front/color/camera_info";
  const std::string aligned_depth_image_topic =
      "/rs_front/aligned_depth_to_color/image";
  const std::string aligned_depth_info_topic =
      "/rs_front/aligned_depth_to_color/camera_info";
  const std::string map_to_camera_tf_topic = "/map_to_camera_tf";

  void SetUp() override {
    std::string current_file(__FILE__);
    auto base_dir = current_file.substr(0, current_file.rfind("/") - 4);
    data_directory = base_dir + "data/";
  }

  PointType get_next_point() {
    PointType p;
    p.x = 0;
    p.y = 0;
    p.z = current_point;
    // Leave other fields, if any, uninitialized.

    current_point += 1;
    return p;
  }

  PointCloudPtr get_point_cloud_of_size(size_t size) {
    PointCloudPtr cloud_ptr(new PointCloud);
    for (size_t i = 0; i < size; ++i) {
      cloud_ptr->insert(cloud_ptr->end(), get_next_point());
    }

    return cloud_ptr;
  }

  void load_example(const std::string& bag_file, const std::string& map_file) {
    rosbag::Bag bag;
    bag.open(data_directory + bag_file);

    for (const rosbag::MessageInstance m : rosbag::View(bag)) {
      const auto& topic = m.getTopic();
      if (topic == color_image_topic) {
        color = m.instantiate<sensor_msgs::Image>();
      } else if (topic == color_info_topic) {
        color_info = m.instantiate<sensor_msgs::CameraInfo>();
      } else if (topic == aligned_depth_image_topic) {
        aligned_depth_to_color = m.instantiate<sensor_msgs::Image>();
      } else if (topic == aligned_depth_info_topic) {
        aligned_depth_to_color_info = m.instantiate<sensor_msgs::CameraInfo>();
      } else if (topic == map_to_camera_tf_topic) {
        map_to_camera_msg = m.instantiate<geometry_msgs::TransformStamped>();
      } else {
        FAIL() << "Unexpected value in bag file";
      }
    }

    ASSERT_NE(color, nullptr) << "Can't load color image";
    ASSERT_NE(color_info, nullptr) << "Can't load color info";
    ASSERT_NE(aligned_depth_to_color, nullptr) << "Can't load aligned depth";
    ASSERT_NE(aligned_depth_to_color_info, nullptr)
        << "Can't load aligned info";
    ASSERT_NE(map_to_camera_msg, nullptr) << "Can't load map_to_camera tf";

    color_cv = cv_bridge::toCvShare(color, "bgr8");
    aligned_depth_to_color_cv = cv_bridge::toCvShare(aligned_depth_to_color);

    bag.close();

    cloud_ptr.reset(new PointCloud);
    ASSERT_NE(
        pcl::io::loadPCDFile<PointType>(data_directory + map_file, *cloud_ptr),
        -1)
        << "Can't load map";
  }

  // Tests that the lidar image roughly looks like the depth image.
  //
  // Checks to make sure that any depth points within 2.5m are matched by the
  // points in the lidar image, to an error of 10% or less. At least 80% of the
  // points must be within this error bound.
  void compare_lidar_to_depth(const cv::Mat& depth_image,
                              const cv::Mat& lidar_image) {
    ASSERT_EQ(depth_image.rows, lidar_image.rows);
    ASSERT_EQ(depth_image.cols, lidar_image.cols);

    const auto DISTANCE_BOUND = 2500;  // 2.5m in mm, don't trust realsense much
    const auto ERROR_BOUND = .1f;
    const auto POINTS_BOUND = .8f;

    int close_points = 0;
    int within_bounds = 0;

    for (int i = 0; i < lidar_image.rows; ++i) {
      for (int j = 0; j < lidar_image.cols; ++j) {
        const auto depth_value =
            static_cast<int>(depth_image.at<uint16_t>(i, j));
        const auto lidar_value =
            static_cast<int>(lidar_image.at<uint16_t>(i, j));
        if (depth_value == 0 || lidar_value == 0) {
          continue;
        }

        if (depth_value > DISTANCE_BOUND) {
          continue;
        }

        close_points++;

        if (std::abs(lidar_value - depth_value) <= ERROR_BOUND * depth_value) {
          within_bounds++;
        }
      }
    }

    EXPECT_GE(within_bounds, POINTS_BOUND * close_points)
        << "Only " << within_bounds << " points out of " << close_points
        << " ( " << static_cast<float>(within_bounds) / close_points
        << "% ) were good enough.";
  }

  float current_point = 0;
  std::string data_directory;

  sensor_msgs::Image::ConstPtr color;
  cv_bridge::CvImageConstPtr color_cv;
  sensor_msgs::CameraInfo::ConstPtr color_info;
  sensor_msgs::Image::ConstPtr aligned_depth_to_color;
  cv_bridge::CvImageConstPtr aligned_depth_to_color_cv;
  sensor_msgs::CameraInfo::ConstPtr aligned_depth_to_color_info;
  geometry_msgs::TransformStamped::ConstPtr map_to_camera_msg;
  PointCloudPtr cloud_ptr;
};

TEST_F(DepthRendererTest, Construct) {
  PointCloudRenderer depth_renderer;
  cloud_ptr = get_point_cloud_of_size(100);
  depth_renderer.set_cloud(cloud_ptr);

  sensor_msgs::CameraInfo info;
  info.width = 640;
  info.height = 360;
  info.K = {460.0f, 0.0f, 324.0f, 0.0f, 461.0f, 181.0f, 0.0f, 0.0f, 1.0f};

  tf2::Transform tf;
  tf.setIdentity();

  auto depth_image = depth_renderer.render(info, tf);
  EXPECT_EQ(depth_image.rows, info.height);
  EXPECT_EQ(depth_image.cols, info.width);
}

TEST_F(DepthRendererTest, Benchmark) {
  load_example("example_1.bag", "map1.pcd");

  PointCloudRenderer depth_renderer;
  depth_renderer.set_cloud(cloud_ptr);

  tf2::Transform map_to_camera_tf;
  tf2::fromMsg(map_to_camera_msg->transform, map_to_camera_tf);

  for (int i = 0; i < 100; ++i) {
    auto depth_image =
        depth_renderer.render(*color_info, map_to_camera_tf.inverse(), 4);
    EXPECT_EQ(depth_image.rows, 360);
    EXPECT_EQ(depth_image.cols, 640);
  }
}

TEST_P(DepthRendererTest, RenderBag) {
  const auto pair = GetParam();
  load_example(pair.first, pair.second);

  PointCloudRenderer depth_renderer;
  depth_renderer.set_cloud(cloud_ptr);

  // TODO(vasua): Regenerate data with inverted transform
  // This data was taken with the transform inverted, which doesn't match what
  // the renderer expects, so it gets inverted here.
  tf2::Transform map_to_camera_tf;
  tf2::fromMsg(map_to_camera_msg->transform, map_to_camera_tf);
  auto depth_image =
      depth_renderer.render(*color_info, map_to_camera_tf.inverse(), 4);

  compare_lidar_to_depth(aligned_depth_to_color_cv->image, depth_image);

  // Can uncomment this to display the images when not headless
  // cv::imshow("Reference Color", color_cv->image);
  // cv::imshow("Reference Depth", aligned_depth_to_color_cv->image);
  // cv::imshow("CPU Lidar Depth", depth_image);
  // cv::waitKey(0);
}

INSTANTIATE_TEST_CASE_P(CpuBags, DepthRendererTest, ::testing::ValuesIn(bags));

// Forward declare these functions because we can't include the CUDA headers
void* cuda_malloc(const size_t n);
void cuda_memcpy_to_device(void* dst, const void* src, const size_t n);
void cuda_free(void* ptr);

class CudaDepthRendererTest : public DepthRendererTest {
 protected:
  void TearDown() override {
    for (const auto& pair : cuda_clouds) {
      cuda_free(pair.first);
    }
  }

  void split_clouds(const int num_splits) {
    // Slight duplicate of the code in rolling_cloud.h, but I don't want to
    // include that file for sake of code separation.
    const size_t split_size = (cloud_ptr->size() / num_splits) + 1;
    for (int i = 0; i < num_splits; ++i) {
      const auto start = i * split_size;
      const auto end = std::min((i + 1) * split_size, cloud_ptr->size());
      const auto num_elements = end - start;
      cuda_clouds.emplace_back(cuda_malloc(num_elements * sizeof(PointType)),
                               num_elements);
      cuda_memcpy_to_device(cuda_clouds.back().first,
                            cloud_ptr->points.data() + start,
                            num_elements * sizeof(PointType));
    }
  }

  void load_cuda_example(const std::string& bag_file,
                         const std::string& map_file, const int num_splits) {
    load_example(bag_file, map_file);
    split_clouds(num_splits);
  }

  std::deque<std::pair<void*, size_t>> cuda_clouds;
};

TEST_F(CudaDepthRendererTest, Construct) {
  PointCloudRendererCuda depth_renderer;
  cloud_ptr = get_point_cloud_of_size(100);
  split_clouds(10);

  depth_renderer.set_cloud(&cuda_clouds);

  sensor_msgs::CameraInfo info;
  info.width = 640;
  info.height = 360;
  info.K = {460.0f, 0.0f, 324.0f, 0.0f, 461.0f, 181.0f, 0.0f, 0.0f, 1.0f};

  tf2::Transform tf;
  tf.setIdentity();

  auto depth_image = depth_renderer.render(info, tf);
  EXPECT_EQ(depth_image.rows, info.height);
  EXPECT_EQ(depth_image.cols, info.width);
}

TEST_F(CudaDepthRendererTest, Benchmark) {
  load_cuda_example("example_1.bag", "map1.pcd", 10);

  PointCloudRendererCuda depth_renderer;
  depth_renderer.set_cloud(&cuda_clouds);

  tf2::Transform map_to_camera_tf;
  tf2::fromMsg(map_to_camera_msg->transform, map_to_camera_tf);

  for (int i = 0; i < 100; ++i) {
    auto depth_image =
        depth_renderer.render(*color_info, map_to_camera_tf.inverse(), 4);
    EXPECT_EQ(depth_image.rows, color_info->height);
    EXPECT_EQ(depth_image.cols, color_info->width);
  }
}

TEST_P(CudaDepthRendererTest, RenderBag) {
  const auto pair = GetParam();
  load_cuda_example(pair.first, pair.second, 10);

  PointCloudRendererCuda depth_renderer;
  depth_renderer.set_cloud(&cuda_clouds);

  // TODO(vasua): Regenerate data with inverted transform
  // This data was taken with the transform inverted, which doesn't match what
  // the renderer expects, so it gets inverted here.
  tf2::Transform map_to_camera_tf;
  tf2::fromMsg(map_to_camera_msg->transform, map_to_camera_tf);
  auto depth_image =
      depth_renderer.render(*color_info, map_to_camera_tf.inverse(), 4);

  compare_lidar_to_depth(aligned_depth_to_color_cv->image, depth_image);

  // Can uncomment this to display the images when not headless
  // cv::imshow("Reference Color", color_cv->image);
  // cv::imshow("Reference Depth", aligned_depth_to_color_cv->image);
  // cv::imshow("CUDA Lidar Depth", depth_image);
  // cv::waitKey(0);
}

TEST_P(CudaDepthRendererTest, CompareToReference) {
  const auto pair = GetParam();
  load_cuda_example(pair.first, pair.second, 10);

  PointCloudRenderer cpu_renderer;
  cpu_renderer.set_cloud(cloud_ptr);

  PointCloudRendererCuda cuda_renderer;
  cuda_renderer.set_cloud(&cuda_clouds);

  // TODO: Regenerate data with inverted transform
  tf2::Transform map_to_camera_tf;
  tf2::fromMsg(map_to_camera_msg->transform, map_to_camera_tf);
  auto depth_image =
      cpu_renderer.render(*color_info, map_to_camera_tf.inverse(), 4);
  auto depth_image_cuda =
      cuda_renderer.render(*color_info, map_to_camera_tf.inverse(), 4);

  ASSERT_EQ(depth_image.rows, depth_image_cuda.rows);
  ASSERT_EQ(depth_image.cols, depth_image_cuda.cols);
  for (int i = 0; i < depth_image.rows; ++i) {
    for (int j = 0; j < depth_image.cols; ++j) {
      const auto ref_depth = static_cast<int>(depth_image.at<uint16_t>(i, j));
      const auto cuda_depth =
          static_cast<int>(depth_image_cuda.at<uint16_t>(i, j));
      EXPECT_LE(std::abs(ref_depth - cuda_depth), 1)
          << "Depth mismatch at (" << j << ", " << i << "), REF: " << ref_depth
          << ", CUDA: " << cuda_depth;
    }
  }

  // Can uncomment this to display the images when not headless
  // cv::imshow("Reference Color", color_cv->image);
  // cv::imshow("Reference Depth", aligned_depth_to_color_cv->image);
  // cv::imshow("CPU Lidar Depth", depth_image);
  // cv::imshow("CUDA Lidar Depth", depth_image_cuda);
  // cv::waitKey(0);
}

INSTANTIATE_TEST_CASE_P(CudaBags, CudaDepthRendererTest,
                        ::testing::ValuesIn(bags));

}  // namespace object_detection

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
