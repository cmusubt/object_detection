#include "modules/gas_localizer.h"
#include <gtest/gtest.h>
#include <iterator>

namespace object_detection {
namespace signal_solvers {

TEST(FindEqualRangesTest, NoRanges) {
  std::vector<int> values;
  auto equal_ranges = find_equal_ranges(values.begin(), values.end(), -1);
  EXPECT_EQ(equal_ranges.size(), 0);

  values = {0, 1, 2, 3, 4, 5};
  equal_ranges = find_equal_ranges(values.begin(), values.end(), -1);
  EXPECT_EQ(equal_ranges.size(), 0);
}

TEST(FindEqualRangesTest, MiddleRange) {
  std::vector<int> values = {0, 1, 2, 3, 3, 3, 4, 2, 2};
  const auto equal_ranges = find_equal_ranges(values.begin(), values.end(), 3);

  ASSERT_EQ(equal_ranges.size(), 1);
  EXPECT_EQ(std::distance(equal_ranges[0].first, equal_ranges[0].second), 3);
}

TEST(FindEqualRangesTest, MultipleRanges) {
  std::vector<int> values = {0, 0, 1, 0, 0, 0, 2, 3, 0, 4, 0, 0, 0, 0, 0};
  const auto equal_ranges = find_equal_ranges(values.begin(), values.end(), 0);
  ASSERT_EQ(equal_ranges.size(), 4);
  EXPECT_EQ(std::distance(equal_ranges[0].first, equal_ranges[0].second), 2);
  EXPECT_EQ(std::distance(equal_ranges[1].first, equal_ranges[1].second), 3);
  EXPECT_EQ(std::distance(equal_ranges[2].first, equal_ranges[2].second), 1);
  EXPECT_EQ(std::distance(equal_ranges[3].first, equal_ranges[3].second), 5);
}

auto make_readings(const std::vector<float>& raw_reading_values) {
  std::vector<ReadingFromPosition> readings;
  readings.resize(raw_reading_values.size());
  for (size_t i = 0; i < raw_reading_values.size(); ++i) {
    readings[i].robot_position = {0, 0, i};
    readings[i].raw_reading = raw_reading_values[i];
  }
  return readings;
}

TEST(FindArtifactTest, SinglePeakValue) {
  const std::vector<float> concentration_values = {1, 2, 3, 2, 1};
  const auto readings = make_readings(concentration_values);

  Eigen::Vector3f artifact;
  EXPECT_TRUE(find_artifact_max_rssi(readings, artifact));
  EXPECT_FLOAT_EQ(artifact[0], 0.0f);
  EXPECT_FLOAT_EQ(artifact[1], 0.0f);
  EXPECT_FLOAT_EQ(artifact[2], 2.0f);
}

TEST(FindArtifactTest, SingleLongPeakValue) {
  const std::vector<float> concentration_values = {1, 2, 3, 3, 3, 3, 3, 2, 1};
  const auto readings = make_readings(concentration_values);

  Eigen::Vector3f artifact;
  EXPECT_TRUE(find_artifact_max_rssi(readings, artifact));
  EXPECT_FLOAT_EQ(artifact[0], 0.0f);
  EXPECT_FLOAT_EQ(artifact[1], 0.0f);
  EXPECT_FLOAT_EQ(artifact[2], 4.0f);
}

}  // namespace signal_solvers
}  // namespace object_detection

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
