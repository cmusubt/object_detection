#include "modules/rolling_cloud.h"
#include <gtest/gtest.h>

namespace object_detection {

class FixedLagRollingCloudTest : public testing::Test {
 protected:
  using PointType = pcl::PointXYZRGBL;
  using PointCloud = pcl::PointCloud<PointType>;
  using PointCloudPtr = PointCloud::Ptr;

  float current_point = 0;
  PointType get_next_point() {
    PointType p;
    p.x = current_point;
    p.y = current_point;
    p.z = current_point;
    // Leave other fields, if any, uninitialized.

    current_point += 1;
    return p;
  }

  PointCloudPtr get_point_cloud_of_size(size_t size) {
    PointCloudPtr cloud_ptr(new PointCloud);
    for (size_t i = 0; i < size; ++i) {
      cloud_ptr->insert(cloud_ptr->end(), get_next_point());
    }

    return cloud_ptr;
  }
};

TEST_F(FixedLagRollingCloudTest, Construct) {
  FixedLagRollingCloud<PointType, CpuCloudManager<PointType>> rolling_cloud;
}

TEST_F(FixedLagRollingCloudTest, GetEmpty) {
  FixedLagRollingCloud<PointType, CpuCloudManager<PointType>> rolling_cloud;
  auto rolling_cloud_ptr = rolling_cloud.get();
  EXPECT_EQ(rolling_cloud_ptr->size(), 0);
}

TEST_F(FixedLagRollingCloudTest, AddSingle) {
  FixedLagRollingCloud<PointType, CpuCloudManager<PointType>> rolling_cloud;

  auto cloud_ptr = get_point_cloud_of_size(10);
  rolling_cloud.add(cloud_ptr);

  auto rolling_cloud_ptr = rolling_cloud.get();
  EXPECT_EQ(rolling_cloud_ptr->size(), 10);
}

TEST_F(FixedLagRollingCloudTest, AddMultiple) {
  FixedLagRollingCloud<PointType, CpuCloudManager<PointType>> rolling_cloud;

  std::vector<PointCloudPtr> clouds;
  int total_points = 0;
  for (int i = 10; i <= 100; i += 10) {
    auto cloud_ptr = get_point_cloud_of_size(i);
    clouds.push_back(cloud_ptr);
    rolling_cloud.add(cloud_ptr);
    total_points += i;
  }

  auto rolling_cloud_ptr = rolling_cloud.get();
  EXPECT_EQ(rolling_cloud_ptr->size(), total_points);
}

TEST_F(FixedLagRollingCloudTest, AddMultiplePastMaxSize) {
  const size_t MAX_SIZE = 8;
  const size_t EXTRAS = 3;
  FixedLagRollingCloud<PointType, CpuCloudManager<PointType>> rolling_cloud(
      MAX_SIZE);

  std::vector<PointCloudPtr> clouds;
  const size_t total_points = ((1 << MAX_SIZE) - 1) << EXTRAS;

  for (size_t i = 1; i < 1 << (MAX_SIZE + EXTRAS); i *= 2) {
    auto cloud_ptr = get_point_cloud_of_size(i);
    clouds.push_back(cloud_ptr);
    rolling_cloud.add(cloud_ptr);
  }

  auto rolling_cloud_ptr = rolling_cloud.get();
  EXPECT_EQ(rolling_cloud_ptr->size(), total_points);
}

TEST_F(FixedLagRollingCloudTest, Caching) {
  FixedLagRollingCloud<PointType, CpuCloudManager<PointType>> rolling_cloud;
  auto rolling_cloud_ptr_1 = rolling_cloud.get();
  auto rolling_cloud_ptr_2 = rolling_cloud.get();
  EXPECT_EQ(rolling_cloud_ptr_1, rolling_cloud_ptr_2);

  auto cloud_ptr = get_point_cloud_of_size(10);
  rolling_cloud.add(cloud_ptr);

  auto rolling_cloud_ptr_3 = rolling_cloud.get();
  EXPECT_NE(rolling_cloud_ptr_1, rolling_cloud_ptr_3);
  EXPECT_NE(rolling_cloud_ptr_2, rolling_cloud_ptr_3);
}

}  // namespace object_detection

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
