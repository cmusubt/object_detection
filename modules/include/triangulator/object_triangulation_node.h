#pragma once
#include <base/BaseNode.h>
#include <cv_bridge/cv_bridge.h>
// #include <common/image_transport.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <objdet_msgs/Detection.h>
#include <objdet_msgs/DetectionArray.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <tf2_ros/transform_listener.h>
#include <chrono>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <random>
#include <string>
#include <vector>
#include "triangulator.h"

namespace object_detection {
namespace object_triangulation {

class ObjectTriangulationNode : public BaseNode {
 public:
  ObjectTriangulationNode(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~ObjectTriangulationNode() = default;

 private:
  cv::Ptr<cv::ORB> orb_detector_;

  int num_cameras_;

  vector<boost::shared_ptr<gtsam::Cal3_S2>> sharedCals_;

  Triangulator<gtsam::Cal3_S2> triangulator_;

  // buffer and listener for each camera
  vector<std::shared_ptr<tf2_ros::Buffer>> tf_buffers_;
  vector<std::unique_ptr<tf2_ros::TransformListener>> tf_listener_ptrs_;

  // flags indicating when to do triangulation
  int counter_for_no_detection_threshold_ = 10;
  vector<int> counters_for_no_detection_;
  int counter_for_no_detection_front_ = 0;
  int counter_for_no_detection_back_ = 0;
  int counter_for_no_detection_left_ = 0;
  int counter_for_no_detection_right_ = 0;
  std::vector<bool> can_do_new_triangulation_;
  bool can_do_new_triangulation_any_ = false;

  std::string label_map_path_;
  int num_object_types_ = 5;

  // swith to turn on or off different cameras
  vector<bool> use_cameras_ = {true, true, true, true, false, false};

  bool use_ransac_ = true;
  int num_observs_thresh_ = 2;  // number of observations to start triangulation
  bool optimize_ =
      false;  // use gtsam non linear optimization to optimize the result
  bool is_debug_ = false;                  // trun on and off debug mode

  int cluster_mean_k_;
  double cluster_std_mul_th_;
  std::string debug_files_path_;

  int report_id_;
  double pose_delay_offset_;

  std::vector<sensor_msgs::Image> cached_images_;
  std::vector<int> cached_images_keypoint_nums_;

  int nearest_neighbor_k_;
  double max_matched_keyframe_distance_;
  double min_translation_between_two_keyframes_;

  vector<shared_ptr<image_transport::SubscriberFilter>> image_subs_;
  vector<shared_ptr<message_filters::Subscriber<objdet_msgs::DetectionArray>>>
      bounding_box_subs_;
  vector<shared_ptr<message_filters::TimeSynchronizer<
      sensor_msgs::Image, objdet_msgs::DetectionArray>>>
      syncs_;

  void process_callback(
      const std::string &camera_name, int &counter_for_no_detection,
      tf2_ros::Buffer &tf_buffer,
      const boost::shared_ptr<gtsam::Cal3_S2> &shared_cal_,
      const sensor_msgs::ImageConstPtr &img_msg,
      const objdet_msgs::DetectionArrayConstPtr &detection_array);

  void image_detection_callback(
      const sensor_msgs::ImageConstPtr &img_msg,
      const objdet_msgs::DetectionArrayConstPtr &detection_array, int camera);

  void publish_results(int object_id);

  void objectLocalizationCallback(
      const objdet_msgs::ArtifactLocalizationArrayConstPtr &localization_array);

  ros::Publisher triangulator_object_pointcloud_pub_;
  ros::Publisher triangulator_filtered_object_pointcloud_pub_;
  ros::Publisher triangulator_averaged_object_pointcloud_pub_;
  ros::Publisher artifact_localizations_publisher;

  /********************** For Debugging ****************************/
  // store the latest artifact locations estimated from lidar
  std::map<int, vector<float>> latest_artifact_locations_;

  // pushlish the ground truth for verification
  ros::Publisher triangulation_ground_truth_pub_;
  message_filters::Subscriber<objdet_msgs::ArtifactLocalizationArray>
      artifact_localizations_sub_;
};

}  // namespace object_triangulation
}  // namespace object_detection
