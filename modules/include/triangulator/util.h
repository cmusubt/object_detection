#pragma once

#include <bits/stdc++.h>
#include <iomanip>
#include <sstream>
#include <string>
using namespace std;

int ChooseKFromN(int n, int k) {
  int res = 1;

  // Since C(n, k) = C(n, n-k)
  if (k > n - k) k = n - k;

  // Calculate value of
  // [n * (n-1) *---* (n-k+1)] / [k * (k-1) *----* 1]
  for (int i = 0; i < k; ++i) {
    res *= (n - i);
    res /= (i + 1);
  }

  return res;
}

std::string ZeroPadNumber(int num) {
  std::ostringstream ss;
  ss << std::setw(3) << std::setfill('0') << num;
  std::string result = ss.str();
  if (result.length() > 7) {
    result.erase(0, result.length() - 7);
  }
  return result;
}

template <class T>
void ReduceVector(vector<T> &v, vector<uchar> status) {
  int j = 0;
  for (int i = 0; i < int(v.size()); i++)
    if (status[i]) v[j++] = v[i];
  v.resize(j);
}