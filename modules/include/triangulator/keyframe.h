/**
 * @file keyframe.h
 *
 * @brief keyframe is used to ensure the baseline is long enough to do
 * triangulation.
 *
 * @author Chenfeng Tu and Tian Liu
 * Contact: {chenfent,tianliu}@andrew.cmu.edu
 *
 */

#pragma once
#include <gtsam/geometry/Cal3Bundler.h>
#include <gtsam/geometry/CameraSet.h>
#include <gtsam/geometry/PinholeCamera.h>
#include <gtsam/geometry/SimpleCamera.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>
#include <ros/console.h>
#include <stdio.h>
#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/Geometry>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <opencv2/features2d/features2d.hpp>
#include <unordered_map>
#include <vector>
#include "landmark.h"

/**
 * Keyframe structure
 *
 * Keyframe contains the properties of this keyframe including landmarks seen,
 * pose of the keyframe and camera model etc.
 *
 */
template <class CALIBRATION>
class KeyFrame {
 public:
  KeyFrame() = default;
  KeyFrame(int object_id, int keyframe_id, const gtsam::Pose3 &pose,
           const boost::shared_ptr<CALIBRATION> &sharedCal,
           const std::vector<cv::KeyPoint> &keypoints,
           const cv::Mat &descriptors)
      : object_id_(object_id),
        keyframe_id_(keyframe_id),
        keyframe_pose_(pose),
        sharedCal_(sharedCal),
        descriptors_(descriptors),
        cv_keypoints_(keypoints) {
    keypoints_size_ = keypoints.size();
    for (int i = 0; i < keypoints.size(); i++) {
      const auto &pt_i = keypoints[i].pt;
      Eigen::Vector2d point_uv_i(pt_i.x, pt_i.y);
      gtsam::Point2 key_point(point_uv_i);
      keypoints_.push_back(key_point);
    }
    keypoints_landmark_id_.resize(keypoints_size_, -1);
  }

  int GetObjectId() const { return object_id_; }
  int GetKeyframeId() const { return keyframe_id_; }

  size_t GetKeypointSize() const { return keypoints_size_; }

  /** Calculate the distance to another keyframe using translation distance.
   *
   * @param another_keyframe another keyframe
   */
  double GetDistance(const KeyFrame<CALIBRATION> &another_keyframe) const {
    return keyframe_pose_.translation().distance(
        another_keyframe.keyframe_pose_.translation());
  }

  /** generate new obeservation of landmarks in this keyframe
   *
   * @param keypoint_id # of keypoint in this frame
   */
  Observation<CALIBRATION> GenerateNewObservation(int keypoint_id) const {
    Observation<CALIBRATION> new_observation(keypoints_[keypoint_id],
                                             keyframe_pose_, sharedCal_,
                                             descriptors_.row(keypoint_id));
    return new_observation;
  }

  gtsam::Point2Vector keypoints_;
  std::vector<cv::KeyPoint> cv_keypoints_;
  cv::Mat descriptors_;  // each row is descriptor for a landmark
  gtsam::Pose3 keyframe_pose_;
  boost::shared_ptr<CALIBRATION> sharedCal_;

 private:
  size_t keypoints_size_;
  int object_id_;
  int keyframe_id_;
  std::vector<int>
      keypoints_landmark_id_;  // id of the landmarks in this keyframe
};

// Keyframes are stored in KDTree to make getting neighbours easily
class KeyframeKDTree {
 public:
  KeyframeKDTree() = default;
  KeyframeKDTree(int object_id, int keyframe_id,
                 gtsam::Point3 keyframe_location) {
    object_id_ = object_id;
    kdtree_pointcloud_ptr_.reset(new pcl::PointCloud<pcl::PointXYZ>);
    kdtree_pointcloud_ptr_->push_back(pcl::PointXYZ(
        keyframe_location.x(), keyframe_location.y(), keyframe_location.z()));
    kdtree_.setInputCloud(kdtree_pointcloud_ptr_);
  }

  void InsertNewKeyFrameLocation(int keyframe_id,
                                 gtsam::Point3 keyframe_location) {
    kdtree_pointcloud_ptr_->push_back(pcl::PointXYZ(
        keyframe_location.x(), keyframe_location.y(), keyframe_location.z()));
    kdtree_.setInputCloud(kdtree_pointcloud_ptr_);
  }

  int RadiusSearch(gtsam::Point3 search_keyframe_location, double search_radius,
                   std::vector<int> &indices_radius_search,
                   std::vector<float> &squared_distance_radius_search) {
    pcl::PointXYZ search_point(search_keyframe_location.x(),
                               search_keyframe_location.y(),
                               search_keyframe_location.z());
    return kdtree_.radiusSearch(search_point, search_radius,
                                indices_radius_search,
                                squared_distance_radius_search);
  }

  int KNearestSearch(gtsam::Point3 search_keyframe_location, int k,
                     std::vector<int> &indices_knn_search,
                     std::vector<float> &squared_distance_knn_search) {
    pcl::PointXYZ search_point(search_keyframe_location.x(),
                               search_keyframe_location.y(),
                               search_keyframe_location.z());
    return kdtree_.nearestKSearch(search_point, k, indices_knn_search,
                                  squared_distance_knn_search);
  }

 private:
  int object_id_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr kdtree_pointcloud_ptr_;
  pcl::KdTreeFLANN<pcl::PointXYZ> kdtree_;
};