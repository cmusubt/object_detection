/**
 * @file triangulator.h
 *
 * @brief class for doing triangulation
 *
 * @author Chenfeng Tu and Tian Liu
 * Contact: {chenfent,tianliu}@andrew.cmu.edu
 *
 */

#pragma once

// note: pcl should be put before opencv, since opencv and opencv would conflict
#include <pcl/common/centroid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <gtsam/geometry/Cal3Bundler.h>
#include <gtsam/geometry/CameraSet.h>
#include <gtsam/geometry/EssentialMatrix.h>
#include <gtsam/geometry/PinholeCamera.h>
#include <gtsam/geometry/SimpleCamera.h>
#include <gtsam/geometry/triangulation.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/Geometry>
#include <boost/functional/hash.hpp>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "keyframe.h"
#include "landmark.h"
#include "util.h"

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <ros/console.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <stdio.h>
#include <unordered_map>
#include <vector>

using namespace std;
using namespace gtsam;
using namespace cv;

template <class CALIBRATION>
class Triangulator {
 public:
  Triangulator() {
    descriptor_matcher_ = DescriptorMatcher::create("BruteForce-Hamming");
    is_debug_ = false;
  };

  ~Triangulator(){};

  int GetNumObjects() const { return objects_.size(); };

  int GetObjectNumLandmark(int object_id) const {
    return objects_[object_id].size();
  };

  // Return ture if at least one landmark has two observations
  bool CanDoTriangulation(int object_id);

  boost::tuple<int, double, Vector> DLT(const Matrix &A, double rank_tol);

  bool TriangulateHomogeneousDLT(
      const std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>>
          &projection_matrices,
      const Point2Vector &measurements, gtsam::Vector4 &v, double rank_tol);

  bool TriangulateDLT(
      const std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>>
          &projection_matrices,
      const Point2Vector &measurements, Point3 &point, double rank_tol = 1e-9);

  bool TriangulateRansacDLT(
      const std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>>
          &projection_matrices,
      const Point2Vector &measurements, Point3 &point, vector<int> &inliers_set,
      int sample_size, double threshold = 2.0, double rank_tol = 1e-9);

  Point3 Optimize(const NonlinearFactorGraph &graph, const Values &values,
                  Key landmarkKey);

  Point3 TriangulateUseGTSAMGraph(const std::vector<Pose3> &poses,
                                  boost::shared_ptr<CALIBRATION> sharedCal,
                                  const Point2Vector &measurements,
                                  const Point3 &initialEstimate);

  template <class CAMERA>
  Point3 TriangulateUseGTSAMGraph(
      const CameraSet<CAMERA> &cameras,
      const typename CAMERA::MeasurementVector &measurements,
      const Point3 &initialEstimate) {
    Values values;
    values.insert(Symbol('p', 0),
                  initialEstimate);  // Initial landmark value
    NonlinearFactorGraph graph;
    Eigen::Matrix2d cov;
    cov << 0.1, 0, 0, 0.1;
    static SharedNoiseModel prior_model(noiseModel::Gaussian::Covariance(cov));
    // static SharedNoiseModel unit(noiseModel::Unit::Create(2));
    static SharedNoiseModel huber_model(noiseModel::Robust::Create(
        noiseModel::mEstimator::Huber::Create(0.5), prior_model));
    static SharedNoiseModel unit(noiseModel::Unit::Create(
        gtsam::traits<typename CAMERA::Measurement>::dimension));
    // static SharedNoiseModel unit(noiseModel::Unit::Create(2));
    for (size_t i = 0; i < measurements.size(); i++) {
      const CAMERA &camera_i = cameras[i];
      graph.emplace_shared<TriangulationFactor<CAMERA>>  //
          (camera_i, measurements[i], huber_model, Symbol('p', 0));
    }
    Point3 optimized_point3 =
        Triangulator::Optimize(graph, values, Symbol('p', 0));
    return optimized_point3;
  }

  // Triangulation using the same camera
  bool TriangulateUseGTSAM(const std::vector<Pose3> &poses,
                           boost::shared_ptr<CALIBRATION> sharedCal,
                           const Point2Vector &measurements, Point3 &point3,
                           double rank_tol = 1e-9, bool optimize = false,
                           bool use_ransac = false, int num_observs_thresh = 2);

  // Triangulation using the same or different cameras
  template <class CAMERA>
  bool TriangulateUseGTSAM(
      const CameraSet<CAMERA> &cameras,
      const typename CAMERA::MeasurementVector &measurements, Point3 &point3,
      double rank_tol = 1e-9, bool optimize = false, bool use_ransac = false,
      int num_observs_thres = 2) {
    Point3 point;
    if (cameras.size() != measurements.size()) {
      ROS_WARN("poses size != measurements.size()");
      return false;
    }
    if (cameras.size() < num_observs_thres) {
      ROS_WARN(
          "poses size < minimum number of observations specified in launch "
          "file!");
      return false;
    }

    // construct projection matrices from poses & calibration
    std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>>
        projection_matrices;
    for (const CAMERA &camera : cameras)
      projection_matrices.push_back(
          CameraProjectionMatrix<typename CAMERA::CalibrationType>(
              camera.calibration())(camera.pose()));

    bool succeed_flag;
    if (use_ransac) {
      vector<int> inliers_set;
      succeed_flag =
          TriangulateRansacDLT(projection_matrices, measurements, point,
                               inliers_set, ransac_sample_size_);
    } else {
      // Triangulate linearly
      succeed_flag = Triangulator::TriangulateDLT(
          projection_matrices, measurements, point, rank_tol);
    }

    if (!succeed_flag) return false;

    for (const CAMERA &camera : cameras) {
      const Point3 &p_local = camera.pose().transform_to(point);
      if (p_local.z() <= 0) {
        ROS_WARN("The initial triangulated point lies behind the camera");
        return false;
      }
    }

    // Then refine using non-linear optimization
    if (optimize) {
      point3 = Triangulator::TriangulateUseGTSAMGraph<CAMERA>(
          cameras, measurements, point);
    } else {
      point3 = point;
    }

    // verify that the triangulated point lies in front of all cameras
    for (const CAMERA &camera : cameras) {
      const Point3 &p_local = camera.pose().transform_to(point3);
      if (p_local.z() <= 0) {
        ROS_WARN("The optimized triangulated point lies behind the camera");
        return false;
      }
    }
    return true;
  }

  void TriangulateAll(double rank_tol = 1e-9, bool optimize = false,
                      bool use_ransac = false, int num_observs_thresh = 2);

  void TriangulateObject(int object_id, double rank_tol = 1e-9,
                         bool optimize = false, bool use_ransac = false,
                         int num_observs_thresh = 2);

  void TriangulateLandMark(Landmark<CALIBRATION> &landmark,
                           double rank_tol = 1e-9, bool optimize = false,
                           bool use_ransac = false, int num_observs_thresh = 2);

  // return the indices of #k nearest neighbours and corresponding distances
  int KNearestSearchKeyFrame(int object_id,
                             const KeyFrame<CALIBRATION> &search_keyframe,
                             int k, std::vector<int> &indices_search,
                             std::vector<float> &squared_distance_search) {
    return object_keyframekdtrees_[object_id].KNearestSearch(
        search_keyframe.keyframe_pose_.translation(), k, indices_search,
        squared_distance_search);
  }

  int RadiusSearchKeyFrame(int object_id,
                           const KeyFrame<CALIBRATION> &search_keyframe,
                           double radius, std::vector<int> &indices_search,
                           std::vector<float> &squared_distance_search) {
    return object_keyframekdtrees_[object_id].RadiusSearch(
        search_keyframe.keyframe_pose_.translation(), radius, indices_search,
        squared_distance_search);
  }

  void InsertNewKeyFrame(int object_id,
                         const KeyFrame<CALIBRATION> &new_keyframe);

  void MatchTwoKeyframe(KeyFrame<CALIBRATION> &old_keyframe,
                        KeyFrame<CALIBRATION> &new_keyframe);

  void BuildConnectedMatches(int object_id, int depth = 10);
  void BuildConnectedMatchesDFS(
      int object_id, pair<int, int> curr_kf_kp_id, int depth,
      unordered_set<int> &visited_keyframes,
      unordered_set<pair<int, int>, boost::hash<pair<int, int>>>
          &visited_keypoints,
      vector<pair<int, int>> &connected_component);

  void ClearAllObjects();
  void ClearObject(int object_id);

  void LoadParameters(const ros::NodeHandle *nh);

  unordered_map<int, vector<Landmark<CALIBRATION>>> objects_;
  unordered_map<int, vector<KeyFrame<CALIBRATION>>> object_keyframes_;
  unordered_map<int, KeyframeKDTree> object_keyframekdtrees_;
  unordered_map<int, unordered_map<pair<int, int>, vector<pair<int, int>>,
                                   boost::hash<pair<int, int>>>>
      object_keypoint_matches_;
  unordered_map<int, vector<pair<int, int>>> object_connected_matches_;

  // for debug
  void PrintKeypointsMatches() const;
  void PrintTriangulationResults() const;

 private:
  cv::Ptr<DescriptorMatcher> descriptor_matcher_;

  int min_num_observations_to_triangulate_;
  int max_ransac_iteration_;
  int ransac_sample_size_;
  int ransac_inlier_threshold_;
  int cluster_mean_k_;
  double cluster_std_mul_th_;
  bool is_debug_;
  string debug_files_path_;
};

// decide if triangulation can be done for object with object_id
template <class CALIBRATION>
bool Triangulator<CALIBRATION>::CanDoTriangulation(int object_id) {
  if (objects_.find(object_id) == objects_.end()) {
    return false;
  }
  // If any landmark on this artifact is observed at two keyframes, then can do
  // triangulation
  for (const Landmark<CALIBRATION> &l : objects_[object_id]) {
    if (l.GetNumObservations() >= 2) {
      return true;
    }
  }
  return false;
}

// load parameters from launch file
template <class CALIBRATION>
void Triangulator<CALIBRATION>::LoadParameters(const ros::NodeHandle *nh) {
  nh->getParam("min_num_observations_to_triangulate",
               min_num_observations_to_triangulate_);
  nh->getParam("max_ransac_iteration", max_ransac_iteration_);
  nh->getParam("ransac_sample_size", ransac_sample_size_);
  nh->getParam("ransac_inlier_threshold", ransac_inlier_threshold_);
  nh->getParam("cluster_mean_k", cluster_mean_k_);
  nh->getParam("cluster_std_mul_th", cluster_std_mul_th_);
  nh->getParam("is_debug", is_debug_);
  nh->getParam("debug_files_path", debug_files_path_);
  if (is_debug_) {
    ROS_WARN(
        "================== Triangulator Parameters Loaded "
        "========================");
    ROS_WARN_STREAM("min_num_observations_to_triangulate_ = "
                    << min_num_observations_to_triangulate_);
    ROS_WARN_STREAM("max_ransac_iteration_ = " << max_ransac_iteration_);
    ROS_WARN_STREAM("ransac_sample_size_ = " << ransac_sample_size_);
    ROS_WARN_STREAM("cluster_mean_k_ = " << cluster_mean_k_);
    ROS_WARN_STREAM("cluster_std_mul_th_ = " << cluster_std_mul_th_);
    ROS_WARN_STREAM("is_debug_ = " << is_debug_);
    ROS_WARN_STREAM("debug_files_path = " << debug_files_path_);
  }
}

// triangulate with factor graph optimization
template <class CALIBRATION>
Point3 Triangulator<CALIBRATION>::TriangulateUseGTSAMGraph(
    const std::vector<Pose3> &poses, boost::shared_ptr<CALIBRATION> sharedCal,
    const Point2Vector &measurements, const Point3 &initialEstimate) {
  Values values;

  values.insert(Symbol('p', 0),
                initialEstimate);  // Initial landmark value
  NonlinearFactorGraph graph;
  Eigen::Matrix2d cov;
  cov << 0.1, 0, 0, 0.1;
  static SharedNoiseModel prior_model(noiseModel::Gaussian::Covariance(cov));
  static SharedNoiseModel unit(noiseModel::Unit::Create(2));
  static SharedNoiseModel huber_model(noiseModel::Robust::Create(
      noiseModel::mEstimator::Huber::Create(0.5), prior_model));
  for (size_t i = 0; i < measurements.size(); i++) {
    const Pose3 &pose_i = poses[i];
    typedef PinholePose<CALIBRATION> Camera;
    Camera camera_i(pose_i, sharedCal);
    graph.emplace_shared<TriangulationFactor<Camera>>(
        camera_i, measurements[i], huber_model, Symbol('p', 0));
  }

  Point3 optimized_point3 =
      Triangulator::Optimize(graph, values, Symbol('p', 0));

  return optimized_point3;
}

template <class CALIBRATION>
bool Triangulator<CALIBRATION>::TriangulateUseGTSAM(
    const std::vector<Pose3> &poses, boost::shared_ptr<CALIBRATION> sharedCal,
    const Point2Vector &measurements, Point3 &point3, double rank_tol,
    bool optimize, bool use_ransac, int num_observs_thres) {
  Point3 point;
  if (poses.size() != measurements.size()) {
    ROS_WARN("poses size != measurements.size()");
    return false;
  }
  if (poses.size() < num_observs_thres) {
    ROS_WARN(
        "poses size < minimum number of observations specified in launch "
        "file!");
    return false;
  }

  // construct projection matrices from poses & CALIBRATION
  std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>> projection_matrices;
  CameraProjectionMatrix<CALIBRATION> createP(*sharedCal);  // partially apply
  for (const Pose3 &pose : poses) projection_matrices.push_back(createP(pose));

  bool succeed_flag;
  if (use_ransac) {
    vector<int> inliers_set;
    succeed_flag =
        TriangulateRansacDLT(projection_matrices, measurements, point,
                             inliers_set, ransac_sample_size_);
  } else {
    // Triangulate linearly
    succeed_flag = Triangulator::TriangulateDLT(projection_matrices,
                                                measurements, point, rank_tol);
  }

  if (!succeed_flag) return false;
  // verify that the triangulated point lies in front of all
  for (const Pose3 &pose : poses) {
    const Point3 &p_local = pose.transform_to(point);
    if (p_local.z() <= 0) {
      ROS_WARN("The triangulated point lies behind the camera");
      return false;
    }
  }

  // Then refine using non-linear optimization
  if (optimize) {
    point3 = Triangulator::TriangulateUseGTSAMGraph(poses, sharedCal,
                                                    measurements, point);
  } else {
    point3 = point;
  }

  // verify that the triangulated point lies in front of all
  for (const Pose3 &pose : poses) {
    const Point3 &p_local = pose.transform_to(point3);
    if (p_local.z() <= 0) {
      ROS_WARN("The triangulated point lies behind the camera");
      return false;
    }
  }
  return true;
}

template <class CALIBRATION>
boost::tuple<int, double, Vector> Triangulator<CALIBRATION>::DLT(
    const Matrix &A, double rank_tol) {
  // Check size of A
  size_t n = A.rows(), p = A.cols(), m = min(n, p);

  // Do SVD on A
  // Eigen::MatrixXd A_e = A;
  Eigen::JacobiSVD<Matrix> svd(A, Eigen::ComputeFullV);
  Vector s = svd.singularValues();
  Matrix V = svd.matrixV();
  // Find rank
  size_t rank = 0;
  for (size_t j = 0; j < m; j++)
    if (s(j) > rank_tol) rank++;

  // Return rank, error, and corresponding column of V
  double error = m < p ? 0 : s(m - 1);
  return boost::tuple<int, double, Vector>((int)rank, error,
                                           Vector(V.col(p - 1)));
}

template <class CALIBRATION>
Point3 Triangulator<CALIBRATION>::Optimize(const NonlinearFactorGraph &graph,
                                           const Values &values,
                                           Key landmarkKey) {
  LevenbergMarquardtParams params;
  params.verbosityLM = LevenbergMarquardtParams::TRYLAMBDA;
  params.verbosity = NonlinearOptimizerParams::ERROR;
  params.lambdaInitial = 1;
  params.lambdaFactor = 10;
  params.maxIterations = 100;
  params.absoluteErrorTol = 1.0;
  params.verbosityLM = LevenbergMarquardtParams::SILENT;
  params.verbosity = NonlinearOptimizerParams::SILENT;
  params.linearSolverType = NonlinearOptimizerParams::MULTIFRONTAL_CHOLESKY;

  LevenbergMarquardtOptimizer optimizer(graph, values, params);

  Values result = optimizer.optimize();

  return result.at<Point3>(landmarkKey);
}

template <class CALIBRATION>
bool Triangulator<CALIBRATION>::TriangulateHomogeneousDLT(
    const std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>>
        &projection_matrices,
    const Point2Vector &measurements, gtsam::Vector4 &v, double rank_tol) {
  // number of cameras
  size_t m = projection_matrices.size();
  if (m < 2) return false;

  // Allocate DLT matrix
  Matrix A = Matrix::Zero(m * 2, 4);

  for (size_t i = 0; i < m; i++) {
    size_t row = i * 2;
    const Matrix34 &projection = projection_matrices.at(i);
    const Point2 &p = measurements.at(i);

    // build system of equations
    A.row(row) = p.x() * projection.row(2) - projection.row(0);
    A.row(row + 1) = p.y() * projection.row(2) - projection.row(1);
  }
  int rank;
  double error;
  boost::tie(rank, error, v) = DLT(A, rank_tol);

  if (rank < 3) {
    ROS_WARN("rank of A is less than 3");
    return false;
  }
  return true;
}

template <class CALIBRATION>
bool Triangulator<CALIBRATION>::TriangulateDLT(
    const std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>>
        &projection_matrices,
    const Point2Vector &measurements, Point3 &point, double rank_tol) {
  if (projection_matrices.size() < 2) return false;
  gtsam::Vector4 v;
  bool triangulation_succeed =
      TriangulateHomogeneousDLT(projection_matrices, measurements, v, rank_tol);
  point = Point3(v.head<3>() / v[3]);
  return triangulation_succeed;
}

template <class CALIBRATION>
bool Triangulator<CALIBRATION>::TriangulateRansacDLT(
    const std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>>
        &projection_matrices,
    const Point2Vector &measurements, Point3 &point, vector<int> &inliers_set,
    int sample_size, double threshold, double rank_tol) {
  size_t num_poses = projection_matrices.size();
  if (num_poses <= sample_size) {
    return TriangulateDLT(projection_matrices, measurements, point, rank_tol);
  }

  int max_num_inliers = 0;
  int num_trials =
      min(2 * ChooseKFromN(num_poses, sample_size), max_ransac_iteration_);

  vector<int> indices(num_poses);
  for (size_t i = 0; i < num_poses; i++) {
    indices[i] = i;
  }
  std::srand(unsigned(std::time(0)));

  for (size_t t = 0; t < num_trials; t++) {
    int num_inliers_this_trial = 0;
    vector<int> inliers_this_trial(num_poses, 0);
    std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>>
        projection_matrices_this_trial;
    Point2Vector measurements_this_trial;
    gtsam::Vector4 v_this_rial;
    vector<int> indices_random = indices;
    random_shuffle(indices_random.begin(), indices_random.end());

    for (size_t j = 0; j < sample_size; j++) {
      int sample_index = indices_random[j];
      projection_matrices_this_trial.push_back(
          projection_matrices[sample_index]);
      measurements_this_trial.push_back(measurements[sample_index]);
    }
    if (!TriangulateHomogeneousDLT(projection_matrices_this_trial,
                                   measurements_this_trial, v_this_rial,
                                   rank_tol)) {
      continue;
    }

    v_this_rial = v_this_rial / v_this_rial[3];
    // calculate inliers
    for (size_t i = 0; i < num_poses; i++) {
      const auto &projection_matrix = projection_matrices[i];
      const Point2 &measurement = measurements[i];
      Vector3 point_in_camera = projection_matrix * v_this_rial;
      if (point_in_camera.z() < 0) {
        continue;
      }
      // reprojection error
      Point2 reprojected_uv =
          Point2(point_in_camera.head<2>() / point_in_camera[2]);
      double error = measurement.distance(reprojected_uv);

      if (error < threshold) {
        num_inliers_this_trial++;
        inliers_this_trial[i] = 1;
      }
    }
    if (num_inliers_this_trial > max_num_inliers) {
      max_num_inliers = num_inliers_this_trial;
      inliers_set = inliers_this_trial;
    }
  }
  if (max_num_inliers < ransac_inlier_threshold_) {  // tunable sample_size
    return false;
  }
  // use all inliers to do triangulation
  std::vector<Matrix34, Eigen::aligned_allocator<Matrix34>>
      projection_matrices_inliers;
  Point2Vector measurements_inliers;
  for (size_t i = 0; i < num_poses; i++) {
    if (inliers_set[i] == 1) {
      projection_matrices_inliers.push_back(projection_matrices[i]);
      measurements_inliers.push_back(measurements[i]);
    }
  }

  return TriangulateDLT(projection_matrices_inliers, measurements_inliers,
                        point, rank_tol);
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::TriangulateAll(double rank_tol, bool optimize,
                                               bool use_ransac,
                                               int num_observs_thresh) {
  for (auto &p : objects_) {
    int object_id = p.first;
    TriangulateObject(object_id, rank_tol, optimize, use_ransac,
                      num_observs_thresh);
  }
}

/** Triangulate artifact with object_id
 *
 * @param object_id: id of the object
 * @param rank_tol: id of the object
 * @param object_id: id of the object
 * @param object_id: id of the object
 */
template <class CALIBRATION>
void Triangulator<CALIBRATION>::TriangulateObject(int object_id,
                                                  double rank_tol,
                                                  bool optimize,
                                                  bool use_ransac,
                                                  int num_observs_thresh) {
  BuildConnectedMatches(object_id);
  for (auto &landmark : objects_[object_id]) {
    int landmark_num_observations = landmark.GetNumObservations();
    if (landmark_num_observations >= 2) {
      TriangulateLandMark(landmark, rank_tol, optimize, use_ransac,
                          num_observs_thresh);
    } else {
      if (is_debug_) {
        ROS_WARN_STREAM(
            "At least need "
            << 2 << "landmarks to triangulate, but this landmark only has "
            << landmark_num_observations);
      }
    }
  }
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::TriangulateLandMark(
    Landmark<CALIBRATION> &landmark, double rank_tol, bool optimize,
    bool use_ransac, int num_observs_thres) {
  gtsam::CameraSet<PinholeCamera<CALIBRATION>> camera_set;
  Point2Vector measurements;
  Point3 triangulated_point;
  vector<Observation<CALIBRATION>> &observations = landmark.GetObservations();
  for (auto &observation : observations) {
    gtsam::Point2 point_uv = observation.GetUv();
    boost::shared_ptr<CALIBRATION> sharedCal = observation.GetCalibration();
    measurements.push_back(point_uv);
    gtsam::PinholeCamera<CALIBRATION> pinhole_cam(observation.GetCameraPose(),
                                                  *sharedCal);
    camera_set.push_back(pinhole_cam);
  }
  if (TriangulateUseGTSAM(camera_set, measurements, triangulated_point,
                          rank_tol, optimize, use_ransac, num_observs_thres)) {
    landmark.SetTriangulationFlag(true);
    landmark.SetTriangulationPoint(triangulated_point);
  } else {
    landmark.SetTriangulationFlag(false);
  }
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::InsertNewKeyFrame(
    int object_id, const KeyFrame<CALIBRATION> &new_keyframe) {
  object_keyframes_[object_id].push_back(new_keyframe);
  if (object_keyframekdtrees_.find(object_id) ==
      object_keyframekdtrees_.end()) {
    KeyframeKDTree object_keyframe_kdtree(
        object_id, new_keyframe.GetKeyframeId(),
        new_keyframe.keyframe_pose_.translation());
    object_keyframekdtrees_[object_id] = object_keyframe_kdtree;
  } else {
    object_keyframekdtrees_[object_id].InsertNewKeyFrameLocation(
        new_keyframe.GetKeyframeId(),
        new_keyframe.keyframe_pose_.translation());
  }
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::MatchTwoKeyframe(
    KeyFrame<CALIBRATION> &old_keyframe, KeyFrame<CALIBRATION> &new_keyframe) {
  int object_id = old_keyframe.GetObjectId();
  int old_keyframe_id = old_keyframe.GetKeyframeId();
  int new_keyframe_id = new_keyframe.GetKeyframeId();

  if (is_debug_ && old_keyframe.GetKeypointSize() == 0 ||
      new_keyframe.GetKeypointSize() == 0) {
    ROS_WARN("One of the keyframes doesn't have keypoints");
  }

  const cv::Mat &descriptors_old_frame = old_keyframe.descriptors_;
  const cv::Mat &descriptors_new_frame = new_keyframe.descriptors_;

  vector<DMatch> matches;
  descriptor_matcher_->match(descriptors_new_frame, descriptors_old_frame,
                             matches);

  // filtering matched points
  double min_dist = 10000, max_dist = 0;

  // Find MinDist and MaxDist
  for (int i = 0; i < descriptors_new_frame.rows; i++) {
    double dist = matches[i].distance;
    if (dist < min_dist) min_dist = dist;
    if (dist > max_dist) max_dist = dist;
  }

  std::vector<cv::DMatch> good_matches;
  for (int i = 0; i < descriptors_new_frame.rows; i++) {
    if (matches[i].distance <= max(2 * min_dist, 30.0)) {
      good_matches.push_back(matches[i]);
    }
  }

  unordered_map<int, int> one_to_one_match;
  for (const cv::DMatch &good_match : good_matches) {
    int new_keypoint_id = good_match.queryIdx;
    int old_keypoint_id = good_match.trainIdx;
    if (one_to_one_match.find(old_keypoint_id) == one_to_one_match.end()) {
      one_to_one_match[old_keypoint_id] = new_keypoint_id;
    } else {
      int previous_new_keypoint_id = one_to_one_match[old_keypoint_id];
      double prev_distance = matches[previous_new_keypoint_id].distance;
      double curr_distance = matches[new_keypoint_id].distance;
      if (curr_distance < prev_distance) {
        one_to_one_match[old_keypoint_id] = new_keypoint_id;
      }
    }
  }

  std::vector<cv::DMatch> one_to_one_good_matches;
  for (const auto &p : one_to_one_match) {
    int new_keypoint_id = p.second;
    one_to_one_good_matches.push_back(matches[new_keypoint_id]);
  }

  // Ransac Fundamental
  vector<uchar> status;
  vector<cv::Point2f> old_keypoints;
  vector<cv::Point2f> new_keypoints;
  for (const cv::DMatch &one_to_one_good_match : one_to_one_good_matches) {
    int new_keypoint_id = one_to_one_good_match.queryIdx;
    int old_keypoint_id = one_to_one_good_match.trainIdx;
    cv::Point2f old_pt2f = old_keyframe.cv_keypoints_[old_keypoint_id].pt;
    cv::Point2f new_pt2f = new_keyframe.cv_keypoints_[new_keypoint_id].pt;
    old_keypoints.push_back(old_pt2f);
    new_keypoints.push_back(new_pt2f);
  }

  cv::findFundamentalMat(old_keypoints, new_keypoints, cv::FM_RANSAC, 3.0, 0.99,
                         status);
  if (status.empty()) return;
  ReduceVector<cv::DMatch>(one_to_one_good_matches, status);

  for (const cv::DMatch &good_match : one_to_one_good_matches) {
    int new_keypoint_id = good_match.queryIdx;
    int old_keypoint_id = good_match.trainIdx;
    pair<int, int> old_keyframe_keypoint_id = {old_keyframe_id,
                                               old_keypoint_id};
    pair<int, int> new_keyframe_keypoint_id = {new_keyframe_id,
                                               new_keypoint_id};
    object_keypoint_matches_[object_id][old_keyframe_keypoint_id].push_back(
        new_keyframe_keypoint_id);
    object_keypoint_matches_[object_id][new_keyframe_keypoint_id].push_back(
        old_keyframe_keypoint_id);
  }

  if (is_debug_) {
    string old_img_file_name = debug_files_path_ + "keyframes/object_" +
                               ZeroPadNumber(object_id) + "/keyframe_" +
                               ZeroPadNumber(old_keyframe_id) + ".jpg";

    string new_img_file_name = debug_files_path_ + "keyframes/object_" +
                               ZeroPadNumber(object_id) + "/keyframe_" +
                               ZeroPadNumber(new_keyframe_id) + ".jpg";

    cv::Mat img_old = cv::imread(old_img_file_name, cv::IMREAD_UNCHANGED);
    cv::Mat img_new = cv::imread(new_img_file_name, cv::IMREAD_UNCHANGED);
    Mat img_goodmatch;

    cv::drawMatches(img_new, new_keyframe.cv_keypoints_, img_old,
                    old_keyframe.cv_keypoints_, one_to_one_good_matches,
                    img_goodmatch);

    string match_img_file_name = debug_files_path_ + "matches/object_" +
                                 ZeroPadNumber(object_id) + "/match_" +
                                 ZeroPadNumber(new_keyframe_id) + "_" +
                                 ZeroPadNumber(old_keyframe_id) + ".jpg";
    cv::imwrite(match_img_file_name, img_goodmatch);
  }
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::BuildConnectedMatches(int object_id,
                                                      int depth) {
  unordered_set<pair<int, int>, boost::hash<pair<int, int>>> visited_keypoints;
  vector<vector<pair<int, int>>> all_connected_components;
  vector<vector<pair<int, int>>> connected_components;
  const auto &keypoint_mathes = object_keypoint_matches_[object_id];
  for (const auto &p2 : keypoint_mathes) {
    pair<int, int> kf_kp_id = p2.first;
    if (visited_keypoints.find(kf_kp_id) == visited_keypoints.end()) {
      vector<pair<int, int>> curr_connected_component;
      unordered_set<int> visited_keyframes;
      BuildConnectedMatchesDFS(object_id, kf_kp_id, depth, visited_keyframes,
                               visited_keypoints, curr_connected_component);
      all_connected_components.push_back(curr_connected_component);
      if (curr_connected_component.size() >=
          min_num_observations_to_triangulate_) {
        connected_components.push_back(curr_connected_component);
      }
    }
  }

  if (connected_components.size() < 3) {
    connected_components.clear();
    for (int i = 0; i < all_connected_components.size(); i++) {
      if (all_connected_components[i].size() >= 2) {
        connected_components.push_back(all_connected_components[i]);
      }
    }
  }

  if (is_debug_) {
    ROS_WARN("========== Printing Connect Components ============");
    ROS_WARN_STREAM("object id = " << object_id);
    ROS_WARN_STREAM(
        "connected_components size = " << connected_components.size());
  }

  objects_[object_id].clear();
  for (int i = 0; i < connected_components.size(); i++) {
    Landmark<CALIBRATION> new_landmark;
    for (int j = 0; j < connected_components[i].size(); j++) {
      const auto &kkid = connected_components[i][j];
      int keyframe_id = kkid.first;
      int keypoint_id = kkid.second;
      const auto &key_frame = object_keyframes_[object_id][keyframe_id];
      Observation<CALIBRATION> new_observation =
          key_frame.GenerateNewObservation(keypoint_id);
      new_landmark.AddObservation(new_observation);
    }

    objects_[object_id].push_back(new_landmark);
  }
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::BuildConnectedMatchesDFS(
    int object_id, pair<int, int> curr_kf_kp_id, int depth,
    unordered_set<int> &visited_keyframes,
    unordered_set<pair<int, int>, boost::hash<pair<int, int>>>
        &visited_keypoints,
    vector<pair<int, int>> &connected_component) {
  if (depth < 0) return;
  if (visited_keyframes.find(curr_kf_kp_id.first) != visited_keyframes.end()) {
    return;
  }
  if (visited_keypoints.find(curr_kf_kp_id) != visited_keypoints.end()) {
    return;
  }
  visited_keyframes.insert(curr_kf_kp_id.first);
  visited_keypoints.insert(curr_kf_kp_id);
  const auto &neighbors = object_keypoint_matches_[object_id][curr_kf_kp_id];
  for (const auto &next_kf_kp_id : neighbors) {
    BuildConnectedMatchesDFS(object_id, next_kf_kp_id, depth - 1,
                             visited_keyframes, visited_keypoints,
                             connected_component);
  }
  connected_component.push_back(curr_kf_kp_id);
  return;
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::PrintKeypointsMatches() const {
  ROS_WARN("========== Printing Keypoint Matches ============");
  for (const auto &p : object_keypoint_matches_) {
    ROS_WARN_STREAM("object id = " << p.first);
    const auto &kepoint_matches = p.second;
    ROS_WARN_STREAM("object has " << kepoint_matches.size() << " keypoints");
    for (const auto &p2 : kepoint_matches) {
      const auto &keframe_keypoint_id = p2.first;
      ROS_WARN_STREAM(" <keyframe , keypoint> "
                      << " < " << keframe_keypoint_id.first << " , "
                      << keframe_keypoint_id.second << " > has matches: ");
      const auto &matched_keyframe_keypoint_ids = p2.second;
      for (const auto &p3 : matched_keyframe_keypoint_ids) {
        ROS_WARN_STREAM(" < " << p3.first << " , " << p3.second << " >  ");
      }
    }
  }
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::PrintTriangulationResults() const {
  ROS_WARN("========== Printing Triangulation Results ============");
  for (const auto &p : objects_) {
    ROS_WARN_STREAM("object id = " << p.first);
    const auto &landmarks = p.second;
    int num_valid_landmarks = 0;
    ROS_WARN_STREAM("object has " << landmarks.size() << " total landmarks");

    for (size_t i = 0; i < landmarks.size(); i++) {
      const auto &landmark = landmarks[i];
      if (landmark.CheckTriangulation()) {
        ROS_WARN_STREAM("landmark " << i << " has "
                                    << landmark.GetNumObservations()
                                    << " observations. ");
        ROS_WARN_STREAM(
            "the triangulation point is : " << landmark.GetTriangulation());
      }
    }
  }
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::ClearAllObjects() {
  objects_.clear();
  object_keyframes_.clear();
  object_keypoint_matches_.clear();
  object_connected_matches_.clear();
  object_keyframekdtrees_.clear();
}

template <class CALIBRATION>
void Triangulator<CALIBRATION>::ClearObject(int object_id) {
  objects_.erase(object_id);
  object_keyframes_.erase(object_id);
  object_keypoint_matches_.erase(object_id);
  object_connected_matches_.erase(object_id);
  object_keyframekdtrees_.erase(object_id);
}