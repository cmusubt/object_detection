/**
 * @file landmark.h
 *
 * @brief class for landmark, landmark contains the poses of seeing this
 * landmark and status of whether the landmark is successfully triangulated
 *
 * @author Chenfeng Tu and Tian Liu
 * Contact: {chenfent,tianliu}@andrew.cmu.edu
 *
 */

#pragma once
#include <gtsam/geometry/Cal3Bundler.h>
#include <gtsam/geometry/CameraSet.h>
#include <gtsam/geometry/PinholeCamera.h>
#include <gtsam/geometry/SimpleCamera.h>
#include <ros/console.h>
#include <stdio.h>
#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/Geometry>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <opencv2/features2d/features2d.hpp>
#include <unordered_map>
#include <vector>

using namespace std;
using namespace gtsam;
using namespace cv;

template <class CALIBRATION>
class Observation {
 public:
  Observation(Point2 uv, Pose3 camera_pose,
              boost::shared_ptr<CALIBRATION> sharedCal, cv::Mat descriptor)
      : uv_(uv),
        camera_pose_(camera_pose),
        sharedCal_(sharedCal),
        descriptor_(descriptor) {}

  Point2 GetUv() const { return uv_; }

  Pose3 GetCameraPose() const { return camera_pose_; }

  boost::shared_ptr<CALIBRATION> GetCalibration() const { return sharedCal_; }

  cv::Mat GetDescriptor() const { return descriptor_; }

 private:
  Point2 uv_;
  Pose3 camera_pose_;
  boost::shared_ptr<CALIBRATION> sharedCal_;
  cv::Mat descriptor_;
};

template <class CALIBRATION>
class Landmark {
 public:
  Landmark() {
    num_observations_ = 0;
    successful_triangulation_ = false;
  }

  Landmark(Observation<CALIBRATION> obs) {
    observations_.push_back(obs);
    num_observations_ = 1;
    successful_triangulation_ = false;
  }

  void AddObservation(Observation<CALIBRATION> obs) {
    observations_.push_back(obs);
    num_observations_ += 1;
  }

  int GetNumObservations() const { return num_observations_; }

  vector<Observation<CALIBRATION>> &GetObservations() { return observations_; }

  void SetTriangulationFlag(bool flag) { successful_triangulation_ = flag; }

  void SetTriangulationPoint(Point3 point) { triangulated_point_ = point; }

  bool CheckTriangulation() const { return successful_triangulation_; }

  Point3 GetTriangulation() const { return triangulated_point_; }

  vector<Observation<CALIBRATION>> observations_;

 private:
  int num_observations_;

  bool successful_triangulation_;
  Point3 triangulated_point_;
};
