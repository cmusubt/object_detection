#ifndef OBJECT_DETECTION_MODULES_SIGNAL_LOCALIZER
#define OBJECT_DETECTION_MODULES_SIGNAL_LOCALIZER

#include <base/BaseNode.h>
#include <common/math.h>
#include <image_transport/image_transport.h>
#include <objdet_msgs/ArtifactLocalization.h>
#include <objdet_msgs/StampedReading.h>
#include <objdet_msgs/WirelessSignalReading.h>
#include <objdet_msgs/WirelessSignalReadingArray.h>
#include <sensor_msgs/Image.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <Eigen/Dense>
#include <cmath>
#include <unordered_map>
#include "common/ClassLabels.h"
#include "modules/CachedArtifact.h"
#include "modules/key_pose_listener.h"
#include "modules/signal_solvers.h"
#include "modules/state_estimate_delay_subscriber.h"

namespace object_detection {

class SignalLocalizer : public BaseNode {
 public:
  SignalLocalizer(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  ~SignalLocalizer() = default;

  std::vector<double> last_img_save_time;
  float img_save_period = 1.0;  // save imgs every __ seconds
  bool save_imgs = true;        // default, should be changed by launch files

 private:
  const std::string MAP_FRAME = "map";
  image_transport::ImageTransport it;

  // images captured recently, indexed by topic name
  std::vector<sensor_msgs::ImageConstPtr> recent_imgs;

  struct ReadingFromKeyPosePosition {
    tf2::Vector3 robot_position;
    uint32_t key_pose_index;
    float rssi;
    std::vector<sensor_msgs::ImageConstPtr> images;
  };

  struct SignalReadings {
    uint32_t id;
    std::vector<ReadingFromKeyPosePosition> readings;
  };

  enum class SignalType { WIFI, BLUETOOTH };

  void signal_cb(
      const objdet_msgs::WirelessSignalReadingArrayConstPtr& readings,
      const SignalType signal_type);

  void img_cb(const sensor_msgs::ImageConstPtr& msg,
              const std::size_t& topic_index);

  std::vector<ros::Subscriber> signal_subs;
  std::vector<image_transport::Subscriber> img_subs;
  ros::Publisher artifact_localizations_publisher;
  StateEstimateDelaySubscriber delay_subscriber;
  KeyPoseListener key_pose_listener{MAP_FRAME};

  tf2_ros::Buffer tf_buffer;
  std::unique_ptr<tf2_ros::TransformListener> tf_listener_ptr;

  std::unordered_map<std::string, SignalReadings> device_readings;
  std::vector<CachedArtifact> cached_artifacts;

  std::vector<std::string> img_topics;

  ClassLabels class_labels;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_SIGNAL_LOCALIZER
