#ifndef OBJECT_DETECTION_MODULES_DET_IMAGES_SUB_H
#define OBJECT_DETECTION_MODULES_DET_IMAGES_SUB_H

#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/connection.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include "objdet_msgs/DetectionArray.h"

namespace object_detection {

// Example (default) topics for a realsense:
//
//   /rs_right/color/image/detections
//   /rs_right/color/camera_info
//   /rs_right/color/image
//   /rs_right/aligned_depth_to_color/image
//
// Additional optional topics:
//   /rs_right/color/aligned_lidar/image
//   /rs_right/combined_depth/image
struct DetDepthSubTopics {
  static const std::string REALSENSE;
  static const std::string REALSENSE_LIDAR;
  static const std::string REALSENSE_COMBINED;
  static const std::string FLIR_LIDAR_DROP;
  static const std::string FLIR_LIDAR;

  void set_realsense(const std::string &rs_namespace) {
    det_topic = rs_namespace + "/color/image/detections";
    color_topic = rs_namespace + "/color/image";
    depth_topic = rs_namespace + "/aligned_depth_to_color/image";
  }

  void set_realsense_lidar(const std::string &camera_namespace) {
    std::size_t found = camera_namespace.find("camera");
    if (found != std::string::npos) {
      // dealing with ueye camera
      det_topic = camera_namespace + "/image_raw/detections";
      color_topic = camera_namespace + "/image_raw";
      depth_topic = camera_namespace + "/aligned_lidar/image";
    } else {
      // dealing with realsense camera
      set_realsense(camera_namespace);
      depth_topic = camera_namespace + "/color/aligned_lidar/image";
    }
  }

  void set_realsense_combined(const std::string &rs_namespace) {
    set_realsense(rs_namespace);
    depth_topic = rs_namespace + "/combined_depth/image";
  }

  void set_flir_lidar_drop(const std::string &flir_drop_namespace) {
    det_topic = flir_drop_namespace + "/image_rect_drop/detections";
    color_topic = flir_drop_namespace + "/image_rect_drop";
    depth_topic = flir_drop_namespace + "/aligned_lidar/image";
  }

  void set_flir_lidar(const std::string &flir_namespace) {
    det_topic = flir_namespace + "/image_rect/detections";
    color_topic = flir_namespace + "/image_rect";
    depth_topic = flir_namespace + "/aligned_lidar/image";
  }

  std::string det_topic;
  std::string color_topic;
  std::string depth_topic;
};

class DetDepthSub {
 public:
  DetDepthSub(ros::NodeHandle *nh, const DetDepthSubTopics &topics)
      : it(*nh),
        det_sub(*nh, topics.det_topic, 30),
        color_sub(it, topics.color_topic, 30),
        depth_sub(it, topics.depth_topic, 30),
        sync(SyncPolicy(30), det_sub, color_sub, depth_sub) {}

  template <class C>
  message_filters::Connection registerCallback(C &callback) {
    return sync.registerCallback(callback);
  }

  template <class C>
  message_filters::Connection registerCallback(const C &callback) {
    return sync.registerCallback(callback);
  }

  template <class C, typename T>
  message_filters::Connection registerCallback(C &callback, T *t) {
    return sync.registerCallback(callback, t);
  }

  template <class C, typename T>
  message_filters::Connection registerCallback(const C &callback, T *t) {
    return sync.registerCallback(callback, t);
  }

 private:
  using SyncPolicy = message_filters::sync_policies::ExactTime<
      objdet_msgs::DetectionArray, sensor_msgs::Image, sensor_msgs::Image>;

  image_transport::ImageTransport it;
  message_filters::Subscriber<objdet_msgs::DetectionArray> det_sub;
  image_transport::SubscriberFilter color_sub;
  image_transport::SubscriberFilter depth_sub;
  message_filters::Synchronizer<SyncPolicy> sync;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_DET_INFO_IMAGES_SUB_H