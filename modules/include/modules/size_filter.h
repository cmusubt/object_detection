#ifndef OBJECT_DETECTION_MODULES_SIZE_FILTER_H
#define OBJECT_DETECTION_MODULES_SIZE_FILTER_H

#include <string>
#include <unordered_map>
#include <vector>

namespace size_filter {
// Use 10% larger than the actual size of the object to make sure that
// we do not filter out the true positive artifacts
// Size is radius of the object
std::unordered_map<std::string, float> size_map_max({
    {"backpack", 0.25},           // 0.46m
    {"cellphone", 0.10},          // 0.18m
    {"fire extinguisher", 0.40},  // 0.60m
    {"survivor", 1.00},           // 1.80m
    {"helmet", 0.30},             // 0.30m
    {"vent", 0.75},               // 0.85m
    {"drill", 0.16},              // 0.30m
    {"cube", 0.15},               // 0.28m
});
// Use half of the actual size of the object to make sure that
// we do not filter out the true positive artifacts
// Size is radius of the object
// survivor and helmet are a bit smaller as sometimes we only see lights
// sometimes
std::unordered_map<std::string, float> size_map_min({
    {"backpack", 0.12},           // 0.46m
    {"cellphone", 0.05},          // 0.18m
    {"fire extinguisher", 0.15},  // 0.60m
    {"survivor", 0.25},           // 1.80m
    {"helmet", 0.02},             // 0.30m
    {"vent", 0.10},               // 0.85m
    {"drill", 0.07},              // 0.30m
    {"cube", 0.03},               // 0.28m
});
}  // namespace size_filter

#endif