#ifndef OBJECT_DETECTION_MODULES_COLOR_FILTER_H
#define OBJECT_DETECTION_MODULES_COLOR_FILTER_H

#include <string>
#include <unordered_map>
#include <vector>

namespace color_filter {
struct threshold {
  const int h_min, h_max, s_min, s_max, v_min, v_max;
  explicit threshold(int h_min, int h_max, int s_min, int s_max, int v_min,
                     int v_max)
      : h_min(h_min),
        h_max(h_max),
        s_min(s_min),
        s_max(s_max),
        v_min(v_min),
        v_max(v_max) {}
};
// minimum percentage of points compared to the original after the color
// filtering if less than this, we do not use color-based filtering
constexpr double MIN_PERCENTAGE = 0.04;

// We will use HSV not HSV_FULL, which means the range of H is [0,180], S,V is
// from [0,255]
std::unordered_map<std::string, std::vector<threshold>> color_map({
    {"backpack", // red
     {threshold(0, 10, 75, 255, 15, 255),
      threshold(160, 180, 75, 255, 15, 255)}},
    {"fire extinguisher", // red
     {threshold(0, 10, 75, 255, 15, 255),
      threshold(160, 180, 75, 255, 15, 255),
      threshold(0, 180, 0, 230, 60, 255)}},
    {"rope", // blue
     {threshold(90, 120, 75, 255, 15, 255)}},
    {"survivor", // yellow & white
     {threshold(0, 180, 0, 140, 120, 255), threshold(20, 50, 75, 255, 15, 255)}},
    {"helmet", // white
     {threshold(0, 180, 0, 230, 60, 255)}},
    {"vent", // white
     {threshold(0, 180, 0, 230, 60, 255)}},
    {"drill", // orange
     {threshold(170, 180, 75, 255, 15, 255),
      threshold(0, 20, 75, 255, 15, 255)}},
    {"cube", // bright light
     {threshold(0, 180, 0, 255, 60, 255)}},

});
}  // namespace color_filter

#endif