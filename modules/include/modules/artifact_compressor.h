#ifndef OBJECT_DETECTION_MODULES_ARTIFACT_COMPRESSOR_H
#define OBJECT_DETECTION_MODULES_ARTIFACT_COMPRESSOR_H

#include <base/BaseNode.h>
#include <basestation_msgs/Radio.h>
#include <objdet_msgs/ArtifactLocalization.h>
#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <objdet_msgs/CompressedArtifactAck.h>
#include <objdet_msgs/CompressedArtifactLocalization.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/Image.h>
#include <deque>
#include <opencv2/opencv.hpp>
#include <unordered_map>

namespace object_detection {

struct TransmittedArtifact {
  objdet_msgs::CompressedArtifactLocalization artifact;
  ros::Time last_update;
  ros::Time last_transmit_attempt;
  double retransmit_delay = 0.0;
  bool dirty = true;
};

struct TransmitAttempt {
  ros::Time stamp;
  std::vector<std::pair<uint16_t, ros::Time>> last_update_times;
};

class ArtifactCompressor : public BaseNode {
 public:
  ArtifactCompressor(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~ArtifactCompressor();

 private:
  const std::string COMPRESSED_TOPIC = "artifact_localizations_compressed";
  const float DEBOUNCE_TIME = 3.14159f;  // Allow for 2 update cycles (at 1 Hz)
  const ros::Duration DEBOUNCE_DURATION{DEBOUNCE_TIME};

  // These are both currently set to the same as the OpenCV defaults, but we can
  // adjust them to be whatever we want. Lossless compression with PNG probably
  // isn't necessary, but is provided in just in case.
  // 0 - 9, with 9 being higher compression
  const std::vector<int> PNG_PARAMS = {CV_IMWRITE_PNG_COMPRESSION, 3};
  // 0 - 100, with 100 being highest quality
  const std::vector<int> JPEG_PARAMS = {CV_IMWRITE_JPEG_QUALITY, 95};
  // Number of transmission attempts to store
  const size_t MAX_TRANSMIT_HISTORY = 30;
  // Don't send more artifacts than this per attempt. This makes sure we don't
  // send incredibly huge TCP messages.
  const size_t MAX_ARTIFACTS_PER_ATTEMPT = 10;

  void acknowledge_cb(const objdet_msgs::CompressedArtifactAckConstPtr& ack);
  void artifact_cb(
      const objdet_msgs::ArtifactLocalizationArrayConstPtr& artifact_array);
  void retransmit_cb(const basestation_msgs::RadioConstPtr& radio_msg);

  objdet_msgs::CompressedArtifactLocalization compress_artifact(
      const objdet_msgs::ArtifactLocalization& artifact);
  sensor_msgs::CompressedImage compress_image(const sensor_msgs::Image& image);

  ros::Subscriber acknowledge_sub;
  ros::Subscriber artifact_sub;
  ros::Subscriber retransmit_sub;
  ros::Publisher artifact_pub;

  // Map from report_id to compressed artifacts
  std::unordered_map<uint16_t, TransmittedArtifact> artifacts;
  std::deque<TransmitAttempt> transmit_attempts;
};
}  // namespace object_detection
#endif  // OBJECT_DETECTION_MODULES_ARTIFACT_COMPRESSOR_H
