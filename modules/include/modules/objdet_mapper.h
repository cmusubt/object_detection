#ifndef OBJECT_DETECTION_MODULES_OBJDET_MAPPER_H
#define OBJECT_DETECTION_MODULES_OBJDET_MAPPER_H

#include <base/BaseNode.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <pcl/common/centroid.h>
#include <pcl/segmentation/extract_clusters.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <array>
#include <atomic>
#include <memory>
#include <mutex>
#include <unordered_map>
#include <vector>
#include "modules/det_images_sub.h"
#include "modules/det_point.h"
#include "modules/key_pose.h"
#include "modules/point_cloud_renderer.h"
#include "modules/state_estimate_delay_subscriber.h"
#include "objdet_msgs/DetectionArray.h"

namespace object_detection {
class ObjdetMapper : public BaseNode {
 public:
  ObjdetMapper(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~ObjdetMapper();

 private:
  // Which frame to create the artifact map with respect to. Key poses and robot
  // poses need to have a transform to this frame. Most (if not all) outgoing
  // data from this node is published in this frame.
  const std::string MAP_FRAME = "map";

  // Parameters for how much the camera must move before being used
  const float MIN_CAMERA_MOVEMENT = .05;  // meters
  const float MIN_CAMERA_MOVEMENT_SQ =
      MIN_CAMERA_MOVEMENT * MIN_CAMERA_MOVEMENT;

  // Parameters to convert bounding box to point cloud
  struct CloudsFromDetsParams {
    int step_size;    // downsampling factor
    float max_depth;  // meters
  };
  // TODO(vasua): Potentially get these parameters from a launch file instead
  // Designated initializers are a c++2a (and C99) feature, but supported by
  // GCC and Clang.
  std::unordered_map<std::string, CloudsFromDetsParams>
      clouds_from_dets_params = {
          {DetDepthSubTopics::REALSENSE, {.step_size = 4, .max_depth = 2.5f}},
          {DetDepthSubTopics::REALSENSE_LIDAR,
           {.step_size = 2, .max_depth = 60.0f}},
          {DetDepthSubTopics::REALSENSE_COMBINED,
           {.step_size = 4, .max_depth = 60.0f}},
          {DetDepthSubTopics::FLIR_LIDAR_DROP,
           {.step_size = 2, .max_depth = 60.0f}},
          {DetDepthSubTopics::FLIR_LIDAR,
           {.step_size = 2, .max_depth = 60.0f}}};

  // Parameters for clustering points together
  const float CLUSTER_TOLERANCE = 2.0f;  // meters
  const int MIN_CLUSTER_SIZE = 3;
  const int MAX_CLUSTER_SIZE = 1000;


  // Parameters for slam threshold
  const float MATCH_THRESHOLD_SLAM = 5.0f; //.25f;  // meters
  const float MATCH_THRESHOLD_SLAM_SQ = MATCH_THRESHOLD_SLAM * MATCH_THRESHOLD_SLAM;
  // Parameters for matching clusters together (deduping)
  const float MATCH_THRESHOLD_SAMELABEL = 3.0f; //.25f;  // meters
  const float MATCH_THRESHOLD_SAMELABEL_SQ = MATCH_THRESHOLD_SAMELABEL * MATCH_THRESHOLD_SAMELABEL;

  const float MATCH_THRESHOLD_DIFFLABEL = 0.75f; //.25f;  // meters
  const float MATCH_THRESHOLD_DIFFLABEL_SQ = MATCH_THRESHOLD_DIFFLABEL * MATCH_THRESHOLD_DIFFLABEL;

  static const size_t NUM_BOX_COLORS = 22;
  static const std::array<cv::Scalar, NUM_BOX_COLORS> BOX_COLORS;

  // Time delay estimator between state estimation and system time
  StateEstimateDelaySubscriber delay_subscriber;

  // Store information about a new key pose.
  //
  // This currently only handles new key poses, not loop closures.
  void key_pose_cb(const nav_msgs::OdometryConstPtr &key_pose);

  // Update stored information about key poses with loop closure information.
  void key_pose_path_cb(const nav_msgs::PathConstPtr &key_pose_path);

  // Main callback for handling a detection.
  //
  // For each detection in the image, convert it into a point cloud, and then
  // transform that point cloud into the latest key pose frame in order to
  // support loop closures
  void det_images_cb(const objdet_msgs::DetectionArrayConstPtr &dets_msg,
                     const sensor_msgs::ImageConstPtr &color_msg,
                     const sensor_msgs::ImageConstPtr &aligned_msg,
                     const image_transport::Publisher &image_pub,
                     const CloudsFromDetsParams &params,
                     const float &min_topic_dist, const std::string &topic);

  // Publish a pizza slice shaped marker representing FOV of input camera.
  //
  // The pizza slice has radius specified by the max depth in the params. The
  // idea is to get a sense of how far the cameras have seen.
  void publish_sensor_range_marker(
      const sensor_msgs::CameraInfoConstPtr &info_msg,
      const CloudsFromDetsParams &params);

  // Determine whether the camera has moved enough since the last detection.
  //
  // This primarily serves to reject spurious detections when the robot is
  // standing still (due to a poor network), but can also help slightly throttle
  // the number of detections made when the robot is driving by an artifact.
  bool camera_moved_enough(const std::string &camera_frame,
                           const tf2::Transform &camera_to_map_tf);

  // Callback to generate artifact detections (run every second).
  //
  // This transforms all of the recorded point clouds and centroids into the map
  // frame, creates artifact localizations from them, and then dedupes against
  // the existing artifact localizations.
  void timer_cb(const ros::TimerEvent &event);

  // Get the key pose corresponding to the specified timestamp.
  //
  // Specifically, the key pose returned will be the nearest key pose whose
  // timestamp is strictly less than the query time. This method is threadsafe.
  std::shared_ptr<KeyPose> get_key_pose_at_time(const ros::Time &time);

  // Convert 2d detections into point clouds.
  //
  // For every detection in the input array, convert it to a point cloud by back
  // projecting the rgb information through the intrinsics. Points are only
  // considered where depth data is provided. Additionally, this method
  // generates a centroid for each detection. Return values are added to the
  // det_clouds and det_centroids. Subsampling and max distance can
  // be controlled by the input parameters.
  std::vector<int> get_clouds_from_dets(const objdet_msgs::DetectionArrayConstPtr &dets_msg,
                            const sensor_msgs::CameraInfoConstPtr &info_msg,
                            const sensor_msgs::ImageConstPtr &color_msg,
                            const sensor_msgs::ImageConstPtr &aligned_msg,
                            const image_transport::Publisher &image_pub,
                            const CloudsFromDetsParams &params,
                            std::vector<DetCloud::Ptr> &det_clouds,
                            DetCloud::Ptr &det_centroids,
                            const float &min_topic_dist);

  // Cluster detection centroids together.
  //
  // This is the heart of the artifact identification. For efficiency, the
  // clustering happens directly on the detection centroids, rather than
  // happening on the complete global point cloud. The centroids are clustered
  // purely by their distance in Euclidean space, and no class labels are
  // considered.
  //
  // A vector of vector of indices is returned, with each inner vector
  // containing indices of input det_centroids which belong to the same cluster.
  std::vector<pcl::PointIndices> cluster_det_centroids(
      const DetCloud::Ptr &centroids);

  // Group images by indices.
  //
  // Use the cluster indices to group images into a vector of images, where each
  // vector corresponds to a single cluster.
  DetImagePairsList group_images_by_indices(
      const std::vector<pcl::PointIndices> &cluster_indices,
      DetImagePairsList &images);

  // Group det clouds by indices.
  //
  // Use the indices to group the input global det clouds together, such that
  // each cluster has a single point cloud associated with it.
  std::vector<DetCloud::Ptr> group_det_clouds_by_indices(
      const std::vector<pcl::PointIndices> &cluster_indices,
      const std::vector<DetCloud::Ptr> &global_det_clouds);

  // Group labels by indices to indicate the detection label of each image. 
  std::vector<std::vector<int>> group_labels_by_indices(
      const std::vector<pcl::PointIndices> &cluster_indices,
      const DetCloud::Ptr &global_det_centroids);

  // Returns a centroid for each point cloud in input vector.
  //
  // The centroids are all in the same point cloud, which is just being used as
  // a convenient wrapper around a vector here.
  DetCloud::Ptr get_centroids_from_clouds(
      const std::vector<DetCloud::Ptr> &clouds);

  // Update artifact locations if something's changed.
  //
  // This tries to match the input artifact data with existing artifacts.
  // New artifacts which match existing ones update the existing ones. Existing
  // artifacts which don't match new ones are removed, and the new ones are
  // added in their place.
  void update_artifact_locations(
      const DetCloud::Ptr &artifact_centroids,
      const DetImagePairsList &artifact_images,
      const std::vector<DetCloud::Ptr> &artifact_clouds,
      const std::vector<std::vector<int>> &label_lists);

  // Publish artifact locations.
  //
  // For efficiency, only updated artifact locations are published. Subscribers
  // are expected to maintain their own list of artifacts based on the deltas
  // published over this topic.
  void publish_artifact_locations();

  // For debugging, publish the global detection cloud.
  //
  // This point cloud contains a combination of all of the points that went into
  // the object detections, as a single cloud.
  void publish_global_det_cloud(
      const std::vector<DetCloud::Ptr> &global_det_clouds);

  // For the user, draw images on the global detection cloud.
  //
  // The input image is modified.
  void draw_detection_on_image(cv::Mat &image,
                               const objdet_msgs::Detection &det,
                               const std::string &topic);

  // Create a cropped image
  //
  // The input image is modified
  void create_zoomed_image(cv::Mat& image,
                           const objdet_msgs::Detection& det);

  // computed square distance between two geometry_msgs::Point
  double distancesquare(const geometry_msgs::Point& p1, const geometry_msgs::Point& p2);

  // ROS subscribers and publishers
  // Thread 1
  std::vector<std::unique_ptr<DetDepthSub>> det_image_subs;
  ros::Subscriber key_pose_sub;
  ros::Subscriber key_pose_path_sub;
  ros::Publisher sensing_range_publisher;
  ros::Publisher local_map_publisher;
  // Thread 2
  ros::Publisher global_map_publisher;
  ros::Publisher global_centroid_publisher;
  ros::Publisher cluster_publisher;
  ros::Publisher artifact_localizations_publisher;

  tf2_ros::Buffer tf_buffer;
  std::unique_ptr<tf2_ros::TransformListener> tf_listener_ptr;
  ros::Timer global_map_gen_timer;

  std::mutex key_poses_mutex;  // Guards all key_pose* members
  std::unordered_map<uint32_t, std::shared_ptr<KeyPose>> key_pose_ptrs;
  std::map<uint32_t, ros::Time> key_pose_timestamps;

  std::unordered_map<std::string, tf2::Transform> last_detection_tfs;
  std::vector<ClusteredDetPoint> artifact_locations;

  std::unordered_map<std::string, sensor_msgs::CameraInfoConstPtr>
      camera_info_list;
  std::map<std::string, std::string> sub_topics;

  // camodocal::CameraPtr camera;
  PointCloudRenderer pc_rend;

  // for color filter
  image_transport::ImageTransport it;
  std::vector<image_transport::Publisher>
      color_filtered_image_pubs;  // publish the color filtered image for debug
  bool USE_COLOR = true;
  bool COLOR_FORCE_FILTER = true;
  bool USE_SIZE = true;
  bool USE_DEPTH_FILTER = true;
  double SIZE_THRESHOLD =
      0.05;  // percentage of points size is outside the size range
  int MINIMUM_PIXELS =
      30;  // mininum number of pixels that each detection should contain
    
  // detection confidence threshold
  float CONFIDENCE_THRESHOLD = 0.3;
  bool LOGGING_DEBUG_INFO = true;
  bool PUBLISH_DEBUG_MSGS = false;

  // for use Median instead of centroid
  bool USE_MEDIAN = false;
  DetCloud::Ptr get_median_from_clouds(
      const std::vector<DetCloud::Ptr> &clouds);
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_OBJDET_MAPPER_H
