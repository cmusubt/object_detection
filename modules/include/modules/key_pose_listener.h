#ifndef OBJECT_DETECTION_MODULES_KEY_POSE_LISTENER_H
#define OBJECT_DETECTION_MODULES_KEY_POSE_LISTENER_H

#include <common/tf2.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <optional>
#include <unordered_map>

namespace object_detection {
class KeyPoseListener {
 public:
  KeyPoseListener(std::string map_frame) : map_frame(map_frame) {}

  void initialize(ros::NodeHandle* nh, tf2_ros::Buffer* tf_buffer) {
    this->tf_buffer = tf_buffer;
    key_pose_sub = nh->subscribe("key_pose_to_map", 10,
                                 &KeyPoseListener::key_pose_cb, this);
    key_pose_path_sub = nh->subscribe("key_pose_path", 10,
                                      &KeyPoseListener::key_pose_path_cb, this);
  }

  void key_pose_cb(const nav_msgs::OdometryConstPtr& key_pose) {
    // This block is (largely) the same as what's in objdet_mapper.
    // TODO(vasua): Consolidate the key pose (and key pose update) functionality

    // Index encoded as first element of covariance matrix
    const auto key_pose_index =
        static_cast<uint32_t>(key_pose->pose.covariance[0]);
    const auto& stamp = key_pose->header.stamp;
    // ROS_INFO("got a key pose: %u", key_pose_index);

    // The key poses aren't necessarily in the MAP_FRAME (usually /map) as
    // they're usually in /map_rot. Since a geometry_msgs/Pose message doesn't
    // store any frame information, we must transform the Pose here.
    geometry_msgs::Pose map_frame_key_pose;
    try {
      const auto key_pose_to_map_tf = tf_buffer->lookupTransform(
          map_frame, sanitize_frame(key_pose->header.frame_id),
          key_pose->header.stamp);
      tf2::doTransform(key_pose->pose.pose, map_frame_key_pose,
                       key_pose_to_map_tf);
    } catch (tf2::TransformException& e) {
      ROS_WARN("Error looking up Key Pose TF: %s", e.what());
      return;
    }

    key_poses[key_pose_index] = map_frame_key_pose;
    key_pose_stamps[key_pose_index] = key_pose->header.stamp;

    // Sanity check that we're not getting key poses out of order. If we do,
    // something's gone wrong (e.g. system time might have jumped too much).
    assert(std::is_sorted(key_pose_stamps.begin(), key_pose_stamps.end(),
                          [](const auto& left, const auto& right) {
                            return left.second < right.second;
                          }));
  }

  void key_pose_path_cb(const nav_msgs::PathConstPtr& key_pose_path) {
    // ROS_INFO_STREAM("Got a key pose path with size "
    //                 << key_pose_path->poses.size());

    geometry_msgs::TransformStamped key_pose_to_map_tf;
    try {
      key_pose_to_map_tf = tf_buffer->lookupTransform(
          map_frame, sanitize_frame(key_pose_path->header.frame_id),
          key_pose_path->header.stamp);
    } catch (tf2::TransformException& e) {
      ROS_WARN("Error looking up Key Pose Path TF: %s", e.what());
      return;
    }

    for (size_t i = 0; i < key_pose_path->poses.size(); ++i) {
      if (key_poses.count(i) == 0) {
        // Don't do work for key poses that aren't already stored
        continue;
      }

      auto& key_pose = key_poses.at(i);
      tf2::doTransform(key_pose_path->poses[i].pose, key_pose,
                       key_pose_to_map_tf);
    }
  }

  const geometry_msgs::Pose& get_key_pose_by_index(
      const uint32_t key_pose_index) {
    return key_poses.at(key_pose_index);
  }

  std::optional<uint32_t> get_key_pose_index_at_time(const ros::Time& time) {
    for (auto it = key_pose_stamps.rbegin(); it != key_pose_stamps.rend();
         ++it) {
      if (it->second <= time) {
        return it->first;
      }
    }

    return std::nullopt;
  }

 private:
  const std::string map_frame;

  // tf buffers are threadsafe, so it doesn't matter what thread we're on when
  // we query for transform information.
  // https://answers.ros.org/question/313498/is-it-safe-and-efficient-for-several-threads-to-share-a-single-tftransformlistener-object/
  tf2_ros::Buffer* tf_buffer;
  ros::Subscriber key_pose_sub;
  ros::Subscriber key_pose_path_sub;

  // TODO(vasua): Maybe this should just be a map of PoseStamped instead ...
  std::unordered_map<uint32_t, geometry_msgs::Pose> key_poses;
  std::map<uint32_t, ros::Time> key_pose_stamps;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_KEY_POSE_LISTENER_H
