#ifndef OBJECT_DETECTION_MODULES_STATE_ESTIMATE_DELAY_SUBSCRIBER_H
#define OBJECT_DETECTION_MODULES_STATE_ESTIMATE_DELAY_SUBSCRIBER_H
#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <atomic>

namespace object_detection {
class StateEstimateDelaySubscriber {
 public:
  void initialize(ros::NodeHandle* nh) {
    delay_sub = nh->subscribe("state_estimate_delay", 10,
                              &StateEstimateDelaySubscriber::delay_cb, this);
  }

  double get_delay() { return delay.load(std::memory_order_acquire); }

  ros::Duration get_delay_duration() { return ros::Duration(get_delay()); }

 private:
  void delay_cb(const std_msgs::Float64ConstPtr& new_delay) {
    delay.store(new_delay->data, std::memory_order_release);
    return;
  }

  ros::Subscriber delay_sub;
  std::atomic<double> delay{0};
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_STATE_ESTIMATE_DELAY_SUBSCRIBER_H
