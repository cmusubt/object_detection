#ifndef OBJECT_DETECTION_MODULES_CACHED_ARTIFACT
#define OBJECT_DETECTION_MODULES_CACHED_ARTIFACT

// For use with the gas_localizer and signal_localizer
#include "modules/signal_solvers.h"

namespace object_detection {

class CachedArtifact {
  const float DIRTY_DISTANCE = 0.05f;  // meters
  const float DIRTY_DISTANCE_SQ = DIRTY_DISTANCE * DIRTY_DISTANCE;

 public:
  CachedArtifact(uint16_t report_id)
      : stamp(ros::Time::now()), report_id(report_id) {}

  bool update_valid(const bool new_valid) {
    if (valid != new_valid) {
      valid = new_valid;
      dirty = true;
      return true;
    }

    return false;
  }

  bool update_position(const Eigen::Vector3f& new_position) {
    // Use a distance threshold to disregard spurious updates when key poses
    // don't change much / at all.
    if ((new_position - position).squaredNorm() > DIRTY_DISTANCE_SQ) {
      position = new_position;
      dirty = true;
      return true;
    }

    return false;
  }

  bool update_confidence(const float new_confidence) {
    if (confidence != new_confidence) {
      confidence = new_confidence;
      dirty = true;
      return true;
    }
    return false;
  }

  bool update_class_id(const uint32_t new_class_id) {
    if (class_id != new_class_id) {
      class_id = new_class_id;
      dirty = true;
      return true;
    }
    return false;
  }

  void update_imgs(const std::vector<sensor_msgs::ImageConstPtr>& img_msg) {
    images = img_msg;
  }

  std::vector<sensor_msgs::Image> get_images(
      const std::vector<sensor_msgs::ImageConstPtr>& img_ptrs) const {
    std::vector<sensor_msgs::Image> new_vector;

    for (const sensor_msgs::ImageConstPtr img_ptr : img_ptrs) {
      if (img_ptr) {
        new_vector.emplace_back(*img_ptr);
      }
    }

    return new_vector;
  }

  void update_readings(
      const std::vector<signal_solvers::ReadingFromPosition>& solver_readings) {
    // update the array of concentration.position (or rssi.position) pairs to be
    // published in the localization message

    bool different_reading;

    if (solver_readings.size() == artifact_readings.size()) {
      different_reading = false;

      // compare element-wise. must be a for loop over
      // each element because solver_readings and
      // artifact_readings contain different types of elements

      for (std::size_t i = 0; i < artifact_readings.size(); ++i) {
        // no need to sort because these are vectors which will not change order
        if (!compare_reading(solver_readings[i], artifact_readings[i])) {
          // if two readings are not the same, we need to update this the
          // readings for this artifact
          different_reading = true;
          continue;
        }
      }
    } else {
      different_reading = true;
    }

    // if none of the artifacts are different, no need to update
    if (!different_reading) {
      return;
    }

    // Must be dirty if there's new readings
    dirty = true;

    artifact_readings.clear();
    artifact_readings.reserve(solver_readings.size());

    for (const auto& reading : solver_readings) {
      // C++17 allows emplace_back to return a reference to an
      // in-place constructed object.
      auto& new_reading = artifact_readings.emplace_back();

      new_reading.header.frame_id = MAP_FRAME;

      new_reading.pose.position.x = reading.robot_position[0];
      new_reading.pose.position.y = reading.robot_position[1];
      new_reading.pose.position.z = reading.robot_position[2];

      new_reading.reading = reading.raw_reading;
    }
  }

  bool compare_reading(signal_solvers::ReadingFromPosition reading_pos,
                       objdet_msgs::StampedReading reading_stamped) {
    // returns true if they're the same reading, else false

    Eigen::Vector3f reading_stamped_pos;
    reading_stamped_pos[0] = reading_stamped.pose.position.x;
    reading_stamped_pos[1] = reading_stamped.pose.position.y;
    reading_stamped_pos[2] = reading_stamped.pose.position.z;

    if ((reading_pos.robot_position - reading_stamped_pos).squaredNorm() >
        DIRTY_DISTANCE_SQ) {
      return false;
    }

    // if we reach here, the distance is very similar, so now we check the
    // concentration (or rssi)
    if (std::abs(reading_pos.raw_reading - reading_stamped.reading) > 1E-3) {
      return false;
    }

    return true;
  }

  bool get_dirty() const { return dirty; }

  void clean() { dirty = false; }

  auto get_artifact() const {
    objdet_msgs::ArtifactLocalization artifact;
    artifact.stamp = stamp;
    artifact.valid = valid;
    artifact.x = position[0];
    artifact.y = position[1];
    artifact.z = position[2];
    artifact.confidence = confidence;
    artifact.class_id = class_id;
    artifact.report_id = report_id;
    artifact.images = get_images(images);
    artifact.readings = artifact_readings;
    artifact.device_code = device_code;
    return artifact;
  }

    std::string device_code;
    
 private:
  // Duplicate most of the fields from ArtifactLocalization.msg
  // These two won't change once the artifact is created
  const ros::Time stamp;
  const uint16_t report_id;

  bool valid = false;
  Eigen::Vector3f position = {0, 0, 0};
  float confidence = 0.0f;
  uint32_t class_id = 0;

  bool dirty = false;

  const std::string MAP_FRAME = "map";

  // images of near max_concentration/rssi location
  std::vector<sensor_msgs::ImageConstPtr> images;

  std::vector<objdet_msgs::StampedReading> artifact_readings;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_CACHED_ARTIFACT
