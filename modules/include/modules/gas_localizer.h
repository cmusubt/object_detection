#ifndef OBJECT_DETECTION_MODULES_GAS_LOCALIZER
#define OBJECT_DETECTION_MODULES_GAS_LOCALIZER

#include <base/BaseNode.h>
#include <common/math.h>
#include <image_transport/image_transport.h>
#include <objdet_msgs/ArtifactLocalization.h>
#include <objdet_msgs/GasInfo.h>
#include <objdet_msgs/StampedReading.h>
#include <sensor_msgs/Image.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <Eigen/Dense>
#include <cmath>
#include <unordered_map>
#include "common/ClassLabels.h"
#include "modules/CachedArtifact.h"
#include "modules/key_pose_listener.h"
#include "modules/signal_solvers.h"
#include "modules/state_estimate_delay_subscriber.h"
#include "std_msgs/Bool.h"

namespace object_detection {
class GasLocalizer : public BaseNode {
 public:
  GasLocalizer(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  ~GasLocalizer() = default;

  std::vector<double> last_img_save_time;
  float img_save_period = 1.0;  // save imgs every __ seconds
  bool save_imgs = true;        // default, should be changed by launch files
  // below is set true for drones to change the MIN_CONCENTRATION for gas
  // to be dependent on the gas level after takeoff. Basically, it does
  // MIN_CONCENTRATION = steady-state-level + gas_buffer
  bool adaptive_thresholding = false;
  bool adaptive_thresholding_ugv = false;
  float gas_buffer = 500;
  bool gas_timer_waiting = false;
  float steady_state_gas = 500; // this way if we dont get a reading, default conc. is 1000
  ros::Duration sensor_detection_time = ros::Duration(
      1.0f);  // how long the delay is from physical gas to sensor reading

  std::string vehicle_type;          // to determine how to find the ground
  std::string downward_depth_topic;  // to determine height of drone
  double last_depth_img_save_time;
  float depth_img_save_period = 0.5;

 private:
  const std::string MAP_FRAME = "map";
  image_transport::ImageTransport it;

  // rgb images captured recently, indexed by topic name
  std::vector<sensor_msgs::ImageConstPtr> recent_imgs;

  // downwards depth img, used for drone height
  sensor_msgs::ImageConstPtr downward_depth_img;

  struct ReadingFromKeyPosePosition {
    tf2::Vector3 robot_position;         // in key pose frame
    tf2::Vector3 reading_pos_map_frame;  // in map frame
    uint32_t key_pose_index;
    float concentration;
    std::vector<sensor_msgs::ImageConstPtr> images;
  };

  struct GasReadings {
    uint32_t id;
    std::vector<ReadingFromKeyPosePosition> readings;
  };

  struct ClosestReading {
    int id;
    float dist;
  };

  ClosestReading find_closest_reading(const tf2::Vector3& new_reading_pos);

  int gen_new_cloud();

  void gas_reading_cb(const objdet_msgs::GasInfoConstPtr& reading);

  void rgb_img_cb(const sensor_msgs::ImageConstPtr& msg,
                  const std::size_t& topic_index);

  // to determine the height above the ground for the drones
  void depth_img_cb(const sensor_msgs::ImageConstPtr& msg);

  float euclid_dist(const tf2::Vector3& point_a, const tf2::Vector3& point_b);

  std::vector<ros::Subscriber> gas_reading_subs;
  std::vector<image_transport::Subscriber> img_subs;
  image_transport::Subscriber depth_img_sub;
  ros::Publisher artifact_localizations_publisher;
  StateEstimateDelaySubscriber delay_subscriber;
  KeyPoseListener key_pose_listener{MAP_FRAME};

  tf2_ros::Buffer tf_buffer;
  std::unique_ptr<tf2_ros::TransformListener> tf_listener_ptr;

  std::vector<GasReadings> gas_readings;
  std::vector<CachedArtifact> cached_artifacts;

  std::vector<std::string> img_topics;

  // minimum distance between readings before
  // we call it a new cloud
  float CLUSTER_SPLIT_DIST = 10;

  // we'll always get readings of ambient co2 concentrations,
  // let's make sure it actually is some gas source
  float MIN_CONCENTRATION = 1000;

  // mapping from class ids to readable strings
  ClassLabels class_labels;

  // gas localization point is on the ground, so we need
  // some logic for that
  float UGV_VELODYNE_HEIGHT = 0.9;
  float find_ground(const ReadingFromKeyPosePosition& gas_reading);
  float get_drone_height();

  ros::Subscriber in_air_sub;
  void adaptive_threshold_timer_cb(const ros::TimerEvent& event);
  void in_air_cb(const std_msgs::BoolConstPtr& msg);
  ros::Timer adaptive_gas_timer;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_SIGNAL_LOCALIZER
