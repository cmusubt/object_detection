#ifndef OBJECT_DETECTION_MODULES_DEPTH_LIDAR_SUB_H
#define OBJECT_DETECTION_MODULES_DEPTH_LIDAR_SUB_H

#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/connection.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>

namespace object_detection {
class DepthLidarSub {
 public:
  DepthLidarSub(ros::NodeHandle* nh, const std::string& depth_image_topic,
                const std::string& lidar_image_topic)
      : it(*nh),
        depth_sub(it, depth_image_topic, 30),
        lidar_sub(it, lidar_image_topic, 30),
        sync(SyncPolicy(30), depth_sub, lidar_sub) {}

  template <class C>
  message_filters::Connection registerCallback(C& callback) {
    return sync.registerCallback(callback);
  }

  template <class C>
  message_filters::Connection registerCallback(const C& callback) {
    return sync.registerCallback(callback);
  }

  template <class C, typename T>
  message_filters::Connection registerCallback(C& callback, T* t) {
    return sync.registerCallback(callback, t);
  }

  template <class C, typename T>
  message_filters::Connection registerCallback(const C& callback, T* t) {
    return sync.registerCallback(callback, t);
  }

 private:
  using SyncPolicy =
      message_filters::sync_policies::ExactTime<sensor_msgs::Image,
                                                sensor_msgs::Image>;

  image_transport::ImageTransport it;
  image_transport::SubscriberFilter depth_sub;
  image_transport::SubscriberFilter lidar_sub;
  message_filters::Synchronizer<SyncPolicy> sync;
};

}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_DEPTH_LIDAR_SUB_H
