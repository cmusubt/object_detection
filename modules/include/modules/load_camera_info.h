#ifndef OBJECT_DETECTION_MODULES_LOAD_CAMERA_INFO_H
#define OBJECT_DETECTION_MODULES_LOAD_CAMERA_INFO_H

#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <opencv2/core.hpp>
#include <vector>

namespace object_detection {

void readSequence(const cv::FileNode f, std::vector<double> &result) {
  if (f.type() != cv::FileNode::SEQ) {
    ROS_ERROR("Load camera info fail! Some of the entry is not sequence!");
    return;
  }

  auto it = f.begin();
  auto it_end = f.end();
  for (; it != it_end; ++it) {
    result.emplace_back(static_cast<double>(*it));
  }
}

template <typename T, size_t N>
void readSequence(const cv::FileNode f, boost::array<T, N> &result) {
  if (f.type() != cv::FileNode::SEQ) {
    ROS_ERROR("Load camera info fail! Some of the entry is not sequence!");
    return;
  }
  if (f.size() != result.size()) {
    ROS_ERROR(
        "Load camera info fail! Some of the entry do not have required size!");
    return;
  }

  auto it_begin = f.begin();
  auto it_end = f.end();
  for (auto it = it_begin; it != it_end; ++it) {
    result[it - it_begin] = static_cast<T>(*it);
  }
}

static void read(const cv::FileNode &node, uint32_t &x,
                 const uint32_t &default_value = 0) {
  if (node.empty()) {
    x = default_value;
  } else {
    x = static_cast<uint32_t>(static_cast<int>(*(node.begin())));
  }
}

static void read(const cv::FileNode &node, sensor_msgs::RegionOfInterest &x,
                 const sensor_msgs::RegionOfInterest &default_value =
                     sensor_msgs::RegionOfInterest()) {
  if (node.empty()) {
    x = default_value;
  } else {
    node["do_rectify"] >> x.do_rectify;
    read(node["height"], x.height);
    read(node["width"], x.width);
    read(node["x_offset"], x.x_offset);
    read(node["y_offset"], x.y_offset);
  }
}

static void read(const cv::FileNode &node, std_msgs::Header &x,
                 const std_msgs::Header &default_value = std_msgs::Header()) {
  if (node.empty()) {
    x = default_value;
  } else {
    x.stamp = ros::Time::now();
    node["frame_id"] >> x.frame_id;
  }
}

sensor_msgs::CameraInfoConstPtr loadCameraInfo(const std::string &filename) {
  sensor_msgs::CameraInfoPtr msg_ptr(new sensor_msgs::CameraInfo());
  cv::FileStorage fs;
  fs.open(filename, cv::FileStorage::READ);
  if (!fs.isOpened()) {
    ROS_ERROR("Could not load camera information. Could be \"%s\" not exists.",
              filename.c_str());
    return nullptr;
  }

  read(fs["header"], msg_ptr->header);
  read(fs["height"], msg_ptr->height);
  read(fs["width"], msg_ptr->width);
  fs["distortion_model"] >> msg_ptr->distortion_model;
  readSequence(fs["D"], msg_ptr->D);
  readSequence(fs["K"], msg_ptr->K);
  readSequence(fs["R"], msg_ptr->R);
  readSequence(fs["P"], msg_ptr->P);
  read(fs["binning_x"], msg_ptr->binning_x);
  read(fs["binning_y"], msg_ptr->binning_y);
  read(fs["roi"], msg_ptr->roi);

  return msg_ptr;
}

}  // namespace object_detection

#endif