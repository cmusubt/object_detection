#ifndef OBJECT_DETECTION_MODULES_ARTIFACT_UNCOMPRESSOR_H
#define OBJECT_DETECTION_MODULES_ARTIFACT_UNCOMPRESSOR_H

#include <base/BaseNode.h>
#include <objdet_msgs/ArtifactLocalization.h>
#include <objdet_msgs/CompressedArtifactLocalizationArray.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/Image.h>

namespace object_detection {

class ArtifactUncompressor : public BaseNode {
 public:
  ArtifactUncompressor(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~ArtifactUncompressor();

 private:
  void artifact_cb(
      const objdet_msgs::CompressedArtifactLocalizationArrayConstPtr& msg);
  objdet_msgs::ArtifactLocalization uncompress_artifact(
      const objdet_msgs::CompressedArtifactLocalization& compressed);
  sensor_msgs::Image uncompress_image(
      const sensor_msgs::CompressedImage& compressed);

  ros::Subscriber artifact_sub;
  ros::Publisher artifact_pub;
  ros::Publisher acknowledge_pub;
};
}  // namespace object_detection
#endif  // OBJECT_DETECTION_MODULES_ARTIFACT_UNCOMPRESSOR_H
