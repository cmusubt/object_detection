#ifndef OBJECT_DETECTION_MODULES_DET_POINT_H
#define OBJECT_DETECTION_MODULES_DET_POINT_H

#include <common/ranges.h>
#include <pcl/common/geometry.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <ros/time.h>
#include <algorithm>

namespace object_detection {
using DetPoint = pcl::PointXYZRGBL;
using DetCloud = pcl::PointCloud<DetPoint>;
using DetCloudPtr = DetCloud::Ptr;
using DetImages = std::vector<sensor_msgs::ImageConstPtr>;
using DetImagePairs = std::vector<std::pair<sensor_msgs::ImageConstPtr, sensor_msgs::ImageConstPtr>>;
using DetImagesList = std::vector<DetImages>;
using DetImagePairsList = std::vector<DetImagePairs>;

inline bool operator==(const DetPoint& lhs, const DetPoint& rhs) {
  // Using exact comparison for floats, which should be fine in this context.
  return (pcl::geometry::squaredDistance(lhs, rhs) <= (1e-4) * (1e-4)) &&
         (lhs.r == rhs.r) && (lhs.g == rhs.g) && (lhs.b == rhs.b) &&
         (lhs.a == rhs.a) && (lhs.label == rhs.label);
}

inline bool operator!=(const DetPoint& lhs, const DetPoint& rhs) {
  return !(lhs == rhs);
}

class ClusteredDetPoint {
  const size_t DISPLAY_IMAGES = 3;
  const float DIRTY_DISTANCE = 0.05f;  // meters
  const float DIRTY_DISTANCE_SQ = DIRTY_DISTANCE * DIRTY_DISTANCE;
  const float COLOR_THRESHOLD = 2;  // Color's allowed to vary a little

 public:
  ClusteredDetPoint(const DetPoint& point, const ros::Time& stamp,
                    bool valid = true)
      : point(point), stamp(stamp), valid(valid){};
  void set_point(const DetPoint& new_point) {
    // Allow for a slight bit of error when updating the point to account for
    // floating point error and such so that we don't flood the system with
    // inconsequential updates.
    //
    // NOTE: This should be updated if DetPoint ever changes. Assertion placed
    // here as a safeguard.
    static_assert(std::is_same_v<DetPoint, pcl::PointXYZRGBL>);
    if (pcl::geometry::squaredDistance(point, new_point) > DIRTY_DISTANCE_SQ ||
        (new_point.r - point.r) > COLOR_THRESHOLD ||
        (new_point.g - point.g) > COLOR_THRESHOLD ||
        (new_point.b - point.b) > COLOR_THRESHOLD ||
        new_point.label != point.label) {
      dirty = true;
      point = new_point;
      return;
    }
  }

  void set_stamp(const ros::Time& new_stamp) {
    if (new_stamp != stamp) {
      stamp = new_stamp;
      dirty = true;
      return;
    }
  }

  void set_valid(const bool new_valid) {
    if (new_valid != valid) {
      valid = new_valid;
      dirty = true;
    }
  }

  void set_dirty(const bool new_dirty) { dirty = new_dirty; }

  void update_images(const DetImagePairs& det_images, const std::vector<int>& label_list) {
    // Keep track of new images
    for (auto itr = det_images.begin(); itr != det_images.end(); itr++) {
      const auto& det_image = *itr;
      if (std::find(images.begin(), images.end(), det_image) == images.end()) {
        // Got a new image
        dirty = true;
        images.push_back(det_image);  // Dedupes input images
        labels.push_back(label_list[itr - det_images.begin()]);
      }
    }
  }

  void update_cloud(const DetCloud::Ptr& det_cloud) { cloud = det_cloud; }

  const DetPoint& get_point() const { return point; }
  const DetCloud::Ptr& get_cloud() const { return cloud; }
  const ros::Time& get_stamp() const { return stamp; }
  bool get_valid() const { return valid; }
  bool get_dirty() const { return dirty; }

  DetImages get_best_images() {
    DetImages to_return;

    // select the images of which the detection result matches the reporting label
    auto matched_ranges = get_indices(images.begin(), images.end(), 
      [this] (const auto itr) {
          return labels[itr - images.begin()] == point.label;
    });

    auto ranges = make_ranges(matched_ranges.begin(), matched_ranges.end(), DISPLAY_IMAGES);
    to_return.reserve(ranges.size() * 2);  

    // Pick out the "best" image from each section. For now, just the first.
    // TODO(vasua): Do something more intelligent here, e.g. use box size
    /*std::transform(
        ranges.begin(), ranges.end(), std::back_inserter(to_return),
        [](std::pair<DetImages::const_iterator, DetImages::const_iterator>
               pair) {
          auto begin = pair.first;
          auto end = pair.second;
          return *begin;
        });*/

    // Append the first *two* from each range -- one of them will be a cropped image
    // Graeme replaced the above, since can't figure out how to modify this convoluted syntax to achieve this
    for(const auto& range : ranges) {
      auto begin = range.first; // Iterator at begin of range
      auto end = range.second;  // Iterator at end of range

      // // Add the first element in range
      // to_return.push_back(*begin);

      // // check if current image pair detections are same with the reporting label
      // // if not, skip it.
      // while (begin != end && !is_image_match_label(begin)) {
      //   begin++;
      // }
      if (begin == end) {  // if cannot find suitable images inside this range, skip this range
        continue;
      }

      // Add both images in the pair
      to_return.push_back((*begin)->first);
      to_return.push_back((*begin)->second);

      // // If the range has more than one element, get the second one too
      // if(begin != end) { // probably redundant check
      //   begin++;
      //   if(begin != end) {
      //     // Add the second element in range
      //     to_return.push_back(*begin);
      //   }
      // }
    }

    // Sort by timestamp to make scrolling easier
    std::sort(to_return.begin(), to_return.end(),
              [](const sensor_msgs::ImageConstPtr& a,
                 const sensor_msgs::ImageConstPtr& b) {
                // If same time, choose the larger image (original one) first, since one is crop of other
                if(a->header.stamp == b->header.stamp) {
                  return (a->height + a->width) > (b->height + b->width);
                }
                // Choose the earlier image first
                return a->header.stamp < b->header.stamp;
              });

    return to_return;
  }

 private:
  DetPoint point;
  DetImagePairs images; // Images associated with this cluster
  DetCloud::Ptr cloud;  // Point cloud associated with this cluster
  ros::Time stamp;      // Stamp for original detection time
  bool valid;           // Whether the cluster point is still valid
  bool dirty = true;    // Whether the cluster point has been updated
  std::vector<int> labels; // list of labels to indicate the detection labels of each image

  template <typename Iterator, class UnaryPredicate>
  std::vector<Iterator> get_indices(Iterator first, Iterator last, UnaryPredicate pred) {
    std::vector<Iterator> return_list;
    while (first!=last) {
      if (pred(first)) {
        return_list.push_back(first);
      }
      ++first;
    }
    return return_list;
  }
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_DET_POINT_H