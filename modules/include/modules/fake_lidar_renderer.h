#ifndef OBJECT_DETECTION_MODULES_FAKE_LIDAR_RENDERER
#define OBJECT_DETECTION_MODULES_FAKE_LIDAR_RENDERER

#include <base/BaseNode.h>
#include <common/math.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <objdet_msgs/DetectionArray.h>
#include <sensor_msgs/Image.h>

namespace object_detection {
class FakeLidarRenderer : public BaseNode {
 public:
  FakeLidarRenderer(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  ~FakeLidarRenderer() = default;

  std::vector<double> lidar_img_times;
  float wait_time =
      1.0;  // time to wait before publishing fake lidar depth images
  float default_depth =
      1.0;  // depth in meters at every pixel of the default lidar depth image

  // bool save_imgs = true;        // default, should be changed by launch files
  // std::string vehicle_type;     // to determine how to find the ground
  // std::string downward_depth_topic;  // to determine height of drone
  // double last_depth_img_save_time;
  // float depth_img_save_period = 0.5;

 private:
  image_transport::ImageTransport it;
  cv_bridge::CvImagePtr default_img_ptr;

  // default lidar depth images
  std::vector<sensor_msgs::ImageConstPtr> default_imgs;

  std::vector<image_transport::Subscriber> lidar_img_subs;
  std::vector<ros::Subscriber> detection_subs;
  std::vector<image_transport::Subscriber> rs_depth_img_subs;

  std::vector<std::string> camera_topics;

  // // Publishes a fake lidar depth image if we havent heard from the
  // lidar_renderer
  // // in a while
  void detection_cb(const objdet_msgs::DetectionArrayConstPtr& dets_msg,
                    const std::size_t& topic_index,
                    const image_transport::Publisher& lidar_pub);

  // Keep track of when we heard from the lidar renderer last
  void lidar_img_cb(const sensor_msgs::ImageConstPtr& msg,
                    const std::size_t& topic_index);

  // Should be called only once. This is the get the size of the lidar
  // depth image, which must match the realsense depth image exactly
  // to be used in the depth combiner
  void rs_depth_cb(const sensor_msgs::ImageConstPtr& msg,
                   const std::size_t& topic_index);
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_FAKE_LIDAR_RENDERER
