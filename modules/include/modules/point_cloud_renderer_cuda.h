#ifndef OBJECT_DETECTION_MODULES_POINT_CLOUD_RENDERER_CUDA_H
#define OBJECT_DETECTION_MODULES_POINT_CLOUD_RENDERER_CUDA_H
#include <common/timer.h>
#include <sensor_msgs/CameraInfo.h>
#include <tf2/LinearMath/Transform.h>
#include <deque>
#include <opencv2/core/mat.hpp>

namespace object_detection {
class PointCloudRendererCuda {
 public:
  // special case for ugv intel sense camera, pls refer to
  // point_cloud_renderer.h for explanation
  static constexpr float kInflateConst = 447.619047619f;

  // Don't allow copies
  PointCloudRendererCuda(const PointCloudRendererCuda &other) = delete;
  PointCloudRendererCuda &operator=(const PointCloudRendererCuda &other) =
      delete;

  PointCloudRendererCuda();
  ~PointCloudRendererCuda();

  // Updates internal cloud to new point list (device pointers).
  void set_cloud(const std::deque<std::pair<void *, size_t>> *new_clouds);

  // Renders point cloud on GPU
  //
  // The optional inflate parameter controls how big to make the projected
  // points. By default (inflate = 0), projected points will occupy 1 pixel.
  // Inflation will happenly evenly on each side - e.g. inflate = 2 results in a
  // 5x5 square projection.
  cv::Mat render(const sensor_msgs::CameraInfoConstPtr &camera_info_ptr,
                 const tf2::Transform &map_to_camera_tf, const int inflate = 0);

  cv::Mat render_fisheye(const sensor_msgs::CameraInfoConstPtr &camera_info_ptr,
                         const tf2::Transform &map_to_camera_tf,
                         const int inflate = 0);

 private:
  const size_t DEFAULT_WIDTH = 640;
  const size_t DEFAULT_HEIGHT = 640;

  // Prints some friendly information about the GPU
  void print_cuda_info();

  // Allocates enough scratch space for an image of width x height
  void set_image_buffers(const size_t width, const size_t height);

  const std::deque<std::pair<void *, size_t>> *clouds;
  uint32_t *image_scratch_space = nullptr;
  uint16_t *image_buffer = nullptr;
  size_t scratch_elems = 0;
  Timer timer;
};

}  // namespace object_detection
#endif  // OBJECT_DETECTION_MODULES_POINT_CLOUD_DEPTH_RENDERER_CUDA_H
