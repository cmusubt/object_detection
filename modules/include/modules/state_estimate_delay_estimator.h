#ifndef OBJECT_DETECTION_MODULES_STATE_ESTIMATE_DELAY_ESTIMATOR_H
#define OBJECT_DETECTION_MODULES_STATE_ESTIMATE_DELAY_ESTIMATOR_H

#include <base/BaseNode.h>
#include <nav_msgs/Odometry.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>

namespace object_detection {
class StateEstimateDelayEstimator : public BaseNode {
 public:
  StateEstimateDelayEstimator(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  ~StateEstimateDelayEstimator() = default;

 private:
  void delay_cb(const nav_msgs::OdometryConstPtr& sensor_pose);

  ros::Subscriber sensor_pose_sub;
  ros::Publisher health_pub;
  ros::Publisher delay_pub;

  std_msgs::Bool delay_is_valid_msg;
  std_msgs::Float64 delay_pub_msg;

  int counter_step = 5;
  double max_delay = 1.0;
  double min_delay = -1.0;

  double sensor_pose_delay = 0;
  const double sensor_pose_delay_alpha = 0.9;
  unsigned int counter = 0;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_STATE_ESTIMATE_DELAY_ESTIMATOR_H
