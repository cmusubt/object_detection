#ifndef OBJECT_DETECTION_MODULES_DEPTH_COMBINER_H
#define OBJECT_DETECTION_MODULES_DEPTH_COMBINER_H

#include <base/BaseNode.h>
#include <sensor_msgs/Image.h>
#include <map>
#include <vector>
#include "modules/depth_lidar_sub.h"

namespace object_detection {
class DepthCombiner : public BaseNode {
 public:
  DepthCombiner(std::string node_name);

  // Sets up the pubs / subs based on the input topics
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~DepthCombiner() = default;

 private:
  void combine_cb(const image_transport::Publisher& image_pub,
                  const uint16_t& thresh_depth_scaled,
                  const sensor_msgs::ImageConstPtr& depth_msg,
                  const sensor_msgs::ImageConstPtr& lidar_msg);

  image_transport::ImageTransport it;
  std::vector<std::unique_ptr<object_detection::DepthLidarSub>> depth_subs;
  std::vector<image_transport::Publisher> combined_pubs;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_DEPTH_COMBINER_H