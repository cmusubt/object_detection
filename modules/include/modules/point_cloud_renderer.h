#ifndef OBJECT_DETECTION_MODULES_POINT_CLOUD_RENDERER_H
#define OBJECT_DETECTION_MODULES_POINT_CLOUD_RENDERER_H
#include <common/timer.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <tf2/LinearMath/Transform.h>
#include <opencv2/core/mat.hpp>
#include "modules/CameraFactory.h"

namespace object_detection {
// Simple CPU based point cloud renderer
//
// Creates depth images out of point clouds, based on camera info. This
// implementation is very naive and is meant to serve as a reference for output.
class PointCloudRenderer {
  // TODO(vasua): See if there's a way to get this info from LOAM
  using PointType = pcl::PointXYZ;
  using PointCloud = pcl::PointCloud<PointType>;
  using PointCloudPtr = PointCloud::Ptr;
  using PointCloudConstPtr = PointCloud::ConstPtr;

 public:
  PointCloudRenderer();

  // special case for ugv intel sense camera, calculate by
  // Z / f = x / (n * p)
  // where Z is the value of Z-value (depth) of the point in homogeneous
  // coordinates f is the focus lens (in meters), here 1.88e-3 m x is the length
  // of voxel grid size n is the number of pixels that one point should inflate
  // as p is the pixel size, here is 4.2e-6 m (the original size is 1.4e-6 m,
  // but the image size is shrink as 640*480, so the pixel size is multiplied)
  // n = x * kInflateConst / Z
  // kInflateConst = f / p
  static constexpr float kInflateConst = 447.619047619f;
  // static constexpr float kInflateConst = 500.0f;

  // Updates internal cloud to new combined point cloud
  void set_cloud(const PointCloudConstPtr &new_cloud_ptr);

  // Renders point cloud on CPU
  //
  // The optional inflate parameter controls how big to make the projected
  // points. By default (inflate = 0), projected points will occupy 1 pixel.
  // Inflation will happen evenly on each side - e.g. inflate = 2 results in a
  // 5x5 square projection.
  cv::Mat render(const sensor_msgs::CameraInfoConstPtr &camera_info_ptr,
                 const tf2::Transform &map_to_camera_tf,
                 const int inflate_const = 0);

  // Same as render() but built for fisheye lens
  cv::Mat render_fisheye(const sensor_msgs::CameraInfoConstPtr &camera_info_ptr,
                         const tf2::Transform &map_to_camera_tf,
                         const int inflate = 0);

  // project from 2d to 3d using fisheye model
  void prLiftProjective(const Eigen::Vector2d &p, Eigen::Vector3d &P);

 private:
  PointCloudConstPtr cloud_ptr;
  Timer timer;

  camodocal::CameraPtr camera;
  ros::NodeHandle nhPriv = ros::NodeHandle("~");
};

}  // namespace object_detection
#endif  // OBJECT_DETECTION_MODULES_POINT_CLOUD_DEPTH_RENDERER_H
