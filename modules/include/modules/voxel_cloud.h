#ifndef OBJECT_DETECTION_MODULES_VOXEL_CLOUD_H
#define OBJECT_DETECTION_MODULES_VOXEL_CLOUD_H

#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <algorithm>
#include <numeric>
#include <type_traits>
#include <vector>

namespace object_detection {

// Manages point clouds on the CPU memory via shared pointers
template <typename PointT>
class CpuVoxelCloudManager2D {
  using PointCloud = pcl::PointCloud<PointT>;
  using PointCloudPtr = typename PointCloud::Ptr;
  using PointCloudConstPtr = typename PointCloud::ConstPtr;

 public:
  explicit CpuVoxelCloudManager2D(float voxel_size = 1.0f,
                                  int voxel_half_width = 20,
                                  float filter_size = 0.1f)
      : voxel_size(voxel_size), voxel_half_width(voxel_half_width) {
    voxel_width = voxel_half_width * 2;
    voxel_num = voxel_width * voxel_width;
    voxel_half_side = voxel_half_width * voxel_size;
    down_sampler.setLeafSize(filter_size, filter_size, filter_size);
    voxelShiftX = 0;
    voxelShiftY = 0;

    clouds.resize(voxel_num);
    for (auto &i : clouds) {
      i.reset(new PointCloud);
    }
    voxel_cloud_ptr.reset(new PointCloud);

    // Only for Debug
    voxel_cloud_intensity_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>);
  }

  // Note: the pointcloud get from registered_to_map is not in a normal xyz
  // coord system but a camera coord system, where we need a transform to fit
  // the normal coord system In order to convert the coord axis, we need: x, y,
  // z = z, x, y To avoid the explicit transformation, we simply to use z, x
  // instead of x, y here
  void add(const PointCloudConstPtr &cloud, const PointT &center) {
    shift(center);

    // use pass through filter to discard points that not inside the voxel
    PointCloudPtr temp_cloud(new PointCloud);
    pass_filter.setInputCloud(cloud);
    pass_filter.setFilterFieldName("z");
    pass_filter.setFilterLimits(center.x - voxel_half_side,
                                center.x + voxel_half_side);
    pass_filter.filter(*temp_cloud);
    pass_filter.setInputCloud(temp_cloud);
    pass_filter.setFilterFieldName("x");
    pass_filter.setFilterLimits(center.y - voxel_half_side,
                                center.y + voxel_half_side);
    pass_filter.filter(*temp_cloud);

    // assign each point to the grid it should stay
    for (auto &p : temp_cloud->points) {
      int indX = static_cast<int>(floor(p.z / voxel_size)) - voxelShiftX +
                 voxel_half_width;
      int indY = static_cast<int>(floor(p.x / voxel_size)) - voxelShiftY +
                 voxel_half_width;
      if (indX >= 0 and indX < voxel_width and indY >= 0 and
          indY < voxel_width) {
        clouds[voxel_width * indX + indY]->push_back(p);
      }
    }

    // // downsample each grid
    for (auto &ptr : clouds) {
      down_sampler.setInputCloud(ptr);
      down_sampler.filter(*ptr);
    }
  }

  // Get the current cloud, as a copy of all points in this manager.
  //
  // Multiple calls to this function will return pointers to the same combined
  // cloud if no new clouds have been added. If a new cloud has been added, the
  // old pointer is invalidated and a new combined is returned.
  PointCloudConstPtr get() {
    voxel_cloud_ptr->clear();
    const size_t total_points =
        std::accumulate(begin(clouds), end(clouds), 0,
                        [](size_t i, const PointCloudConstPtr &cloud_ptr) {
                          return i + cloud_ptr->size();
                        });
    voxel_cloud_ptr->reserve(total_points);

    for (const auto &cloud_ptr : clouds) {
      voxel_cloud_ptr->insert(voxel_cloud_ptr->end(), cloud_ptr->begin(),
                              cloud_ptr->end());
    }

    return voxel_cloud_ptr;
  }

  // Only for Debug
  // Get the current cloud, as a copy of all points in this manager.
  pcl::PointCloud<pcl::PointXYZI>::Ptr get_colored_voxel() {
    voxel_cloud_intensity_ptr->clear();
    const size_t total_points =
        std::accumulate(begin(clouds), end(clouds), 0,
                        [](size_t i, const PointCloudConstPtr &cloud_ptr) {
                          return i + cloud_ptr->size();
                        });
    voxel_cloud_intensity_ptr->reserve(total_points);

    for (int i = 0; i < voxel_num; i++) {
      for (int idx = 0; idx < clouds[i]->points.size(); idx++) {
        pcl::PointXYZI new_point;
        new_point.x = clouds[i]->points[idx].x;
        new_point.y = clouds[i]->points[idx].y;
        new_point.z = clouds[i]->points[idx].z;
        new_point.intensity = static_cast<float>(i);
        voxel_cloud_intensity_ptr->push_back(new_point);
      }
    }

    return voxel_cloud_intensity_ptr;
  }

 private:
  std::vector<PointCloudPtr> clouds;
  PointCloudPtr voxel_cloud_ptr;
  pcl::VoxelGrid<PointT> down_sampler;
  pcl::PassThrough<PointT> pass_filter;

  float voxel_size;
  int voxel_half_width;
  float voxel_half_side;
  int voxel_width;
  int voxel_num;
  int voxelShiftX, voxelShiftY;

  // Only for Debug
  pcl::PointCloud<pcl::PointXYZI>::Ptr voxel_cloud_intensity_ptr;

  // shift the voxel grid to make the robot is around the center of voxel
  // refer to func add() to get futher info of the coord system conversion
  void shift(const PointT &center) {
    float voxel_center_x = voxel_size * voxelShiftX;
    float voxel_center_y = voxel_size * voxelShiftY;

    while (center.x - voxel_center_x < -voxel_size) {
      for (int indY = 0; indY < voxel_width; indY++) {
        PointCloudPtr tempPtr = clouds[voxel_width * (voxel_width - 1) + indY];
        for (int indX = voxel_width - 1; indX >= 1; indX--) {
          clouds[voxel_width * indX + indY] =
              clouds[voxel_width * (indX - 1) + indY];
        }
        clouds[indY] = tempPtr;
        clouds[indY]->clear();
      }
      voxelShiftX--;
      voxel_center_x -= voxel_size;
    }

    while (center.x - voxel_center_x > voxel_size) {
      for (int indY = 0; indY < voxel_width; indY++) {
        PointCloudPtr tempPtr = clouds[indY];
        for (int indX = 0; indX < voxel_width - 1; indX++) {
          clouds[voxel_width * indX + indY] =
              clouds[voxel_width * (indX + 1) + indY];
        }
        clouds[voxel_width * (voxel_width - 1) + indY] = tempPtr;
        clouds[voxel_width * (voxel_width - 1) + indY]->clear();
      }
      voxelShiftX++;
      voxel_center_x += voxel_size;
    }

    while (center.y - voxel_center_y < -voxel_size) {
      for (int indX = 0; indX < voxel_width; indX++) {
        PointCloudPtr tempPtr = clouds[voxel_width * indX + (voxel_width - 1)];
        for (int indY = voxel_width - 1; indY >= 1; indY--) {
          clouds[voxel_width * indX + indY] =
              clouds[voxel_width * indX + (indY - 1)];
        }
        clouds[voxel_width * indX] = tempPtr;
        clouds[voxel_width * indX]->clear();
      }
      voxelShiftY--;
      voxel_center_y -= voxel_size;
    }

    while (center.y - voxel_center_y > voxel_size) {
      for (int indX = 0; indX < voxel_width; indX++) {
        PointCloudPtr tempPtr = clouds[voxel_width * indX];
        for (int indY = 0; indY < voxel_width - 1; indY++) {
          clouds[voxel_width * indX + indY] =
              clouds[voxel_width * indX + (indY + 1)];
        }
        clouds[voxel_width * indX + (voxel_width - 1)] = tempPtr;
        clouds[voxel_width * indX + (voxel_width - 1)]->clear();
      }
      voxelShiftY++;
      voxel_center_y += voxel_size;
    }
  }
};

// This class maintains a voxel grid of point clouds. The entire cloud can
// be queried through the get() method. This class is not thread safe. Every
// input point cloud needs to be in the same frame.
//
// voxel_size is the size of voxel grid
// voxel_half_width is the half of the width of total voxel grid. The width of
// the grid should be 2 * voxel_half_width filter_size is the downsample filter
// leaf size
//
// Example:
//  VoxelCloud<PointXYZ, CpuVoxelCloudManager2D<PointXYZ>> voxel_cloud;
template <typename PointT, typename M>
class VoxelCloud {
  using PointCloud = pcl::PointCloud<PointT>;
  using PointCloudPtr = typename PointCloud::Ptr;
  using PointCloudConstPtr = typename PointCloud::ConstPtr;

 public:
  explicit VoxelCloud(float voxel_size = 1.0f, int voxel_half_width = 20,
                      float filter_size = 0.1f)
      : manager(voxel_size, voxel_half_width, filter_size) {}

  // Add a point cloud to the CloudManager.
  // Here, center are the current coordinate of the robot.
  // See the CloudManager documentation for more information on ownership.
  void add(const PointCloudConstPtr &cloud, const PointT &center) {
    // Just in case we get handed an empty cloud, don't waste a slot on it.
    if (cloud->empty()) {
      return;
    }
    manager.add(cloud, center);
  }

  // Queries the CloudManager for the map
  typename std::result_of<decltype (&M::get)(M)>::type get() {
    return manager.get();
  }

  // Only for Debug
  // Get the current cloud, as a copy of all points in this manager.
  pcl::PointCloud<pcl::PointXYZI>::Ptr get_colored_voxel() {
    return manager.get_colored_voxel();
  }

 private:
  M manager;
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_ROLLING_CLOUD_H
