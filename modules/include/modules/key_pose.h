#ifndef OBJECT_DETECTION_MODULES_KEY_POSE_H
#define OBJECT_DETECTION_MODULES_KEY_POSE_H

#include <geometry_msgs/Pose.h>
#include <mutex>
#include <ostream>
#include <vector>
#include "modules/det_point.h"

namespace object_detection {

struct KeyPose {
  // 6DOF Pose of this KeyPose. Must be in the fixed frame (usually /map).
  geometry_msgs::Pose pose;

  // Vector of individual (potentially downsampled) point clouds corresponding
  // to detections in this key pose.
  std::vector<DetCloud::Ptr> det_points;

  // The centroid for each point cloud, stored as an element of this cloud.
  DetCloud::Ptr det_centroids;

  // Vector of pair images corresponding to each detection. Currently, each vector
  // only contains one pair of image (one original and one cropped), 
  // though this may increase in the future (e.g.
  // adding thermal information to each detection, or recording all images with
  // every detection rather than just the one that detected the image).
  DetImagePairsList det_images;
  std::mutex mutex;

  friend std::ostream& operator<<(std::ostream& os, const KeyPose& key_pose) {
    os << "Key Pose at " << key_pose.pose;
    // os << "Key Pose at " << key_pose.pose << "with "
    //    << key_pose.map->points.size() << " points";
    return os;
  }
};
}  // namespace object_detection

#endif  // OBJECT_DETECTION_MODULES_KEY_POSE_H
