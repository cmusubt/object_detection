#ifndef OBJECT_DETECTION_MODULES_LOCAL_LIDAR_RENDERER_H
#define OBJECT_DETECTION_MODULES_LOCAL_LIDAR_RENDERER_H

#include <base/BaseNode.h>
#include <image_transport/image_transport.h>
#include <message_filters/subscriber.h>
#include <nav_msgs/Odometry.h>
#include <pcl_ros/point_cloud.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <tf2_ros/message_filter.h>
#include <tf2_ros/transform_listener.h>
#include <unordered_map>
#include "modules/point_cloud_renderer.h"
#include "modules/point_cloud_renderer_cuda.h"
#include "modules/state_estimate_delay_subscriber.h"
#include "modules/voxel_cloud.h"
#include "objdet_msgs/DetectionArray.h"

namespace object_detection {
class LocalLidarRenderer : public BaseNode {
  using PointType = pcl::PointXYZ;
  using PointCloud = pcl::PointCloud<PointType>;

 public:
  LocalLidarRenderer(std::string node_name);

  // Sets up the pubs / subs based on the input topics
  virtual bool initialize() override;
  virtual bool execute() override;
  virtual ~LocalLidarRenderer() = default;

 private:
  // Switch values based on CPU / GPU
#if USE_CUDA
  const float VOXEL_SIZE = 5.0f;
  const int VOXEL_HALF_WIDTH = 3;
  const float FILTER_SIZE = 0.1f;
#else
  const float VOXEL_SIZE = 4.0f;
  const int VOXEL_HALF_WIDTH = 5;
  const float FILTER_SIZE = 0.1f;
#endif

  // Updates the rolling cloud with the new cloud
  //
  // Note that this assumes that all clouds are in the same frame (e.g. map),
  // from which a transform can be queried to the frames of the CameraInfos.
  // Also, currently "sensor_init_rot" is hardcoded as the frame.
  void cloud_cb(const PointCloud::ConstPtr &msg);

  // Publishes rendered image with the camera parameters
  //
  // Note that currently distortion isn't handled - only rectified images are
  // generated and published.
  void info_cb(const objdet_msgs::DetectionArrayConstPtr &dets_msg,
               const image_transport::Publisher &image_pub,
               const std::string &topic);

  // Updates the position of current robot position
  void highRateOdomHandler(const nav_msgs::Odometry::ConstPtr &odom);

  // Returns the topic rendered lidar images are published on
  //
  // Example:
  //   /rs_front/color/image/detections -> "/rs_front/aligned_lidar/image"
  std::string get_pub_topic(const std::string &topic);

  tf2_ros::Buffer tf_buffer;
  tf2_ros::TransformListener tf_listener;

  std::unordered_map<std::string, sensor_msgs::CameraInfoConstPtr>
      camera_info_list;

  image_transport::ImageTransport it;
  ros::Subscriber registered_cloud_sub;
  ros::Subscriber highrate_odom_sub;
  std::vector<image_transport::Publisher> image_pubs;
  std::vector<ros::Subscriber> info_msg_subs;
  PointType robot_pos = PointType(0, 0, 0);
  PointType robot_last_pos = PointType(0, 0, 0);

  ros::Publisher voxel_grid_pub;
  int COUNT = 0;
  const int COUNT_MAX = 15;

  std::string voxel_cloud_frame;

  const double MIN_MOVE_DISTANCE = 0.05;
  const double MIN_MOVE_DISTANCE_SQ = MIN_MOVE_DISTANCE * MIN_MOVE_DISTANCE;

  // detection confidence threshold
  float CONFIDENCE_THRESHOLD = 0.3;
  bool LOGGING_DEBUG_INFO = true;
  bool PUBLISH_DEBUG_MSGS = false;

  // Switch implementations based on available capabilities
  // TODO: fix the cuda version later
#if USE_CUDA
  VoxelCloud<PointType, CpuVoxelCloudManager2D<PointType>> voxel_cloud;
  PointCloudRenderer cloud_renderer;
#else
  VoxelCloud<PointType, CpuVoxelCloudManager2D<PointType>> voxel_cloud;
  PointCloudRenderer cloud_renderer;
#endif

  StateEstimateDelaySubscriber delay_subscriber;
};
}  // namespace object_detection
#endif  // OBJECT_DETECTION_MODULES_LOCAL_LIDAR_RENDERER_H
