#ifndef OBJECT_DETECTION_MODULES_ARTIFACT_FILTER_H
#define OBJECT_DETECTION_MODULES_ARTIFACT_FILTER_H

#include <base/BaseNode.h>
#include <objdet_msgs/ArtifactLocalization.h>
#include <objdet_msgs/ArtifactLocalizationArray.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/extract_clusters.h>
#include <unordered_map>
#include <fstream>

namespace object_detection {
class ArtifactFilter : public BaseNode {
 public:
  ArtifactFilter(std::string node_name);
  virtual bool initialize() override;
  virtual bool execute() override;
  ~ArtifactFilter() = default;

 private:
  // Parameters for clustering points together
  float CLUSTER_TOLERANCE = 1.5f; //0.5f;  // meters
  const int MIN_CLUSTER_SIZE = 1;
  const int MAX_CLUSTER_SIZE = 1000;

  // How many images to keep
  const size_t DISPLAY_IMAGES = 4;

  // Parameters for matching clusters together (deduping)
  float MATCH_THRESHOLD = 1.5f; // .5f;  // meters
  float MATCH_THRESHOLD_SQ = MATCH_THRESHOLD * MATCH_THRESHOLD;

  // Filter visual cell phone detections;
  bool ignore_visual_cell_phones = true;

  //If True, no filtering -- just increment the report ID and send it through
  bool bypass_artifact_filter = false;
  uint32_t bypass_counter = 0;

  std::ofstream report_id_stream;
  uint16_t last_report_id = 0;

  std::vector<pcl::PointIndices> cluster_points(
      const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud);
  // std::vector<objdet_msgs::ArtifactLocalization> make_artifacts_from_clusters(
  //    const std::vector<pcl::PointIndices>& cluster_indices,
  //    const pcl::PointCloud<pcl::PointXYZ>::Ptr& artifact_centroids);
   std::unordered_map<uint16_t, std::vector<sensor_msgs::Image> > update_artifacts(const objdet_msgs::ArtifactLocalizationArray& new_artifacts);
  //      std::vector<objdet_msgs::ArtifactLocalization>& new_artifacts);
  float artifact_distance_sq(const objdet_msgs::ArtifactLocalization& a,
                             const objdet_msgs::ArtifactLocalization& b);

  // Returns true if ArtifactLocalizations are equal
  bool compare_artifacts(const objdet_msgs::ArtifactLocalization& a,
                         const objdet_msgs::ArtifactLocalization& b);

  bool check_duplicate_artifact(const objdet_msgs::ArtifactLocalization& a,
                         const objdet_msgs::ArtifactLocalization& b);
  void publish_artifacts();

  void artifact_cb(const objdet_msgs::ArtifactLocalizationArray& incoming_artifacts);
  objdet_msgs::ArtifactLocalization createDummyArtifact(const objdet_msgs::ArtifactLocalization& newArtifact);

  bool compare_readings(objdet_msgs::StampedReading a,
                        objdet_msgs::StampedReading b);

  std::string map_frame = "";

  
  std::vector<ros::Subscriber> artifact_subs;
  ros::Publisher artifact_localizations_publisher;

  std::vector<std::unordered_map<uint16_t, objdet_msgs::ArtifactLocalization>>
      all_artifacts;

  std::vector<objdet_msgs::ArtifactLocalization> current_artifacts;
  std::vector<bool> current_artifacts_dirty;
  std::vector<uint16_t> pending_reports;
  pcl::PointCloud<pcl::PointXYZ>::Ptr artifact_centroids;
};
}  // namespace object_detection
#endif  // OBJECT_DETECTION_MODULES_ARTIFACT_FILTER_H
