#ifndef OBJECT_DETECTION_MODULES_SIGNAL_SOLVERS
#define OBJECT_DETECTION_MODULES_SIGNAL_SOLVERS

#include <objdet_msgs/ArtifactLocalization.h>
#include <Eigen/Dense>
#include <algorithm>
#include <iostream>
#include <vector>

namespace object_detection {
namespace signal_solvers {
struct ReadingFromPosition {
  Eigen::Vector3f robot_position;
  float raw_reading;
};

// https://en.cppreference.com/w/cpp/algorithm/find
template <class InputIt, class UnaryPredicate>
auto find_equal_ranges_if(const InputIt& first, const InputIt& last,
                          const UnaryPredicate& p) {
  std::vector<std::pair<InputIt, InputIt>> equal_ranges;

  auto in_range = false;
  InputIt range_start;

  for (auto it = first; it != last; ++it) {
    auto match = p(*it);
    if (!in_range && match) {  // Rising edge
      range_start = it;
      in_range = true;
    } else if (in_range && !match) {  // Falling edge
      equal_ranges.emplace_back(range_start, it);
      in_range = false;
    }
  }

  if (in_range) {  // Terminate the range at the last element
    equal_ranges.emplace_back(range_start, last);
  }

  return equal_ranges;
}

template <class InputIt, class T>
auto find_equal_ranges(const InputIt& first, const InputIt& last,
                       const T& value) {
  return find_equal_ranges_if(first, last,
                              [&value](const auto& a) { return a == value; });
}

template <class InputIt>
Eigen::Vector3f get_arc_midpoint(const InputIt& first, const InputIt& last) {
  // Short circuit when we have only one point to save compute.
  const auto dist = std::distance(first, last);
  if (dist == 1) {
    return first->robot_position;
  }

  // Build cumulative distance along the arc
  float cumulative_segment_length = 0;
  std::vector<float> cumulative_segment_lengths;
  cumulative_segment_lengths.reserve(dist);

  auto previous_point = first->robot_position;
  for (auto it = first; it != last; ++it) {
    const auto& current_point = it->robot_position;
    cumulative_segment_length += (current_point - previous_point).norm();
    cumulative_segment_lengths.push_back(cumulative_segment_length);
    previous_point = current_point;
  }

  // Find and locate the midpoint of the arc
  // Just do a linear search since the number of elements is small.
  const auto midpoint = cumulative_segment_length / 2.0f;
  size_t lower_bound = 0;
  for (size_t i = 0; i < cumulative_segment_lengths.size(); ++i) {
    if (cumulative_segment_lengths[i] <= midpoint) {
      lower_bound = i;
    } else {
      break;
    }
  }
  size_t upper_bound =
      std::min(lower_bound + 1, cumulative_segment_lengths.size() - 1);

  // Take weighted average.
  const auto& lower_point = (first + lower_bound)->robot_position;
  const auto& upper_point = (first + upper_bound)->robot_position;

  const auto segment_length = cumulative_segment_lengths[upper_bound] -
                              cumulative_segment_lengths[lower_bound];
  if (segment_length <= 0.1f) {  // Just average in this case, and avoid denorms
    return lower_point + (upper_point - lower_point) / 2.0f;
  }

  const auto upper_weight =
      (midpoint - cumulative_segment_lengths[lower_bound]) / segment_length;
  return lower_point * (1.0f - upper_weight) + upper_point * upper_weight;
}

bool find_artifact_max_rssi(const std::vector<ReadingFromPosition>& readings,
                            Eigen::Vector3f& artifact, int& max_rssi_index) {
  if (readings.empty()) {
    return false;
  }

  // First, find the max RSSI, and all of the ranges it occurs
  const auto it = std::max_element(readings.begin(), readings.end(),
                                   [](const auto& a, const auto& b) {
                                     return a.raw_reading < b.raw_reading;
                                   });

  max_rssi_index = std::distance(readings.begin(), it);

  const auto max_rssi = it->raw_reading;
  const auto max_rssi_ranges = find_equal_ranges_if(
      readings.begin(), readings.end(), [max_rssi](const auto& reading) {
        return reading.raw_reading == max_rssi;
      });

  // Treat each of the ranges as an arc, and use the center of the arc as the
  // artifact location.
  artifact = {0, 0, 0};
  for (const auto& [first, last] : max_rssi_ranges) {
    artifact += get_arc_midpoint(first, last) / max_rssi_ranges.size();
  }

  return true;
}

}  // namespace signal_solvers
}  // namespace object_detection
#endif
