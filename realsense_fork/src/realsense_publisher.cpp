#include "../include/realsense_publisher.h"
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <chrono>
#include <cstdint>
#include "realsense_fork/RealsenseMetadata.h"

namespace realsense_fork {
RealsensePublisher::RealsensePublisher(const ros::NodeHandle& nh,
                                       const ros::NodeHandle& private_nh,
                                       const std::string& name,
                                       const rs2::device& dev)
    : nh(nh),
      private_nh(private_nh),
      it(nh),
      name(name),
      dev(dev),
      stream(false) {
  try {
    serial_no = dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
    ROS_INFO("Starting streaming for camera %s (%s)", name.c_str(),
             serial_no.c_str());

    for (auto&& sensor : dev.query_sensors()) {
      std::string sensor_name = get_sensor_name(sensor);
      ROS_INFO_STREAM("  Found sensor " << sensor_name.c_str());
      if (sensor_name == "RGB Camera") {
        color_sensor = sensor;
      } else if (sensor_name == "Stereo Module") {
        depth_sensor = sensor;
      }
    }
  } catch (const rs2::error& e) {
    ROS_FATAL_STREAM("Error opening devices " << e.what());
  }

  // Set the image parameters. Use the current values as the defaults.
  private_nh.param("fps", fps, fps);
  private_nh.param("width", width, width);
  private_nh.param("height", height, height);
  private_nh.param("enable_ir", ir, ir);
  private_nh.param("enable_imu", imu, imu);
  private_nh.param("enable_colorized_depth", colorized, colorized);

  if (!(color_sensor && depth_sensor)) {
    ROS_FATAL("Unable to open all required sensors for sensor!");
  }

  try {
    configure_color_sensor();
    configure_depth_sensor();
  } catch (const rs2::error& e) {
    ROS_FATAL_STREAM("Error configuring sensors " << e.what());
  }

  configure_ros_topics();

  start();
}

RealsensePublisher::~RealsensePublisher() {
  stop();

  try {
    pipe.stop();  // Need to free resources!
  } catch (const rs2::error& e) {
    // The pipe must not have been started, so nothing to catch.
  }
}

void RealsensePublisher::start() {
  if (stream.load()) {  // already started
    ROS_WARN_STREAM("Start called multiple times on " << name);
    return;
  }

  ROS_INFO("Starting to stream!");
  stream = true;
  stream_thread = std::thread([&]() {
    try {
      cfg.enable_device(serial_no);
      cfg.enable_stream(RS2_STREAM_DEPTH, width, height, RS2_FORMAT_Z16, fps);
      cfg.enable_stream(RS2_STREAM_COLOR, width, height, RS2_FORMAT_RGB8, fps);

      rs2::pipeline_profile profile = pipe.start(cfg);

      // Configure the camera info messages
      depth_stream_profile = profile.get_stream(RS2_STREAM_DEPTH);
      color_stream_profile = profile.get_stream(RS2_STREAM_COLOR);
      update_camera_info(depth_info,
                         depth_stream_profile.as<rs2::video_stream_profile>(),
                         DEPTH_OPT_FRAME_ID);
      update_camera_info(color_info,
                         color_stream_profile.as<rs2::video_stream_profile>(),
                         COLOR_OPT_FRAME_ID);

    } catch (const rs2::error& e) {
      ROS_ERROR_STREAM("Error during stream setup for camera "
                       << name << ", error: " << e.what());
    } catch (const std::exception& e) {
      ROS_ERROR_STREAM("General error during stream setup for camera "
                       << name << ", error: " << e.what());
    }

    try {
      rs2::align align(RS2_STREAM_COLOR);
      auto last_time = std::chrono::high_resolution_clock::now();
      while (stream.load()) {
        auto loop_time = std::chrono::high_resolution_clock::now();
        auto process_time =
            std::chrono::duration_cast<std::chrono::milliseconds>(loop_time -
                                                                  last_time)
                .count();
        rs2::frameset data = pipe.wait_for_frames();
        last_time = std::chrono::high_resolution_clock::now();
        auto wait_time = std::chrono::duration_cast<std::chrono::milliseconds>(
                             last_time - loop_time)
                             .count();

        rs2::frame color = data.get_color_frame();
        rs2::frame depth = data.get_depth_frame();

        // TODO(vasua): Report some kind of status here, not just print
        ROS_INFO_STREAM_THROTTLE(
            1, name << " got a frame set. Color: " << static_cast<bool>(color)
                    << ", depth: " << static_cast<bool>(depth) << ", waited "
                    << wait_time << " ms"
                    << ", processing took " << process_time << " ms");

        // Librealsense v2.22.0 added a new feature to try and convert the
        // sensor timestamps to system timestamps. If we have the feature, we'll
        // use it, else we'll fall back on the previous implementation of using
        // the current ROS time.
        //
        // Note that the time for the overall frame is the same as the time for
        // the depth frame, which is transmitted first. The color arrives
        // shortly after and has a slightly different frame time.
#ifdef RS2_TIMESTAMP_DOMAIN_GLOBAL_TIME
        ros::Time time;
        if (data.get_frame_timestamp_domain() ==
            RS2_TIMESTAMP_DOMAIN_GLOBAL_TIME) {
          // Time from the frame comes in as double milliseconds, not seconds.
          time = ros::Time(data.get_timestamp() / 1000.0);
        } else {
          ROS_WARN_ONCE(
              "Not using global timestamp. Consider upgrading to librealsense "
              "v2.22.0+.");
          time = ros::Time::now();
        }
#else
        ROS_WARN_ONCE(
            "Not using global timestamp. Consider upgrading to librealsense "
            "v2.22.0+.");
        ros::Time time = ros::Time::now();
#endif

        // In order to publish efficiently, we need to use a shared_ptr.
        // We would like to use the buffer that's already inside the
        // frame, but that requires keeping the frame around
        // indefinitely since the buffer becomes invalidated once the
        // frame goes out of scope. There might be some way to tie the
        // lifetime of the frame into the lifetime of the shared_ptr for
        // the data, but for now, we'll keep it simple and make a copy.
        // Should save about .25ms per copy.
        // TODO(vasua): Figure out how to avoid a copy here.

        // This time is shared across all frames (depth, color,
        // aligned, colorized), so that we can keep them synchronized
        // even when they're republished at different times.
        // TODO(vasua): Replace this time with something from PPS

        publish_frame(color_pub, color, color_info, time,
                      sensor_msgs::image_encodings::RGB8);
        publish_frame(depth_pub, depth, depth_info, time,
                      sensor_msgs::image_encodings::TYPE_16UC1);

        publish_frame_metadata(color_metadata_pub, color, time);
        publish_frame_metadata(depth_metadata_pub, depth, time);

        // Align the depth frame to the color frame. Takes about 4 ms on
        // my laptop, probably slightly longer on the Xavier. That's
        // non-negligible, so we'll send this frame off later.
        rs2::frameset aligned_data = align.process(data);
        publish_frame(aligned_depth_pub, aligned_data.get_depth_frame(),
                      color_info, time,
                      sensor_msgs::image_encodings::TYPE_16UC1);

        // Colorization of the depth frame is optional, but helpful for
        // debugging, and doesn't take too long.
        if (colorized) {
          rs2::colorizer colorize;
          rs2::frame colorized_depth = depth.apply_filter(colorize);
          publish_frame(colorized_depth_pub, colorized_depth, depth_info, time,
                        sensor_msgs::image_encodings::RGB8);
        }

        // Publish transforms every frame. This is required to make rviz
        // work properly, since I guess tf buffers aren't that big?
        // Do it over /tf instead of /tf_static to make rosbags work
        // properly.
        publish_transforms(time);
      }
    } catch (const rs2::error& e) {
      ROS_ERROR_STREAM("Error during actual streaming for camera "
                       << name << ", error: " << e.what());
    } catch (const std::exception& e) {
      ROS_ERROR_STREAM("General error during stream setup for camera "
                       << name << ", error: " << e.what());
    }
  });
}

void RealsensePublisher::stop() {
  stream = false;
  stream_thread.join();
}

// Taken from the RealSense code
std::string RealsensePublisher::get_sensor_name(const rs2::sensor& sensor) {
  // Sensors support additional information, such as a human readable name
  if (sensor.supports(RS2_CAMERA_INFO_NAME)) {
    return sensor.get_info(RS2_CAMERA_INFO_NAME);
  }
  return "Unknown Sensor";
}

void RealsensePublisher::configure_color_sensor() {
  // TODO(vasua): maybe set manual white balance based on the LEDs?
  // TODO(vasua): maybe just use autoexposure, with priority turned off
  color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 1);
  color_sensor.set_option(RS2_OPTION_FRAMES_QUEUE_SIZE, 1);
  color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1);
  color_sensor.set_option(RS2_OPTION_AUTO_EXPOSURE_PRIORITY, 0);

  // TODO(vasua): set gain and exposure dynamically?
  // These can be adjusted while the sensor is running.
  // color_sensor.set_option(RS2_OPTION_GAIN, 64);  // half way through range
  // color_sensor.set_option(RS2_OPTION_EXPOSURE, 10 * (1000 / fps));
}

void RealsensePublisher::configure_depth_sensor() {
  // There might be a firmware bug at lower resolutions with autoexposure?
  depth_sensor.set_option(RS2_OPTION_OUTPUT_TRIGGER_ENABLED, 1);
  depth_sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1);
  depth_sensor.set_option(RS2_OPTION_DEPTH_UNITS, 0.001);  // 1 mm
  // This option doesn't seem to work on the payload realsenses
  // depth_sensor.set_option(RS2_OPTION_MAX_DISTANCE, 5000);  // 5000 mm = 5m

  // We can leave auto exposure on for now, seems to be okay.
}

void RealsensePublisher::configure_ros_topics() {
  // Disable plugins we don't use, to clean up the topic list.
  const std::string color_pub_name = name + "/color/image";
  const std::string depth_pub_name = name + "/depth/image";
  const std::string aligned_depth_pub_name =
      name + "/aligned_depth_to_color/image";
  const std::string colorized_depth_pub_name = name + "/colorized_depth/image";
  const std::string color_metadata_name = name + "/color/metadata";
  const std::string depth_metadata_name = name + "/depth/metadata";

  // Disable all compressed transports for the given topic. We don't
  // typically used compressed (eats too much CPU, and we have bandwidth
  // to spare at the moment), so no reason to clog the rostopic list.
  disable_transports(color_pub_name);
  disable_transports(depth_pub_name);
  disable_transports(aligned_depth_pub_name);
  disable_transports(colorized_depth_pub_name);

  color_pub = it.advertiseCamera(color_pub_name, 1);
  depth_pub = it.advertiseCamera(depth_pub_name, 1);
  aligned_depth_pub = it.advertiseCamera(aligned_depth_pub_name, 1);
  if (colorized) {
    colorized_depth_pub = it.advertiseCamera(colorized_depth_pub_name, 1);
  }

  color_metadata_pub = nh.advertise<RealsenseMetadata>(color_metadata_name, 1);
  depth_metadata_pub = nh.advertise<RealsenseMetadata>(depth_metadata_name, 1);
}

void RealsensePublisher::disable_transports(const std::string& name) {
  ROS_INFO_STREAM("Disabling plugins for " << name);
  nh.setParam(name + "/disable_pub_plugins",
              std::vector<std::string>{"image_transport/compressed",
                                       "image_transport/compressedDepth",
                                       "image_transport/theora"});
}

void RealsensePublisher::update_camera_info(
    sensor_msgs::CameraInfo& info,
    const rs2::video_stream_profile& video_profile,
    const std::string& frame_id) {
  auto intrinsics = video_profile.get_intrinsics();
  info.width = intrinsics.width;
  info.height = intrinsics.height;
  info.header.frame_id = frame_id;

  // Note that ROS messages are initialized to 0 by default, so we don't
  // need to copy over non-zero elements.

  // Intrinsics matrix K
  info.K[0] = intrinsics.fx;
  info.K[2] = intrinsics.ppx;  // cx, "principal point x"
  info.K[4] = intrinsics.fy;
  info.K[5] = intrinsics.ppy;  // cy, "principal point y"
  info.K[8] = 1;

  // Projection matrix P. Usually, P = [K | 0] for monocular cameras.
  info.P[0] = info.K[0];
  info.P[2] = info.K[2];
  info.P[5] = info.K[4];
  info.P[6] = info.K[5];
  info.P[10] = info.K[8];

  // Set R [rotation matrix] values to identity.
  // 0 initialization means only 3 values need to be set.
  info.R[0] = 1.0;
  info.R[4] = 1.0;
  info.R[8] = 1.0;

  // Copy distortion coefficients into D [given in coeffs].
  info.distortion_model = "plumb_bob";  // Always has 5 coefficients
  info.D.reserve(5);
  for (const auto coeff : intrinsics.coeffs) {
    info.D.push_back(coeff);
  }

  // For depth aligned to color, we want to use the color info rather than
  // the originl depth info. For colorized depth, we use the original
  // depth information. This will also implicitly use the correct frame
  // ID.
}

void RealsensePublisher::publish_frame(
    const image_transport::CameraPublisher& pub, const rs2::frame& frame,
    const sensor_msgs::CameraInfo& info, const ros::Time& time,
    const std::string& ros_encoding) {
  if (!frame.is<rs2::video_frame>()) {
    // This shouldn't happen, both frames are coming from video streams.
    ROS_WARN_STREAM(name << " received not a video frame.");
    return;

    // TODO(vasua): Health checks?
  }

  sensor_msgs::ImagePtr img(new sensor_msgs::Image);
  auto image = frame.as<rs2::video_frame>();

  // This is basically duplicating the functionality here:
  // http://docs.ros.org/diamondback/api/cv_bridge/html/c++/cv__bridge_8cpp_source.html#l00185
  // http://docs.ros.org/melodic/api/cv_bridge/html/c++/cv__bridge_8cpp_source.html#l00362
  img->width = image.get_width();
  img->height = image.get_height();
  img->is_bigendian = false;  // NOLINT, taken from base_realsense_node.cpp
  img->step = image.get_stride_in_bytes();
  img->encoding = ros_encoding;

  // See https://github.com/VasuAgrawal/vector_benchmarks
  auto size = img->step * img->height;
  auto data = reinterpret_cast<const uint8_t*>(image.get_data());  // NOLINT
  img->data.insert(img->data.begin(), data, data + size);

  img->header.frame_id = info.header.frame_id;
  img->header.stamp = time;
  // Is seq handled automatically?

  sensor_msgs::CameraInfoPtr info_ptr(new sensor_msgs::CameraInfo(info));
  info_ptr->header.stamp = time;  // This is the only thing not set

  pub.publish(img, info_ptr);
}

void RealsensePublisher::publish_frame_metadata(const ros::Publisher& pub,
                                                const rs2::frame& frame,
                                                const ros::Time& time) {
  RealsenseMetadata metadata;
  metadata.header.stamp = time;

  for (int i = 0; i < RS2_FRAME_METADATA_COUNT; ++i) {
    const auto meta_type = static_cast<rs2_frame_metadata_value>(i);
    if (frame.supports_frame_metadata(meta_type)) {
      metadata.data[i] = frame.get_frame_metadata(meta_type);
      metadata.valid[i] = true;  // NOLINT
    } else {
      metadata.valid[i] = false;  // NOLINT
    }
  }

  pub.publish(metadata);
}

void RealsensePublisher::publish_transforms(const ros::Time& time) {
  tf::Quaternion quaternion_optical;
  quaternion_optical.setRPY(-M_PI / 2, 0.0, -M_PI / 2);

  // Depth frame is the same as the base link (same as in librealsense),
  // so no additional transformation is done from link to depth frame.
  const tf::Vector3 zero_translation{0, 0, 0};
  const tf::Quaternion zero_rotation{0, 0, 0, 1};
  publish_transform(time, zero_translation, zero_rotation, BASE_FRAME_ID,
                    DEPTH_FRAME_ID);

  // Transform from depth frame to depth optical frame
  publish_transform(time, zero_translation, quaternion_optical, DEPTH_FRAME_ID,
                    DEPTH_OPT_FRAME_ID);

  // Extrinsics has data in column major format, while everything else
  // expects row-major. We need to change this.
  const auto& ex = color_stream_profile.get_extrinsics_to(depth_stream_profile);
  const tf::Matrix3x3 rot_m(ex.rotation[0], ex.rotation[3], ex.rotation[6],
                            ex.rotation[1], ex.rotation[4], ex.rotation[7],
                            ex.rotation[2], ex.rotation[5], ex.rotation[8]);
  tf::Quaternion rot_q;
  rot_m.getRotation(rot_q);
  rot_q = quaternion_optical * rot_q * quaternion_optical.inverse();

  const tf::Vector3 trans(ex.translation[2], -ex.translation[0],
                          -ex.translation[1]);

  publish_transform(time, trans, rot_q, BASE_FRAME_ID, COLOR_FRAME_ID);
  publish_transform(time, zero_translation, quaternion_optical, COLOR_FRAME_ID,
                    COLOR_OPT_FRAME_ID);
}

void RealsensePublisher::publish_transform(const ros::Time& time,
                                           const tf::Vector3& trans,
                                           const tf::Quaternion& q,
                                           const std::string& from,
                                           const std::string& to) {
  geometry_msgs::TransformStamped msg;
  msg.header.stamp = time;
  msg.header.frame_id = from;
  msg.child_frame_id = to;

  tf::vector3TFToMsg(trans, msg.transform.translation);
  tf::quaternionTFToMsg(q, msg.transform.rotation);
  br.sendTransform(msg);
}

}  // namespace realsense_fork
