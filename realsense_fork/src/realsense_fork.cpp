#include "../include/realsense_fork.h"
#include <librealsense2/rs.hpp>
#include "ros/init.h"

#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

#include <algorithm>
#include <map>
#include <thread>

PLUGINLIB_EXPORT_CLASS(realsense_fork::RealsenseFork, nodelet::Nodelet)

namespace realsense_fork {

void RealsenseFork::onInit() {
  nh = getNodeHandle();
  private_nh = getPrivateNodeHandle();

  // Figure out which devices the user wants.
  std::map<std::string, std::string> serial_no;
  private_nh.getParam("serial_no", serial_no);

  // Graeme added this loop
  // Keep trying serial numbers until they connect
  while(!serial_no.empty()) {

    NODELET_INFO_STREAM("Attempting to start " << serial_no.size() << " cameras");

    std::unordered_map<std::string, rs2::device> new_devices;

    // Then, query devices and match against serial number.
    try {
      for (auto&& dev : ctx.query_devices()) {
        std::string sn(dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
        auto it = std::find_if(serial_no.begin(), serial_no.end(),
                               [&sn](const decltype(serial_no)::value_type& kv) {
                                 return kv.second == sn;
                               });
        if (it == serial_no.end()) {
          NODELET_INFO("Realsense device with sn %s wasn't requested, skipping!",
                       sn.c_str());
        } else {
          NODELET_INFO("Matched a realsense device with sn %s (%s)", sn.c_str(),
                       it->first.c_str());
          devices[sn] = dev;
          new_devices[sn] = dev;
          device_names[sn] = it->first;
          serial_no.erase(it);
        }
      }
    } catch (const rs2::error& e) {
      NODELET_FATAL_STREAM("Error when searching for devices" << e.what());
    }

    // Any serial numbers that are left haven't been matched. Let the user know.
    for (const auto& kv : serial_no) {
      NODELET_WARN("Unable to match device \"%s\" with serial number \"%s\".",
                   kv.first.c_str(), kv.second.c_str());
    }

    // Attempt to reset all of the devices if necessary.
    private_nh.param("initial_reset", initial_reset, initial_reset);
    if (initial_reset) {
      // Add the reset callback
      ctx.set_devices_changed_callback(
          [this](const rs2::event_information& info) {
            for (const auto& new_device : info.get_new_devices()) {
              std::string sn(new_device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
              if (devices.find(sn) == devices.end()) {
                continue;
              }

              NODELET_INFO_STREAM("Got new device " << device_names[sn]
                                                    << " with sn " << sn);
              start_device(sn, new_device);
            }
          });

      // And then reset everything
      for (auto& kv : devices) {
        try {
          NODELET_INFO("Attempting to reset device %s (%s)",
                       device_names[kv.first].c_str(), kv.first.c_str());
          kv.second.hardware_reset();
          kv.second = rs2::device();
        } catch (const std::exception& ex) {
          NODELET_WARN_STREAM("Exception when resetting devices: " << ex.what());
        }
      }
    } else {
      // Otherwise, start normally.
      for (const auto& kv : new_devices) {
        NODELET_INFO_STREAM("Starting device " << kv.second << " with sn " << kv.first);
        start_device(kv.first, kv.second);
      }
    }

    // Delay and try again
    if(!serial_no.empty()) {
      NODELET_WARN_STREAM(serial_no.size() << " cameras did not start. Sleeping and trying again...");
      ros::Duration(1.0).sleep();
    }
  }

  NODELET_INFO("Done initializing!");
}

void RealsenseFork::start_device(const std::string& sn,
                                 const rs2::device& dev) {
  const std::string& name = device_names[sn];
  std::lock_guard<std::mutex> lock(devices_mutex);
  publishers[sn] = std::unique_ptr<RealsensePublisher>(
      new RealsensePublisher(nh, private_nh, name, dev));
  std::this_thread::sleep_for(std::chrono::seconds(2));
}

RealsenseFork::~RealsenseFork() {
  // do something in the destructor
  NODELET_INFO("destructor called?");

  // The publishers should all have their destructors called, which will
  // automatically clean up the threads and cameras properly.
}
}  // namespace realsense_fork
