# Multi Realsense Driver

This driver replaces the Intel RealSense Driver and provides better support for
launching and managing multiple cameras, as on the payload for SubT.

## Installation and usage:

1. Install librealsense. On x86-64, you should be able to just use the
   instructions found [here](https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md)
   to install through the package manager. On an Xavier or similar (AARCH64),
   you'll have to build from source, as listed [here](https://github.com/IntelRealSense/librealsense/blob/master/doc/installation.md).
	***Note: You may need to disable secure boot in BIOS.
1. Patch the kernel modules for librealsense. This will be done as a part of
   packages being installed on x86-64, and will be done manually otherwise. On
   Xavier, the patches don't work (for kernel 4.9), so follow the instructions
   [here](https://docs.google.com/document/d/1vYK9-Jz0PKkNzqu5F_ARxl9zsEAdTOQs9ZdAL-Clobk/edit?usp=sharing)
   to do it manually.
1. Clone repo into a workspace somewhere.
1. Create a new launch file which follows the format in either `single.launch`
   or `payload_multi.launch`. The fps, width, and height settings are applied
   across all cameras (and streams) equally. You'll need to find the serial 
   number for the cameras, which can be done through e.g. `realsense-viewer`.
1. Build with `catkin build` and launch your launch file.

## Extra information on librealsense installation (for NUC installation)
Follow the guide [here](https://github.com/IntelRealSense/librealsense/blob/master/doc/installation.md) to build from source.

1. Download librealsense2 source version 2.20.0 [here](https://github.com/IntelRealSense/librealsense/releases/tag/v2.20.0).
1. Install dependencies:

    ```
    sudo apt-get install git libssl-dev libusb-1.0-0-dev pkg-config libgtk-3-dev
    ```

    For Ubuntu 18:

    ```
    sudo apt-get install libglfw3-dev libgl1-mesa-dev libglu1-mesa-dev
    ```

1. Setup udev rules

    ```
    ./scripts/setup_udev_rules.sh
    ```

1. Patch kernel (x86):
    
    ```
    ./scripts/patch-realsense-ubuntu-lts.sh
    ```

1. Build from source:
    
    * `mkdir build && cd build && cmake .. -DCMAKE_BUILD_TYPE=Release`
    * `make -jx`
    * `sudo make install`

## Topics:

For a camera with name `[name]`, 4 topics will be generated. Specificially:

* `/[name]/aligned_depth_to_color`: Depth data transformed so that it is
  aligned to the color frame.
* `/[name]/color`: Color frame (RGB8).
* `/[name]/colorized_depth`: Colorized visualization of the raw depth image. Use
  parameter `enable_colorized_depth` to turn this publisher on.
* `/[name]/depth`: Depth frame (Y16).

Notes:

* Color camera exposure is manually set to the max exposure allowed for a given
  frame rate (e.g. 16 ms for 60 Hz, 33 ms for 30 Hz).

TODO:

* Instrinsics / extrinsics publishing
* Dropped frames detection
* Separate visualization from driver
* Configurable max depth, which is also published
* Focus mode for a single camera
* Automatic best configuration detection?
