#ifndef REALSENSE_FORK_REALSENSE_FORK_H
#define REALSENSE_FORK_REALSENSE_FORK_H

#include <nodelet/nodelet.h>
#include <memory>
#include <mutex>
#include <unordered_map>
#include "../include/realsense_publisher.h"

namespace realsense_fork {
class RealsenseFork : public nodelet::Nodelet {
 public:
  RealsenseFork() = default;
  virtual ~RealsenseFork();

 private:
  virtual void onInit() override;
  void start_device(const std::string& sn, const rs2::device& dev);

  bool initial_reset = false;

  ros::NodeHandle nh;
  ros::NodeHandle private_nh;

  rs2::context ctx;
  std::mutex devices_mutex;
  std::unordered_map<std::string, rs2::device> devices;
  std::unordered_map<std::string, std::string> device_names;
  std::unordered_map<std::string, std::unique_ptr<RealsensePublisher>>
      publishers;
};
}  // namespace realsense_fork

#endif  // REALSENSE_FORK_REALSENSE_FORK_H
