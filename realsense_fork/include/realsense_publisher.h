#ifndef REALSENSE_FORK_REALSENSE_PUBLISHER_H
#define REALSENSE_FORK_REALSENSE_PUBLISHER_H

#include <image_transport/image_transport.h>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <atomic>
#include <librealsense2/rs.hpp>
#include <string>
#include <thread>

namespace realsense_fork {
class RealsensePublisher {
 public:
  RealsensePublisher(const ros::NodeHandle& nh,
                     const ros::NodeHandle& private_nh, const std::string& name,
                     const rs2::device& dev);
  ~RealsensePublisher();
  RealsensePublisher(const RealsensePublisher&) = delete;

  void start();  // called inside the constructor already
  void stop();

 private:
  static std::string get_sensor_name(const rs2::sensor& sensor);
  void publish_frame(const image_transport::CameraPublisher& pub,
                     const rs2::frame& frame,
                     const sensor_msgs::CameraInfo& info, const ros::Time& time,
                     const std::string& ros_encoding);
  void publish_frame_metadata(const ros::Publisher& pub,
                              const rs2::frame& frame, const ros::Time& time);
  void configure_color_sensor();
  void configure_depth_sensor();
  void configure_ros_topics();
  void update_camera_info(sensor_msgs::CameraInfo& info,
                          const rs2::video_stream_profile& video_profile,
                          const std::string& frame_id);
  void publish_transforms(const ros::Time& time);
  void publish_transform(const ros::Time& time, const tf::Vector3& trans,
                         const tf::Quaternion& q, const std::string& from,
                         const std::string& to);
  void disable_transports(const std::string& name);

  ros::NodeHandle nh;
  ros::NodeHandle private_nh;
  image_transport::ImageTransport it;
  std::string name;
  rs2::device dev;
  std::string serial_no;

  rs2::sensor color_sensor;
  rs2::sensor depth_sensor;

  const std::string BASE_FRAME_ID = name + "/camera_link";
  const std::string COLOR_FRAME_ID = name + "/camera_color_frame";
  const std::string COLOR_OPT_FRAME_ID = name + "/camera_color_optical_frame";
  const std::string DEPTH_FRAME_ID = name + "/camera_depth_frame";
  const std::string DEPTH_OPT_FRAME_ID = name + "/camera_depth_optical_frame";
  sensor_msgs::CameraInfo color_info;
  sensor_msgs::CameraInfo depth_info;
  rs2::stream_profile color_stream_profile;
  rs2::stream_profile depth_stream_profile;

  image_transport::CameraPublisher color_pub;
  image_transport::CameraPublisher depth_pub;
  image_transport::CameraPublisher colorized_depth_pub;
  image_transport::CameraPublisher aligned_depth_pub;
  ros::Publisher color_metadata_pub;
  ros::Publisher depth_metadata_pub;

  tf2_ros::TransformBroadcaster br;

  rs2::pipeline pipe;
  rs2::config cfg;
  std::atomic<bool> stream;
  std::thread stream_thread;

  int fps = 15;
  int width = 640;
  int height = 360;
  bool ir = false;
  bool imu = false;
  bool colorized = false;
};

}  // namespace realsense_fork

#endif  // REALSENSE_FORK_REALSENSE_PUBLISHER_H
