#!/usr/bin/env python3

import time
import cv2
import numpy as np


def main():
    while True:
        height = 360
        width = 640
        image = np.ones((height, width, 3), dtype=np.uint8) * 255
        cv2.putText(
            image,
            "Time: %f" % time.time(),
            (10, height // 2),
            cv2.FONT_HERSHEY_SIMPLEX,
            1.25,
            (0, 0, 255),
            4,
        )
        cv2.imshow("I'm a clock", image)
        if (cv2.waitKey(1) & 0xFF) == ord("q"):
            return


if __name__ == "__main__":
    main()
