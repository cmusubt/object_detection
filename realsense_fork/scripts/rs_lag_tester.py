#!/usr/bin/env python3

import numpy as np
import sys
import rospy
from sensor_msgs.msg import Image
from realsense_fork.msg import RealsenseMetadata
import cv2
import time_synchronizer
import drawing_utils
from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()

pause = False


def cb(meta_color, color, meta_depth, depth):
    #  c = bridge.imgmsg_to_cv2(color, "rgb8")
    #  d = bridge.imgmsg_to_cv2(depth, "y16")
    image_cb(meta_color, color, "color")
    image_cb(meta_depth, depth, "depth")

    global pause
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        sys.exit()
    elif key == ord(" "):
        pause = not pause


def image_cb(metadata, image, name):
    global pause
    img = bridge.imgmsg_to_cv2(image)

    if img.dtype == np.uint16:
        img = img.astype(np.uint8)
        img = np.stack([img, img, img], axis=2)

    header_time = image.header.stamp.to_sec()
    backend_time = metadata.data[metadata.BACKEND_TIMESTAMP] / 1000.0
    time_of_arrival = metadata.data[metadata.TIME_OF_ARRIVAL] / 1000.0
    transmit_timestamp = metadata.data[metadata.FRAME_TIMESTAMP] / 1e6
    sensor_timestamp = metadata.data[metadata.SENSOR_TIMESTAMP] / 1e6
    exposure_time = metadata.data[metadata.ACTUAL_EXPOSURE]

    for i, line in enumerate(
        [
            "Header time: %f" % header_time,
            "Backend time: %f" % backend_time,
            "Time of arrival: %f" % time_of_arrival,
            "Transmit timestamp: %f" % transmit_timestamp,
            "Sensor timestamp: %f" % sensor_timestamp,
            "Actual exposure: %d (us)" % exposure_time,
        ]
    ):
        drawing_utils.shadow_text(img, line, (10, 20 * i + 25))

    if not pause:
        cv2.imshow(name, img)


def main():
    rospy.init_node("rs_lag_tester")
    subs = [
        time_synchronizer.Subscriber(
            "/rs_vasu/color/metadata", RealsenseMetadata
        ),
        time_synchronizer.Subscriber("/rs_vasu/color/image", Image),
        time_synchronizer.Subscriber(
            "/rs_vasu/depth/metadata", RealsenseMetadata
        ),
        time_synchronizer.Subscriber("/rs_vasu/depth/image", Image),
    ]

    ts = time_synchronizer.TimeSynchronizer(subs, callback=cb)
    rospy.spin()


if __name__ == "__main__":
    main()
