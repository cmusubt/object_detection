#!/bin/sh

# Instructions borrowed from these two links:
# https://medium.com/@beta_b0t/how-to-setup-ros-with-python-3-44a69ca36674
# https://stackoverflow.com/questions/49221565/unable-to-use-cv-bridge-with-ros-kinetic-and-python3

# Go to the right directory, assuming the proper directory structure.
OBJDET_DIR=`pwd`/$(dirname $0)
WORKSPACE_DIR=$OBJDET_DIR/../..

PARENT=$WORKSPACE_DIR/..
cd $PARENT

# Create the workspace
WORKSPACE_NAME=objdet_deps
mkdir -p $WORKSPACE_NAME
cd $WORKSPACE_NAME

# Clone the repo
OPENCV_TARGET=src/vision_opencv
if [ ! -d $OPENCV_TARGET ] ; then
    git clone https://github.com/ros-perception/vision_opencv.git \
        -b melodic $OPENCV_TARGET
else
    echo "Repo has been cloned already. Skipping clone."
fi

# Configure the catkin workspace permanently.
PYVER=3.6
catkin config --extend /opt/ros/melodic
catkin config --install
catkin config \
    -DCMAKE_BUILD_TYPE=Release \
    -DPYTHON_EXECUTABLE=/usr/bin/python${PYVER} \
    -DPYTHON_INCLUDE_DIR=/usr/include/python${PYVER}m \
    -DPYTHON_LIBRARY=/usr/lib/$(uname -m)-linux-gnu/libpython${PYVER}m.so

if ! catkin build ; then
    echo "Unable to build packages for some reason."
    exit 1
fi

echo ""
echo "cv_bridge has successfully been built."

cd $WORKSPACE_DIR
DEPS_INSTALL_PATH=$PARENT/$WORKSPACE_NAME/install
catkin config --extend $(realpath $DEPS_INSTALL_PATH)
catkin config \
    -DPYTHON_EXECUTABLE=/usr/bin/python${PYVER} \
    -DPYTHON_INCLUDE_DIR=/usr/include/python${PYVER}m \
    -DPYTHON_LIBRARY=/usr/lib/$(uname -m)-linux-gnu/libpython${PYVER}m.so
